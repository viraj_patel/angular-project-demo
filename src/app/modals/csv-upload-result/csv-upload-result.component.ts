import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';
import { ViewFailCsvResultComponent } from '../view-fail-csv-result/view-fail-csv-result.component';
import { STATIC_DATA } from 'src/app/constants/constants';

@Component({
  selector: 'app-csv-upload-result',
  templateUrl: './csv-upload-result.component.html',
  styleUrls: ['./csv-upload-result.component.scss']
})
export class CsvUploadResultComponent implements OnInit {
  csvResult;
  type;
  private response: Subject<any>;
  constructor(private modalRef: BsModalRef, private modalService: BsModalService,
    private router: Router) { }

  ngOnInit(): void {
    this.response = new Subject();
  }
  closeModal() {
    this.response.next(false);
  }
  choice(bool: boolean){
    this.response.next(bool);
    this.modalRef.hide();
  }
  openFailData(type,count){
    if(type == 2 && count >0){
      let successRecord=[], errorRecord=[];
      let successMessage = "The "+this.type +STATIC_DATA.UPLOAD_MSG;
      if(this.csvResult.success.length > 0){
        this.csvResult.success.map(function(data, i) {
          successRecord.push(
            {
              name:data.code?data.code:data.name?data.name:data['category.en']?data['category.en']:data.title?data.title:data.configurableKeyDisplayText?data.configurableKeyDisplayText:'-', errorsArray:successMessage
            }
          )
      });
      }
      if(this.csvResult.error.length > 0){
        this.csvResult.error.map(function(data, i) {
          errorRecord.push(
            {
              name:data.record.code?data.record.code:data.record.name?data.record.name:data.record.title?data.record.title:data.record.configurableKeyDisplayText?data.record.configurableKeyDisplayText:'-', errorsArray:data.errorsArray.join(', ')
            }
          )
      });
      }
     
      const result = successRecord.length > 0 ? errorRecord.length > 0 ? successRecord.concat(errorRecord) : successRecord : errorRecord;
      this.response.next(result);
    }else if(type == 1 && count >0){
      let successRecord=[];
      let successMessage = "The "+this.type +STATIC_DATA.UPLOAD_MSG;
      this.csvResult.success.map(function(data, i) {
        successRecord.push(
          {
            name:data.code?data.code:data.name?data.name:data['category.en']?data['category.en']:data.title?data.title:data.configurableKeyDisplayText?data.configurableKeyDisplayText:'-', errorsArray:successMessage
          }
        )
    });
      this.response.next(successRecord);
    }else if(type == 0 && count >0){
      let errorRecord=[];
      this.csvResult.error.map(function(data, i) {
        errorRecord.push(
          {
            name:data.record.code?data.record.code:data.record.name?data.record.name:data.record.title?data.record.title:data.record.configurableKeyDisplayText?data.record.configurableKeyDisplayText:'-', errorsArray:data.errorsArray.join(', ')
          }
        )
    });
      this.response.next(errorRecord);
    }
    
  }
}
