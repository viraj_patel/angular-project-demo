import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CsvUploadResultComponent } from './csv-upload-result.component';

describe('CsvUploadResultComponent', () => {
  let component: CsvUploadResultComponent;
  let fixture: ComponentFixture<CsvUploadResultComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CsvUploadResultComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CsvUploadResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
