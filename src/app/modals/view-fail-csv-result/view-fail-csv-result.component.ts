import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';
import { LOCATION_TYPE } from 'src/app/constants/constants';


@Component({
  selector: 'app-view-fail-csv-result',
  templateUrl: './view-fail-csv-result.component.html',
  styleUrls: ['./view-fail-csv-result.component.scss']
})
export class ViewFailCsvResultComponent implements OnInit {
  type:string;
  locationType=LOCATION_TYPE;
  csvResult:Array<object>;
  private response: Subject<boolean>;
  constructor(private modalRef: BsModalRef, private modalService: BsModalService,
    private router: Router) { }

  ngOnInit(): void {
  }

  closeModal() {
    this.modalRef.content.modalRef.hide();
    // this.response.next(false);
  }
 

}
