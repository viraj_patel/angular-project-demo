import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewFailCsvResultComponent } from './view-fail-csv-result.component';

describe('ViewFailCsvResultComponent', () => {
  let component: ViewFailCsvResultComponent;
  let fixture: ComponentFixture<ViewFailCsvResultComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewFailCsvResultComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewFailCsvResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
