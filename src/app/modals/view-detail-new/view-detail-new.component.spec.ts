import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewDetailNewComponent } from './view-detail-new.component';

describe('ViewDetailNewComponent', () => {
  let component: ViewDetailNewComponent;
  let fixture: ComponentFixture<ViewDetailNewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewDetailNewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewDetailNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
