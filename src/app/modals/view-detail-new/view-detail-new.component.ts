import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-view-detail-new',
  templateUrl: './view-detail-new.component.html',
  styleUrls: ['./view-detail-new.component.scss']
})
export class ViewDetailNewComponent implements OnInit {

  title;
  viewData;
  private response: Subject<boolean>;
  noDataFound:boolean=false;
  constructor(
    private modalRef: BsModalRef,
    private translate: TranslateService,
  ) { }

  ngOnInit(): void {
    this.response = new Subject();

    if(this.viewData.cartItems.length==0){   
         this.noDataFound=!this.noDataFound;
    }
    
  }

  getFieldCount(index, priceType){
    let count:any = 0;
   
      
      for (let item of this.viewData.cartItems[index].itemPrice){
        
        if(item.priceType == priceType){
          count++;
        }
      }
    
    count = '('+count+'x'+')';
    return  count=='(0x)' ? '' : count ;
  }

  closeModal() {
    this.modalRef.hide();
  }


}
