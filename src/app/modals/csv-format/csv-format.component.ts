import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-csv-format',
  templateUrl: './csv-format.component.html',
  styleUrls: ['./csv-format.component.scss']
})
export class CsvFormatComponent implements OnInit {
  title;
  data;
  private response: Subject<boolean>;
  constructor(private modalRef: BsModalRef) { }

  ngOnInit(): void {
    this.response = new Subject();
  }
  closeModal() {
    this.modalRef.hide();
  }

}
