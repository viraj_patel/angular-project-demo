import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmationModalComponent } from './confirmation-modal/confirmation-modal.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { RecordViewComponent } from './record-view/record-view.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { createTranslateLoader } from '../app.module';
import { HttpClient } from '@angular/common/http';
import { CsvFormatComponent } from './csv-format/csv-format.component';
import { CsvUploadResultComponent } from './csv-upload-result/csv-upload-result.component';
import { ViewFailCsvResultComponent } from './view-fail-csv-result/view-fail-csv-result.component';
import { ViewDetailNewComponent } from './view-detail-new/view-detail-new.component';
import { JsonEditorComponent } from './json-editor/json-editor.component';



@NgModule({
  declarations: [
    ConfirmationModalComponent,
    RecordViewComponent,
    CsvFormatComponent,
    CsvUploadResultComponent,
    ViewFailCsvResultComponent,
    ViewDetailNewComponent,
    JsonEditorComponent,
  ],
  imports: [
    CommonModule,
    NgMultiSelectDropDownModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient],
      },
    }),
  ],
  exports: [
    ConfirmationModalComponent,

  ],
  entryComponents: [
    ConfirmationModalComponent,

  ]
})
export class ModalsModule { }
