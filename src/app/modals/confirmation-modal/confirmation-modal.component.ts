import { Component, OnInit } from '@angular/core';
import { interval, Subject } from 'rxjs';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { STATIC_DATA } from '@constant/constants';

@Component({
  selector: 'app-confirmation-modal',
  templateUrl: './confirmation-modal.component.html',
  styleUrls: ['./confirmation-modal.component.scss'],
})
export class ConfirmationModalComponent implements OnInit {
  confirmText;
  title;
  acceptButtonText;
  cancleButtonText;
  closeButtonText;
  idleTimerLeft: any;
  timeRemain: number;
  FULL_DASH_ARRAY = 283;
  list: any;
  timeOut:boolean = false;
  private response: Subject<boolean>;
  constructor(private modalRef: BsModalRef,
    private translate: TranslateService,
    private router: Router) { }

  ngOnInit() {
   
    this.response = new Subject();
    if(this.title === STATIC_DATA.SESSION_TIMEOUT){
      this.timeOut = true;
      const timer$ = interval(1000);
      const sub = timer$.subscribe((sec) => {
      this.idleTimerLeft =  30 - sec;
      
      if (this.idleTimerLeft === 0) {
        sub.unsubscribe();
        this.choice(true);
      }else{
        this.setCircleDasharray(sec);
      }
    });
    
     
    }
  }

  setCircleDasharray = (elapsedTime: number) => {
    // const inputValue: any = this.inputForm.value;
    // const timeLimit = inputValue.firstLevelTimer * 60;

    this.timeRemain = elapsedTime / 30;
    const circleDasharray = `${(
      this.timeRemain * this.FULL_DASH_ARRAY
    ).toFixed(0)} 283`;
    if(this.timeOut == true){
      document.getElementById('base-timer-path-remaining').setAttribute('stroke-dasharray', circleDasharray);
    }
    }
   
  
  choice(bool: boolean){
    this.timeOut = false;
    this.response.next(bool);
    this.modalRef.hide();
  }

  hideConfirmationModal() {
    this.timeOut = false;
    this.modalRef.hide();
  }

  closeModal() {
    this.timeOut = false;
    this.modalRef.hide();
  }

}
