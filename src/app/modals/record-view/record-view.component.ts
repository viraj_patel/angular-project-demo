import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-record-view',
  templateUrl: './record-view.component.html',
  styleUrls: ['./record-view.component.scss']
})
export class RecordViewComponent implements OnInit {
  title;
  redordList;
  private response: Subject<boolean>;
  constructor(
    private modalRef: BsModalRef,
    private translate: TranslateService,
  ) { }

  ngOnInit(): void {
    this.response = new Subject();
  }

  closeModal() {
    this.modalRef.hide();
  }

}
