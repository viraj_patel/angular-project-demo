import { Component, OnInit, Renderer2, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { DOCUMENT } from '@angular/common';
import { Subject } from 'rxjs';
import { STATIC_DATA } from '@constant/constants';
declare function jsonEditorInit(p1,p2,p3,p4,p5,p6,p7,p8):any ;
declare function getJson():any ;
@Component({
  selector: 'app-json-editor',
  templateUrl: './json-editor.component.html',
  styleUrls: ['./json-editor.component.scss']
})
export class JsonEditorComponent implements OnInit {

  records:any=[];
  configurableKey;
  configDisableJsonFields;
  description;
  addRow:boolean=false;
  error:boolean=false;
  errorMsg:any;
  private response: Subject<any>;
  constructor(private modalRef: BsModalRef, private modalService: BsModalService,
    private _renderer2: Renderer2,
    @Inject(DOCUMENT) private _document: Document,
    private router: Router) { }

  ngOnInit(): void {

    let script = this._renderer2.createElement('script');
    script.text = `$(document).ready(function () {
      jsonEditorInit('table_container', 'Textarea1', 'result_container', 'json_to_table_btn', 'table_to_json_btn');
    })`;
    
   if(this.configDisableJsonFields != ""){
    this.configDisableJsonFields = JSON.parse(this.configDisableJsonFields);
   }else{
    this.configDisableJsonFields = [];
   }
    this._renderer2.appendChild(this._document.body, script);
    this.response = new Subject();
    this.addRow = this.isObject(this.records) ? false : true;
    const json = this.isObject(this.records) ? [this.records] : this.records;
    if(this.addRow){
      for (const iterator of this.records) {
        iterator.action ="";
      }
    }
      setTimeout(() => {
        jsonEditorInit('table_container', 'Textarea1', 'result_container', 'json_to_table_btn', 'table_to_json_btn',json,this.configDisableJsonFields,this.addRow);
      });
   
  }
  
  isObject(objValue) {
    return objValue && typeof objValue === 'object' && objValue.constructor === Object;
  }
  closeModal() {
    this.response.next(false);
  }
  choice(bool: boolean){
    this.response.next(bool);
    this.modalRef.hide();
  }

  callFunction(){
    // con
    // jsonEditorInit('table_container', 'Textarea1', 'result_container', 'json_to_table_btn', 'table_to_json_btn');
  }

  addOneRow(){
    var keys = Object.keys(this.records[0]);
  
        var obj ={};
        for (const key of keys) {
            obj[key]="";
        }
        this.records.push(obj);
        setTimeout(() => {
          jsonEditorInit('table_container', 'Textarea1', 'result_container', 'json_to_table_btn', 'table_to_json_btn', this.response,this.configDisableJsonFields,this.addRow);
        });
  }

  async getJson(){
   const data = getJson(); 
    if(this.isObject(this.records)){
      const resultData = await this.arrayToObject(data[0]);
      if(resultData == false){
        return false;
      }else{
        this.response.next({configurableValue:resultData});
      }
    }else{
      for (const iterator of data) {
        var keys = Object.keys(data[0]);
        for (const key of keys) {
          // var checkKey = key; 
          // if(key.includes(" (Yes/No)")){
          //   checkKey = key.replace(' (Yes/No)','');
          // }
          // if(key.includes(" (true/false)")){
          //   checkKey = key.replace(' (Yes/No)','');
          // }
          if(iterator[key].toString() == ""){
            this.error = true;
            this.errorMsg = STATIC_DATA.JSON_ERROR_ALL_INPUTS;
            return;
          }else if(key.includes(" (Yes/No)") && (iterator[key].toString()).toLowerCase() != "yes" && (iterator[key].toString()).toLowerCase() != "no"){
            this.error = true;
            this.errorMsg = STATIC_DATA.JSON_ERROR_REQUIERD_INPUTS;
            return;
          }else if(key.includes(" (true/false)") && iterator[key] != true && iterator[key] != false){
            this.error = true;
            this.errorMsg = STATIC_DATA.JSON_ERROR_REQUIERD_INPUTS;
            return;
          }else if(key.includes(" (Yes/No)")){
            iterator[key.replace(' (Yes/No)','')] = await iterator[key].toString().toLowerCase() == "yes" ? "1":"0";
            setTimeout(() => {
              delete iterator[key];
            });
            
          }else if(key.includes(" (true/false)")){
            iterator[key.replace(' (true/false)','')] = await iterator[key];
            setTimeout(() => {
              delete iterator[key];
            });
            
          }
          
        }
        delete iterator.action;
      }
      this.error = false;
      setTimeout(() => {
      this.response.next({configurableValue:data});
      });
    }
    
  }

  async arrayToObject(data){
    for (const [key, value] of Object.entries(data)) {
      if(Array.isArray(value) && value.length == 1){
        data[key] = value[0];
        if(this.isObject(data[key])){
          const res = await this.arrayToObject(data[key]);
          if(res == false){
            return false;
          }
        }
      }else{
        
        if(key.includes(" (Yes/No)") && (data[key].toString()).toLowerCase() != "yes" && (data[key].toString()).toLowerCase() != "no"){
          this.error = true;
          this.errorMsg = STATIC_DATA.JSON_ERROR_REQUIERD_INPUTS;
          return false;
        }else if(key.includes(" (true/false)") && data[key] != true && data[key] != false){
          this.error = true;
          this.errorMsg = STATIC_DATA.JSON_ERROR_REQUIERD_INPUTS;
          return false;
        }else  if(key.includes(" (Yes/No)")){
          data[key.replace(' (Yes/No)','')] = data[key].toString().toLowerCase() == "yes" ? "1":"0";
          delete data[key];
        }else if(key.includes(" (true/false)")){
          data[key.replace(' (true/false)','')] = data[key];
            delete data[key];
        }
      }
    }
    return data;
  }
  

}
