import { HttpParams } from '@angular/common/http';
import { Component, OnDestroy, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from '@services/localService/localStorage.service';
import { NgIdleService } from '@services/ng-idle.service';
import { StaffService } from '@services/staff.service';
import { ConfirmDialogComponent } from '@shared/components/confirmation-dialog/confirmation-dialog.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Children } from 'preact/compat';
import { adminLteConf } from 'src/app/admin-lte.conf';
import { CONFIG_KEYS, LOCAL_STORAGE, STATIC_DATA} from '@constant/constants';
import { TranslateService } from '@ngx-translate/core';
import { ConfirmationModalComponent } from '../modals/confirmation-modal/confirmation-modal.component';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-menu-sidebar',
  templateUrl: './menu-sidebar.component.html',
  styleUrls: ['./menu-sidebar.component.scss']
})
export class MenuSidebarComponent implements OnInit, OnDestroy {
  public sideMenu:any = adminLteConf.sidebarLeftMenu;
  public currentPage = '';
  public activeMenu: any = '';
  public event = '-';
  public todo = '-';
  public agenda = '-';
  modalRef: BsModalRef;
  permissionsList:any=[];
  obs: Subscription;
  timer:any;
  roles:any=[];
  idleTime:any={};
  constructor(
    private modalService: BsModalService,
    public localStorage: LocalStorageService,
    private rolesService : StaffService,
    private ngIdle: NgIdleService,
    private translate: TranslateService,
    private router: Router
  ) { }
ngOnDestroy(){
  this.sideMenu = [];
  this.timer = 1
  this.obs.unsubscribe();
}
  async ngOnInit(){
    await this.getRolesList();
    await this.getPermissionsList(); 
    await this.callConfiguration();
    if(this.idleTime?.configurableValue != undefined){
      this.ngIdle.USER_IDLE_TIMER_VALUE_IN_MIN = +this.idleTime.configurableValue.match(/\d+/)[0];
    }else{
      this.ngIdle.USER_IDLE_TIMER_VALUE_IN_MIN = 30;
    }
    this.ngIdle.FINAL_LEVEL_TIMER_VALUE_IN_MIN = 0;
    const lastEvent:any = await this.localStorage.getDataFromIndexedDB(LOCAL_STORAGE.SESSION_TIME);
    if(lastEvent != null){
        const now = Date.now();
        const timeleft = lastEvent + this.ngIdle.USER_IDLE_TIMER_VALUE_IN_MIN * 60 * 1000;
        const diff = timeleft - now;
        const isTimeout = diff < 0;
        if(isTimeout){
          this.showSendTimerDialog();
          this.ngIdle.startTimer();
          this.ngIdle.initilizeSessionTimeout();
          this.obs = this.ngIdle.userIdlenessChecker.subscribe((status: string) => {
            
            this.initiateFirstTimer(status);
          });
        }else{
          this.ngIdle.startTimer();
          this.ngIdle.initilizeSessionTimeout();
          this.obs = this.ngIdle.userIdlenessChecker.subscribe((status: string) => {
            
            this.initiateFirstTimer(status);
          });
        }
    }else{
    // Watcher on timer
    this.ngIdle.startTimer();
    this.ngIdle.initilizeSessionTimeout();
    this.obs = this.ngIdle.userIdlenessChecker.subscribe((status: string) => {
      
      this.initiateFirstTimer(status);
    });
    }
  }

  async callConfiguration(){
    const keyParam = {fields:CONFIG_KEYS.IDLE_KEY}
    let param = new HttpParams();
    Object.keys(keyParam).forEach((key) => {
      if (keyParam[key] != "") {
        param = param.set(
          key,
          keyParam[key]
        );
      }
    });
    const res: any = await this.rolesService.getConfig(param);
      if(res?.result){
       this.idleTime = res?.result[0];
      }
             
  }

  initiateFirstTimer = (status: string) => {
    this.timer = status;
    switch (status) {
      case 'INITIATE_TIMER':
        break;

      case 'RESET_TIMER':
        break;

      case 'STOPPED_TIMER':
        this.showSendTimerDialog();
        break;

      default:
        break;
    }
  };

  showSendTimerDialog(): void {
    if(this.modalRef !== undefined){
      this.modalRef.hide();
    }
    const initialState = {
      title: STATIC_DATA.SESSION_TIMEOUT,
      confirmText: STATIC_DATA.SESSION_TIMEOUT_NOTE,
    };
    this.modalRef = this.modalService.show(ConfirmationModalComponent, {
      class: 'modal-dialog-centered',
      backdrop: 'static',
      initialState,
    });
    this.modalRef.content.acceptButtonText= this.translate.instant(
      'MODAL.YES'
    );
    this.modalRef.content.closeButtonText = this.translate.instant(
      'MODAL.NO'
    );
    this.modalRef.content.response.subscribe(async (result) => {
      if (result && (this.timer <= 0|| this.timer == 'RESET_TIMER'|| this.timer == 'STOPPED_TIMER')) {
        this.logout();
      } else if (this.timer <= 0 || this.timer == 'RESET_TIMER') {
        this.continue();
      }
    });

  }

  async getRolesList(){
    this.roles = await this.rolesService.getLoginUserRoles();
   
  }

  continue(): void {
    // this.modalRef.hide();
    this.modalService.hide;
    NgIdleService.runSecondTimer = false;
    this.ngIdle.initilizeSessionTimeout();
  
  }

  async getPermissionsList(){
    const data = {roles:this.roles}
   
    this.permissionsList = await this.rolesService.getLoginPermissions(data);
    this.sideMenu = [];
    // this.sideMenu = [{ label: 'Dashboard', route: 'home', iconClasses: '', imgName:'home.png', key:'dashboard'}];
    
    for (const iterator of this.permissionsList.reverse()) {
     
      if(iterator.View == true){
        if(iterator.children.length > 0){
          const childPermissions = await this.rolesService.getLoginPermissionsForModuleChildList(iterator.key);
          let childPath="";
          for (const iteratorChild of childPermissions) {
            if(iteratorChild.View == true){
              childPath = iteratorChild.key;
            }
          }
          
          this.sideMenu.push({
            label: iterator.name,
            route: iterator.key+"/"+childPath,
            iconClasses: '',
            imgName:iterator.key+'.png',
            key:iterator.key
    
          });

        }else
        if(iterator.key == "admin-access-management"){
          const loginRoles = await this.roles.map(v=>{return v.name});
          const index = loginRoles.indexOf('Super Admin');
          if (index > -1) { 
            this.sideMenu.push( { label: 'Roles and Permissions', route: 'roles-and-permissions-management/role', iconClasses: '', key:iterator.key, imgName:iterator.key+'.png',children:[
              {label: 'Staff Management' ,route:'roles-and-permissions-management/staff', key:'staff-management'}
            ] });
          }else{
            this.sideMenu.push( { label: 'Staff Management', route: 'roles-and-permissions-management/staff', iconClasses: '', key:'staff-management', imgName:iterator.key+'.png',});
          }
        }
        else if(iterator.key == "system-configuration"){
        let systemchildren=[];
        if(iterator.Import == true){
          systemchildren.push( {label: 'Import' ,route:'system-configuration/import', key:'import'})
        }
        if( iterator.Export == true){
          systemchildren.push({label: 'Export' ,route:'system-configuration/export', key:'export'})
        }
        if(systemchildren.length>0){
          this.sideMenu.push( { label: 'System Configuration', route: 'system-configuration', iconClasses: '', key:iterator.key, imgName:iterator.key+'.png',children:systemchildren });
        }else{
            this.sideMenu.push({
            label: iterator.name,
            route: iterator.key,
            iconClasses: '',
            imgName:iterator.key+'.png',
            key:iterator.key
    
          });
        }
       
        }
       
        else{
          this.sideMenu.push({
            label: iterator.name,
            route: iterator.key,
            iconClasses: '',
            imgName:iterator.key+'.png',
            key:iterator.key
    
          });
        }
        
      }
     
    }
  }

  checkPermissions(key){
    if(key != 'dashboard' && key != 'staff-management'){
      for (const iterator of this.permissionsList) {
        if(key == iterator.key && iterator.View == true){
          return true;
        }
      }
      return false;
    }else{
      return true;
    }
    
  }

  public activeClass(currentRoute, tag, parentRount: any = '') {
    currentRoute = currentRoute.toLowerCase().replace(' ', '-');
    if (parentRount) {
      parentRount = parentRount.toLowerCase().replace(' ', '-');
    }
    if (tag === 'li') {
     
      if (this.router.url.includes('/home') === true && currentRoute === 'home') {
        this.activeMenu = '';
        return 'active mm-active';
      } else if (this.router.url.includes(currentRoute.toLowerCase()) === true && currentRoute != 'home' && this.router.url.includes(currentRoute+"/".toLowerCase()) != true) {
        this.activeMenu = 'menuHover';
        return 'mm-active menu-open active';
      } else {
        return '';
      }
    } else if (tag === 'angle') {
      return 'fa fa-angle-left';

    } else if (tag === 'child1-li') {
      if (currentRoute === 'to-do') {
        currentRoute = 'todo';
      }
      if (currentRoute === 'housekeeping') {
        currentRoute = 'house-keeping';
      }

      if (this.router.url.includes(currentRoute.toLowerCase()) === true) {
        this.activeMenu = 'menuHover';
        return 'mm-active menu-open active';
      } else {
        return '';
      }
    } else if (tag === 'child2-li') {
      if (currentRoute === 'certificates') {
        currentRoute = 'certifications';
      }
      if (currentRoute === 'sector-related') {
        currentRoute = 'sector';
        if (this.currentPage.includes('sector/usdaw')) {
          this.currentPage = this.currentPage.replace('usdaw/', '');
        }
      }
      if (currentRoute === 'interest-related') {
        currentRoute = 'interests';
        if (this.currentPage.includes('interests/usdaw')) {
          this.currentPage = this.currentPage.replace('usdaw/', '');
        }
      }
      if (currentRoute === 'courses') {
        if (this.currentPage.includes('courses/usdaw')) {
          this.currentPage = this.currentPage.replace('usdaw/', '');
        }
      }

      if (currentRoute === 'courses' || currentRoute === 'assessments') {
        if (this.currentPage.includes('training')) {
          this.currentPage = this.currentPage.replace('training', 'learning');
        }
      }
      if (this.currentPage.includes(currentRoute.toLowerCase()) === true && this.currentPage.includes(parentRount.toLowerCase()) === true) {
        return 'mm-active menu-open active';
      } else {
        return '';
      }
    } else if (tag === 'bedge') {
      if (currentRoute === 'to-do' || currentRoute === 'calendar-management' || currentRoute === 'agenda') {
        return '';
      } else {
        return 'hide';
      }
    } else {
      if (this.currentPage.includes(currentRoute.toLowerCase()) === true) {
        return 'mm-collapse mm-show';
      } else {
        return 'mm-collapse';
      }
    }
  }

  logoutConfirms(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template,{ class: 'modal-sm modal-dialog-centered custom-modal' });
  }

  async logout(){
    this.modalRef.hide()
    await this.localStorage.clearStorage();
    await this.localStorage.clearDataFromIndexedDB();
    this.timer = 1
    this.router.navigate(['/']);
      
  }

}
