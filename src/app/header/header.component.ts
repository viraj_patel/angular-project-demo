import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { LOCAL_STORAGE, Web_Language } from '@constant/constants';
import { ApiService } from '@services/api.service';
import { LocalStorageService } from '@services/localService/localStorage.service';
import { LocationService } from '@services/location.service';
import { LoginService } from '@services/login.service';
import { MyProfileService } from '@services/my-profile.service';
import { SystemCinfigurationsService } from '@services/system-cinfigurations.service';
import { UsersService } from '@services/users.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
  @Input() userInfo:any;
  language:string="en";
  obs: Subscription;
  constructor(
    private usersService: UsersService,
    private localStorage: LocalStorageService,
    private loginService:LoginService,
    private locationService:LocationService,
    private ApiService:ApiService,
    private router: Router,
    private profileService:MyProfileService,
  ) { }
  ngOnDestroy(){
    this.obs.unsubscribe();
  }
  async ngOnInit() {
    this.obs = this.profileService
    .getProfile()
    .subscribe((flag) => {
      this.getProfile();
    });
    this.getProfile();
  }

  async getProfile(){
    const response:any = await this.profileService.getUserDeatails();
    this.userInfo = response;
    await this.localStorage.setDataInIndexedDB(
      LOCAL_STORAGE.USER_INFO,
      JSON.stringify(response)
    );
  }

  async changeLanguage(){
    await this.localStorage.setDataInIndexedDB(
      LOCAL_STORAGE.LANGUAGE,
      Web_Language.EN
    );
  }

}
