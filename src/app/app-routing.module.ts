import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Web_Language } from '@constant/constants';
import { TranslateService } from '@ngx-translate/core';
import { ApiService } from '@services/api.service';
import { AuthGuard } from '@services/auth.guard';
import { LoginAuthGuard } from '@services/login-auth.guard';
import { UsersService } from '@services/users.service';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  
  {
    path: 'main-home',
    canActivate: [AuthGuard],
    component: HomeComponent,
    children:[
      {
        path: 'home',
        canActivate: [AuthGuard],
        loadChildren: () => import('./pages/dashboard/dashboard.module').then(m => m.DashboardModule),
      },
      {
        path: 'user-management',
        canActivate: [AuthGuard],
        loadChildren: () => import('./pages/user-management/user-management.module').then(m => m.UserManagementModule),
      },
      {
        path: 'master-data-configuration',
        canActivate: [AuthGuard],
        loadChildren: () => import('./pages/data-configuration/data-configuration.module').then(m => m.DataConfigurationModule),
      },
      {
        path: 'cart-management',
        canActivate: [AuthGuard],
        loadChildren: () => import('./pages/cart-management/cart-management.module').then(m => m.CartManagementModule),
      },
      {
        path: 'roles-and-permissions-management',
        loadChildren: () => import('./pages/roles-and-permissions/roles-and-permissions.module').then(m => m.RolesAndPermissionsModule),
      },

      {
        path: 'system-configuration',
        canActivate: [AuthGuard],
        loadChildren: () => import('./pages/system-configuration/system-configuration.module').then(m => m.SystemConfigurationModule),
      },
      {
        path: 'my-profile',
        canActivate: [AuthGuard],
        loadChildren: () => import('./pages/my-profile/my-profile.module').then(m => m.MyProfileModule),
      },
    ]
  },
  // {
  //   path: 'main-home',
  //   loadChildren: () => import('./pages/home/home.module').then(m => m.HomeModule)
  // },

  {
    path: '', loadChildren: () => import('./pages/login/login.module').then(m => m.LoginModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule],
})
export class AppRoutingModule { 
  constructor(
    public translate: TranslateService,
    public usersService: UsersService
  ) {
    translate.setDefaultLang(Web_Language.EN);
  }
}
