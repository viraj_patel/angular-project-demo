import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  EventEmitter,
  Output,
  OnInit
} from '@angular/core';
import {MatCalendar} from '@angular/material/datepicker';
import {DateAdapter, MAT_DATE_FORMATS, MatDateFormats} from '@angular/material/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { STATIC_DATA } from '@constant/constants';
import { FormService } from '@services/form.service';

/** @title Datepicker with custom calendar header */
@Component({
  selector: 'app-date-picker',
  templateUrl: './date-picker.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DatePickerComponent implements OnInit {
  dateForm: FormGroup;
  exampleHeader = ExampleHeader;
  showDataFilter: boolean = false;
  @Output() dateFilterSearch = new EventEmitter<object>();
  startdate;
  endDate;
  errorMsg: any={};
  today = new Date();
  constructor(
    private formService: FormService
  ) {
    const today = new Date();
    const month = today.getMonth();
    const year = today.getFullYear();
    this.dateForm = new FormGroup({
      start: new FormControl(''),
      end: new FormControl(''),
    });
  }
  ngOnInit(){
    this.endDate = this.today;
   this.intializingMessage();
  }
  intializingMessage() {
    this.errorMsg.start = {
      required: STATIC_DATA.ERR_MSG_REQUIERD_START_DATE,
    };
    this.errorMsg.end = {
      required: STATIC_DATA.ERR_MSG_REQUIERD_END_DATE,
    };
  }


  dateFilter(){
    this.formService.markFormGroupTouched(this.dateForm);
    if (this.dateForm.valid) {
    this.dateFilterSearch.emit(this.dateForm.controls);
    }
  }
  setdate(event){
    this.dateForm.controls["end"].setValidators([
      Validators.required,
    ]);
    this.dateForm.controls["end"].updateValueAndValidity();
    this.startdate= new Date(event.target.value);
  }
  setEndDate(event){
    this.dateForm.controls["start"].setValidators([
      Validators.required,
    ]);
    this.dateForm.controls["start"].updateValueAndValidity();
    this.endDate= new Date(event.target.value);
  }
  async resetFilter(){
    this.dateForm.get('start').clearValidators();   
    this.dateForm.get('end').clearValidators();   
    this.dateForm.patchValue({
      start:null,
      end:null
    });
    this.startdate='';
    this.endDate='';
    this.dateFilterSearch.emit(this.dateForm.controls);
  }
  openDateFilter(campaignOnePicker){
    if(this.showDataFilter == true){
      this.showDataFilter = false;
    }else{
      this.showDataFilter = true
      campaignOnePicker.open();
    }
    
  }

  selectDate(event){
    if(this.dateForm.controls.end.value != null){
    
    }
  }
}

/** Custom header component for datepicker. */
@Component({
  selector: 'example-header',
  styles: [
    `
    .example-header {
      display: flex;
      align-items: center;
      padding: 0.5em;
    }

    .example-header-label {
      flex: 1;
      height: 1em;
      font-weight: 500;
      text-align: center;
    }

    .example-double-arrow .mat-icon {
      margin: -22%;
    }
  `,
  ],
  template: `
    <div class="example-header">
      <button mat-icon-button (click)="previousClicked('month')">
      <i class="fa-solid fa-angle-left"></i>
      </button>
      <span class="example-header-label">{{periodLabel}}</span>
      <button mat-icon-button (click)="nextClicked('month')">
      <i class="fa-solid fa-angle-right"></i>
      </button>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExampleHeader<D> implements OnDestroy {
  private _destroyed = new Subject<void>();

  constructor(
    private _calendar: MatCalendar<D>,
    private _dateAdapter: DateAdapter<D>,
    @Inject(MAT_DATE_FORMATS) private _dateFormats: MatDateFormats,
    cdr: ChangeDetectorRef,
  ) {
    _calendar.stateChanges.pipe(takeUntil(this._destroyed)).subscribe(() => cdr.markForCheck());
  }

  ngOnDestroy() {
    this._dateFormats.parse.dateInput = 'DD-MM-YYYY';
    this._destroyed.next();
    this._destroyed.complete();
  }

  get periodLabel() {
    return this._dateAdapter
      .format(this._calendar.activeDate, this._dateFormats.display.monthYearLabel)
      .toLocaleUpperCase();
  }

  previousClicked(mode: 'month' | 'year') {
    this._calendar.activeDate =
      mode === 'month'
        ? this._dateAdapter.addCalendarMonths(this._calendar.activeDate, -1)
        : this._dateAdapter.addCalendarYears(this._calendar.activeDate, -1);
  }

  nextClicked(mode: 'month' | 'year') {
    this._calendar.activeDate =
      mode === 'month'
        ? this._dateAdapter.addCalendarMonths(this._calendar.activeDate, 1)
        : this._dateAdapter.addCalendarYears(this._calendar.activeDate, 1);
  }
}