import { Component, HostListener, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { SharedService } from 'src/app/shared/shared.service';
import { AvailableListService } from 'src/app/shared/available-list.service';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { TABLE_DATA_TYPE, STATIC_DATA, TRUE_FALSE_RADIO_GROUP} from 'src/app/constants/constants';
import { FormService } from '@services/form.service';
import { CsvUploadResultComponent } from 'src/app/modals/csv-upload-result/csv-upload-result.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { JsonEditorComponent } from 'src/app/modals/json-editor/json-editor.component';

@Component({
  selector: 'app-available-list',
  templateUrl: './available-list.component.html',
  styleUrls: ['./available-list.component.scss']
})
export class AvailableListComponent implements OnInit {
  @Input() tablename:string;
  @Input() recordList: any;
  @Input() shortingFilters:any;
  @Input() columnCount: any;
  @Input() tableFields: any;
  @Input() showNoData: boolean = false;
  @Input() totalItems: any;
  @Input() showFilters: boolean = false;
  @Input() searchFields: any;
  @Input() editupdate:boolean;
  @Input() editInputs:boolean;
  @Input() editStatus:boolean;
  @Input() shorting:boolean;
  @Input() searching:boolean;
  @Input() firstPage: number = 1;
  @Input() tableHeading;
  @Input() tableSort;
  @Input() typeTable;
  @Input() language :string='en';
  @Input() tablePageLimit:number = 10;
  @Input() editArray:boolean=false;
  @Input() downloadCsv:boolean=false;
  @Input() exportData:boolean=false;
  @Input() deleteData:boolean=false;
  @Input() editTableItem:boolean=false;
  @Input() showTablePageLimit:boolean =true;
  @Input() deleteButtonShow:boolean=true;
  validMsg=STATIC_DATA;
  currentpage;
  tableDataType:any=TABLE_DATA_TYPE;
  userTable = []; 
  paramsObj:any = {
    sortBy: '',
    page: this.firstPage,
    limit:10
  };
  sortType = ['ASC','DESC'];
  trueFalseRadioGroup = TRUE_FALSE_RADIO_GROUP;
  sortValue = 1;
  dataLoad:boolean=false;
  formUpdate:boolean=false;
  tableForm: FormGroup;
  tableBodyForm: FormGroup;
  oldData:any;
  modalRef: BsModalRef;

  toppings = new FormControl('');
  toppingList: any[] = [1,0,'111','jpg', 'png', 'jpeg', 'svg'];
  selectedObjects = ['jpg','png','svg']
  @Output() generateExcelFile = new EventEmitter<object>();
   @Output() listApiCall = new EventEmitter<object>();
   @Output() searchCall = new EventEmitter<object>();
   @Output() viewInfo = new EventEmitter<object>();
   @Output() deleteLocation = new EventEmitter<object>();
   @Output() editLocation = new EventEmitter<object>();
   @Output() editInfo = new EventEmitter<object>();
  constructor(
    public shared: SharedService,
    public availableList: AvailableListService,
    private formbuilder: FormBuilder,
    private formService: FormService,
    private modalService: BsModalService,
  ) {
  }

  /**
   * ngOnInit() => default value set
   */
  ngOnInit() {
    const initialValue = {editInfo: this.formbuilder.array([]),tablePageLimit:this.tablePageLimit};
    
    
    this.tableForm = this.formbuilder.group(this.tableFields.reduce((obj, item) => {
      return {
        ...obj,
        [item]: false,
        [item+"_text"]: '',
      };
    }, initialValue));
    if(this.editInputs == true){
      this.recordList.forEach(element => {
        (this.tableForm.controls['editInfo'] as FormArray).push(
          this.formbuilder.group(element.editObj)
        );
       
      });
     }else{
      this.recordList.forEach(element => {
        (this.tableForm.controls['editInfo'] as FormArray).push(
          this.formbuilder.group( {searchKey:''})
        );
      });
      
     }
     this.formUpdate = true;
     this.dataLoad = true;

     

  }
  @Input() 
  public set tableFormReset(value: any) {
    if(value == true && this.recordList != undefined){
      this.recordList = this.recordList.map(function(data,ind){
        data.edit=false;
      return data;
    });
    }
   
  }

  @Input() 
    public set formDataSet(tableList: any) {
      if(this.formUpdate){
        this.dataLoad = false;
        if(this.tableForm.controls['editInfo'].value.length > 0){
        
          const control = <FormArray>this.tableForm.controls['editInfo'];
        for(let i = control.length-1; i >= 0; i--) {
            control.removeAt(i)
          }
        
        }
        if(tableList.length > 0){
          if(this.editInputs == true){
            for (const iterator of tableList) {
              (this.tableForm.controls['editInfo'] as FormArray).push(
                this.formbuilder.group( iterator.editObj)
              );
            }
            this.dataLoad = true;
           }else{
            for (const iterator of tableList) {
              (this.tableForm.controls['editInfo'] as FormArray).push(
                this.formbuilder.group( {searchKey:''})
              );
            }
            this.dataLoad = true;
           }
        }
       
       
      }
     
    }


  editData(item,i){
      
    
    if(this.editInputs == true){

      if(item.data[3].json==true){
        const initialState = {
          // records:JSON.stringify(item.configurableValue),
          records : JSON.parse(item.data[3].value),
          configurableKey : item.configurableKey,
          description : item.configurableValueDisplayText,
          configDisableJsonFields : item.configDisableJsonFields
        };
        this.modalRef = this.modalService.show(JsonEditorComponent, {
          class: "modal-dialog-centered modal-xl preview-modal",
          backdrop: "static",
          initialState:initialState
        });
        this.modalRef.content.response.subscribe(async (result) => {
          if (result) {
            this.editInfo.emit({row:item,value:result})
            this.modalRef.hide();
          } else {
            this.modalRef.hide();
          }
        });

        return;
      }


      item.edit = true;
      item.array = true;
      this.oldData = item.editObj;
      this.recordList = this.recordList.map(function(data,ind){
        if(i!==ind){
          data.edit=false;
        }
        return data;
      });
    }else{
      this.editLocation.emit(item)
    }
  }

  saveData(info,index){
    this.formService.markFormGroupTouched(this.tableForm.controls['editInfo']['controls'][index]);
    if (this.tableForm.controls['editInfo']['controls'][index].valid) {
       this.editInfo.emit({row:info,value:this.tableForm.controls['editInfo'].value[index]})
    }
  }

  dontSaveData(info,index){
    
    info.edit = false;
    info.editObj=this.oldData;
    
    if(info.edittype && info.edittype == 'array'){
      (this.tableForm.controls['editInfo'] as FormArray).removeAt(index);
      setTimeout(() => {
      (this.tableForm.controls['editInfo'] as FormArray).insert(index, this.formbuilder.group({
        [this.tableFields[3]]:this.getArrayForm(info[this.tableFields[3]])}));
      });
    }else if(info.edittype && info.edittype == 'radio'){
      (this.tableForm.controls['editInfo'] as FormArray).removeAt(index);
      (this.tableForm.controls['editInfo'] as FormArray).insert(index, this.formbuilder.group({
        [this.tableFields[3]]:info.data[3].value}));
    }
    else if(info.edittype && info.edittype == 'multiSelect'){
      (this.tableForm.controls['editInfo'] as FormArray).removeAt(index);
      (this.tableForm.controls['editInfo'] as FormArray).insert(index, this.formbuilder.group({
        [this.tableFields[3]]:[info.configurableValue,[Validators.required]]}));
    }
    else if(info.edittype && info.edittype == 'textArea'){
      (this.tableForm.controls['editInfo'] as FormArray).removeAt(index);
      (this.tableForm.controls['editInfo'] as FormArray).insert(index, this.formbuilder.group({
        [this.tableFields[3]]:[info.configurableValue,[Validators.required]]}));
    }
    else{
      let dataArray = [];
      this.recordList.forEach(element => {
        const keys = Object.keys(element.editObj);
        let valuesObj={};
        for (let index = 0; index < keys.length; index++) {
          const key = keys[index];
            if(Array.isArray(element.editObj[key])){
              const keyObj = {[key]:element.editObj[key][0]}
              valuesObj = {...keyObj,...valuesObj};
            }else{
              const keyObj = {[key]:element.editObj[key]}
              valuesObj = {...keyObj,...valuesObj};
            }
        }
        dataArray.push(valuesObj);
      
      });
      setTimeout(() => {
      this.tableForm.controls.editInfo.patchValue(dataArray);
      });
    }
  


    

  }

  toggleSort(){
    this.sortValue = this.sortValue == 0 ? 1:0;
  }
  
  sortData(type, key) {
    
    this.toggleSort();
    this.paramsObj.sortBy = key+":"+this.sortType[this.sortValue];
    this.paramsObj.page = 1;
    this.firstPage = 1;
    this.listApiCall.emit(this.paramsObj);  
    
  }

  selectSearch(event,item){
    if(event.target.checked == false){
      this.tableForm.patchValue({[item+"_text"]:'' });
    }
    this.tableFields.map((obj) => {
      if(obj != item){
        this.tableForm.patchValue({[obj]:false });
        this.tableForm.patchValue({[obj+"_text"]:'' });
      }});
  }

  @HostListener('window:keyup', ['$event'])
  onKeySearch(event,lable,call){
    if(call){
      this.searchCall.emit(this.tableForm.controls);
    }
  }

  tablelimit(event){
    this.firstPage=1;
    this.paramsObj.page=1;
    this.tablePageLimit=event.target.value;
    this.paramsObj.limit=event.target.value;
    this.listApiCall.emit(this.paramsObj);
  }

  dropdownPages = [5,10,15,25,50,100];

  changePage(event){
    this.paramsObj.page = event;
    this.firstPage = event;
    this.listApiCall.emit(this.paramsObj);
  }

  searchFilter(){
    this.shortingFilters = false;
    if(this.shortingFilters == false){
      this.paramsObj.sortBy = '';
    }
    this.showFilters = !this.showFilters;
    if(this.showFilters == false){
      this.tableFields.map((obj) => {
        this.tableForm.patchValue({[obj]:false });
        this.tableForm.patchValue({[obj+"_text"]:'' });
    });
    }
  }

  shortFilter(){
    this.showFilters = false;
    if(this.showFilters == false){
      this.tableFields.map((obj) => {
        this.tableForm.patchValue({[obj]:false });
        this.tableForm.patchValue({[obj+"_text"]:'' });
    });
    }
    this.shortingFilters = !this.shortingFilters;
    if(this.shortingFilters == false){
      if(this.paramsObj.sortBy != ''){
        this.paramsObj.sortBy = '';
        this.listApiCall.emit(this.paramsObj);
      }
      // this.paramsObj.page = 1;
      // this.firstPage = 1;
    }
  }

  checkInput(event,index,fieldName,item){   
    if(event.code === "Space" && this.tableForm.get('editInfo')['controls'][index].controls[fieldName].value.trim().length === 0){
      setTimeout(() => {
      (this.tableForm.controls['editInfo'] as FormArray).at(index).patchValue({[fieldName]:''});
      });
    }else  if (event.key === "Enter") {
      (this.tableForm.controls['editInfo'] as FormArray).at(index).patchValue({[fieldName]:this.tableForm.get('editInfo')['controls'][index].controls[fieldName].value.trim()});
      setTimeout(() => {
        this.saveData(item,index)
      });
    }else if(event.type === "paste"){
      setTimeout(() => {
        if(this.tableForm.get('editInfo')['controls'][index].controls[fieldName].value.trim().length === 0){
          (this.tableForm.controls['editInfo'] as FormArray).at(index).patchValue({[fieldName]:''});
        }
      });
    }
  }

  checkBlanck(index,fieldName){
    (this.tableForm.controls['editInfo'] as FormArray).at(index).patchValue({[fieldName]:this.tableForm.get('editInfo')['controls'][index].controls[fieldName].value.trim()});
  }

  arrayData(index,fieldName) : FormArray {
    return this.tableForm.get("editInfo")['controls'][index].controls[fieldName] as FormArray
  }
  newArrayData(id): FormGroup {
    return this.formbuilder.group({
      id: id+1,
      value: ['',[Validators.required,Validators.minLength(3),Validators.maxLength(256)]],
    })
  }

  addArrayData(index,fieldName,id) {
    this.arrayData(index,fieldName).push(this.newArrayData(id));
  }

  checkArrayInput(event,index,fieldName,item,iData){   
    if(event.code === "Space" && this.tableForm.get('editInfo')['controls'][index].controls[fieldName].controls[iData].controls['value'].value.trim().length === 0){
      setTimeout(() => {
      (this.tableForm.controls['editInfo']['controls'][index].controls[fieldName] as FormArray).at(iData).patchValue({value:''});
      });
    }else  if (event.key === "Enter") {
      (this.tableForm.controls['editInfo']['controls'][index].controls[fieldName] as FormArray).at(iData).patchValue({value:this.tableForm.get('editInfo')['controls'][index].controls[fieldName].controls[iData].controls['value'].value.trim()});
      setTimeout(() => {
        this.saveData(item,index)
      });
    }else if(event.type === "paste"){
      setTimeout(() => {
        if(this.tableForm.get('editInfo')['controls'][index].controls[fieldName].controls[iData].controls['value'].value.trim().length === 0){
          (this.tableForm.controls['editInfo']['controls'][index].controls[fieldName] as FormArray).at(iData).patchValue({value:''});
        }
      });
    }
  }

  checkArrayBlanck(index,fieldName,iData){
    (this.tableForm.controls['editInfo']['controls'][index].controls[fieldName] as FormArray).at(iData).patchValue({value:this.tableForm.get('editInfo')['controls'][index].controls[fieldName].controls[iData].controls['value'].value.trim()});
  }

  removeQuantity(index,fieldName,i:number) {
    this.arrayData(index,fieldName).removeAt(i);
  }

  getArrayForm(data){
    if(data){
      if(typeof data =='string'){
        return data;
      }
      else if(Array.isArray(data)){
        if(this.isObject(data[0])){
          const values = data.map(v => { 
            return this.formbuilder.group({
              id:v.id,
              value:[v.value,[Validators.required,Validators.minLength(3),Validators.maxLength(256)]]
            })  });
          return this.formbuilder.array(values);
        }else{
          let i = 0;
          const values = data.map(v => { 
            return this.formbuilder.group({
              id:i=i+1,
              value:[v,[Validators.required,Validators.minLength(3),Validators.maxLength(256)]]
            })  });
          return this.formbuilder.array(values);
        }
    
      }
    }
    return '';
  }

  isObject(objValue) {
    return objValue && typeof objValue === 'object' && objValue.constructor === Object;
  }

  generateExcel() {
    this.generateExcelFile.emit()
    // this.locationService.generateExcel();
   }


   getRadioButtonValues(val  ){
    if(val == ''){
      return 'true';
    }
    if(val == ''){
      return '0';
    }
    if(val == ''){
      return '1';
    }
   }

}
