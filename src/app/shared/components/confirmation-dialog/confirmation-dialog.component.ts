import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
    selector: 'app-confirmation',
    templateUrl: './confirmation-dialog.component.html',
    styleUrls: ['./confirmation-dialog.component.scss'],
})
export class ConfirmDialogComponent implements OnInit {
    title: string;
    message: string;

    constructor(public dialogRef: MatDialogRef<ConfirmDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: ConfirmDialogModel) {
        // Update view with given values
        this.title = this.title;
        this.message = this.message;
    }

    ngOnInit() {
    }
    deleteconsent:boolean;
    onConfirm(): void {
        // Close the dialog, return true
        this.deleteconsent=true;
        this.dialogRef.close(true);
    }

    onDismiss(): void {
        // Close the dialog, return false
        this.deleteconsent=false;
        this.dialogRef.close(false);
    }
}

/**
 * Class to represent confirm dialog model.
 *
 * It has been kept here to keep it as part of shared component.
 */
export class ConfirmDialogModel {

    constructor(public title: string, public message: string) {
    }
}
