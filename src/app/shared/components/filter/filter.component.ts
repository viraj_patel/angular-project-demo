import {
    Component, OnInit, Output, EventEmitter, Input
} from '@angular/core';

@Component({
    selector: 'app-filter',
    templateUrl: './filter.component.html',
    styleUrls: ['./filter.component.scss'],
})
export class FilterComponent implements OnInit {
    @Output() filterApplied = new EventEmitter<any>();

    @Input() mainList;
    @Input() filteredList;
    @Input() filterLabel;
    @Input() searchLabel;
    search: string;

    async ngOnInit() {
    }

    filterChange() {
        const filter = this.search;
        this.filterApplied.emit(filter);
    }

    onKey(value) {
        this.filteredList = this.Search(value);
    }

    Search(value: string) {
        const filter = value.toLowerCase();
        const mainlist = this.mainList;
        return mainlist.filter(option => option.toLowerCase().startsWith(filter));
    }

}
