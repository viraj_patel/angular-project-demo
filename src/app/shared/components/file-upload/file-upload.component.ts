import {
  Component,
  OnInit,
  Input,
  HostListener,
  ElementRef,
  Output,
  EventEmitter,
  ViewChild,
  Renderer2,
} from "@angular/core";
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from "@angular/forms";
import { HttpRequestsService } from "src/app/services/http-requests.service";
import { HttpEvent, HttpEventType } from "@angular/common/http";
import { ToastrService } from "ngx-toastr";
import { TranslateService } from "@ngx-translate/core";

export interface FileUpload {
  attachmentId: number;
  thumb: string;
  original: string;
}

@Component({
  selector: "app-file-upload",
  templateUrl: "./file-upload.component.html",
  styleUrls: ["./file-upload.component.scss"],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: FileUploadComponent,
      multi: true,
    },
  ],
})
export class FileUploadComponent implements OnInit, ControlValueAccessor {
  public progress = -1;
  @Input() fileMeta: FileUpload = {
    thumb: null,
    original: null,
    attachmentId: null,
  };
  @Input() uploadPath;
  isDisabled;

  @Output()
  delete: EventEmitter<{}> = new EventEmitter<{}>();

  @Output()
  imageUpdates: EventEmitter<{}> = new EventEmitter<{}>();

  @ViewChild("fileEle") fileEle: ElementRef;

  onChange: (_: any) => {};
  private file: File | null = null;

  constructor(
    private host: ElementRef<HTMLInputElement>,
    private httpRequest: HttpRequestsService,
    private renderer: Renderer2,
    private toastr: ToastrService,
    private translation: TranslateService
  ) { }

  ngOnInit(): void { }

  @HostListener("change", ["$event.target.files"]) emitFiles(event: FileList) {
    const file = event && event.item(0);
    this.uploadImage(file);
    this.file = file;
  }

  writeValue(value: FileUpload) {
    this.fileMeta = value;
    this.host.nativeElement.value = "";
    this.file = null;
  }

  registerOnChange(fn: () => {}) {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => {}) { }

  public onDeleteClicked() {
    this.delete.emit(this.fileMeta);
    this.fileMeta = null;
    this.onChange(this.fileMeta);
    this.imageUpdates.emit();
    this.fileEle.nativeElement.value = "";
  }

  private async uploadImage(file) {
    if (file.size > 10000000) {
      this.toastr.error(
        this.translation.instant("FILE_MUST_BE_LESS_THAN_10MB")
      );
    } else {
      this.progress = 0;
      const uploadData = new FormData();
      uploadData.append("file", file);

      const reader = new FileReader();
      reader.onload = (events: any) => {
        this.fileMeta.thumb = events.target.result;
        this.fileMeta.original = events.target.result;
      };

      reader.readAsDataURL(file);
      this.httpRequest.uploadWithProgress(this.uploadPath, uploadData)
        .subscribe((event: HttpEvent<any>) => {
          switch (event.type) {
            case HttpEventType.Sent:
              console.log("Request has been made!");
              break;
            case HttpEventType.ResponseHeader:
              console.log("Response header has been received!");
              break;
            case HttpEventType.UploadProgress:
              this.progress = Math.round((event.loaded / event.total) * 100);
              console.log(`Uploaded! ${this.progress}%`);
              break;
            case HttpEventType.Response:
              this.progress = -1;
              console.log("User successfully created!", event.body);
              const result = event.body;
              this.fileMeta.attachmentId = result.attachmentId;
              this.onChange(this.fileMeta);
              this.imageUpdates.emit();
              this.fileEle.nativeElement.value = "";
          }
        });
    }
  }

  setDisabledState(isDisabled: boolean) {
    this.isDisabled = isDisabled;
    if (isDisabled) {
      this.renderer.setAttribute(
        this.fileEle.nativeElement,
        "disabled",
        isDisabled ? "true" : ""
      );
    } else {
      this.renderer.removeAttribute(this.fileEle.nativeElement, "disabled");
    }
  }
}
