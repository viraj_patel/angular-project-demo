import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import {
  FileSystemDirectoryEntry,
  FileSystemFileEntry,
  NgxFileDropEntry,
} from "ngx-file-drop";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { CsvFormatComponent } from "src/app/modals/csv-format/csv-format.component";
import { constants, ERROR_CODE, LOCAL_STORAGE, LOCATION_PREREQUISITES, LOCATION_ROUTE, LOCATION_TABLE, LOGIN_ROUTE, REDIRECT_AFTER_ERROR_CODE, STATIC_DATA, USER_ROUTES } from "@constant/constants";
import { updateLocale } from "moment";
import { LocationService } from "@services/location.service";
import { HttpClient, HttpEventType, HttpHeaders, HttpParams } from "@angular/common/http";
import { LocalStorageService } from "@services/localService/localStorage.service";
import { environment } from "src/environments/environment";
import { ToastrService } from "ngx-toastr";
import { CsvUploadResultComponent } from "src/app/modals/csv-upload-result/csv-upload-result.component";
import { ViewFailCsvResultComponent } from "src/app/modals/view-fail-csv-result/view-fail-csv-result.component";
import { Router } from "@angular/router";
import { HttpErrorHandler } from "@services/error-handler/http-error-handler.service";
import { NgxSpinnerService } from "ngx-spinner";
import { TranslateService } from "@ngx-translate/core";
import { UtilityService } from "@services/utility.service";

@Component({
  selector: "app-drag-drop",
  templateUrl: "./drag-drop.component.html",
  styleUrls: ["./drag-drop.component.scss"],
})
export class DragDropComponent implements OnInit {
  URL = environment.apiEndPoint;
  url = process.env.NG_APP_BACKOFFICE_URL;
  @Input() forminputs;
  @Input() formFields;
  @Output() back = new EventEmitter<object>();
  @Input() tableData;
  @Input() type;
  @Input() prerequisitiesData;
  @Input() uploadUrl;
  @Input() paramObj;
  @Input() csvUploadTitles;
  @Output() downloadCSVSample = new EventEmitter<object>();
  csvFileData:any=null;
  progress:string = '0%';
  progessBar:boolean=false;
  selectFile:boolean=false;
  selectFileName:string="";
  urlUser = process.env.NG_APP_BACKOFFICE_URL;
  // localStorage: any;
  constructor(private http: HttpClient,
    private toastr: ToastrService,
    private localStorageService: LocalStorageService,
    private httpErrorHandler: HttpErrorHandler,
    private spinner: NgxSpinnerService,
    private modalService: BsModalService,
    private translate: TranslateService,
    private router: Router,
    public helper: UtilityService,
  
    ) {}
  public files: NgxFileDropEntry[] = [];
  public tempdata: any;
  modalRef: BsModalRef;
  modalRef1: BsModalRef;
  ngOnInit(): void {
  }
  

  public dropped(files: NgxFileDropEntry[]) {
    this.files = files;
    for (const droppedFile of files) {
      // Is it a file?
      if (droppedFile.fileEntry.isFile && this.isFileAllowed(droppedFile.fileEntry.name)) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {
          // Here you can access the real file
          // You could upload it like this:
          const formData = new FormData();
          formData.append("file", file);
          this.csvFileData=formData;
          this.selectFile = true;
          this.selectFileName = file.name;
        });
      } else {
        // It was a directory (empty directories are added, otherwise only files)
        this.toastr.error("Only files in '.xls', '.xlsx', '.csv' format are accepted.");
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
      }
    }
  }

  isFileAllowed(fileName: string) {
    let isFileAllowed = false;
    const allowedFiles = [".xls", ".xlsx", ".csv"];
    const regex = /(?:\.([^.]+))?$/;
    const extension = regex.exec(fileName);
    if (undefined !== extension && null !== extension) {
      for (const ext of allowedFiles) {
        if (ext === extension[0]) {
          isFileAllowed = true;
        }
      }
    }
    return isFileAllowed;
  }

  clearFile(){
    this.csvFileData=null;
    this.selectFile = false;
    this.progessBar = false;
    this.progress = "0%";
    this.back.emit();
  }
  async uploadFile(){
    let path;
    if(this.paramObj != null){
      let params = new HttpParams();
      Object.keys(this.paramObj).forEach((key) => {
        params = params.set(key, this.paramObj[key]);
      });

      path = this.url + this.uploadUrl + '?' + params;

    }
    else{
      path = this.url + this.uploadUrl;
    }

    
    this.selectFile = false;
    this.progessBar = true;
    

    const netowrkIsConnected = await this.getNetworkConnection();
    if (netowrkIsConnected) {
        let headers: any;
        if (path.indexOf(USER_ROUTES.SIGN_UP) === -1) {
          headers = await this.getHeaders(false,true);
          
      }
       this.http.post<any>(`${this.URL}${path}`, this.csvFileData, { headers, reportProgress: true,
            observe: 'events'})
            .subscribe(async resp => {
                if (resp.type === HttpEventType.Response) {
                    if(ERROR_CODE.includes(resp.body.code) || ERROR_CODE.includes(resp.body.responseCode)){
                      this.toastr.error(resp.body.responseMessage);
                      this.progress = "0%";
                      this.progessBar = false;
                      
                    }else{
                      const initialState = {
                        id: 10,
                        csvResult: resp.body.result.result  ,
                        type:this.type
                      };
                      this.modalRef = this.modalService.show(CsvUploadResultComponent, {
                        class: "modal-dialog-centered modal-xl preview-modal",
                        backdrop: "static",
                        initialState:initialState
                      });
                      this.modalRef.content.response.subscribe(async (result) => {
                        if (result != false) {
                            const initialState = {
                            id: 20,
                            type:this.type,
                            csvResult: result ,
                          };
                          this.modalRef1 = this.modalService.show(ViewFailCsvResultComponent, {
                              class: "modal-dialog-centered modal-xl preview-modal",
                              backdrop: "static",
                              initialState:initialState
                            });
                        } else {
                          this.modalRef.content.modalRef.hide();
                          this.progress = "0%";
                          this.progessBar = false;
                          this.csvFileData=null
                        }
                      });
                    }
                }
                if (resp.type === HttpEventType.UploadProgress) {
                    const percentDone = Math.round(100 * resp.loaded / resp.total);
                    this.progress = percentDone.toString()+"%";
                } 
                if (resp.type === HttpEventType.ResponseHeader && REDIRECT_AFTER_ERROR_CODE.includes(resp.status)) {
                 
                  this.unauthorize_401_Handler();
                   
                  }
            });
    } else {
        this.toastr.error(constants.NO_INTERNET_CONNECTION_MSG);
    }
    
    
  }
  
  async unauthorize_401_Handler(){
    
    const refreshToken: any = await this.localStorageService.getDataFromIndexedDB(LOCAL_STORAGE.REFRESH_TOKEN);
    const objr={'refreshToken':refreshToken}
    const response:any =  await this.resetToken(this.urlUser + LOGIN_ROUTE.REFRESH_TOKEN, objr, false);
    
    await this.localStorageService.removeItem(LOCAL_STORAGE.TOKEN);
    await this.localStorageService.removeItem(LOCAL_STORAGE.REFRESH_TOKEN);
    await this.localStorageService.setDataInIndexedDB(
        LOCAL_STORAGE.TOKEN,
        response.result.token
      );
      await this.localStorageService.setDataInIndexedDB(
        LOCAL_STORAGE.REFRESH_TOKEN,
        response.result.refreshToken
      );
      await this.uploadFile();

     
}
 
async resetToken(path: any, obj: any, authorize, fromData?, isToaster: boolean = false,noLoder?,loading?) {
 

  return new Promise(async (resolve, _) => {
      const rolePath = path.indexOf('user-type/') !== -1;
      if(!noLoder){
          this.spinner.show();
      }
      const success = async (res) => {
          if (res && (res.message || res.msg || res.responseMessage) && !rolePath) {
              if(REDIRECT_AFTER_ERROR_CODE.includes(res.code) ||  REDIRECT_AFTER_ERROR_CODE.includes(res.responseCode)){
                  this.toastr.error(res.message ? res.message : res.msg);
                  await this.localStorageService.clearStorage();
                  await this.localStorageService.clearDataFromIndexedDB();
                      this.router.navigate(['/']);
                      this.spinner.hide();
              }else if(ERROR_CODE.includes(res.code) || ERROR_CODE.includes(res.responseCode) ){
                  this.toastr.error(res.responseMessage);
              }
              else{
                  if(isToaster){
                      this.toastr.success(res.message ? res.message : res.msg?res.msg:res.responseMessage);
                  }
                  
              }
          }
          if(!noLoder && !loading){
              this.spinner.hide();
          }
          resolve(res);
      };
      const error = async (err) => {
          this.spinner.hide();
          this.toastr.error(err.error.responseMessage);
          if(REDIRECT_AFTER_ERROR_CODE.includes(err.error.responseCode)){
              await this.localStorageService.clearStorage();
              await this.localStorageService.clearDataFromIndexedDB();
                  this.router.navigate(['/']);

          }
      };
      const netowrkIsConnected = await this.getNetworkConnection();
      if (netowrkIsConnected) {
          let headers: any;
          if (path.indexOf(USER_ROUTES.SIGN_UP) === -1) {
              headers = await this.getHeaders(authorize, fromData ? fromData : false);
          }
          return this.http.post<any>(`${this.URL}${path}`, obj, { headers })
              .subscribe(success, error);
      } else {
          
          if(!noLoder){
              this.spinner.hide();
          }
          this.toastr.error(constants.NO_INTERNET_CONNECTION_MSG);
      }

  });
}

  viewCSVDemo(data) {
      
    const initialState = {
      title:  data === 'prerequisitiesData' ? 'Prerequisites For CSV' :'CSV Format',
      data: data === 'prerequisitiesData' ? this.prerequisitiesData :this.tableData  ,
    };

    this.modalRef = this.modalService.show(CsvFormatComponent, {
      class: "modal-dialog-centered modal-xl preview-modal",
      backdrop: "static",
      initialState,
    });
    this.modalRef.content.response.subscribe(async (result) => {
      if (result) {
      } else {
      }
    });
  }

  public fileOver(event) {
  }

  public fileLeave(event) {
  }

  async getHeaders(tokenRequired, formData?) {
    const token: any = await this.localStorageService.getDataFromIndexedDB(LOCAL_STORAGE.TOKEN);
    if (tokenRequired) {
                const headers = new HttpHeaders()
                .set('Authorization', token)
                .set('Content-Type', 'application/json')
                ;
                return headers;
    } else if (formData) {
            const headers = new HttpHeaders()
            .set('Authorization', token)
            ;
            return headers;
    } else {
        const headers = new HttpHeaders()
            .set('Authorization', '')
            .set('Content-Type', 'application/json');
        return headers;
    }
}

  getNetworkConnection() {
    const isOnline: boolean = navigator.onLine;
    if (isOnline) {
        return true;
    }
    return false;
}

}
