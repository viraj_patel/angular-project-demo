import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { PagerService } from '@services/pager.service';
import { UtilityService } from '@services/utility.service';

@Component({
    selector: 'app-pagination',
    templateUrl: './common-pagination.component.html',
    styleUrls: ['./common-pagination.component.scss']
})

export class CommonPaginationComponent implements OnInit, OnChanges {
    @Input() currentPage: number;
    @Input() totalRecords: number;
    @Input() limitObject = { limit: 0, limits: [] };

    @Output() limitChange = new EventEmitter<number>();
    @Output() pageChange = new EventEmitter<number>();

    pager: any = {};

    pagedItems: any[];

    constructor(
        public pagerService: PagerService,
        public utility: UtilityService,
    ) {
    }

    ngOnInit() {
    }

    ngOnChanges() {
        this.setPage(this.currentPage, false);
    }

    setPage(page: number, callPageChange: boolean = true) {
        if (page < 1) {
            return;
        }
        this.pager = this.pagerService.getPager(this.totalRecords, page, this.limitObject.limit);
        if (callPageChange) {
            this.changePage(page);
        }
    }

    changePage(pageNumber: number) {
        this.currentPage = pageNumber;
        this.pageChange.emit(pageNumber);
    }

    changelimit(limit: number) {
        const totalPages = Math.ceil(this.totalRecords / this.limitObject['limit']);
        this.currentPage = this.currentPage > totalPages ? totalPages : this.currentPage;
        this.setPage(this.currentPage);
        this.limitChange.emit(limit);
    }


}
