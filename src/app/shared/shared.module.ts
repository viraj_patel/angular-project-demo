import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { MatSortModule } from '@angular/material/sort';
import { UtilityService } from '@services/utility.service';
import { HttpRequestsService } from '@services/http-requests.service';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatNativeDateModule, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialogModule } from '@angular/material/dialog';
import { FullCalendarModule } from '@fullcalendar/angular'; // for FullCalendar!
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { FormService } from '@services/form.service';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { MatIconModule } from '@angular/material/icon';
import { MatTabsModule } from '@angular/material/tabs';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { MatDividerModule } from '@angular/material/divider';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { FileUploadComponent } from './components/file-upload/file-upload.component';
import { AvailableListComponent } from './components/available-list/available-list.component';
import { OnlyNumberPipe } from './components/pipe/custom.pipe';
import { ConfirmDialogComponent } from './components/confirmation-dialog/confirmation-dialog.component';
import { ValidationMessageModule } from './components/validation-message/validation-message.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { createTranslateLoader } from '../app.module';
import { HttpClient } from '@angular/common/http';
import { DatePickerComponent } from './components/date-picker/date-picker.component';
import { DragDropComponent } from './components/drag-drop/drag-drop.component';
import { NgxFileDropModule } from 'ngx-file-drop';
import { MatTooltip, MatTooltipModule } from '@angular/material/tooltip';
// import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { MatSelect, MatSelectModule } from '@angular/material/select';
@NgModule({
  declarations: [
    AvailableListComponent,
    OnlyNumberPipe,
    ConfirmDialogComponent,
    FileUploadComponent,
    DatePickerComponent,
    DragDropComponent,
  ],
  imports: [
    CommonModule,
    MatDatepickerModule,
    MatTabsModule,
    // NgxMatSelectSearchModule,
    MatNativeDateModule,
    MatFormFieldModule,
    NgxMaterialTimepickerModule,
    NgxMaterialTimepickerModule.setLocale('en-GB'),
    FullCalendarModule,
    MatDialogModule,
    MatInputModule,
    ReactiveFormsModule,
    FormsModule,
    PaginationModule.forRoot(),
    MatSortModule,
    ModalModule.forRoot(),
    NgMultiSelectDropDownModule.forRoot(),
    MatExpansionModule,
    MatIconModule,
    MatButtonModule,
    MatCheckboxModule,
    MatRadioModule,
    MatAutocompleteModule,
    MatDividerModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    NgxFileDropModule,
    MatSelectModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient],
      },
    }),
    NgxPaginationModule
  ],
  exports: [
    MatDatepickerModule,
    MatTabsModule,
    MatNativeDateModule,
    MatFormFieldModule,
    NgxMaterialTimepickerModule,
    FullCalendarModule,
    MatDialogModule,
    MatInputModule,
    ReactiveFormsModule,
    FormsModule,
    PaginationModule,
    MatSortModule,
    ValidationMessageModule,
    AvailableListComponent,
    NgMultiSelectDropDownModule,
    OnlyNumberPipe,
    MatExpansionModule,
    MatButtonModule,
    MatCheckboxModule,
    MatRadioModule,
    MatAutocompleteModule,
    MatDividerModule,
    MatProgressSpinnerModule,
    FileUploadComponent,
    DatePickerComponent,
    DragDropComponent,
    TranslateModule,
    MatSelectModule,
    MatTooltipModule,
    MatIconModule
  ],
  providers: [
    UtilityService,
    HttpRequestsService,
    FormService,
    { provide: MAT_DATE_LOCALE, useValue: 'en-GB' },
  ]
})
export class SharedModule { }
