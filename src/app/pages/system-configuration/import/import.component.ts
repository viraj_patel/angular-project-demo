import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { STATIC_DATA, SYS_ROUTES, TABLE_DATA_TYPE } from '@constant/constants';
import { TranslateService } from '@ngx-translate/core';
import { JsonToCsvService } from '@services/json-to-csv.service';
import { JsonToXlsxService } from '@services/json-to-xlsx.service';
import { SystemCinfigurationsService } from '@services/system-cinfigurations.service';
@Component({
  selector: 'app-import',
  templateUrl: './import.component.html',
  styleUrls: ['./import.component.scss']
})
export class ImportComponent implements OnInit {
  typeTable:string=STATIC_DATA.SYSTEM_CONFIG;
  uploadUrl:string=SYS_ROUTES.BULK_UPLOAD;
  csvUploadTitles:string="";
  uploadCsvParamsObj = null;
  downloadSampleList;
  public prerequisitiesData: any;
  public tableData: any;
  constructor(private systemService: SystemCinfigurationsService,private jsonToCsvService: JsonToCsvService,
    private translate: TranslateService,private jsonToXlsx : JsonToXlsxService) {
    translate.setDefaultLang('en');
   }

  async ngOnInit() {
    this.downloadSampleList = await this.downloadSampleFunction()
    this.table_data();
    this.setPrerequisitiesData();
  }
  async downloadSampleFunction(){
    return await await this.systemService.downloadSampleCsv();
  }
  async downloadCSVSample(){
    const response:any = await this.systemService.downloadSampleCsv();

    let header = Object.keys(response.result.format[0]);
    let data = response.result.format;
    this.jsonToXlsx.exportToCsv(data, this.typeTable+"_Sample_CSV",header);
  }

  async table_data() { 
    this.tableData = { header: [], data: [] };
    const response:any = this.downloadSampleList;
    this.tableData.header = Object.keys(response.result.format[0])
    let recordsList = [...response.result.format]
    let data = [];
    recordsList.map((item)=>{
      
      let row = [];
      for(let key of Object.keys(item)){
        let obj= {
          value:item[key],
          type: TABLE_DATA_TYPE.TEXT,
          class1:""
        };
        row.push(obj);
      }
      data.push({row})
    })

    this.tableData.data = data;
  }

  setPrerequisitiesData(){
    this.prerequisitiesData = { header: [], data: [] };
    const response:any = this.downloadSampleList;
    this.prerequisitiesData.header = Object.keys(response.result.prerequisites[0]);
    let preRecordsList = [...response.result.prerequisites];
    let perData = [];
    preRecordsList.map((item)=>{
      let row = [];
      for(let key of Object.keys(item)){
        let obj= {
          value:item[key],
          type: key =="required" ?  TABLE_DATA_TYPE.ICON : TABLE_DATA_TYPE.TEXT,
          class1:key=="required" ? (item.required ? "fa solid fa-check" : "fa solid fa-xmark") : ""
        };
        row.push(obj);
      }
      perData.push({row})
    })
    
    this.prerequisitiesData.data = perData;
  }

  
  hideCsvUpload(){

  }
}
