import { Component, Input, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { SystemCinfigurationsService } from '@services/system-cinfigurations.service';

@Component({
  selector: 'app-b2-b-table',
  templateUrl: './b2-b-table.component.html',
  styleUrls: ['./b2-b-table.component.scss']
})
export class B2BTableComponent implements OnInit {

  constructor( private systemService:SystemCinfigurationsService,public translation: TranslateService) { translation.setDefaultLang('en');}
  showNoData = false;
  tableHeading;
  extraColumns;
  response;
  recordList;
  @Input() activeConfigurationTab;
  @Input() edit;

  subscriptionArray: any[] = [
    {
      name:'Postpaid',
      value:'Postpaid'
    },
    {
      name:'Prepaid',
      value:'Prepaid'
    },
    {
      name:'All',
      value:'All'
    }
  ];

  segmentArray: any[] = [
    {
      name:'B2B',
      value:'B2B'
    },
    {
      name:'B2C',
      value:'B2C'
    },
    {
      name:'All',
      value:'All'
    }
  ];
  
  isChecked(ind,roleType){
    let roles = this.recordList[ind].roles ? this.recordList[ind].roles : [];
    for(let role of roles){
      if(role.name==roleType){
        return true
      }
    }
    return false;
  }

  async getPermissionRecords(){
    this.recordList = await  this.systemService.getCustomerAccessDataData();
    this.recordList = this.recordList.result;
    this.recordList = this.recordList.map(v => ({
      ...v,
      oldsubscriptionType : v.subscriptionType,
      oldcustomerTypeSegment : v.customerTypeSegment, 
      edit: false
    }))
  }
  
  async ngOnInit() {
    this.tableHeading = ['id','Operations', 'Service Type', 'Customer Segment'];
    await this.getPermissionRecords()
    
    this.response =  await this.systemService.getCustomerAccessExtraColumns();
    
    this.extraColumns = this.response.data.roles; 
    for(let data of this.response.data.roles){
      this.tableHeading.push(data.description)
    }
  }

  

  async onCheckboxChange(roleId,permissionId){
    let data = {
      roleId,
      permissionId
    }
    await this.systemService.postSysCheckBoxStatus(data);
    await this.getPermissionRecords();
  }

  async onDropdownChange(ind, type){
    let data = {
      permissionId:this.recordList[ind].id,
      subscriptionType:'',
      customerTypeSegment:''
    }

    if(type=="subscriptionType"){
      data.subscriptionType = this.recordList[ind].subscriptionType;
    }
    else{
      data.customerTypeSegment = this.recordList[ind].customerTypeSegment;
    }
    data = this.cleanObject(data);
    await this.systemService.editCustomerAccess(data);
    await this.getPermissionRecords();
  }


  cleanObject(obj) {
    for (var propName in obj) {
      if (obj[propName] === '' || obj[propName] === undefined) {
        delete obj[propName];
      }
    }
    return obj
  }

}
