import { ComponentFixture, TestBed } from '@angular/core/testing';

import { B2BTableComponent } from './b2-b-table.component';

describe('B2BTableComponent', () => {
  let component: B2BTableComponent;
  let fixture: ComponentFixture<B2BTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ B2BTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(B2BTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
