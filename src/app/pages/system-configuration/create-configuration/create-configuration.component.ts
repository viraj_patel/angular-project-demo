import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { UsersService } from '@services/users.service';

@Component({
  selector: 'app-create-configuration',
  templateUrl: './create-configuration.component.html',
  styleUrls: ['./create-configuration.component.scss']
})
export class CreateConfigurationComponent implements OnInit {
  @Input() activeConfigurationTab;
  @Input() mainTital;
  @Input() paramsObj;
  isChecked = true;
  
  errorMsg: any = {};
  configForm:FormGroup;
  isSearchDone = false;
  isTabChanged = false;
  
  @Input() createConfig = false;
  @Output() back = new EventEmitter<object>();
  @Output() listApiCall = new EventEmitter<object>();
  @Output() searchByForm = new EventEmitter<object>();

  constructor(
    private fb:FormBuilder,
    public translation: TranslateService,
    private userService:UsersService,
  ) { 
    translation.setDefaultLang('en');
    this.configForm = this.fb.group({
      countryName: [''],
      // status: ['100'],
      // isChecked: ['true']
    });
  }

  ngOnInit(): void {

    this.userService.getConfigTab().subscribe (tab=>{
      this.isTabChanged = true;
      this.resetForm();
      this.isTabChanged = false;
    });
  }

  

  intializingMessage() {
    this.errorMsg.countryName = {
      required: this.translation.instant('ERR_MSG_REQUIERD_NAME'),
    };
  }

  onSubmit(data){

    if(this.paramsObj.search == this.configForm.controls['countryName'].value.trim() || this.paramsObj.search == undefined){
      return;
    }

    this.configForm.patchValue({
      countryName:this.configForm.controls['countryName'].value.trim()
    })

    this.paramsObj.search = this.configForm.controls['countryName'].value;
    // this.paramsObj.status = this.configForm.controls['status'].value;
    // this.paramsObj.category = this.activeConfigurationTab;

    this.searchByForm.emit(this.paramsObj);
    this.isSearchDone = true;
  }

  checkInput(event){
    if(event.code === "Space" && this.configForm.value.countryName.trim().length === 0){
      setTimeout(() => {
        this.configForm.patchValue({["countryName"]: "" });
      });
    }else if(event.type === "paste"){
      setTimeout(() => {
        if(this.configForm.value.countryName.trim().length === 0){
          this.configForm.patchValue({["countryName"]: "" });
        }
      });
    } 
    
  }  

  resetForm(){
    this.configForm.reset()
    this.configForm.patchValue({
      countryName:'',
      // status:'100'
    })

    if(this.isTabChanged){
      this.paramsObj.search = this.configForm.controls['countryName'].value;
      // this.paramsObj.category = this.activeConfigurationTab;
      this.paramsObj.page = 1;
      this.isSearchDone = false;
      return;
    }

    if(this.isSearchDone){
      this.paramsObj.search = this.configForm.controls['countryName'].value;
      // this.paramsObj.status = this.configForm.controls['status'].value;
      // this.paramsObj.category = this.activeConfigurationTab;
      this.paramsObj.page = 1;
      this.searchByForm.emit(this.paramsObj);
      this.isSearchDone = false;
    }
  }



}
