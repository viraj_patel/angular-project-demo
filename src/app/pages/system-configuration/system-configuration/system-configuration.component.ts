import { HttpParams } from "@angular/common/http";
import {
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild,
} from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
import {
  FILTER_DATE_FORMAT,
  SYSTEM_CONFIGURATION_TABS_LIST,
  CUSTOMER_TABS_LIST,
  SYS_TABLE,
  SYS_TABLE_SEARCH,
  TABLE_DATA_TYPE,
  SYSTEM_CONFIGURATION_TABLE,
  CUSTOMER_MANAGEMENT_TAB_LIST,
  LOCAL_STORAGE,
  TAB_FUNCTION,
} from "@constant/constants";
import { TranslateService } from "@ngx-translate/core";
import { LocalStorageService } from "@services/localService/localStorage.service";
import { StaffService } from "@services/staff.service";
import { SystemCinfigurationsService } from "@services/system-cinfigurations.service";
import { UsersService } from "@services/users.service";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { Observable } from "rxjs";
import { map, startWith } from "rxjs/operators";

declare var $: any;

@Component({
  selector: "app-system-configuration",
  templateUrl: "./system-configuration.component.html",
  styleUrls: ["./system-configuration.component.scss"],
})
export class SystemConfigurationComponent implements OnInit, OnDestroy {
  activeConfigurationTab: string = CUSTOMER_TABS_LIST.AUTHENTICATION;

  systemConfigTab = SYSTEM_CONFIGURATION_TABS_LIST;
  currentTab;
  parentTab = "Authentication";
  customerTab = CUSTOMER_TABS_LIST;
  tablename: string = SYSTEM_CONFIGURATION_TABLE.AUTHENTICATION_TABLE;
  customerManagementTab = CUSTOMER_MANAGEMENT_TAB_LIST;
  public event = '-';
  public todo = '-';
  // tablename:string=SYSTEM_CONFIGURATION_TABLE.CHECKOUT_TABLE;
  activeDropDown;
  activeTab = CUSTOMER_TABS_LIST.CHECKOUT;
  firstDropDown;
  view: boolean = false;
  @ViewChild("tabBox") inputBox: ElementRef;
  myControl = new FormControl("");
 
  
//  customerArray=["Authentication","Checkout","Consent","Customer Detail","eKYC & Upload Document","Expression","Inventory","Payment Management","Quotation","Subscription Limit"];
customerArray:any=[];  
options: string[] = ['Customer Management',"Authentication","Customer","E-Care","Product","Shopping Cart","System","User",'Other Configuration',"Checkout","Consent","Customer Detail","eKYC & Upload Document","Expression","Inventory","Payment Management","Quotation","Subscription Limit","B2B","Customer Access","Billing Details","Customer Details","Update Profile","Vanity Categories"];
  filteredOptions: Observable<string[]>;
  systemMenu:any;
  constructor(
    private router: Router,
    private userService: UsersService,
    private localStorage: LocalStorageService,
    private rolesService: StaffService, 
    private translate: TranslateService,
    private SystemCinfigurationsService: SystemCinfigurationsService
  ) {
    translate.setDefaultLang("en");
  }
  async ngOnDestroy() {
    await this.localStorage.setDataInIndexedDB(
      LOCAL_STORAGE.DATA_MAIN_ACTIVE_TAB,
      null
    );
    await this.localStorage.setDataInIndexedDB(
      LOCAL_STORAGE.DATA_ACTIVE_TAB,
      null
    );
  }
  async ngOnInit() {
    const response:any = await this.SystemCinfigurationsService.getTabs();
    this.systemMenu=response.result;
    for (const iterator of this.systemMenu) {
      iterator.show = false;
      if(iterator.configGroupName == "E-care"){
        iterator.configGroupName = "E-Care";
      }
      if(iterator.children.length > 0 && iterator.configGroupName != 'Authentication'){
        this.customerArray.push({name:iterator.configGroupName,children:true,childName:iterator.children[0],parent:true});
        for (const child of iterator.children) {
          this.customerArray.push({name:child,children:true,parentName:iterator.configGroupName});
        }
      }else{
        this.customerArray.push({name:iterator.configGroupName,children:false,parent:true});
        this.customerArray.push({name:'Customer Access',children:false,parent:true})
      }
       
    }
    
    const url = this.router.url;
    const path = url;
    const route = `/${path.split("/")[2]}`;
    const currentRoute = route.split("?")[0].replace("/", "");

    const permissions = await this.rolesService.getLoginPermissionsForModule(currentRoute      
    );

    if (permissions.length > 0) {
      if (permissions[0].View == false) {
        this.router.navigate(["/main-home/my-profile"]);
      } else {
        this.view = true;
      }
    } else {
      this.view = true;
    }
    const active_main_tab: any = await this.localStorage.getDataFromIndexedDB(
      LOCAL_STORAGE.DATA_MAIN_ACTIVE_TAB
    );

   
    if (active_main_tab == null) {
     this.getSearch(this.customerArray[0]);
    } else {
      this.CurrentRoute();
    }

    //   document.getElementById("Dropdown").addEventListener('click', function (event) {
    //     event.stopPropagation();
    // });

    $("#customerManage").on("hide.bs.dropdown", function (e: any) {
      if (e.clickEvent) {
        e.preventDefault();
      }
    });
    $("#customerLink").on("hide.bs.dropdown", function (e: any) {
      if (e.clickEvent) {
        e.preventDefault();
      }
    });
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(""),
      map((value) => this._filter(value || ""))
    );
    

  }
    private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.customerArray.filter(option => option.name.toLowerCase().includes(filterValue));
  }

  async CurrentRoute() {
    const active_main_tab: any = await this.localStorage.getDataFromIndexedDB(
      LOCAL_STORAGE.DATA_MAIN_ACTIVE_TAB
    );
    const active_tab: any = await this.localStorage.getDataFromIndexedDB(
          LOCAL_STORAGE.DATA_ACTIVE_TAB
        );
    if(active_main_tab != null){
      if(active_tab != null){
        this.clickOnTab(active_tab,true,active_main_tab);
      }else{
        this.clickOnTab(active_main_tab,true);
      }
    }
    this.currentTab = active_main_tab;

    // if (this.currentTab == this.systemConfigTab.CUSTOMER) {
    //   if (this.activeDropDown != this.systemConfigTab.CUSTOMER) {
    //     $("#customerDropDown").dropdown("toggle");
    //     this.activeDropDown = this.currentTab;
    //     this.firstDropDown = this.currentTab;
    //   }
    //   const active_tab: any = await this.localStorage.getDataFromIndexedDB(
    //     LOCAL_STORAGE.DATA_ACTIVE_TAB
    //   );
    //   if (active_tab != null) {
    //     this.activeTab = active_tab;
    //     await this.localStorage.setDataInIndexedDB(
    //       LOCAL_STORAGE.DATA_ACTIVE_FUNCTION,
    //       TAB_FUNCTION.LIST
    //     );
    //     this.getConfigurationTab(this.activeTab, "", this.currentTab);
    //   } else {
    //     this.activeTab = this.customerTab.CHECKOUT;

    //     await this.localStorage.setDataInIndexedDB(
    //       LOCAL_STORAGE.DATA_ACTIVE_TAB,
    //       this.activeTab
    //     );
    //     await this.localStorage.setDataInIndexedDB(
    //       LOCAL_STORAGE.DATA_ACTIVE_FUNCTION,
    //       TAB_FUNCTION.LIST
    //     );
    //     this.getConfigurationTab(this.activeTab, "", this.currentTab);
    //   }
    // } else if (this.currentTab == this.systemConfigTab.CUSTOMER_MANAGEMENT) {
    //   if (this.activeDropDown != this.systemConfigTab.CUSTOMER_MANAGEMENT) {
    //     $("#customerManagementDropDown").dropdown("toggle");
    //     this.activeDropDown = this.currentTab;
    //     this.firstDropDown = this.currentTab;
    //   }

    //   const active_tab: any = await this.localStorage.getDataFromIndexedDB(
    //     LOCAL_STORAGE.DATA_ACTIVE_TAB
    //   );
    //   if (active_tab != null) {
    //     this.activeTab = active_tab;
    //     await this.localStorage.setDataInIndexedDB(
    //       LOCAL_STORAGE.DATA_ACTIVE_FUNCTION,
    //       TAB_FUNCTION.LIST
    //     );
    //     this.getConfigurationTab(this.activeTab, "", this.currentTab);
    //   } else {
    //     this.activeTab = this.customerManagementTab.B2B;
    //     await this.localStorage.setDataInIndexedDB(
    //       LOCAL_STORAGE.DATA_ACTIVE_TAB,
    //       this.activeTab
    //     );
    //     await this.localStorage.setDataInIndexedDB(
    //       LOCAL_STORAGE.DATA_ACTIVE_FUNCTION,
    //       TAB_FUNCTION.LIST
    //     );
    //     this.getConfigurationTab(this.activeTab, "", this.currentTab);
    //   }
    // } else if (this.currentTab != null) {
    //   this.activeDropDown = this.currentTab;
    //   await this.localStorage.setDataInIndexedDB(
    //     LOCAL_STORAGE.DATA_ACTIVE_FUNCTION,
    //     TAB_FUNCTION.LIST
    //   );
    //   if (this.currentTab == SYSTEM_CONFIGURATION_TABLE.AUTHENTICATION_TABLE) {
    //     this.tablename = SYSTEM_CONFIGURATION_TABLE.AUTHENTICATION_TABLE;
    //   }
    //   this.userService.sendConfigTab({ tab: this.currentTab });

    //   if ($("#Dropdown").is(".show")) {
    //     $(function () {
    //       $("#locationDropDown").dropdown("toggle");
    //     });
    //   } else if ($("#Dropdown2").is(".show")) {
    //     $(function () {
    //       $("#ticketDropDown").dropdown("toggle");
    //     });
    //   }
    // } else {
    //   this.currentTab = this.systemConfigTab.AUTHENTICATION;
    // }
  }
  async searchEnter(event){
    // event.stopPropagation();
    let value ;
    await this.customerArray.map((v)=>{
      if(v.name == event.option.value){
        value =  v;
      }
    });
    this.getSearch(value);
  }

  async getSearch(value){
    
    if(value.parent == true){
      if(value.children == true){
        this.clickOnTab(value.childName,true,value.name,value.name);
      }else{
        this.clickOnTab(value.name,true);
      }

    }else if(value.children == true){
      this.clickOnTab(value.name,true,value.parentName);
    }
  }
  async clickOnTab(tab, dropDown,parentTab?,childParent?){
    
  // const response=await Object.keys(this.systemConfigTab).find(key => this.systemConfigTab[key] == tab)
  if(parentTab!=undefined){
    if(this.currentTab == parentTab && childParent!=undefined){
      for (const iterator of this.systemMenu) {
        if(iterator.configGroupName == parentTab){
          iterator.show = iterator.show==true?false:true;
        }else{
          iterator.show = false;
        }
      } 
    }else{
      this.currentTab = parentTab;
      this.activeTab = tab;
      for (const iterator of this.systemMenu) {
       if(iterator.configGroupName == parentTab){
         iterator.show = true;
         await this.localStorage.setDataInIndexedDB(
          LOCAL_STORAGE.DATA_MAIN_ACTIVE_TAB,
          this.currentTab 
        );
         await this.localStorage.setDataInIndexedDB(
          LOCAL_STORAGE.DATA_ACTIVE_TAB,
          this.activeTab
        );
         this.userService.sendConfigTab({ tab: tab, groupName: parentTab, subGroupName: tab});
       }else{
         iterator.show = false;
       }
     } 
    }
    
  }
  else{
    this.currentTab = tab;
    this.userService.sendConfigTab({ tab: tab, groupName: tab});
    for (const iterator of this.systemMenu) {
      if(iterator.configGroupName == tab){
        iterator.show=false;
      }else{
        iterator.show = false;
      }
    } 
    await this.localStorage.setDataInIndexedDB(
      LOCAL_STORAGE.DATA_MAIN_ACTIVE_TAB,
      this.currentTab 
    );
    await this.localStorage.setDataInIndexedDB(
      LOCAL_STORAGE.DATA_ACTIVE_TAB,
      null
    );
  }
  // if(response!=undefined){
  //   this.getCurrentRoute(tab, event, true)
  // }
  // else{
  //   const customerResponse=await Object.keys(this.customerTab).find(key => this.customerTab[key] == tab)
  //   if(customerResponse!=undefined){
  //     this.getConfigurationTab(tab, '', this.systemConfigTab.CUSTOMER)
  //   }
  //   else{
  //     const customerManagementResponse=await Object.keys(this.customerManagementTab).find(key => this.customerManagementTab[key] == tab)
  //     if(customerManagementResponse!=undefined){
  //       this.getConfigurationTab(tab, '', this.systemConfigTab.CUSTOMER_MANAGEMENT)
  //     }
  //   }    
  // } 
  }
  

  async getCurrentRoute(tab, event, dropDown) {
    this.currentTab = tab;
    if (event != "") {
      event.stopPropagation();
    }
    await this.localStorage.setDataInIndexedDB(
      LOCAL_STORAGE.DATA_MAIN_ACTIVE_TAB,
      tab
    );


    if (tab == this.systemConfigTab.CUSTOMER) {
      if (this.activeDropDown != this.systemConfigTab.CUSTOMER) {
        this.activeDropDown = tab;
        this.activeTab = this.customerTab.CHECKOUT;
        await this.localStorage.setDataInIndexedDB(
          LOCAL_STORAGE.DATA_ACTIVE_TAB,
          this.activeTab
        );
        this.getConfigurationTab(this.activeTab, "", this.currentTab);
      }
      if (
        this.firstDropDown != this.systemConfigTab.CUSTOMER &&
        this.firstDropDown != this.systemConfigTab.CUSTOMER_MANAGEMENT
      ) {
        this.firstDropDown = tab;
        $(function () {
          $("#customerDropDown").dropdown("toggle");
        });
      }
      if (
        $("#Dropdown").is(".show") == false &&
        (dropDown || this.firstDropDown != this.systemConfigTab.CUSTOMER)
      ) {
        this.firstDropDown = tab;
        $("#customerDropDown").dropdown("toggle");
      } else if ($("#Dropdown").is(".show") == true && dropDown) {
        $("#customerDropDown").dropdown("toggle");
      }
    } else if (tab == this.systemConfigTab.CUSTOMER_MANAGEMENT) {
      if (this.activeDropDown != this.systemConfigTab.CUSTOMER_MANAGEMENT) {
        this.activeDropDown = tab;
        this.activeTab = this.customerManagementTab.B2B;
        await this.localStorage.setDataInIndexedDB(
          LOCAL_STORAGE.DATA_ACTIVE_TAB,
          this.activeTab
        );
        this.getConfigurationTab(this.activeTab, "", this.currentTab);
      }
      if (
        this.firstDropDown != this.systemConfigTab.CUSTOMER &&
        this.firstDropDown != this.systemConfigTab.CUSTOMER_MANAGEMENT
      ) {
        this.firstDropDown = tab;
        $(function () {
          $("#customerManagementDropDown").dropdown("toggle");
        });
      }
      if (
        $("#Dropdown2").is(".show") == false &&
        (dropDown ||
          this.firstDropDown != this.systemConfigTab.CUSTOMER_MANAGEMENT)
      ) {
        this.firstDropDown = tab;
        $("#customerManagementDropDown").dropdown("toggle");
      } else if ($("#Dropdown2").is(".show") == true && dropDown) {
        $("#customerManagementDropDown").dropdown("toggle");
      }
    } else {
      this.activeDropDown = tab;
      await this.localStorage.setDataInIndexedDB(
        LOCAL_STORAGE.DATA_ACTIVE_FUNCTION,
        TAB_FUNCTION.LIST
      );
      if (tab == SYSTEM_CONFIGURATION_TABLE.AUTHENTICATION_TABLE) {
        this.tablename = SYSTEM_CONFIGURATION_TABLE.AUTHENTICATION_TABLE;
      }
      this.userService.sendConfigTab({ tab: tab });
      if ($("#Dropdown").is(".show")) {
        $(function () {
          $("#customerDropDown").dropdown("toggle");
        });
      } else if ($("#Dropdown2").is(".show")) {
        $(function () {
          $("#customerManagementDropDown").dropdown("toggle");
        });
      }
    }
    // ------------------------------------------
    this.currentTab = tab;
  }

  getConfigurationTab(tab, event, currentTab) {
    
    if (event != "") {
      event.stopPropagation();
    }

    this.activeConfigurationTab = tab;
    this.activeTab = tab;
    this.currentTab = currentTab;
    if (tab == this.customerTab.CHECKOUT) {
      this.tablename = SYSTEM_CONFIGURATION_TABLE.CHECKOUT_TABLE;
    } else if (tab == this.customerTab.CONSENT) {
      this.tablename = SYSTEM_CONFIGURATION_TABLE.CONSENT_TABLE;
    } else if (tab == this.customerTab.CUSTOMER_DETAIL) {
      this.tablename = SYSTEM_CONFIGURATION_TABLE.CUSTOMER_TABLE;
    } else if (tab == this.customerTab.EKYC) {
      this.tablename = SYSTEM_CONFIGURATION_TABLE.KYC_TABLE;
    } else if (tab == this.customerTab.EXPRESSION) {
      this.tablename = SYSTEM_CONFIGURATION_TABLE.EXPRESSION_TABLE;
    } else if (tab == this.customerTab.INVENTORY) {
      this.tablename = SYSTEM_CONFIGURATION_TABLE.INVENTORY_TABLE;
    } else if (tab == this.customerTab.PAYMENT_MANAGEMENT) {
      this.tablename = SYSTEM_CONFIGURATION_TABLE.PAYMENT_TABLE;
    } else if (tab == this.customerTab.QUOTATION) {
      this.tablename = SYSTEM_CONFIGURATION_TABLE.QUOTATION_TABLE;
    } else if (tab == this.customerTab.SUBSCRIPTION) {
      this.tablename = SYSTEM_CONFIGURATION_TABLE.SUBSCRIPTION_TABLE;
    } else if (tab == this.systemConfigTab.AUTHENTICATION) {
      this.tablename = SYSTEM_CONFIGURATION_TABLE.AUTHENTICATION_TABLE;
    } else if (tab == this.customerManagementTab.B2B) {
      this.tablename = SYSTEM_CONFIGURATION_TABLE.Config_TABLE;
    } else if (tab == this.customerManagementTab.CUSTOMER_ACCESS) {
      this.tablename = SYSTEM_CONFIGURATION_TABLE.Config_TABLE;
    } else if (tab == this.customerManagementTab.BILLING_DETAILS) {
      this.tablename = SYSTEM_CONFIGURATION_TABLE.Config_TABLE;
    } else if (tab == this.customerManagementTab.CUSTOMER_DETAILS) {
      this.tablename = SYSTEM_CONFIGURATION_TABLE.Config_TABLE;
    } else if (tab == this.customerManagementTab.UPDATE_PROFILE) {
      this.tablename = SYSTEM_CONFIGURATION_TABLE.Config_TABLE;
    } else if (tab == this.customerManagementTab.VANITY_CATEGORIES) {
      this.tablename = SYSTEM_CONFIGURATION_TABLE.Config_TABLE;
    }
    
    if($("#Dropdown2").is(".show") == false && currentTab == this.systemConfigTab.CUSTOMER_MANAGEMENT){
      this.firstDropDown = tab;
      $('#customerManagementDropDown').dropdown('toggle');
    }
    if($("#Dropdown").is(".show") == false && currentTab == this.systemConfigTab.CUSTOMER){
      this.firstDropDown = tab;
      $('#customerDropDown').dropdown('toggle');
    }
    this.userService.sendConfigTab({ tab: tab });
  }

  async tabDropDown(tab, event) {
    event.stopPropagation();
    for (const iterator of this.systemMenu) {
      if(iterator.configGroupName == tab){
        iterator.show = iterator.show == true?false:true;
      }else{
        iterator.show = false;
      }
    }   
  }
}
