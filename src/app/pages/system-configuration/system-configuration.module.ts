import { ErrorHandler, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SystemConfigurationRoutingModule } from './system-configuration-routing.module';
import { SystemConfigurationComponent } from './system-configuration/system-configuration.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatTabsModule } from '@angular/material/tabs';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatNativeDateModule } from '@angular/material/core';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { SharedModule } from '@shared/shared.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrModule } from 'ngx-toastr';
import { NgxSpinnerModule } from 'ngx-spinner';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ModalsModule } from 'src/app/modals/modals.module';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { createTranslateLoader } from 'src/app/app.module';
import { ApiService } from '@services/api.service';
import { FormService } from '@services/form.service';
import { SystemCinfigurationsService } from '@services/system-cinfigurations.service';
import { GlobalErrorHandler } from '@services/globalErrorHandler';
import { SystemConfigurationListComponent } from './system-configuration-list/system-configuration-list.component';
import { CreateConfigurationComponent } from './create-configuration/create-configuration.component';
import { CapitalfirstLetterPipe } from '@pipes/capitalfirst-letter.pipe';
import { JsonToCsvService } from '@services/json-to-csv.service';
import { B2BTableComponent } from './b2-b-table/b2-b-table.component';
import { ExportComponent } from './export/export.component';
import { ImportComponent } from './import/import.component';
import { TooltipModule } from 'ngx-bootstrap/tooltip';


@NgModule({
  declarations: [
    SystemConfigurationComponent,
    SystemConfigurationListComponent,
    CreateConfigurationComponent,
    B2BTableComponent,
    ExportComponent,
    ImportComponent
  ],
  imports: [
    CommonModule,
    MatDatepickerModule,
    MatTabsModule,
    FormsModule,
    ReactiveFormsModule,
    MatNativeDateModule,
    NgxMaterialTimepickerModule,
    HttpClientModule,
    SharedModule,
    NgxPaginationModule,
    ToastrModule.forRoot(),
    NgxSpinnerModule,
    MatFormFieldModule,
    MatExpansionModule,
    ModalsModule,
    SystemConfigurationRoutingModule,
    TooltipModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient],
      },
    }),
  ],
  providers: [
    ApiService,
    FormService,
    TranslateService,
    CapitalfirstLetterPipe,
    JsonToCsvService,
    { provide: ErrorHandler, useClass: GlobalErrorHandler }
  ]
})
export class SystemConfigurationModule { }
