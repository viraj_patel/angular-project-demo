import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { Web_Language } from "@constant/constants";
import { TranslateService } from "@ngx-translate/core";
import { ApiService } from "@services/api.service";
import { SystemCinfigurationsService } from "@services/system-cinfigurations.service";
import { ExportComponent } from "./export/export.component";
import { ImportComponent } from "./import/import.component";
import { SystemConfigurationComponent } from "./system-configuration/system-configuration.component";

const routes: Routes = [
  {
    path: "",
    component: SystemConfigurationComponent,
  },
  {
    path:"export",
    component : ExportComponent
  },
  {
    path:"import",
    component : ImportComponent
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [ApiService],
})
export class SystemConfigurationRoutingModule {
  constructor(
    public translate: TranslateService,
    private ApiService: ApiService
  ) {}
}
