import { HttpParams } from '@angular/common/http';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FILTER_DATE_FORMAT, SYS_TABLE, SYS_TABLE_SEARCH, TABLE_DATA_TYPE, SYSTEM_CONFIGURATION_TABLE, SYSTEM_PREREQUISITES_DEMO, SYSTEM_CSV_DEMO, SYS_ROUTES, SYSTEM_CONFIGURATION_TABS_LIST, SYS_TAB_NAME, CUSTOMER_MANAGEMENT_TAB_LIST, SYS_TABLE_SORT } from '@constant/constants';
import { TranslateService } from '@ngx-translate/core';
import { SystemCinfigurationsService } from '@services/system-cinfigurations.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import * as moment from 'moment';
import { ConfirmationModalComponent } from 'src/app/modals/confirmation-modal/confirmation-modal.component';
import { UsersService } from '@services/users.service';
import { JsonToCsvService } from '@services/json-to-csv.service';
import { Subscription } from 'rxjs';
import { StaffService } from '@services/staff.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-system-configuration-list',
  templateUrl: './system-configuration-list.component.html',
  styleUrls: ['./system-configuration-list.component.scss']
})
export class SystemConfigurationListComponent implements OnInit, OnDestroy {
  @Input() tablename:string;
  @Input() activeConfigurationTab;
  @Input() parentTab;  
  
  createConsent:boolean=false;
  tabList=SYSTEM_CONFIGURATION_TABS_LIST;
  paramsObj:any = {
    page:1,
    limit:10,
    search:'',
    // status:'',
    searchBy:'',
    searchValue:'',
    sortBy:''
  };

  public tableData: any;
  public prerequisitiesData: any;
  mainTital:string="";
  activeTab = 0;
  uploadCsv:boolean=false;
  createConfig = false;
  sysList;
  editData:{}
  columnCount:number=8;
  totalItems: any;
  editconfig:boolean=false;
  sysTable = [];
  firstPage: number = 1;
  showNoData: boolean = false;
  sysTableFields:any = SYS_TABLE;
  sysTablSearchFields:any=SYS_TABLE_SEARCH;
  sysTablSort:any=SYS_TABLE_SORT;
  shortingFilters:boolean=true;
  dateForm: FormGroup;
  modalRef: BsModalRef;
  listTable:boolean=false;
  uploadUrl= SYS_ROUTES.BULK_UPLOAD;
  csvUploadParamObj = null;
  csvUploadType = "systemConfiguration";
  tablePageLimit = 10;
  editType;
  typeTable = 'Configurations'
  customerManagementTabList = CUSTOMER_MANAGEMENT_TAB_LIST
  edit:boolean=true;
  delete:boolean=true;
  export:boolean=true;
  import:boolean=true;
  create:boolean=true;
  groupName;
  subGroupName;
  otp:boolean=true;
  password=false;
  csvUploadTitles = {
    sampleCsv : "SYSTEM_SAMPLE_TITLE",
    preRequisites:"SYSTEM_CONFIG_PREREQUISITES_TITLE",
    csvFormat:"SYSTEM_CONFIG_CSV_TITLE"
  }
  obs: Subscription;

  constructor( private systemService: SystemCinfigurationsService,
    private modalService: BsModalService,
    private translate: TranslateService,
    private userService:UsersService,
    private jsonToCSVService:JsonToCsvService,
    private rolesService : StaffService,
    private spinner: NgxSpinnerService,
    private router:Router,
    private fb:FormBuilder) { 
      translate.setDefaultLang('en');
  }
  ngOnDestroy(){
    this.obs.unsubscribe();
  }
  showFilters:boolean=false;

  async ngOnInit() {

    // const data = {roleNames:["Admin"]}
    const url = this.router.url;
    const path = url;
    const route = `/${path.split('/')[2]}`;
    const currentRoute = route.split('?')[0].replace('/','');
    const permissions = await this.rolesService.getLoginPermissionsForModule(currentRoute);
    if(permissions.length > 0){
      this.create = permissions[0].Create;
      this.edit = permissions[0].Edit;
      this.delete = permissions[0].Delete;
      this.export = permissions[0].Export;
      this.import = permissions[0].Import;
    }
   
    this.obs =  this.userService.getConfigTab().subscribe(
      tab=>{
        
        this.mainTital = tab.tab;
        this.listTable=false;
      this.activeConfigurationTab = tab.tab;  
      this.groupName = tab.groupName;
      this.subGroupName = tab.subGroupName;
      
      if(this.activeConfigurationTab == 'B2B'){
        this.mainTital = tab.tab+" Customer";
      }
      if(this.activeConfigurationTab == 'Authentication'){
        this.otp=true;
        this.password=false;
        this.activeConfigurationTab='Otp';
        this.subGroupName = 'OTP Management'
        this.paramsObj.subGroup= this.subGroupName;
      }
      this.paramsObj = {
        page:1,
        limit:10,
        search:'',
        // status:'',
        searchBy:'',
        searchValue:'',
        sortBy:'',
        group: this.groupName,
        subGroup: this.subGroupName? this.subGroupName : '',
      };
      this.tablePageLimit = this.paramsObj.limit;
      if(tab.tab!='Customer Access'){
        this.listApiCall(this.paramsObj);
      }
      
      
      
    });
    if(this.activeConfigurationTab == 'Authentication'){
      this.activeConfigurationTab = 'Otp'
    }
    // this.listApiCall(this.paramsObj);
  }
  

  // remove unneccery data from object
  cleanObject(obj) {
    for (var propName in obj) {
      if (obj[propName] === '' || obj[propName] === undefined) {
        delete obj[propName];
      }
    }
    return obj
  }

  getLocationTab


  searchByForm(param){
    
    if(param.search != ''){
      this.paramsObj.search = param.search;
    }
    else{
      this.paramsObj.search = '';
    }

    this.paramsObj.page = 1;
    this.firstPage = 1;
    this.tablePageLimit = param.limit;
    this.listApiCall(this.paramsObj)
  }

  getConfigValue(type,itemType){
      if(type==itemType){
        return true;
      }
      return false;
  }

  async listApiCall(param) {

    this.paramsObj.sortBy = param.sortBy;
    this.paramsObj.page = param.page;
    this.paramsObj.limit = param.limit;
    this.firstPage = param.page;
    this.tablePageLimit = param.limit;

    if(this.activeConfigurationTab==SYSTEM_CONFIGURATION_TABS_LIST.OTHER_CONFIGURATION){
      this.tablename= this.activeConfigurationTab;
    }
    else{
      this.tablename = this.activeConfigurationTab;
    }
    this.firstPage = this.paramsObj.page;
    let params = new HttpParams();
    Object.keys(this.paramsObj).forEach((key) => {
      params = params.set(key, this.paramsObj[key]);
    });
    
      const response:any = await this.systemService.getSysConfig(params);
     
      setTimeout(() => {
        this.spinner.hide();

      },2000);
      
      
      for (const iterator of response.result.configurations) {
        var data=[];
        data[0]={value:iterator?.id,type:TABLE_DATA_TYPE.ID};
        data[1]={value:iterator?.configurableKeyDisplayText == null || iterator?.configurableKeyDisplayText == ''  ? '--' :iterator?.configurableKeyDisplayText ,type:TABLE_DATA_TYPE.TEXT};
        data[2]={value:iterator?.configurableValueDisplayText != null  || iterator?.configurableValueDisplayText != '' ? iterator?.configurableValueDisplayText :'--' ,type:TABLE_DATA_TYPE.TEXT};
        // data[3]={value:element?.ticketSCLevel1,type:TABLE_DATA_TYPE.TEXT};
        data[3]={
          value: JSON.stringify(iterator?.configurableValue),
          type:TABLE_DATA_TYPE.TEXT,
          dropDown:this.getConfigValue('singleSelectDropdown',iterator?.configurableDataType),
          textArea:this.getConfigValue('textarea',iterator?.configurableDataType),
          multiSelect:this.getConfigValue('multiSelectDropdown',iterator?.configurableDataType),
          radio:this.getConfigValue('boolean',iterator?.configurableDataType),
          json:this.getConfigValue('JSON',iterator?.configurableDataType),
          number:this.getConfigValue('number',iterator?.configurableDataType),
          string:this.getConfigValue('string',iterator?.configurableDataType) || iterator?.configurableDataType == "" ? true:false,
          array:this.getConfigValue('array',iterator?.configurableDataType)
        };
        data[4] = { value:iterator?.createdAt == null ? '--' :iterator?.createdAt , type: iterator?.createdAt == null ? TABLE_DATA_TYPE.TEXT : TABLE_DATA_TYPE.DATE };
        data[5] = { value:iterator?.updatedAt == null ? '--' :iterator?.updatedAt ,type: iterator?.updatedAt == null ? TABLE_DATA_TYPE.TEXT : TABLE_DATA_TYPE.DATE};
        // data[4]={value:iterator?.isActive == 1 ? true:false,type:TABLE_DATA_TYPE.BUTTON,color:iterator?.isActive == 1 ? "Green" : "red",status:true};
        if(data[3].json){
          iterator.edittype = 'json';

          if(typeof iterator?.configurableValue == 'string'){
            data[3].value=iterator?.configurableValue;
            iterator.editObj={
              [this.sysTableFields[3]]: JSON.parse(iterator?.configurableValue),
            } 
          }
          else{
            data[3].value = JSON.stringify(iterator?.configurableValue);
            iterator.editObj={
              [this.sysTableFields[3]]:iterator?.configurableValue,
            } 
          }

           
            // [this.sysTableFields[4]]:iterator?.isActive == 1 ? true:false;
        }

        if(data[3].radio){
          iterator.edittype = 'radio';
          data[3].value = iterator.configurableValue;
          iterator.editObj = {
            [this.sysTableFields[3]]: [(iterator.configurableValue =='yes' || iterator.configurableValue=='true' || iterator.configurableValue=='1') ? 'Yes' :'No' ,[Validators.required]]
          }
          data[3].value = (iterator.configurableValue =='yes' || iterator.configurableValue=='true' || iterator.configurableValue=='1') ? 'Yes' :'No';
        }

        if(data[3].multiSelect){
          iterator.edittype = 'multiSelect';
          iterator.configPossibleValues = JSON.parse(iterator.configPossibleValues)
          if(typeof iterator?.configurableValue == 'string'){
            iterator.editObj = {
              [this.sysTableFields[3]]: [JSON.parse(iterator?.configurableValue), [Validators.required]]
            }
            data[3].value = this.getValue(JSON.parse(iterator?.configurableValue))
          }
          else{
            iterator.editObj = {
              [this.sysTableFields[3]]: [iterator?.configurableValue, [Validators.required]]
            }
            data[3].value = this.getValue(iterator?.configurableValue)
          }
          
        }


        if(data[3].textArea){
          iterator.edittype = 'textArea';
          data[3].value = iterator.configurableValue;
          iterator.editObj = {
            [this.sysTableFields[3]]: [iterator?.configurableValue, [Validators.required]]
          }
        }

        if(data[3].dropDown){
          iterator.edittype = 'dropDown';
          iterator.configPossibleValues = JSON.parse(iterator.configPossibleValues)
          data[3].value = iterator.configurableValue;
          iterator.editObj = {
            [this.sysTableFields[3]]: [iterator?.configurableValue, [Validators.required]]
          }
        }

        if(data[3].number){
          
          iterator.edittype = 'number';
          data[3].value = iterator.configurableValue;
          data[3].edit=true;
          iterator.editObj = {
            [this.sysTableFields[3]]: [iterator?.configurableValue.match(/\d+/)[0], [Validators.required,Validators.pattern('^[0-9]*$')]]
          }
          
        }

        
        if(data[3].string){
          iterator.edittype = 'textArea';
            data[3].value = iterator.configurableValue;
            data[3].textArea = true;
            iterator.editObj = {
              [this.sysTableFields[3]]: [iterator?.configurableValue, [Validators.required,Validators.minLength(2),Validators.maxLength(256)]]
            }
        }

        if(data[3].array){
          data[3].class="text-data";
          iterator.edittype = 'array';
          iterator.editObj = {
            [this.sysTableFields[3]]:this.getArrayForm(iterator?.configurableValue)
          }
          data[3].value = this.getValue(iterator?.configurableValue)  
        }
        if(data[3].array == false && data[3].string == false && data[3].number == false && data[3].textArea == false && data[3].multiSelect == false && data[3].radio == false&& data[3].json == false && data[3].dropDown==false){
          iterator.edittype = 'textArea';
          data[3].value =JSON.stringify(iterator?.configurableValue);
          data[3].textArea = true;
          iterator.editObj = {
            [this.sysTableFields[3]]: [iterator?.configurableValue, [Validators.required,Validators.minLength(2),Validators.maxLength(256)]]
          }
        }

        iterator.data=data;
        iterator.edit = false;
        
      }
      
      this.totalItems = response.result.totalCount;
      this.sysTable = response.result.configurations;
      
      this.listTable = true;
    if (this.sysTable.length === 0 && this.paramsObj.searchBy == '' && this.paramsObj.searchValue == '' && this.paramsObj.fromDate == '' && this.paramsObj.toDate == '') {
      this.showNoData = true;
    }
  }

  
  async editInfo(data){
    if(data.row.configurableDataType=='JSON' || data.row.configurableDataType=='multiSelectDropdown'){
      data.row.configurableValue = JSON.stringify(data.value.configurableValue );
    }
    else if(data.row.configurableDataType=='boolean'){
      data.row.configurableValue = this.getBooleanValue(data.value.configurableValue, data.row.configurableValue)
    }
    else if(data.row.configurableDataType=='array'){

      if(this.isObject(data.row.configurableValue[0])){
        data.row.configurableValue = JSON.stringify(data.value.configurableValue);
      }else{
        const values = data.value.configurableValue.map(v => { return v.value })
        data.row.configurableValue = JSON.stringify(values);
      }

      
    }
    else{
      data.row.configurableValue = data.value.configurableValue;
    }
    const obj = {
      configurableKey: data.row.configurableKey,
      configurableKeyDisplayText: data.row.configurableKeyDisplayText,
      configurableValue:data.row.configurableValue ,
      configurableValueDisplayText: data.row.configurableValueDisplayText,
      configurableType: data.row.configurableType,
      isActive: data.row.isActive
    };
    await this.systemService.putSysConfig(obj);
    this.listApiCall(this.paramsObj);
  }

  getBooleanValue(editValue, configValue){
    if(configValue=='1' || configValue=='0'){
      return editValue=='Yes' ? '1':'0';
    }
    if(configValue=='true' || configValue=='false'){
      return editValue=='Yes' ? 'true':'false';
    }
    if(configValue=='Yes' || configValue=='no'){
      return editValue=='Yes'? 'yes' : 'no' ;
    }
  }


  async sortData(type, key) {
    this.paramsObj.sortBy = key+":"+type;
    this.paramsObj.page = 1;
    this.paramsObj.group = this.groupName,
    this.paramsObj.subGroup = this.subGroupName? this.subGroupName : '',
    this.firstPage = 1;
    await this.listApiCall(this.paramsObj); 

  }

  dateFilterSearch(data){
    this.paramsObj.fromDate = moment(data.start.value).format(FILTER_DATE_FORMAT.DATE_FORMAT);
    this.paramsObj.toDate = moment(data.end.value).format(FILTER_DATE_FORMAT.DATE_FORMAT);
    this.listApiCall(this.paramsObj);
  }

  searchFilter(){
    this.showFilters = !this.showFilters;
    
  }

  
  keySearch(param){
    if(param[this.sysTableFields[0]+"_text"]?.value != ''){
      this.paramsObj.searchBy  = this.sysTableFields[0];
      this.paramsObj.searchValue = param[this.sysTableFields[0]+"_text"]?.value;
    }
    else if(param[this.sysTableFields[1]+"_text"]?.value != ''){
      this.paramsObj.searchBy  = this.sysTableFields[1];
      this.paramsObj.searchValue = param[this.sysTableFields[1]+"_text"]?.value;
    }
    else if(param[this.sysTableFields[2]+"_text"]?.value != ''){
      this.paramsObj.searchBy  = this.sysTableFields[2];
      this.paramsObj.searchValue = param[this.sysTableFields[2]+"_text"]?.value;
    }
    else if(param[this.sysTableFields[3]+"_text"]?.value != ''){
      this.paramsObj.searchBy  = this.sysTableFields[3];
      this.paramsObj.searchValue = param[this.sysTableFields[3]+"_text"]?.value;
    }
    else{
      this.paramsObj.searchBy  = '';
      this.paramsObj.searchValue = '';
    }
    this.paramsObj.page = 1;
    this.firstPage = 1; 
    this.listApiCall(this.paramsObj);
  }
 
  
  async editLocation(data) {
  }

  closeForm(obj){
    this.createConfig= obj.status
  }


  async deleteLocation(data) { 
    
    const editConsent=true;
    const initialState = {
      title:this.translate.instant('MODAL.CONFIRM'),
      confirmText:this.translate.instant('MODAL.DELETE'),
    };
    this.modalRef = this.modalService.show(ConfirmationModalComponent, {
      class: 'modal-sm modal-dialog-centered custom-modal',
      backdrop: 'static',
      initialState,
    });
    this.modalRef.content.closeButtonText = this.translate.instant('MODAL.NO_BTN');
    this.modalRef.content.acceptButtonText = this.translate.instant('MODAL.YES_BTN');
    this.modalRef.content.response.subscribe(async (result) => {
      if (result) {
        let deleteParam={
          key:data.configurableKey
        }

        let param = new HttpParams();
        Object.keys(deleteParam).forEach((key) => {
          if(deleteParam[key]!=''){
            param = param.set(key, deleteParam[key]);
          }
        });

      
        await this.systemService.deleteSysConfig(param);
        if(this.sysTable.length === 1){
          this.paramsObj.page = this.paramsObj.page - 1;
        }
        await this.listApiCall(this.paramsObj); 

      } else {
        
      }
    });
  }

  getValue(data){
    if(data){
      if(typeof data =='string'){
        return data;
      }
      else if(Array.isArray(data)){
        if(this.isObject(data[0])){
          const values = data.map(v => { return v.value })
          return values.join(', \n' );
        }else{
          return data.join(', \n' );
        }
       
      }
    }
    return '';
  }

  getArrayForm(data){
    if(data){
      if(typeof data =='string'){
        return data;
      }
      else if(Array.isArray(data)){
        if(this.isObject(data[0])){
          const values = data.map(v => { 
            return this.fb.group({
              id:v.id,
              value:[v.value,[Validators.required,Validators.minLength(3),Validators.maxLength(256)]]
            })  });
          return this.fb.array(values);
        }else{
          let i = 0;
          const values = data.map(v => { 
            return this.fb.group({
              id:i=i+1,
              value:[v,[Validators.required,Validators.minLength(3),Validators.maxLength(256)]]
            })  });
          return this.fb.array(values);
        }
    
      }
    }
    return '';
  }

  isObject(objValue) {
    return objValue && typeof objValue === 'object' && objValue.constructor === Object;
  }

  showCsvUpload() {
      setTimeout(() => {
        this.uploadCsv = true;
      });
  }
  
  hideCsvUpload() {
      setTimeout(() => {
        this.uploadCsv = false;
      });
  }

 
  
 

  async downloadCSVSample(){
    const response:any = await this.systemService.downloadSampleCsv()
    let header = Object.keys(response.result.format[0]);
    let data = response.result.format;
    this.jsonToCSVService.downloadFile(header,data,this.csvUploadType+"_Sample_CSV")
  }

  password_table(){
    this.password=true;
    this.otp=false;
    this.activeConfigurationTab='Password';
    this.subGroupName = 'Password Management';
    this.paramsObj.subGroup= this.subGroupName;
    this.listApiCall(this.paramsObj);
  }
  otp_table(){
    this.otp=true;
    this.password=false;
    this.activeConfigurationTab='Otp';
    this.subGroupName = 'OTP Management';
    this.paramsObj.subGroup= this.subGroupName;
    this.listApiCall(this.paramsObj);
  }

}


