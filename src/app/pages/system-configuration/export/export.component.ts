import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { SystemCinfigurationsService } from '@services/system-cinfigurations.service';
import { UsersService } from '@services/users.service';
import { ToastrService } from 'ngx-toastr';
import { STATIC_DATA } from '@constant/constants';
import { JsonToCsvService } from '@services/json-to-csv.service';
import { JsonToXlsxService } from '@services/json-to-xlsx.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { Children } from 'preact/compat';
@Component({
  selector: 'app-export',
  templateUrl: './export.component.html',
  styleUrls: ['./export.component.scss']
})
export class ExportComponent implements OnInit {
  moduleIndexArray=[];
  newflag:boolean = false;
  export: boolean = true;
  activeTab;
  paramsObj: any = {};
  public bulkparam: FormGroup;
  submodules:any = [];
  modules = [];
  exportData:any=[];
  submoduelsArray=[];
  flag:boolean;
  successMsg:string=STATIC_DATA.EXPORT_SUCCESSFUL;
  errorMsg:string=STATIC_DATA.EXPORT_ERROR;
  systemMenu:any
  modalRef: BsModalRef;
  constructor(private userService: UsersService, private fb: FormBuilder ,private SystemCinfigurationsService : SystemCinfigurationsService,
    private downloadCSVService: JsonToCsvService,private jsonToXlsx : JsonToXlsxService,
    private spinner: NgxSpinnerService,
    private modalService: BsModalService,
    private toastr: ToastrService) {
    this.bulkparam = this.fb.group({
      modules: [""],
      submoduleName: this.fb.array([]),
    });
  }
  selectedModule = {}
  selectedSubMod = [];

  async ngOnInit() {
    const response:any = await this.SystemCinfigurationsService.getTabs()
    this.spinner.hide();

    this.systemMenu=response.result;
    for (const iterator of this.systemMenu) {
        iterator.child=[];
        
        
        for(const chldren of iterator.children){
          const item = {
           'name':chldren,
           'check':true
          };
          iterator.child.push(item);
          
        }
        
        
    }
    
    
  }
  submit(moduleName, submoduleName,event) {

    
    if(event.checked){
      if(this.selectedModule[moduleName]==undefined){
        this.selectedModule[moduleName]=[];
      }  
      this.selectedModule[moduleName].push(submoduleName)

    }
    else{
        this.selectedModule[moduleName] = this.selectedModule[moduleName].filter((item)=> item!==submoduleName);
     
        for (const iterator of this.systemMenu) {        
          if(iterator.configGroupName==moduleName){
            
            for(const chldren of iterator.child){
              
              if(chldren.name == submoduleName){
                chldren.check = false;
              }
    
            }
          }
          
     
        
        
    }
    }

    
  }
  async moduleChange(moduleName,event){

    if(event.checked){
      this.newflag=true;
      this.submodules = this.systemMenu.find(e=>e.configGroupName==moduleName)
      this.submoduelsArray.push(this.submodules)
      this.moduleIndexArray.push(moduleName);
        for (const iterator of this.systemMenu) {
            if(moduleName == iterator.configGroupName){
                iterator.check=true;
            }
        }
        this.selectedModule[moduleName]=[];
        if(this.submodules.children.length > 0){
            this.flag=true;
            for (const iterator of this.submodules.child) {
              iterator.check=true
                this.selectedModule[moduleName].push(iterator.name)
            }
            
            
        }
        else{
          this.flag=false;
         
          
        }
      
    }else{
      
    
          if(this.submoduelsArray.length>0){
            let i=0;
            for(const iterator of this.submoduelsArray ){
               i++;
              if(iterator.children.length > 0 ){
                if(iterator.configGroupName==moduleName){
                  await this.submoduelsArray.splice(i-1,1);
                }
                this.flag=true;

              }
              else{
                if(iterator.configGroupName==moduleName){
                  await this.submoduelsArray.splice(i-1,1);
                }
                  this.flag=false;
              }
            }

          }
          else{
            this.newflag=false
          }

          if(this.submoduelsArray.length==0){
            this.newflag=false
          }
          
     delete this.selectedModule[moduleName];
    }

  }
  async onSubmit(type,template: TemplateRef<any>,errorTemp:TemplateRef<any>) {

    
    var subModuleList:boolean=false;
    
    for(const iterator in this.selectedModule ){
      if(this.selectedModule[iterator].length==0){
        delete this.selectedModule[iterator];
      }      
    }
    
    for (const [key, value] of Object.entries(this.selectedModule)) {
      const valueArray:any = value;
        const submodules = this.systemMenu.find(e=>e.configGroupName==key);      
      
      if(valueArray.length > 0 || submodules.children.length == 0){
        subModuleList = true;
      }
    }
    if(subModuleList == false){
      this.toastr.error(STATIC_DATA.SUB_MODULE_ERROR_REQUIERD);
    }else{
        const obj={
            modules:this.selectedModule
        }
        const response:any = await this.SystemCinfigurationsService.exportData(obj);
        this.spinner.hide();

        this.exportData = response?.result;
        if(this.exportData.length == 0){
            this.modalRef = this.modalService.show(errorTemp,{ class: 'modal-sm modal-dialog-centered custom-modal' });
        }else{
            this.modalRef = this.modalService.show(template,{ class: 'modal-sm modal-dialog-centered custom-modal' });
        }
        
   
    if(type == 'csv'){
        this.downloadCSV();
    }else{
        this.exportAsXLSX();
    }
    
    }

  }

  exportAsXLSX() {
    this.jsonToXlsx.exportAsExcelFile(this.exportData, STATIC_DATA.SYSTEM_CONFIG+'Data');
  }

  async downloadCSV(){
    let header = Object.keys(this.exportData[0]); 
    this.jsonToXlsx.exportToCsv(this.exportData, STATIC_DATA.SYSTEM_CONFIG+'Data',header);
  }

}
