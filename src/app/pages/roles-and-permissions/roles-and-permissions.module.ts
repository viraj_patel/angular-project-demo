import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RolesAndPermissionsRoutingModule } from './roles-and-permissions-routing.module';
import { RolesAndPermissionsComponent } from './roles-and-permissions/roles-and-permissions.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatNativeDateModule } from '@angular/material/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { SharedModule } from '@shared/shared.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrModule } from 'ngx-toastr';
import { NgxSpinnerModule } from 'ngx-spinner';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatExpansionModule } from '@angular/material/expansion';
import { ModalsModule } from 'src/app/modals/modals.module';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { createTranslateLoader } from 'src/app/app.module';
import { MatTabsModule } from '@angular/material/tabs';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { CreateRolesComponent } from './create-roles/create-roles.component';
import { StaffManagementComponent } from './staff-management/staff-management.component';
import { MatTableModule } from '@angular/material/table' ;

@NgModule({
  declarations: [
    RolesAndPermissionsComponent,
    CreateRolesComponent,
    StaffManagementComponent
  ],
  imports: [
    CommonModule,
    RolesAndPermissionsRoutingModule,
    MatDatepickerModule,
    MatTabsModule,
    FormsModule,
    ReactiveFormsModule,
    MatNativeDateModule,
    NgxMaterialTimepickerModule,
    HttpClientModule,
    SharedModule,
    NgxPaginationModule,
    ToastrModule.forRoot(),
    NgxSpinnerModule,
    MatFormFieldModule,
    MatTableModule,
    MatExpansionModule,
    ModalsModule,
    
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient],
      },
    }),
  ],
})
export class RolesAndPermissionsModule { }
