import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StaffService } from '@services/staff.service';

@Component({
  selector: 'app-staff-management',
  templateUrl: './staff-management.component.html',
  styleUrls: ['./staff-management.component.scss']
})
export class StaffManagementComponent implements OnInit {
  staffFields=['Staff Name','Roles','View Permission'];
  staffRecords={};
  constructor( private staffUser: StaffService,private router:Router ) { }
  createRole:boolean;
  editRole:boolean;
  staff:boolean=true;
  viewrole:boolean=false;
  roleName:any;
  roleId:any;
  roleValue:any;
  listTable:boolean=false;
  view:boolean=false;
  async ngOnInit() {
  
    const permissions = await this.staffUser.getLoginPermissionsForModule('admin-access-management');
    if(permissions.length > 0){
      if(permissions[0].View == false){
        this.router.navigate(['/main-home/my-profile']);
      }else{
        this.view = true;
      }
    }else{
      this.view = true;
    }
    this.listTable = false;
    this.staffRecords = await this.staffUser.getStaffUser();
    this.listTable = true;
  }
  editRoles(data){
    this.editRole = true;
    this.createRole = true;
  }
  showCreateRoleForm(){
    this.createRole = true;
  }

  hideRoleForm(){
    this.roleId = '';
    this.roleName = ''
    this.viewrole = false
  }
  callview(item) {
    this.roleId=item.id;
    this.roleName=item.name;
    this.roleValue = item;
    this.viewrole=true;
  }
}
