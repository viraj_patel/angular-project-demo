import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ROLE_TABLE_FIELDS, ROLE_TABLE_NAME, TABLE_DATA_TYPE } from '@constant/constants';
import { TranslateService } from '@ngx-translate/core';
import { StaffService } from '@services/staff.service';

@Component({
  selector: 'app-roles-and-permissions',
  templateUrl: './roles-and-permissions.component.html',
  styleUrls: ['./roles-and-permissions.component.scss']
})
export class RolesAndPermissionsComponent implements OnInit {

  createRole =  false;
  editRoleId;
  editRoleName;
  paramsObj:any = {
    page:1,
    limit:10,
  };

  tablePageLimit = 10;
  firstPage=1;
  roleTableFields = ROLE_TABLE_FIELDS;
  tablename = "Roles";//ROLE_TABLE_NAME;
  roleTable;   
  columnCount = 4;
  showFilters = false;
  showNoData;
  totalItems;
  response;
  roleValue:any;
  editRole = false;
  staff=false;
  listTable:boolean=false;
  view:boolean=false;

  constructor(private translate: TranslateService,private rolesService : StaffService,private router:Router) {
    
  }





  async ngOnInit() {
    const roles = await this.rolesService.getLoginUserRoles();
    const loginRoles = await roles.map(v=>{return v.name});
    const index = loginRoles.indexOf('Super Admin');
    if (index > -1) { 
      this.view = true;
    }else{
      this.router.navigate(['/main-home/my-profile']);
    }
   
    this.listApiCall({});
  }
  editRoles(data){
    this.editRoleId = data.id;
    this.roleValue=data;
    this.editRoleName = data.name;
    this.editRole = true;
    this.createRole = true;
  }

  deleteRole(data){
  }

  async listApiCall(params){
    
    this.response = await this.rolesService.getRoles();
    this.roleTable = this.response.result;
    this.listTable = true;
    
    // this.listTable = true;
    if (this.roleTable.length === 0 ) {
      this.showNoData = true;
    }
    

  }


  showCreateRoleForm(){
    this.createRole = true;
  }

  hideRoleForm(){
    this.createRole = false
  }



}
