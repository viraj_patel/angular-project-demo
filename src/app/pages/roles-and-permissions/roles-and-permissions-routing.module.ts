import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RolesAndPermissionsComponent } from './roles-and-permissions/roles-and-permissions.component';
import { StaffManagementComponent } from './staff-management/staff-management.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'role',
    pathMatch: 'full'
  },
  {
    path: 'role', component: RolesAndPermissionsComponent,
  },
  {
    path:'staff',component:StaffManagementComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RolesAndPermissionsRoutingModule { }
