import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {MatTreeFlatDataSource, MatTreeFlattener} from '@angular/material/tree';
import {FlatTreeControl} from '@angular/cdk/tree';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ROLE_Titles } from '@constant/constants';
import { TranslateService } from '@ngx-translate/core';
import { StaffService } from '@services/staff.service';
import { HttpParams } from '@angular/common/http';
interface FoodNode {
  id:number;
  alias:string;
  parentId:any;
  actions?:any;
  name: string;
  children?: FoodNode[];
}


interface ExampleFlatNode {
  expandable: boolean;
  name: string;
  level: number;
}

@Component({
  selector: 'app-create-roles',
  templateUrl: './create-roles.component.html',
  styleUrls: ['./create-roles.component.scss']
})

export class CreateRolesComponent implements OnInit {
  
  TREE_DATA: FoodNode[];
  isChecked = true;
  roleForm;
  @Input() createRole = true;
  @Input() staff;
  @Input() editRole;
  @Input() roleId;
  @Input() roleName;
  @Input() roleValue;
 
  @Output() back = new EventEmitter<object>();
  errorMsg: any = {};
  toggledData:any=[];
  displayedColumns: string[]; 
  isSuperAdmin = true;
  permissionTable:boolean = false;
  norecord:boolean=false;
  actions;
  roleNotFind = false;

  async getPermissions(){
   
    const data = {roles:[this.roleValue]}
    let response:any= await this.rolesService.getPermissions(data);
    if(response.responseCode == 200){
      this.roleId = response.result.roleId;
      this.TREE_DATA = response.result.permissions;
      this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
      this.dataSource.data = this.TREE_DATA;
      if(this.toggledData.length > 0){
        for (const iterator of this.treeControl.dataNodes) {
          const index = this.toggledData.indexOf(iterator.name);
          if (index > -1) { 
            this.treeControl.toggle(iterator);
          }
        }
      }
      this.permissionTable = true;
    }else{
      this.norecord = true;
    }
   
    
  }
  
  private transformer = (node: FoodNode, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      name: node.name,
      level: level,
      id:node.id,
      actions:node.actions,
      childrens:node.children,
      key:node.alias
    };
  }

  treeControl = new FlatTreeControl<ExampleFlatNode>(
      node => node.level, node => node.expandable);

  treeFlattener = new MatTreeFlattener(
      this.transformer, node => node.level, 
      node => node.expandable, node => node.children);

  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  hasChild = (_: number, node: ExampleFlatNode) => node.expandable;

  constructor(
    private translateService:TranslateService,
    private rolesService : StaffService,
    private fb:FormBuilder
    ) {
    translateService.setDefaultLang('en');
    
  }

  async ngOnInit() {

    this.permissionTable = false;
    this.displayedColumns =  ['name'];
    let response:any = await this.rolesService.getActionsList();
    this.actions = response.result;

    for(let iterator of this.actions){
      this.displayedColumns.push(iterator.name.toLowerCase());
    }
    this.getPermissions(); 
  }


  async updateChildreanStatus(data,actionId){
    let obj = {
      roleId:this.roleId,
      actionId,
      permissionId:data.id,
      hasChildren: data.childrens?.length > 0 ? true : false
    }
    this.permissionTable = false;
    await this.rolesService.updatePermission(obj);
   
    setTimeout(async () => {
      this.getPermissions();
    });
    
  }

  changeToggle(data){
      const index = this.toggledData.indexOf(data.name);
      if (index == -1) { 
        this.toggledData.push(data.name);
      }else
      if (index > -1) { 
        this.toggledData.splice(index, 1); 
      }
    this.treeControl.toggle(data);
  }

  isActionChecked(data, actionId){
    if(data.actions == undefined){
      return false;
    }
    for(let iterator of data.actions){
      if(iterator.id == actionId){
        return true;
      }
    }
    return false;
  }


}
