import { ErrorHandler, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { ApiService } from '@services/api.service';
import { FormService } from '@services/form.service';
import { GlobalErrorHandler } from '@services/globalErrorHandler';
import { UsersService } from '@services/users.service';
import { LocalStorageService } from '@services/localService/localStorage.service';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatTabsModule } from '@angular/material/tabs';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatNativeDateModule } from '@angular/material/core';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { SharedModule } from '@shared/shared.module';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrModule } from 'ngx-toastr';
import { NgxSpinnerModule } from 'ngx-spinner';
import { MatExpansionModule } from '@angular/material/expansion';
import { ModalsModule } from 'src/app/modals/modals.module';
import { createTranslateLoader } from 'src/app/app.module';

@NgModule({
  declarations: [
    DashboardComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    TranslateModule,
    MatDatepickerModule,
    MatTabsModule,
    FormsModule,
    ReactiveFormsModule,
    MatNativeDateModule,
    NgxMaterialTimepickerModule,
    NgxMaterialTimepickerModule.setLocale('en-GB'),
    HttpClientModule,
    SharedModule,
    AppRoutingModule,
    NgxPaginationModule,
    ToastrModule.forRoot(),
    NgxSpinnerModule,
   
    MatExpansionModule,
    ModalsModule,
    
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient],
      },
    }),
  ],
  providers: [
    ApiService,
    FormService,
    UsersService,
    LocalStorageService,
    { provide: ErrorHandler, useClass: GlobalErrorHandler }
  ]
})
export class DashboardModule { }
