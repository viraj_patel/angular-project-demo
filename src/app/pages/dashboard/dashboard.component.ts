import { Component, OnInit, Output, EventEmitter} from '@angular/core';
import { Router } from '@angular/router';
import { LOCAL_STORAGE } from '@constant/constants';
import { ApiService } from '@services/api.service';
import { LocalStorageService } from '@services/localService/localStorage.service';
import { UsersService } from '@services/users.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  sectorCount = 0;
  repsCount = 0;
  adminCount = 0;
  memberCount = 0;
  totalUserCount = 0;
  courseCount = 0;
  eventCount = 0;
  userInfo:any;
  sendMessage(item?) {
  }
  @Output() profileData = new EventEmitter<object>();
  constructor(
    private router: Router,
    private usersService: UsersService,
    private apiService: ApiService,
    private localStorage: LocalStorageService
  ) {
    this.sendMessage();
  }

  async ngOnInit() {
    setTimeout(async() => {
      const user_info:any = await this.localStorage.getDataFromIndexedDB(LOCAL_STORAGE.USER_INFO);
        if(user_info != null){
          this.userInfo = JSON.parse(user_info);
        }
    },1000);
  }

  async getProfile(){
    const response:any = await this.usersService.getUserProfile();
    this.userInfo = response.user;
    this.apiService.changeData(this.userInfo);
  }
  redirect(val) {
    this.router.navigate([val]);
  }


}
