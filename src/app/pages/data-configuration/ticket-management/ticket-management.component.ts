import { HttpClient, HttpParams } from "@angular/common/http";
import { Component, OnChanges, OnDestroy, OnInit } from "@angular/core";
import { FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import {
  FILTER_DATE_FORMAT,
  TABLE_DATA_TYPE,
  TABLE_NAMES,
  TICKET_INFO,
  TICKET_TABLE_SEARCH,
  TICKET_TABS_LIST,
  TICKET_TABLE,
  TICKET_TABLE_FIELDS,
  TICKET_TABLE_HEADING,
  TAB_FUNCTION,
  LOCAL_STORAGE,
  DATA_CONFIGURATION_TABS_LIST,
  TICKET_TABS,
  TICKET_TABS_NAME,
  STATIC_DATA,
  TICKET_UPLOAD_CSV_TYPE,
  TICKET_ROUTES,
  TICKET_TABLE_NAMES,
  TICKET_CSV_HEADER,
  TICKET_TABLE_FIELDS_ARABIC,
  CONFIG_KEYS
} from "@constant/constants";
import { TranslateService } from "@ngx-translate/core";
import { JsonToCsvService } from "@services/json-to-csv.service";
import { JsonToXlsxService } from "@services/json-to-xlsx.service";
import { LocalStorageService } from "@services/localService/localStorage.service";
import { StaffService } from "@services/staff.service";
import { SystemCinfigurationsService } from "@services/system-cinfigurations.service";
import { TicketService } from "@services/ticket.service";
import { UsersService } from "@services/users.service";
import { param } from "jquery";
import * as moment from "moment";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { identity, Subscription } from "rxjs";
import { ConfirmationModalComponent } from "src/app/modals/confirmation-modal/confirmation-modal.component";
import { CsvFormatComponent } from "src/app/modals/csv-format/csv-format.component";
import { RecordViewComponent } from "src/app/modals/record-view/record-view.component";

@Component({
  selector: "app-ticket-management",
  templateUrl: "./ticket-management.component.html",
  styleUrls: ["./ticket-management.component.scss"],
})
export class TicketManagementComponent implements OnInit, OnDestroy  {
  createTicket: boolean = false;
  formFields;
  dataConfigTab=DATA_CONFIGURATION_TABS_LIST;
  activeTab = 0;
  activeTabName;
  showFilters: boolean = false;
  tabs = TICKET_TABS_LIST;
  csvType =TICKET_UPLOAD_CSV_TYPE;
  forminputs = {
    0: {
      ticketType: true,
      ticketTypeDropdown: false,
      category: false,
      categoryDropdown: false,
      subCategory: false,
    },
    1: {
      ticketType: false,
      ticketTypeDropdown: true,
      category: true,
      categoryDropdown: false,
      subCategory: false,
    },
    2: {
      ticketType: false,
      ticketTypeDropdown: false,
      category: false,
      categoryDropdown: true,
      subCategory: true,
    },
  };
  language:string='en';
  paramsObj: any = {};
  ticketList;
  columnCount: number = 8;
  tableName = TICKET_TABLE_NAMES.TICKET_TYPE;
  totalItems: any;
  ticketTable = [];
  firstPage: number = 1;
  showNoData: boolean = false;
  ticketTableFields: any = TICKET_TABLE_FIELDS.ticketType;
  ticketTablSearchFields: any = TICKET_TABLE_SEARCH;
  shortingFilters: boolean = false;
  dateForm: FormGroup;
  modalRef: BsModalRef;
  listTable: boolean = false;
  ipAddress: any;
  paramForSystemApi:any={};
  valueForTicketType: any;
  ticketTypeId: number;
  ticketForm:boolean=false;
  typeTable:string;
  pageHead;
  uploadUrl:string=TICKET_ROUTES.UPLOAD_TICKET;
  uploadCsvParamsObj = null;
  uploadCsv:boolean=false;
  downloadSampleList;
  downloadCsv:boolean=true;
  tableFormReset:boolean;
  otherLanguage:any;
  edit:boolean=false;
  delete:boolean=false;
  export:boolean=false;
  import:boolean=false;
  create:boolean=false;
  public tableData: any;
  public prerequisitiesData: any;
  csvUploadTitles = {
    sampleCsv : "TICKET_SAMPLE_TITLE",
    preRequisites:"TICKET_PREREQUISITES_FORMAT_TITLE",
    csvFormat:"TICKET_CSV_FORMAT_TITLE",
  }
  obs: Subscription;
  responseForTicketType: any;
  view:boolean=false;

  constructor(
    private translate: TranslateService,
    private ticketService: TicketService,
    private modalService: BsModalService,
    private http: HttpClient,
    private userService: UsersService,
    private systemService: SystemCinfigurationsService,
    private localStorage: LocalStorageService,
    private jsonToCsvService:JsonToCsvService,
    private router:Router,
    private jsonToXlsx : JsonToXlsxService,
    private rolesService : StaffService,

  ) {}

  async ngOnInit() {
    this.paramsObj = {
      page: 1,
      limit: 10,
      sortBy:'id:DESC'
    };
    this.callConfiguration();
    this.firstPage = this.paramsObj.page;
    this.activeTabName = TICKET_TABS_LIST.TYPE_MANAGEMENT;
    this.pageHead=TICKET_TABS.TYPE_MANAGEMENT;
    this.obs = this.userService.getTicketTab().subscribe(async (flag) => {
    this.tableFormReset = true;
      setTimeout( async () => {
        this.activeTab = 0;
        this.listTable = false;
        this.ticketForm = false;
        const url = this.router.url;
        const path = url;
        const route = `/${path.split('/')[3]}`;
        const currentRoute = route.split('?')[0].replace('/','');
        if (flag.tab === TICKET_TABS_LIST.TYPE_MANAGEMENT) {

          this.activeTab = 0;
          this.createTicket=false;
          this.typeTable = TICKET_TABS_NAME.TYPE_MANAGEMENT;
          this.activeTabName = flag.tab;
          this.ticketTableFields = TICKET_TABLE_HEADING.ticketType;
          this.pageHead=TICKET_TABS.TYPE_MANAGEMENT;
          this.tableName = TICKET_TABLE_NAMES.TICKET_TYPE;

          const permissions = await this.rolesService.getLoginPermissionsForModule('ticket-type');
          if(permissions.length > 0){
            if(permissions[0].View == false){
              this.router.navigate(['/main-home/my-profile']);
            }else{
              this.view = true;
            }
            this.create = permissions[0].Create;
            this.edit = permissions[0].Edit;
            this.delete = permissions[0].Delete;
            this.export = permissions[0].Export;
            this.import = permissions[0].Import;
          }else{
            this.view = true;
          }

          this.paramsObj = {
            page: 1,
            limit: 10,
            sortBy:'id:DESC'

          };
          this.firstPage = this.paramsObj.page;
          setTimeout(() => {
            this.ticketForm = true;
          });
         
          this.activeFuction();
          this.listApiCall(this.paramsObj);

          // this.getTiketType();
        }
        if (flag.tab === TICKET_TABS_LIST.CATEGORY_MANAGEMENT) {
          this.activeTab = 1;
          this.activeTabName = flag.tab;
          this.typeTable = TICKET_TABS_NAME.CATEGORY_MANAGEMENT;
          this.ticketTableFields = TICKET_TABLE_HEADING.category;
          this.pageHead=TICKET_TABS.CATEGORY_MANAGEMENT;
          this.tableName = TICKET_TABLE_NAMES.TICKET_CATEGORY;
          this.paramsObj = {
            page: 1,
            limit: 10,
            sortBy:'id:DESC'

          };
          const permissions = await this.rolesService.getLoginPermissionsForModule('category');
          if(permissions.length > 0){
            if(permissions[0].View == false){
              this.router.navigate(['/main-home/my-profile']);
            }else{
              this.view = true;
            }
            this.create = permissions[0].Create;
            this.edit = permissions[0].Edit;
            this.delete = permissions[0].Delete;
            this.export = permissions[0].Export;
            this.import = permissions[0].Import;
          }else{
            this.view = true;
          }

          this.firstPage = this.paramsObj.page;
          setTimeout(() => {
            this.ticketForm = true;
          });
          this.language = 'en';
          this.activeFuction();
          this.listApiCall(this.paramsObj);
          this.uploadCsvParamsObj = {
            type:this.csvType.CATEGORY_MANAGEMENT
          }
          this.downloadSampleList = await this.downloadSampleFunction()
          this.table_data();
          this.setPrerequisitiesData();
        }
        if (flag.tab === TICKET_TABS_LIST.SUB_CATEGORY_MANAGEMENT) {
          this.activeTab = 2;
          this.typeTable = TICKET_TABS_NAME.SUB_CATEGORY_MANAGEMENT;
          this.ticketTableFields = TICKET_TABLE_HEADING.subCategoy;
          this.activeTabName = flag.tab;
          this.tableName = TICKET_TABLE_NAMES.TICKET_SUB_CATEGORY;
          this.pageHead=TICKET_TABS.SUB_CATEGORY_MANAGEMENT;
          this.paramsObj = {
            page: 1,
            limit: 10,
            sortBy:'id:DESC'

          };
          
          const permissions = await this.rolesService.getLoginPermissionsForModule('sub-category');
          if(permissions.length > 0){
            if(permissions[0].View == false){
              this.router.navigate(['/main-home/my-profile']);
            }else{
              this.view = true;
            }
            this.create = permissions[0].Create;
            this.edit = permissions[0].Edit;
            this.delete = permissions[0].Delete;
            this.export = permissions[0].Export;
            this.import = permissions[0].Import;
          }else{
            this.view = true;
          }

          this.firstPage = this.paramsObj.page;
          setTimeout(() => {
            this.ticketForm = true;
          });
          this.language = 'en';
          this.activeFuction();
          this.listApiCall(this.paramsObj);
          this.uploadCsvParamsObj = {
            type:this.csvType.SUB_CATEGORY_MANAGEMENT
          }
          this.downloadSampleList = await this.downloadSampleFunction()
          this.table_data();
          this.setPrerequisitiesData();
        }
        this.uploadCsv = false;
        this.tableFormReset = false;
      });
    });
    
    // this.listApiCall(this.paramsObj);
  }
  ngOnDestroy() {
    this.obs.unsubscribe();
  }

  async activeFuction(){
    const active_tab_function: any = await this.localStorage.getDataFromIndexedDB(LOCAL_STORAGE.DATA_ACTIVE_FUNCTION);
    const url = this.router.url;
    const path = url;
    const route = `/${path.split('/')[3]}`;
    const currentRoute = route.split('?')[0].replace('/','');
    if(active_tab_function == TAB_FUNCTION.CREATE && currentRoute == this.dataConfigTab.TICKET){
      this.listTable = false;
      this.createTicket = true;
      await this.localStorage.setDataInIndexedDB(
        LOCAL_STORAGE.DATA_ACTIVE_FUNCTION,
        TAB_FUNCTION.CREATE
      );
    }else if(active_tab_function == TAB_FUNCTION.UPLOAD && currentRoute == this.dataConfigTab.TICKET){
    }else if(active_tab_function == TAB_FUNCTION.LIST && currentRoute == this.dataConfigTab.TICKET){
      if(this.activeTab != 0){
        this.listTable = true;
        this.createTicket = false;
      }else{
        this.createTicket = false;
      }
      
      await this.localStorage.setDataInIndexedDB(
        LOCAL_STORAGE.DATA_ACTIVE_FUNCTION,
        TAB_FUNCTION.LIST
      );
    }
  }

  // search from fromdata
  sendSearchData(item) {
    
    this.paramsObj.searchBy = "title";
    this.paramsObj.searchValue = item.title;
    if (item.categoryId != null) {
      this.paramsObj.categoryId = item.categoryId;
    }else{
      this.paramsObj.categoryId = '';
    }
    if (item.ticketId != null) {
      this.paramsObj.typeId   = item.ticketId;
    }else{
      this.paramsObj.typeId   = '';
    }
    if (item.isActive == 100) {
      delete this.paramsObj.status;
    } else {
      this.paramsObj.status = item.isActive;
    }
    this.paramsObj.page = 1;
    this.firstPage = this.paramsObj.page;
    this.listApiCall(this.paramsObj);
  }

  hideShowCreateForm() {
    this.ticketForm = false;
    setTimeout(async () => {
      this.listTable = false;
      this.createTicket = !this.createTicket;
      this.paramsObj.page = 1;
      this.firstPage = this.paramsObj.page;
      this.paramsObj.searchBy = "title";
      this.paramsObj.searchValue = "";
      delete this.paramsObj.status;
      this.ticketForm = true;
      
      if(this.createTicket == true){
        await this.localStorage.setDataInIndexedDB(
          LOCAL_STORAGE.DATA_ACTIVE_FUNCTION,
          TAB_FUNCTION.CREATE
        );
      }else{
        this.listApiCall(this.paramsObj);
        await this.localStorage.setDataInIndexedDB(
          LOCAL_STORAGE.DATA_ACTIVE_FUNCTION,
          TAB_FUNCTION.LIST
        );
      }
    });
  }

  async callConfiguration(){
    const keyParam = {fields:CONFIG_KEYS.LANGUAGE_KEY}
    let param = new HttpParams();
    Object.keys(keyParam).forEach((key) => {
      if (keyParam[key] != "") {
        param = param.set(
          key,
          keyParam[key]
        );
      }
    });
    const res: any = await this.ticketService.getConfig(param);
      if(res?.result){
        this.otherLanguage = res?.result[0];
      }
             
  }

  async listApiCall(paramsObj) {
    
    this.paramsObj.page = paramsObj.page;
    this.firstPage = this.paramsObj.page;
    this.paramsObj.limit = paramsObj.limit;
   

    
    let params = new HttpParams();
    Object.keys(this.paramsObj).forEach((key) => {
      if (this.paramsObj[key] != "") {
        params = params.set(key, this.paramsObj[key]);
      } else if (paramsObj[key] == 0) {
        params = params.set(key, this.paramsObj[key]);
      }
    });
    
    const response: any = await this.ticketService.getTickets(
      params,
      this.activeTabName
    );


    if (this.activeTabName == TICKET_TABS_LIST.TYPE_MANAGEMENT) {
      let ticketListResult = response.result?.types;
      for (const iterator of ticketListResult) {
        var data = [];
        data[0] = { value: iterator?.id, type: TABLE_DATA_TYPE.ID };
        data[1] = {
          value:  this.language == 'en'?iterator?.title:iterator[this.otherLanguage?.configurableValue?.title],
          type: TABLE_DATA_TYPE.TEXT,
          edit:true
        };
        data[2]={
          value:iterator?.createdAt,
          type:TABLE_DATA_TYPE.DATE
        }
        data[3]={
          value:iterator?.updatedAt,
          type:TABLE_DATA_TYPE.DATE  
        }
        data[4] = {
          value: iterator?.isActive,
          type: TABLE_DATA_TYPE.BUTTON,
          color: iterator?.isActive == 1 ? STATIC_DATA.GREEN : STATIC_DATA.RED,
          status: true,
        };

        iterator.editObj = {
          [this.ticketTableFields[1]]: [this.language == 'en'?iterator?.title:iterator[this.otherLanguage?.configurableValue?.title],[Validators.required,Validators.minLength(2),Validators.maxLength(256)]],
          [this.ticketTableFields[4]]: iterator?.isActive == 1 ? true : false,
        };
        iterator.data = data;
        iterator.edit = false;
      }
      this.totalItems = response.result?.totalCount;
      
      this.ticketTable = ticketListResult;
      this.listTable = true;
      
    }else{
      if(this.language == this.otherLanguage?.configurableValue?.abbreviation){
        const index = this.ticketTableFields.indexOf(this.activeTabName.charAt(0).toUpperCase() + this.activeTabName.slice(1) +'(EN)');
         if (index == -1) { 
          this.ticketTableFields.splice(3, 0, this.activeTabName.charAt(0).toUpperCase() + this.activeTabName.slice(1) +'(EN)');
         }
       }else{
         const index = this.ticketTableFields.indexOf(this.activeTabName.charAt(0).toUpperCase() + this.activeTabName.slice(1) +'(EN)');
         if (index > -1) { 
           this.ticketTableFields.splice(index, 1); 
         }
       }
       if(this.language=='en'){
        if (this.activeTabName == TICKET_TABS_LIST.CATEGORY_MANAGEMENT) {
          let ticketListResult = response.result?.categories;
    
          for (const iterator of ticketListResult) {
                    
            var data = [];
                data[0] = { value: iterator?.id, type: TABLE_DATA_TYPE.ID };
                data[1] = {
                  value: iterator?.ticketType == null ? " -- " : iterator?.ticketType.title  ,
                  type: TABLE_DATA_TYPE.TEXT,
                };
                
                data[2]={
                  value: iterator?.title,
                  type: TABLE_DATA_TYPE.TEXT,
                  edit:true
                }
                data[3]={
                  value:iterator?.createdAt,
                  type:TABLE_DATA_TYPE.DATE
                }
                data[4]={
                  value:iterator?.updatedAt,
                  type:TABLE_DATA_TYPE.DATE  
                }
    
                data[5] = {
                  value: iterator?.isActive,
                  type: TABLE_DATA_TYPE.BUTTON,
                  color: iterator?.isActive == 1 ? STATIC_DATA.GREEN : STATIC_DATA.RED,
                  status: true,
                };
                iterator.editObj = {
                  [this.ticketTableFields[2]]: [this.language == 'en'?iterator?.title:iterator[this.otherLanguage?.configurableValue?.title],[Validators.required,Validators.minLength(3),Validators.maxLength(256)]],
                  [this.ticketTableFields[5]]: iterator?.isActive == 1 ? true : false,
                };
                iterator.data = data;
                iterator.edit = false;
          }
          this.totalItems = response.result?.totalCount;
          this.ticketTable = ticketListResult;
    
          this.listTable = true;
        } 
        else if (this.activeTabName == TICKET_TABS_LIST.SUB_CATEGORY_MANAGEMENT) {
          let ticketListResult = response.result?.subCategories;
    
          for (const iterator of ticketListResult) {
            var data = [];
            data[0] = { value: iterator?.id, type: TABLE_DATA_TYPE.ID };
           
            data[1] = {
              value:this.language == 'en'?iterator?.ticketCategory.title:iterator?.ticketCategory[this.otherLanguage?.configurableValue?.title],
              type: TABLE_DATA_TYPE.TEXT,
            };
            data[2] = {
              value: this.language == 'en'?iterator?.title:iterator[this.otherLanguage?.configurableValue?.title],
              type: TABLE_DATA_TYPE.TEXT,
              edit: true,
            };
            
            data[3]={
              value:iterator?.createdAt,
              type:TABLE_DATA_TYPE.DATE
            }
            data[4]={
              value:iterator?.updatedAt,
              type:TABLE_DATA_TYPE.DATE  
            }
            data[5] = {
              value: iterator?.isActive,
              type: TABLE_DATA_TYPE.BUTTON,
              color: iterator?.isActive == 1 ? STATIC_DATA.GREEN : STATIC_DATA.RED,
              status: true,
            };
            iterator.editObj = {
              [this.ticketTableFields[2]]: [this.language == 'en'?iterator?.title:iterator[this.otherLanguage?.configurableValue?.title],[Validators.required,Validators.minLength(3),Validators.maxLength(256)]],
              [this.ticketTableFields[5]]: iterator?.isActive == 1 ? true : false,
            };
            iterator.data = data;
            iterator.edit = false;
          }
    
          this.totalItems = response.result?.totalCount;
          this.ticketTable = ticketListResult;
    
          this.listTable = true;
        }
      }
      else{
        if (this.activeTabName == TICKET_TABS_LIST.CATEGORY_MANAGEMENT) {
          let ticketListResult = response.result?.categories;
    
          for (const iterator of ticketListResult) {
                    
            var data = [];
              data[0] = { value: iterator?.id, type: TABLE_DATA_TYPE.ID };
              data[1] = {
                value: iterator?.ticketType == null ? " -- " : iterator?.ticketType.title  ,
                type: TABLE_DATA_TYPE.TEXT,
              };
              data[2] = {
                value: iterator[this.otherLanguage?.configurableValue?.title],
                type: TABLE_DATA_TYPE.TEXT,
                edit: true,
              };
              data[3]={
                value: iterator?.title,
                type: TABLE_DATA_TYPE.TEXT,
                
              }
              data[4]={
                value:iterator?.createdAt,
                type:TABLE_DATA_TYPE.DATE
              }
              data[5]={
                value:iterator?.updatedAt,
                type:TABLE_DATA_TYPE.DATE  
              }
  
              data[6] = {
                value: iterator?.isActive,
                type: TABLE_DATA_TYPE.BUTTON,
                color: iterator?.isActive == 1 ? STATIC_DATA.GREEN : STATIC_DATA.RED,
                status: true,
              };
              iterator.editObj = {
                [this.ticketTableFields[2]]: [this.language == 'en'?iterator?.title:iterator[this.otherLanguage?.configurableValue?.title],[Validators.required,Validators.minLength(3),Validators.maxLength(256)]],
                [this.ticketTableFields[6]]: iterator?.isActive == 1 ? true : false,
              };
              iterator.data = data;
              iterator.edit = false;
                 }
          this.totalItems = response.result?.totalCount;
          this.ticketTable = ticketListResult;
    
          this.listTable = true;
        } 
        else if (this.activeTabName == TICKET_TABS_LIST.SUB_CATEGORY_MANAGEMENT) {
          let ticketListResult = response.result?.subCategories;
    
          for (const iterator of ticketListResult) {
            var data = [];
            data[0] = { value: iterator?.id, type: TABLE_DATA_TYPE.ID };
            // data[1] = {
            //   value: iterator?.ticketType.title,
            //   type: TABLE_DATA_TYPE.TEXT,
            // };
            data[1] = {
              value:this.language == 'en'?iterator?.ticketCategory.title:iterator?.ticketCategory[this.otherLanguage?.configurableValue?.title],
              type: TABLE_DATA_TYPE.TEXT,
            };
            data[2] = {
              value: this.language == 'en'?iterator?.title:iterator[this.otherLanguage?.configurableValue?.title],
              type: TABLE_DATA_TYPE.TEXT,
              edit: true,
            };
            data[3] = {
              value: iterator?.title,
              type: TABLE_DATA_TYPE.TEXT,
            };
            data[4]={
              value:iterator?.createdAt,
              type:TABLE_DATA_TYPE.DATE
            }
            data[5]={
              value:iterator?.updatedAt,
              type:TABLE_DATA_TYPE.DATE  
            }
            data[6] = {
              value: iterator?.isActive,
              type: TABLE_DATA_TYPE.BUTTON,
              color: iterator?.isActive == 1 ? STATIC_DATA.GREEN : STATIC_DATA.RED,
              status: true,
            };
            iterator.editObj = {
              [this.ticketTableFields[2]]: [this.language == 'en'?iterator?.title:iterator[this.otherLanguage?.configurableValue?.title],[Validators.required,Validators.minLength(3),Validators.maxLength(256)]],
              [this.ticketTableFields[6]]: iterator?.isActive == 1 ? true : false,
            };
            iterator.data = data;
            iterator.edit = false;
          }
    
          this.totalItems = response.result?.totalCount;
          this.ticketTable = ticketListResult;
    
          this.listTable = true;
        }
      }
    }
    

   

    if (
      this.ticketTable.length === 0 &&
      this.paramsObj.searchBy == "" &&
      this.paramsObj.searchValue == "" &&
      this.paramsObj.fromDate == "" &&
      this.paramsObj.toDate == ""
    ) {
      this.showNoData = true;
    }
  }

  editData(item) {}

  async editInfo(data) {
    
    let data1 = {
      id: data.row.id,
      title:"",
      isActive: this.activeTabName == this.tabs.CATEGORY_MANAGEMENT || this.activeTabName == this.tabs.TYPE_MANAGEMENT ?data.value.Status == true ? 1 : 0:data.value.isActive == true ? 1 : 0,
    };
    data1[this.otherLanguage?.configurableValue?.title] ="";
    
    if(this.language == 'en'){
      data1.title = this.activeTabName == this.tabs.CATEGORY_MANAGEMENT
      ? data.value[this.ticketTableFields[2]]
      : data.value[this.ticketTableFields[2]];
      delete data1[this.otherLanguage?.configurableValue?.title];
      if(data1.title == data.row.title){
        delete data1.title;
      }
    }else{
      data1[this.otherLanguage?.configurableValue?.title] = this.activeTabName == this.tabs.CATEGORY_MANAGEMENT
      ? data.value[this.ticketTableFields[2]]
      : data.value[this.ticketTableFields[2]];
      delete data1.title;
      if(data1[this.otherLanguage?.configurableValue?.title] == data.row[this.otherLanguage?.configurableValue?.title]){
        delete data1[this.otherLanguage?.configurableValue?.title];
      }
    }
    
    if(this.activeTabName==this.tabs.TYPE_MANAGEMENT){
      if(data.value.Type == data.row.title){
         delete data1.title;
      }
      else{
        data1.title=data.value.Type;
      }
    }
    if(data.value.Status == data.row.isActive){
      delete data1.isActive;
    }
    if(data1.isActive == 0 || data1.isActive == 1 || data1.title || data1[this.otherLanguage?.configurableValue?.title]){
      await this.ticketService.editTicket(data1, this.activeTabName);
    }

    this.listApiCall(this.paramsObj);
  }
  
  async deleteLocation(event) { 
    let title = this.activeTab == 0 ?TICKET_TABS_NAME.TYPE_MANAGEMENT:this.activeTab == 1 ?TICKET_TABS_NAME.CATEGORY_MANAGEMENT:TICKET_TABLE[2];
    const initialState = {
      title: STATIC_DATA.DELETE+title,
      confirmText: STATIC_DATA.DELETE_MSG + title.toLowerCase() + "?",
    };
    this.modalRef = this.modalService.show(ConfirmationModalComponent, {
      class: "modal-sm modal-dialog-centered custom-modal",
      backdrop: "static",
      initialState,
    });
    this.modalRef.content.closeButtonText =
      this.translate.instant("MODAL.NO_BTN");
    this.modalRef.content.acceptButtonText =
      this.translate.instant("MODAL.YES_BTN");
    this.modalRef.content.response.subscribe(async (result) => {
      if (result) {
        let param = {
          id: event.id,
        };
        await this.ticketService.deleteTicket(param, this.activeTabName);
        if (this.ticketTable.length === 1) {
          this.paramsObj.page = this.paramsObj.page - 1;
          this.firstPage = this.paramsObj.page;
        }
        await this.listApiCall(this.paramsObj);
      } else {
      }
    });
  }

  sortData(type, key) {
    this.paramsObj.sortBy = key + ":" + type;
    this.paramsObj.page = 1;
    this.firstPage = 1;
    this.listApiCall(this.paramsObj);
  }

  dateFilterSearch(data) {
    this.paramsObj.fromDate =
      data.start.value != ""
        ? moment(data.start.value).format(FILTER_DATE_FORMAT.DATE_FORMAT)
        : "";
    this.paramsObj.toDate =
      data.end.value != ""
        ? moment(data.end.value).format(FILTER_DATE_FORMAT.DATE_FORMAT)
        : "";
    this.listApiCall(this.paramsObj);
  }

  searchFilter() {
    this.showFilters = !this.showFilters;
  }

  keySearch(param) {
    if (param[this.ticketTableFields[0] + "_text"]?.value != "") {
      this.paramsObj.searchBy = this.ticketTableFields[0];
      this.paramsObj.searchValue =
        param[this.ticketTableFields[0] + "_text"]?.value;
    } else if (param[this.ticketTableFields[1] + "_text"]?.value != "") {
      this.paramsObj.searchBy = this.ticketTableFields[1];
      this.paramsObj.searchValue =
        param[this.ticketTableFields[1] + "_text"]?.value;
    } else if (param[this.ticketTableFields[2] + "_text"]?.value != "") {
      this.paramsObj.searchBy = this.ticketTableFields[2];
      this.paramsObj.searchValue =
        param[this.ticketTableFields[2] + "_text"]?.value;
    } else if (param[this.ticketTableFields[3] + "_text"]?.value != "") {
      this.paramsObj.searchBy = this.ticketTableFields[3];
      this.paramsObj.searchValue =
        param[this.ticketTableFields[3] + "_text"]?.value;
    } else {
      this.paramsObj.searchBy = "";
      this.paramsObj.searchValue = "";
    }
    this.listApiCall(this.paramsObj);
  }
  cleanObject(obj) {
    for (var propName in obj) {
      if (obj[propName] === "" || obj[propName] === undefined) {
        delete obj[propName];
      }
    }
    return obj;
  }
  generateQueryParams() {
    this.paramsObj = this.cleanObject(this.paramsObj);
    let params = new HttpParams();
    Object.keys(this.paramsObj).forEach((key) => {
      if (this.paramsObj[key] != "") {
        params = params.set(key, this.paramsObj[key]);
      }
    });
    return params;
  }
  async viewInfo(data) {
    const recordData = [
      { key: TICKET_INFO.TICKETNO, value: data.ticketNo },
      { key: TICKET_INFO.SUBJECT, value: data.subject },
      { key: TICKET_INFO.CATEGORY, value: data.category },
      { key: TICKET_INFO.TICKETSCLEVEL1, value: data.ticketSCLevel1 },
      { key: TICKET_INFO.STATUS, value: data.createdTime },
      { key: TICKET_INFO.CREATION_DATE, value: data.modifiedTime },
    ];

    const initialState = {
      title: "View Deatile",
      redordList: recordData,
    };
    this.modalRef = this.modalService.show(RecordViewComponent, {
      class: "modal-dialog-centered",
      backdrop: "static",
      initialState,
    });
    this.modalRef.content.response.subscribe(async (result) => {
      if (result) {
      } else {
      }
    });
  }

  viewCSVDemo() {
    const initialState = {
      title: "CSV Format",
    };
    this.modalRef = this.modalService.show(CsvFormatComponent, {
      class: "modal-dialog-centered modal-lg",
      backdrop: "static",
      initialState,
    });
    this.modalRef.content.response.subscribe(async (result) => {
      if (result) {
      } else {
      }
    });
  }

  exportAsXLSX(data, name):void {
    this.jsonToXlsx.exportAsExcelFile(data, name);
  }

  async downloadCSVSample(){
    let params = new HttpParams();
    Object.keys(this.uploadCsvParamsObj).forEach((key) => {
      if(this.uploadCsvParamsObj[key]!=''){
        params = params.set(key, this.uploadCsvParamsObj[key]);
      }
    });
    const response:any = await this.ticketService.getCSVTickets(this.activeTabName,params);
    let header = Object.keys(response.result.format[0]);
    header = this.addToLast(header, 'isActive')
    let data = response.result.format;
    this.jsonToCsvService.downloadFile(header,data,this.typeTable+"_Sample_CSV")
    // this.exportAsXLSX(data, this.typeTable+"_Sample_Excel" )
  }

  addToLast(header, n) {
    const newArray = [];

    for ( let i = 0; i < header.length; i++) {
        if(header[i] !== n) {
            newArray.push(header[i]);
        }
    }
    newArray.push(n)
    return newArray;
  }



  async showCsvUpload() {
    this.ticketForm = false;
      setTimeout(() => {
        this.uploadCsv = true;
      });
      await this.localStorage.setDataInIndexedDB(
        LOCAL_STORAGE.DATA_ACTIVE_FUNCTION,
        TAB_FUNCTION.UPLOAD
      );
  }

  async hideCsvUpload() {
    setTimeout(() => {
      this.uploadCsv = false;
      this.paramsObj.page = 1;
      this.firstPage = 1;
      this.listApiCall(this.paramsObj);
      this.ticketForm = true;
    });
    await this.localStorage.setDataInIndexedDB(
      LOCAL_STORAGE.DATA_ACTIVE_FUNCTION,
      TAB_FUNCTION.LIST
    );
  }

  async downloadSampleFunction(){
    let params = new HttpParams();
    Object.keys(this.uploadCsvParamsObj).forEach((key) => {
      if(this.uploadCsvParamsObj[key]!=''){
        params = params.set(key, this.uploadCsvParamsObj[key]);
      }
    });
    return await this.ticketService.getCSVTickets(this.activeTabName,params);
  }

  async table_data() { 
    this.tableData = { header: [], data: [] };
    const response:any = this.downloadSampleList;
    this.tableData.header = Object.keys(response.result.format[0])
    let recordsList = [...response.result.format]
    let data = [];
    recordsList.map((item)=>{
      
      let row = [];
      for(let key of Object.keys(item)){
        let obj= {
          value:item[key],
          type: TABLE_DATA_TYPE.TEXT,
          class1:""
        };
        row.push(obj);
      }
      data.push({row})
    })

    this.tableData.data = data;
  }

  setPrerequisitiesData(){
    this.prerequisitiesData = { header: [], data: [] };
    const response:any = this.downloadSampleList;
    this.prerequisitiesData.header = Object.keys(response.result.prerequisites[0]);
    let preRecordsList = [...response.result.prerequisites];
    let perData = [];
    preRecordsList.map((item)=>{
      let row = [];
      for(let key of Object.keys(item)){
        let obj= {
          value:item[key],
          type: key =="required" ?  TABLE_DATA_TYPE.ICON : TABLE_DATA_TYPE.TEXT,
          class1:key=="required" ? (item.required ? "fa solid fa-check" : "fa solid fa-xmark") : ""
        };
        row.push(obj);
      }
      perData.push({row})
    })
    
    this.prerequisitiesData.data = perData;
  }

  async getTiketType(){
    this.paramForSystemApi = {
    };
    let paramForSystemApi1 = new HttpParams();
    Object.keys(this.paramForSystemApi).forEach((key) => {
      if (this.paramForSystemApi[key] != "") {
        paramForSystemApi1 = paramForSystemApi1.set(
          key,
          this.paramForSystemApi[key]
        );
      }
    });
    this.responseForTicketType = await this.ticketService.getTickets(
      paramForSystemApi1,'ticketType'
    );
    this.valueForTicketType = this.responseForTicketType?.result?.types.title;
      
    this.ticketTypeId =
      this.responseForTicketType?.result?.types[0]?.id;
  }
  languageSelect(event){
    if(this.activeTabName=='subCategory'){
      this.language = event;
      if(this.language == this.otherLanguage?.configurableValue?.abbreviation){
        const index = this.ticketTableFields.indexOf('Sub-Category(EN)');
        if (index == -1) { 
          this.ticketTableFields.splice(3, 0, 'Sub-Category(EN)');
        }
       
      }else{
        const index = this.ticketTableFields.indexOf('Sub-Category(EN)');
        if (index > -1) { 
          this.ticketTableFields.splice(index, 1); 
        }
      }
    }
    else{
      this.language = event;
      if(this.language == this.otherLanguage?.configurableValue?.abbreviation){
        const index = this.ticketTableFields.indexOf(this.activeTabName.charAt(0).toUpperCase() + this.activeTabName.slice(1) +'(EN)');
        if (index == -1) { 
          this.ticketTableFields.splice(3, 0, this.activeTabName.charAt(0).toUpperCase() + this.activeTabName.slice(1) +'(EN)');
        }
       
      }else{
        const index = this.ticketTableFields.indexOf(this.activeTabName.charAt(0).toUpperCase() + this.activeTabName.slice(1) +'(EN)');
        if (index > -1) { 
          this.ticketTableFields.splice(index, 1); 
        }
      }
    }
 
    const ticketListResult =  this.ticketTable;
    this.listTable = false;
    if(this.language=='en'){
      setTimeout(() => {
        if (this.activeTabName == TICKET_TABS_LIST.CATEGORY_MANAGEMENT) {
          for (const iterator of ticketListResult) {
            var data = [];
            data[0] = { value: iterator?.id, type: TABLE_DATA_TYPE.ID };
            data[1] = {
              value: iterator?.ticketType == null ? " -- " : iterator?.ticketType.title  ,
              type: TABLE_DATA_TYPE.TEXT,
            };
            
            data[2]={
              value: iterator?.title,
              type: TABLE_DATA_TYPE.TEXT,
              edit: true,
            }
            data[3]={
              value:iterator?.createdAt,
              type:TABLE_DATA_TYPE.DATE
            }
            data[4]={
              value:iterator?.updatedAt,
              type:TABLE_DATA_TYPE.DATE  
            }

            data[5] = {
              value: iterator?.isActive,
              type: TABLE_DATA_TYPE.BUTTON,
              color: iterator?.isActive == 1 ? STATIC_DATA.GREEN : STATIC_DATA.RED,
              status: true,
            };
            iterator.editObj = {
              [this.ticketTableFields[2]]: [this.language == 'en'?iterator?.title:iterator[this.otherLanguage?.configurableValue?.title],[Validators.required,Validators.minLength(3),Validators.maxLength(256)]],
              [this.ticketTableFields[5]]: iterator?.isActive == 1 ? true : false,
            };
            iterator.data = data;
            iterator.edit = false;
          }
         
          this.ticketTable = ticketListResult;
    
          this.listTable = true;
        } else if (this.activeTabName == TICKET_TABS_LIST.SUB_CATEGORY_MANAGEMENT) {
          for (const iterator of ticketListResult) {
            var data = [];
            data[0] = { value: iterator?.id, type: TABLE_DATA_TYPE.ID };
           
            data[1] = {
              value:this.language == 'en'?iterator?.ticketCategory.title:iterator?.ticketCategory[this.otherLanguage?.configurableValue?.title],
              type: TABLE_DATA_TYPE.TEXT,
            };
            data[2] = {
              value: this.language == 'en'?iterator?.title:iterator[this.otherLanguage?.configurableValue?.title],
              type: TABLE_DATA_TYPE.TEXT,
              edit: true,
            };
            
            data[3]={
              value:iterator?.createdAt,
              type:TABLE_DATA_TYPE.DATE
            }
            data[4]={
              value:iterator?.updatedAt,
              type:TABLE_DATA_TYPE.DATE  
            }
            data[5] = {
              value: iterator?.isActive,
              type: TABLE_DATA_TYPE.BUTTON,
              color: iterator?.isActive == 1 ? STATIC_DATA.GREEN : STATIC_DATA.RED,
              status: true,
            };
            iterator.editObj = {
              [this.ticketTableFields[2]]: [this.language == 'en'?iterator?.title:iterator[this.otherLanguage?.configurableValue?.title],[Validators.required,Validators.minLength(3),Validators.maxLength(256)]],
              [this.ticketTableFields[5]]: iterator?.isActive == 1 ? true : false,
            };
            iterator.data = data;
            iterator.edit = false;
          }
    
         
          this.ticketTable = ticketListResult;
    
          this.listTable = true;
        }
        });
      }
    else{
      setTimeout(() => {
        if (this.activeTabName == TICKET_TABS_LIST.CATEGORY_MANAGEMENT) {
          for (const iterator of ticketListResult) {
            var data = [];
            data[0] = { value: iterator?.id, type: TABLE_DATA_TYPE.ID };
            data[1] = {
              value: iterator?.ticketType == null ? " -- " : iterator?.ticketType.title  ,
              type: TABLE_DATA_TYPE.TEXT,
            };
            data[2] = {
              value: iterator[this.otherLanguage?.configurableValue?.title],
              type: TABLE_DATA_TYPE.TEXT,
              edit: true,
            };
            data[3]={
              value: iterator?.title,
              type: TABLE_DATA_TYPE.TEXT,
              
            }
            data[4]={
              value:iterator?.createdAt,
              type:TABLE_DATA_TYPE.DATE
            }
            data[5]={
              value:iterator?.updatedAt,
              type:TABLE_DATA_TYPE.DATE  
            }

            data[6] = {
              value: iterator?.isActive,
              type: TABLE_DATA_TYPE.BUTTON,
              color: iterator?.isActive == 1 ? STATIC_DATA.GREEN : STATIC_DATA.RED,
              status: true,
            };
            iterator.editObj = {
              [this.ticketTableFields[2]]: [this.language == 'en'?iterator?.title:iterator[this.otherLanguage?.configurableValue?.title],[Validators.required,Validators.minLength(3),Validators.maxLength(256)]],
              [this.ticketTableFields[6]]: iterator?.isActive == 1 ? true : false,
            };
            iterator.data = data;
            iterator.edit = false;
          }
         
          this.ticketTable = ticketListResult;
    
          this.listTable = true;
        } else if (this.activeTabName == TICKET_TABS_LIST.SUB_CATEGORY_MANAGEMENT) {
          for (const iterator of ticketListResult) {
            var data = [];
            data[0] = { value: iterator?.id, type: TABLE_DATA_TYPE.ID };
            // data[1] = {
            //   value: iterator?.ticketType.title,
            //   type: TABLE_DATA_TYPE.TEXT,
            // };
            data[1] = {
              value:this.language == 'en'?iterator?.ticketCategory.title : iterator?.ticketCategory[this.otherLanguage?.configurableValue]==undefined ? '--' :iterator?.ticketCategory[this.otherLanguage?.configurableValue] ,
              type: TABLE_DATA_TYPE.TEXT,
            };
            data[2] = {
              value: this.language == 'en'?iterator?.title:iterator[this.otherLanguage?.configurableValue?.title],
              type: TABLE_DATA_TYPE.TEXT,
              edit: true,
            };
            data[3] = {
              value: iterator?.title,
              type: TABLE_DATA_TYPE.TEXT,
            };
            data[4]={
              value:iterator?.createdAt,
              type:TABLE_DATA_TYPE.DATE
            }
            data[5]={
              value:iterator?.updatedAt,
              type:TABLE_DATA_TYPE.DATE  
            }
            data[6] = {
              value: iterator?.isActive,
              type: TABLE_DATA_TYPE.BUTTON,
              color: iterator?.isActive == 1 ? STATIC_DATA.GREEN : STATIC_DATA.RED,
              status: true,
            };
            iterator.editObj = {
              [this.ticketTableFields[2]]: [this.language == 'en'?iterator?.title:iterator[this.otherLanguage?.configurableValue?.title],[Validators.required,Validators.minLength(3),Validators.maxLength(256)]],
              [this.ticketTableFields[6]]: iterator?.isActive == 1 ? true : false,
            };
            iterator.data = data;
            iterator.edit = false;
          }
    
         
          this.ticketTable = ticketListResult;
    
          this.listTable = true;
        }
        });
    }
  }


  async ticketDownloadCSV(event){
    let header = TICKET_CSV_HEADER[this.activeTabName]

    let params = new HttpParams();
    Object.keys(this.paramsObj).forEach((key) => {
      if (this.paramsObj[key] != "" && key != "page" && key != "limit") {
        params = params.set(key, this.paramsObj[key]);
      } else if (this.paramsObj[key] == 0 && key != "page" && key != "limit") {
        params = params.set(key, this.paramsObj[key]);
      }
    });
    
    const response: any = await this.ticketService.getTickets(
      params,
      this.activeTabName
    );
     const csvData = this.activeTabName == TICKET_TABS_LIST.TYPE_MANAGEMENT?response.result.types:  this.activeTabName == TICKET_TABS_LIST.CATEGORY_MANAGEMENT ? response.result.categories:response.result.subCategories;

    let data;
    if(this.activeTabName == TICKET_TABS_LIST.TYPE_MANAGEMENT){
      data = csvData.map(v => ({
        TicketType:v.title,
        Status:v.isActive==1?'Active':'InActive'
      }));
    }
    const otherNameCategoryKey = "OtherLanguageCategory ("+this.otherLanguage.configurableValue.title.charAt(0).toUpperCase() + this.otherLanguage.configurableValue.title.slice(1)+")";
    const otherNameSubCategoryKey = "OtherLanguageSubCategory ("+this.otherLanguage.configurableValue.title.charAt(0).toUpperCase() + this.otherLanguage.configurableValue.title.slice(1)+")";
    if(this.activeTabName == TICKET_TABS_LIST.CATEGORY_MANAGEMENT){
      data = csvData.map(v => (
        {
        Category:v.title,
        [otherNameCategoryKey]:v[this.otherLanguage?.configurableValue?.title],
        TicketType:v.ticketType == null ? '-':v.ticketType.title,
        Status:v.isActive==1?'Active':'InActive'
      }
      ));
    }

    if(this.activeTabName == TICKET_TABS_LIST.SUB_CATEGORY_MANAGEMENT){
      data = csvData.map(v => ({
        SubCategory:v.title,
        [otherNameSubCategoryKey]:v[this.otherLanguage?.configurableValue?.title],
        Category:v.ticketCategory.title,
        [otherNameCategoryKey]:v.ticketCategory[this.otherLanguage?.configurableValue?.title],
        Status:v.isActive==1?'Active':'InActive'
      }));
    }
    
    this.jsonToCsvService.downloadFile(header,data, this.activeTabName+'Data',true);
  }

}
