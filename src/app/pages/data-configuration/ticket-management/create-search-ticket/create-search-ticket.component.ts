import { HttpParams } from "@angular/common/http";
import {
  Component,
  Input,
  OnInit,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
} from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import {
  LOCATION_CSV_HEADER,
  TICKET_TABS,
  TICKET_TABS_LIST,
} from "@constant/constants";
import { TranslateService } from "@ngx-translate/core";
import { FormService } from "@services/form.service";
import { JsonToCsvService } from "@services/json-to-csv.service";
import { SystemCinfigurationsService } from "@services/system-cinfigurations.service";
import { TicketService } from "@services/ticket.service";
import { UsersService } from "@services/users.service";

@Component({
  selector: "app-create-search-ticket",
  templateUrl: "./create-search-ticket.component.html",
  styleUrls: ["./create-search-ticket.component.scss"],
})
export class CreateSearchTicketComponent implements OnInit {
  @Input() createTicket;
  @Input() activeTabName;
  @Input() tableFilds;
  @Input() formFields;
  @Input() forminputs: any;
  @Input() valueForTicketType: any;
  @Input() responseForTicketType: any;
  @Input() otherLanguage:any;
  showNoData: boolean = false;
  @Output() back = new EventEmitter<object>();
  @Output() languageSelect = new EventEmitter<object>();
  @Output() searchData = new EventEmitter<object>();
  @Output() callApi = new EventEmitter<object>();
  firstPage: number = 1;
  paramsObj: any = {};
  public ticketForm: FormGroup;
  errorMsg: any = {};

  ticketList: any = [];
  ticketDropDownList1: any = [];
  ticketDropDownList2: any = [];
  // dropdown values
  ticketData: any;
  category;
  categoryData;
  response;

  isChecked = true;
  formData;
  Name = "";
  data = {
    title: "",
    ticketTypeId: "",
    categoryId: "",
    ticketId:"",
    isActive: null,
    ticketCategoryId: "",
  };
  language:string='en';
  ticketTypevalue;
  searchList: boolean = false;
  constructor(
    private fb: FormBuilder,
    public translation: TranslateService,
    private formService: FormService,
    private ticketService: TicketService,
    private sysConfig: SystemCinfigurationsService,
    private downloadCSVService: JsonToCsvService,
    private userService: UsersService
  ) {
    translation.setDefaultLang("en");
    this.ticketForm = this.fb.group({
      ticketType: [""],
      ticketTypeDropdown: [""],
      category: [""],
      categoryDropdown: [""],
      subCategory: [""],
      status: ["100"],
      isChecked: ["true"],
      language:['en']
    });
  }

  async ngOnInit() {
    this.formData = this.ticketForm.value;
    this.Name = this.activeTabName; //to access form values

    this.data = {
      title: this.formData[this.Name],
      ticketTypeId: this.formData.ticketTypeDropdown,
      categoryId: this.formData.categoryDropdown,
      ticketCategoryId: this.formData.categoryDropdown,
      ticketId:this.formData.ticketId,
      isActive: this.createTicket
        ? this.formData.isChecked
          ? 1
          : 0
        : parseInt(this.formData.status),
    };
    this.data[this.otherLanguage?.configurableValue?.title]=this.formData[this.Name];
    
    if(this.activeTabName == TICKET_TABS_LIST.CATEGORY_MANAGEMENT){
      let paramData={sortBy:'id:DESC',status:'1'}
      let params = new HttpParams();
    Object.keys(paramData).forEach((key) => {
      if (paramData[key] != "") {
        params = params.set(key, paramData[key]); 
      }
    });
      this.response = await this.ticketService.getTickets(params, 'ticketType');
      this.ticketData = this.response.result.types;
    }
    
    if(this.activeTabName == TICKET_TABS_LIST.SUB_CATEGORY_MANAGEMENT){
      let paramData={sortBy:'id:DESC'}
      let params = new HttpParams();
    Object.keys(paramData).forEach((key) => {
      if (paramData[key] != "") {
        params = params.set(key, paramData[key]);
      }
    });
      this.response = await this.ticketService.getTickets(params, "category");
      this.categoryData = this.response.result.categories;
      
    }
   
    if(this.createTicket == true){
      
      this.validating();

      this.intializingMessage();
    }
  }

  async pageChange(params) {
    let bodyData = {
      search: this.data.title,
      status: this.data.isActive,
    };
    this.paramsObj.page = params.page;
    this.paramsObj.limit = params.limit;
    params = this.generateQueryParams();
    const response: any = await this.ticketService.getTickets(
      bodyData,
      this.activeTabName
    );
    this.ticketList = response.result.map((v) => ({
      ...v,
      edit: false,
      status: v.isActive == 1 ? true : false,
      oldEn: v.name.en,
      oldAr: v.name[this.otherLanguage?.configurableValue?.abbreviation],
    }));
  }

  async getTickets(body) {
   
    let params = this.generateQueryParams();

    const response: any = await this.ticketService.getTickets(
      params,
      this.activeTabName
    );

    this.ticketList = response.result.types.map((v) => ({
      ...v,
      edit: false,
      status: v.isActive == 1 ? true : false,
    }));
    return this.ticketList;
  }

  async getDropDownTicket(body, type) {
    let params = this.generateQueryParams();

    const response: any = await this.ticketService.getTickets(params, type);

    if ((type = "subCategory")) {
      this.ticketDropDownList2 = response.result.categories;
    }
  }

  async addNewTickets(data) {
    let params = this.generateQueryParams();
    let response = await this.ticketService.postTicket(
      data,
      this.activeTabName
    );
    setTimeout(async () => {
      this.ticketForm.patchValue({
        ["category"]: "",
        ["categoryDropdown"]: "",
        ["subCategory"]: "",
        ["isChecked"]: "true",
        ["status"]: "100",
      });
      if (this.activeTabName == TICKET_TABS_LIST.SUB_CATEGORY_MANAGEMENT) {
        this.response = await this.ticketService.getTickets(
          "",
          this.activeTabName
        );
        this.categoryData = this.response.result.categories;
      }
    });
    
    

    this.back.emit();
  }

  func() {
    setTimeout(() => {
      console.log(this.isChecked);
    }, 10);
  }

  validating() {
    if (this.forminputs.ticketType) {
      this.ticketForm.controls["ticketType"].setValidators([
        Validators.required,
        Validators.pattern("^[a-zA-Z ]*$"),
        Validators.minLength(2),
        Validators.maxLength(256),
      ]);
      this.ticketForm.controls["ticketType"].updateValueAndValidity();
    }

    if (this.forminputs.ticketTypeDropdown) {
      this.ticketForm.controls["ticketTypeDropdown"].setValidators([
        Validators.required,
      ]);
      this.ticketForm.controls["ticketTypeDropdown"].updateValueAndValidity();
    }

    if (this.forminputs.category) {
      this.ticketForm.controls["category"].setValidators([
        Validators.required,
        // Validators.pattern("^[a-zA-Z ]*$"),
        Validators.minLength(3),
        Validators.maxLength(256),
      ]);
      this.ticketForm.controls["category"].updateValueAndValidity();
    }

    if (this.forminputs.categoryDropdown) {
      this.ticketForm.controls["categoryDropdown"].setValidators([
        Validators.required,
      ]);
      this.ticketForm.controls["categoryDropdown"].updateValueAndValidity();
    }

    if (this.forminputs.subCategory) {
      this.ticketForm.controls["subCategory"].setValidators([
        Validators.required,
        // Validators.pattern("^[a-zA-Z ]*$"),
        Validators.minLength(3),
        Validators.maxLength(256),
      ]);
      this.ticketForm.controls["subCategory"].updateValueAndValidity();
    }
  }

  

  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || "").trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { whitespace: true };
  }
  intializingMessage() {
    this.errorMsg.ticketType = {
      pattern: this.translation.instant('ERR_MSG_CHARACTER_ONLY'),
    };
    this.errorMsg.ticketTypeDropdown = {
      required: this.translation.instant("ERR_MSG_REQUIERD_NAME"),
    };
    this.errorMsg.category = {
      pattern: this.translation.instant('ERR_MSG_CHARACTER_ONLY'),
    };
    this.errorMsg.categoryDropdown = {
      required: this.translation.instant("ERR_MSG_REQUIERD_NAME"),
    };
    this.errorMsg.subCategory = {
      pattern: this.translation.instant('ERR_MSG_CHARACTER_ONLY'),
    };
  }

  async onSubmit(showTable) {
    this.formData = this.ticketForm.value;
    this.ticketForm.patchValue({
      [this.activeTabName]:this.formData[this.activeTabName].trim()
    });
    this.formService.markFormGroupTouched(this.ticketForm);
    if (this.ticketForm.valid) {
      this.data = {
        title: this.formData[this.activeTabName].trim(),
        ticketTypeId: this.formData.ticketTypeDropdown,
        categoryId: this.formData.categoryDropdown,
        ticketCategoryId: this.formData.categoryDropdown,
        ticketId:this.formData.ticketTypeDropdown,
        isActive: this.createTicket
          ? this.formData.isChecked
            ? 1
            : 0
          : parseInt(this.formData.status),
      };
      this.data[this.otherLanguage?.configurableValue?.title]=this.formData[this.activeTabName].trim();
      // when click on "Search Button"
      if (showTable) {
        
          this.searchList = true;
          this.searchData.emit(this.data);
        
        
      }
      // when click on "SAVE Button"
      else {
        // when tickettype onsubit :

        this.paramsObj.id = "";
        this.paramsObj.page = "";
        this.paramsObj.limit = "";
        this.data = this.cleanObject(this.data);
        this.formService.markFormGroupTouched(this.ticketForm);
        if (this.ticketForm.valid) {
          this.addNewTickets(this.data);
        }
      }
    }
  }

  // on select any coutry,category,district,city dropdown
  async onSelectDropdown(field, type) {
    this.paramsObj.id = this.ticketForm.controls[field].value;
    (this.paramsObj.page = ""), (this.paramsObj.limit = "");

    const bodyData = {
      searchBy: "",
      status: "",
    };

    await this.getDropDownTicket(bodyData, type);
    if (type == "subCategories") {
      
      this.category = this.ticketDropDownList2;
    }
    if (type == "subCategories") {
      
      this.category = this.ticketDropDownList2;
    }
  }

  // remove unneccery data from object
  cleanObject(obj) {
    for (var propName in obj) {
      if (obj[propName] === "" || obj[propName] === undefined) {
        delete obj[propName];
      }
    }
    return obj;
  }

  // generates query params
  generateQueryParams() {
    this.paramsObj = this.cleanObject(this.paramsObj);
    let params = new HttpParams();
    Object.keys(this.paramsObj).forEach((key) => {
      if (this.paramsObj[key] != "") {
        params = params.set(key, this.paramsObj[key]);
      }
    });
    return params;
  }

  resetForm() {
    this.ticketForm.patchValue({
      ["ticketType"]: '',
      ["ticketTypeDropdown"]:'',
      ["category"]: "",
      ["categoryDropdown"]: "",
      ["subCategory"]: "",
      ["isChecked"]: "true",
    });

    this.ticketForm.patchValue({
      status: "100",
    });
    this.showNoData = false;
    this.category = null;
    if(this.searchList == true){
      this.searchData.emit({title:'',categoryId:null,isActive:100,});
      this.searchList = false;
    }
  }

  @Input()
  public set getValueTicketType(TicketTypeValue: any) {
    if (TicketTypeValue != undefined) {
      this.ticketForm.patchValue({ ["ticketType"]: TicketTypeValue });
      this.ticketTypevalue = TicketTypeValue;
    }
  }

  @Input()
  public set getValueTicketTypeID(TicketTypeID: any) {
    if (TicketTypeID != undefined) {
      this.ticketForm.patchValue({ ["ticketTypeDropdown"]: TicketTypeID });
    }
  }

  downloadCSV() {
    let header = LOCATION_CSV_HEADER[this.activeTabName];

    let data;
    if (this.activeTabName == TICKET_TABS[0]) {
      data = this.ticketList.map((v) => ({
        ticketTypeId: v.id,
        ticketType: v.name.en,
        ArabicCountryName: v.name[this.otherLanguage?.configurableValue?.abbreviation],
        Status: v.isActive == 1 ? "Active" : "Deactive",
      }));
    }

    if (this.activeTabName == TICKET_TABS[1]) {
      data = this.ticketList.map((v) => ({
        categoryId: v.id,
        category: v.name.en,
        ArabicStateName: v.name[this.otherLanguage?.configurableValue?.abbreviation],
        ticketTypeId: v.ticketTypeId,
        ticketType: v.ticketType.name,
        ArabicCountryName: v.ticketType[this.otherLanguage?.configurableValue?.title],
        Status: v.isActive == 1 ? "Active" : "Deactive",
      }));
    }

    if (this.activeTabName == TICKET_TABS[2]) {
      data = this.ticketList.map((v) => ({
        subCategoryId: v.id,
        subCategory: v.code,
        categoryId: v.categoryId,
        category: v.category.name,
        ArabicStateName: v.category[this.otherLanguage?.configurableValue?.title],
        ticketTypeId: v.ticketTypeId,
        ticketType: v.ticketType.name,
        ArabicCountryName: v.ticketType[this.otherLanguage?.configurableValue?.title],
        Status: v.isActive == 1 ? "Active" : "Deactive",
      }));
    }

    this.downloadCSVService.downloadFile(
      header,
      data,
      this.activeTabName + "Data"
    );
  }

  backToTable() {
    this.back.emit();
  }
  checkInput(event){
    if(event.code === "Space" && this.ticketForm.value.category.trim().length === 0){
      setTimeout(() => {
        this.ticketForm.patchValue({["category"]: "" });
      });
    }else if(event.type === "paste"){
      setTimeout(() => {
        if(this.ticketForm.value.category.trim().length === 0){
          this.ticketForm.patchValue({["category"]: "" });
        }
      });
    } 
    
    if(event.code === "Space" && this.ticketForm.value.ticketType.trim().length === 0){
      setTimeout(() => {
        this.ticketForm.patchValue({["ticketType"]: "" });
      });
      }else if(event.type === "paste"){
        setTimeout(() => {
          if(this.ticketForm.value.ticketType.trim().length === 0){
            this.ticketForm.patchValue({["ticketType"]: "" });
          }
        });
      } 
      
    if(event.code === "Space" && this.ticketForm.value.subCategory.trim().length === 0){
        setTimeout(() => {
          this.ticketForm.patchValue({["subCategory"]: "" });
        });
      }else if(event.type === "paste"){
        setTimeout(() => {
          if(this.ticketForm.value.subCategory.trim().length === 0){
            this.ticketForm.patchValue({["subCategory"]: "" });
          }
        });
      } 
  }

  changeLanguage(type){
    this.ticketForm.patchValue({["language"]: type });
    this.languageSelect.emit(type);
  }
}
