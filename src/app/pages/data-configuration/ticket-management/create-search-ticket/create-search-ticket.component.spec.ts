import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateSearchTicketComponent } from './create-search-ticket.component';

describe('CreateSearchTicketComponent', () => {
  let component: CreateSearchTicketComponent;
  let fixture: ComponentFixture<CreateSearchTicketComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateSearchTicketComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateSearchTicketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
