import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchReasonComponent } from './search-reason.component';

describe('SearchReasonComponent', () => {
  let component: SearchReasonComponent;
  let fixture: ComponentFixture<SearchReasonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchReasonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchReasonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
