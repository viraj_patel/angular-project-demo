import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-search-reason',
  templateUrl: './search-reason.component.html',
  styleUrls: ['./search-reason.component.scss']
})
export class SearchReasonComponent implements OnInit {
  public reasonForm: FormGroup;
  searchList: boolean = false;
  @Output() searchData = new EventEmitter<object>();
  @Output() back = new EventEmitter<object>();
  constructor( private fb: FormBuilder,) { 
    this.reasonForm = this.fb.group({
      text: [""],
      status: ["100"],
    });
  }

  ngOnInit(): void {
  }
  async onSubmit(showTable) {

    this.searchData.emit({search: this.reasonForm.controls['text'].value});
    this.searchList = true;

    }

    resetForm() {
      this.reasonForm.patchValue({
        ["text"]: "",
      });
      this.reasonForm.patchValue({
        status: "100",
      });
     
      if(this.searchList == true){
        this.back.emit({search:''});
        this.searchList = false;
      }
    }
}
