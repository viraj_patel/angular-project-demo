import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { REASON_TABS_LIST,REASON_TABLE_SHORT, REASON__TABS_NAME, REASON_TABLE, REASON_KEY_LIST, TABLE_DATA_TYPE, REASON_TABLE_SEARCH, STATIC_DATA, REASON_TEXT } from '@constant/constants';
import { TranslateService } from '@ngx-translate/core';
import { StaffService } from '@services/staff.service';
import { SystemCinfigurationsService } from '@services/system-cinfigurations.service';
import { UsersService } from '@services/users.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { ConfirmationModalComponent } from 'src/app/modals/confirmation-modal/confirmation-modal.component';
import { __values } from 'tslib';

@Component({
  selector: 'app-reason-management',
  templateUrl: './reason-management.component.html',
  styleUrls: ['./reason-management.component.scss']
})
export class ReasonManagementComponent implements OnInit {
  pageHead;
  tableSort=REASON_TABLE_SHORT;
  obs: Subscription;
  tableFormReset:boolean;
  activeTab = 0;
  activeTabName;
  typeTable:string;
  listTable: boolean = false;
  tableFields=REASON_TABLE;
  reasonTablSearchFields=REASON_TABLE_SEARCH;
  firstPage:number=1;
  showNoData: boolean = false;
  totalItems: any;
  reasonTable = [];
  showFilters: boolean = false;
  tabKey:string;
  modalRef: BsModalRef;
  tablename:string= "Reason";
  edit:boolean=false;
  delete:boolean=false;
  export:boolean=false;
  import:boolean=false;
  create:boolean=false;
  paramsObj:any = {
    page:1,
    limit:10
  };
  view:boolean=false;
  constructor(
    private userService: UsersService,
    private modalService: BsModalService,
    private systemService: SystemCinfigurationsService,
    private fb:FormBuilder,
    private translate: TranslateService,
    private rolesService : StaffService,
    private router:Router
  ) { }

  async ngOnInit() {
    const url = this.router.url;
    const path = url;
    const route = `/${path.split('/')[3]}`;
    const currentRoute = route.split('?')[0].replace('/','');
    const permissions = await this.rolesService.getLoginPermissionsForModule(currentRoute);
    if(permissions.length > 0){
      if(permissions[0].View == false){
        this.router.navigate(['/main-home/my-profile']);
      }else{
        this.view = true;
      }
      this.create = permissions[0].Create;
      this.edit = permissions[0].Edit;
      this.delete = permissions[0].Delete;
      this.export = permissions[0].Export;
      this.import = permissions[0].Import;
    }else{
      this.view = true;
    }
             this.activeTab = 0;
          this.listTable = false;
            this.activeTab = 0;
            this.typeTable = REASON__TABS_NAME.ADD_ON_UNSUBSCRIBE;
            this.activeTabName = REASON_TABS_LIST.ADD_ON_UNSUBSCRIBE;
            this.pageHead=REASON__TABS_NAME.ADD_ON_UNSUBSCRIBE;
            this.paramsObj = {
              page: 1,
              limit: 10,
              sortBy:'id:DESC'
  
            };
            this.firstPage = this.paramsObj.page;
            this.tabKey = REASON_KEY_LIST.REASON;
            this.listApiCall(this.paramsObj,this.tabKey);
  }
 
  getConfigValue(type,itemType){
    if(type==itemType){
      return true;
    }
    return false;
}
  async listApiCall(param,type) {
    
    this.paramsObj = param;
    
    this.paramsObj.group =type + ' management';

    this.firstPage = this.paramsObj.page;
    let params = new HttpParams();
    Object.keys(this.paramsObj).forEach((key) => {
      params = params.set(key, this.paramsObj[key]);
    });
    
      const response:any = await this.systemService.getSysConfig(params);
     
      for (const iterator of response.result.configurations) {
        var data=[];
        data[0]={value:iterator?.id,type: TABLE_DATA_TYPE.ID };
        data[1]={value:iterator?.configurableKeyDisplayText,type:TABLE_DATA_TYPE.TEXT};
        data[2]={value:iterator?.configurableValueDisplayText != null ? iterator?.configurableValueDisplayText :'' ,type:TABLE_DATA_TYPE.TEXT};
        // data[3]={value:element?.ticketSCLevel1,type:TABLE_DATA_TYPE.TEXT};
      data[3]={
          value: JSON.stringify(iterator?.configurableValue),
          type:TABLE_DATA_TYPE.TEXT,
          dropDown:this.getConfigValue('DROPDOWN',iterator?.configurableDataType),
          textArea:this.getConfigValue('textarea',iterator?.configurableDataType),
          multiSelect:this.getConfigValue('multiSelectDropdown',iterator?.configurableDataType),
          radio:this.getConfigValue('boolean',iterator?.configurableDataType),
          json:this.getConfigValue('JSON',iterator?.configurableDataType),
          number:this.getConfigValue('number',iterator?.configurableDataType),
          string:this.getConfigValue('string',iterator?.configurableDataType) || iterator?.configurableDataType == "" ? true:false,
          array:this.getConfigValue('array',iterator?.configurableDataType)
        };
        if(data[3].json){
          iterator.edittype = 'json';

          if(typeof iterator?.configurableValue == 'string'){
            data[3].value=iterator?.configurableValue;
            iterator.editObj={
              [this.tableFields[3]]: JSON.parse(iterator?.configurableValue),
            } 
          }
          else{
            data[3].value = JSON.stringify(iterator?.configurableValue);
            iterator.editObj={
              [this.tableFields[3]]:iterator?.configurableValue,
            } 
          }

           
            // [this.tableFields[4]]:iterator?.isActive == 1 ? true:false;
        }

        if(data[3].radio){
          iterator.edittype = 'radio';
          data[3].value = iterator.configurableValue;
          iterator.editObj = {
            [this.tableFields[3]]: [(iterator.configurableValue =='yes' || iterator.configurableValue=='true' || iterator.configurableValue=='1') ? 'Yes' :'No' ,[Validators.required]]
          }
          data[3].value = (iterator.configurableValue =='yes' || iterator.configurableValue=='true' || iterator.configurableValue=='1') ? 'Yes' :'No';
        }

        if(data[3].multiSelect){
          iterator.edittype = 'multiSelect';
          iterator.configPossibleValues = JSON.parse(iterator.configPossibleValues)
          if(typeof iterator?.configurableValue == 'string'){
            iterator.editObj = {
              [this.tableFields[3]]: [JSON.parse(iterator?.configurableValue), [Validators.required]]
            }
            data[3].value = this.getValue(JSON.parse(iterator?.configurableValue))
          }
          else{
            iterator.editObj = {
              [this.tableFields[3]]: [iterator?.configurableValue, [Validators.required]]
            }
            data[3].value = this.getValue(iterator?.configurableValue)
          }
          
        }

        if(data[3].textArea){
          iterator.edittype = 'textArea';
          data[3].value = iterator.configurableValue;
          iterator.editObj = {
            [this.tableFields[3]]: [iterator?.configurableValue, [Validators.required]]
          }
        }

        if(data[3].number){
          
          iterator.edittype = 'number';
          data[3].value = iterator.configurableValue;
          data[3].edit=true;
          iterator.editObj = {
            [this.tableFields[3]]: [iterator?.configurableValue.match(/\d+/)[0], [Validators.required,Validators.pattern('^[0-9]*$')]]
          }
          
        }

        
        if(data[3].string){
          iterator.edittype = 'textArea';
            data[3].value = iterator.configurableValue;
            data[3].textArea = true;
            iterator.editObj = {
              [this.tableFields[3]]: [iterator?.configurableValue, [Validators.required,Validators.minLength(2),Validators.maxLength(256)]]
            }
        }

        if(data[3].array){
          data[3].class="text-data";
          iterator.edittype = 'array';
          iterator.editObj = {
            [this.tableFields[3]]:this.getArrayForm(iterator?.configurableValue)
          }
          data[3].value = this.getValue(iterator?.configurableValue)  
        }
        data[4] = { value: iterator?.createdAt, type: TABLE_DATA_TYPE.DATE };
        data[5] = { value :iterator?.updatedAt,type: TABLE_DATA_TYPE.DATE};
       
        iterator.data=data;
        iterator.edit = false;
      }
     
      this.totalItems = response.result.totalCount;
      this.reasonTable = response.result.configurations;
      
 
      this.listTable = true;
    if (this.reasonTable.length === 0 && this.paramsObj.searchBy == '' && this.paramsObj.searchValue == '' && this.paramsObj.fromDate == '' && this.paramsObj.toDate == '') {
      this.showNoData = true;
    }
  }

  searchData(data){
    if(data.search != ''){
      this.paramsObj.search = data.search.trim();
    }  
   
    
    this.paramsObj.page = 1;
    this.listApiCall(this.paramsObj,this.tabKey); 
  }

  resetData(event){
   
    this.paramsObj.search = "";
    this.paramsObj.page = 1;
    this.listApiCall(this.paramsObj,this.tabKey); 
  }
  

  getValue(data){
    if(data){
      if(typeof data =='string'){
        return data;
      }
      else if(Array.isArray(data)){
        const values = data.map(v => { return v.value })
        return values.join( ', \n' );
      }
    }
    return '';
  }

  getArrayForm(data){
    if(data){
      if(typeof data =='string'){
        return data;
      }
      else if(Array.isArray(data)){
        const values = data.map(v => { 
          return this.fb.group({
            id:v.id,
            value:[v.value,[Validators.required,Validators.minLength(3),Validators.maxLength(256)]]
          })  });
        return this.fb.array(values);
      }
    }
    return '';
  }


  async editInfo(data){
    
    if(data.row.configurableDataType=='JSON' || data.row.configurableDataType=='DROPDOWN' || data.row.configurableDataType=='multiSelectDropdown'){
      data.row.configurableValue = JSON.stringify(data.value.configurableValue );
    }
    else if(data.row.configurableDataType=='boolean'){
      data.row.configurableValue = this.getBooleanValue(data.value.configurableValue, data.row.configurableValue)
    }
    else if(data.row.configurableDataType=='array'){

      if(this.isObject(data.row.configurableValue[0])){
        data.row.configurableValue = JSON.stringify(data.value.configurableValue);
      }else{
        const values = data.value.configurableValue.map(v => { return v.value })
        data.row.configurableValue = JSON.stringify(values);
      }

      
    }
    else{
      data.row.configurableValue = data.value.configurableValue;
    }
    const obj = {
      configurableKey: data.row.configurableKey,
      configurableKeyDisplayText: data.row.configurableKeyDisplayText,
      configurableValue:data.row.configurableValue ,
      configurableValueDisplayText: data.row.configurableValueDisplayText,
      configurableType: data.row.configurableType,
      isActive: data.row.isActive
    };
    await this.systemService.putSysConfig(obj);
    this.listApiCall(this.paramsObj,this.tabKey);

  }
  getBooleanValue(editValue, configValue){
    if(configValue=='1' || configValue=='0'){
      return editValue=='Yes' ? '1':'0';
    }
    if(configValue=='true' || configValue=='false'){
      return editValue=='Yes' ? 'true':'false';
    }
    if(configValue=='Yes' || configValue=='no'){
      return editValue=='Yes'? 'yes' : 'no' ;
    }
  }

  keySearch(param){
    if(param[this.tableFields[1]+"_text"]?.value != ''){
      this.paramsObj.searchBy  = this.tableFields[1];
      this.paramsObj.searchValue = param[this.tableFields[1]+"_text"]?.value;
    }else if(param[this.tableFields[2]+"_text"]?.value != ''){
      this.paramsObj.searchBy  = this.tableFields[2];
      this.paramsObj.searchValue = param[this.tableFields[2]+"_text"]?.value;
    }else if(param[this.tableFields[3]+"_text"]?.value != ''){
      this.paramsObj.searchBy  = this.tableFields[3];
      this.paramsObj.searchValue = param[this.tableFields[3]+"_text"]?.value;
    }else{
      this.paramsObj.searchBy  = "";
      this.paramsObj.searchValue = "";
    }
    this.listApiCall(this.paramsObj,this.tabKey);
  }

  deleteLocation(data){
    const editConsent=true;
    let title = REASON_TEXT.LABLE;
    const initialState = {
      title: STATIC_DATA.DELETE+title,
      confirmText: STATIC_DATA.DELETE_MSG + title.toLowerCase() + "?",
    };
    this.modalRef = this.modalService.show(ConfirmationModalComponent, {
      class: 'modal-sm modal-dialog-centered custom-modal',
      backdrop: 'static',
      initialState,
    });
    this.modalRef.content.closeButtonText = this.translate.instant('MODAL.NO_BTN');
    this.modalRef.content.acceptButtonText = this.translate.instant('MODAL.YES_BTN');
    this.modalRef.content.response.subscribe(async (result) => {
      if (result) {
        let deleteParam={
          key:data.configurableKey
        }

        let param = new HttpParams();
        Object.keys(deleteParam).forEach((key) => {
          if(deleteParam[key]!=''){
            param = param.set(key, deleteParam[key]);
          }
        });

      
        await this.systemService.deleteSysConfig(param);
        if(this.reasonTable.length === 1){
          this.paramsObj.page = this.paramsObj.page - 1;
        }
        await this.listApiCall(this.paramsObj,this.tabKey); 

      } else {
        
      }
    });
  }
  isObject(objValue) {
    return objValue && typeof objValue === 'object' && objValue.constructor === Object;
  }
  isEqual(value, other) {

    // Get the value type
    const a = value;
    const b = other;
    
    // A comparer used to determine if two entries are equal.
    const isSameUser = (a, b) => a.value === b.value && a.id === b.display;
    
    // Get items that only occur in the left array,
    // using the compareFunction to determine equality.
    const onlyInLeft = (left, right, compareFunction) => 
      left.filter(leftValue =>
        !right.some(rightValue => 
          compareFunction(leftValue, rightValue)));
    
    const onlyInA = onlyInLeft(a, b, isSameUser);
    const onlyInB = onlyInLeft(b, a, isSameUser);
    
    const result = [...onlyInA, ...onlyInB];
    return result;
  
  }
}
