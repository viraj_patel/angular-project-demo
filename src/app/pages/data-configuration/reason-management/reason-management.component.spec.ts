import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReasonManagementComponent } from './reason-management.component';

describe('ReasonManagementComponent', () => {
  let component: ReasonManagementComponent;
  let fixture: ComponentFixture<ReasonManagementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReasonManagementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReasonManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
