import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Web_Language } from '@constant/constants';
import { TranslateService } from '@ngx-translate/core';
import { LocationService } from '@services/location.service';
import { UsersService } from '@services/users.service';
import { ConsentManagementListComponent } from './consent-management-list/consent-management-list.component';
import { DataConfigurationComponent } from './data-configuration/data-configuration.component';
import { LocationManagementComponent } from './location-management/location-management.component';
import { ReasonManagementComponent } from './reason-management/reason-management.component';
import { TicketManagementComponent } from './ticket-management/ticket-management.component';

const routes: Routes = [
  
  {
    path: '', component: DataConfigurationComponent,
    children: [
      {
        path: '',
        redirectTo: 'location-management',
        pathMatch: 'full'
      },
      {
        path: 'location-management', component: LocationManagementComponent,
      },
      {
        path: 'ticket-management', component: TicketManagementComponent,
      },
      {
        path: 'consent-management', component: ConsentManagementListComponent,
      },
      {
        path: 'reason-management', component: ReasonManagementComponent,
      }
    ]
},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DataConfigurationRoutingModule { 
  constructor(
    public translate: TranslateService,
    public locationService:LocationService
  ) {
    translate.setDefaultLang(Web_Language.EN);
  }
}
