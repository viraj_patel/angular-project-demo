import { LocationStrategy } from '@angular/common';
import { HttpParams } from '@angular/common/http';
import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { LOCATION_TABS_LIST, DATA_CONFIGURATION_TABS_LIST, TICKET_TABS_LIST, TICKET_TABS_NAME, LOCAL_STORAGE, TAB_FUNCTION, REASON_TABS_LIST, REASON__TABS_NAME, TICKET_TABS_KEY,} from '@constant/constants';
import { TranslateService } from '@ngx-translate/core';
import { LocalStorageService } from '@services/localService/localStorage.service';
import { StaffService } from '@services/staff.service';
import { UsersService } from '@services/users.service';
import { Observable } from 'rxjs';
import {map, startWith} from 'rxjs/operators';

declare var $: any;

@Component({
  selector: 'app-data-configuration',
  templateUrl: './data-configuration.component.html',
  styleUrls: ['./data-configuration.component.scss']
})
export class DataConfigurationComponent implements OnInit  {

  activeTab = LOCATION_TABS_LIST.COUNTRY;
  activeTicketTab = TICKET_TABS_LIST.TYPE_MANAGEMENT
  locationMenu:boolean=false;
  dataConfigTab=DATA_CONFIGURATION_TABS_LIST;
  currentRoute;
  locationTab=LOCATION_TABS_LIST;
  ticketTab=TICKET_TABS_LIST;
  ticketTabName=TICKET_TABS_NAME;
  reasonTab=REASON_TABS_LIST;
  reasonTabName=REASON__TABS_NAME;
  activeDropDown:string;
  firstDropDown:string;
  childPermissions:any=[];
  locationarray=[]
  
  @ViewChild('tabBox') inputBox: ElementRef;

  myControl = new FormControl('');
 
  options: string[] = ["location-management","country-management","state-management","district-management","city-management","zip-code-management","consent-management","ticket-management","ticketType","category","subCategory","reason-management"];
  filteredOptions: Observable<string[]>;

  constructor(
    private router:Router,
    private userService:UsersService,
    private localStorage: LocalStorageService,
    private locationStrategy: LocationStrategy,
    private rolesService : StaffService,
    private translate : TranslateService
  ) {
   }
  ngOnInit(): void {
    // check if back or forward button is pressed.
    this.getPermissionsList('master-data-configuration');
    this.filteredOptions = this.myControl.valueChanges.pipe(startWith(''),map(value => this._filter(value || '')),
    );
  }
  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    
    return this.options.filter(option => this.translate.instant(option).toLowerCase().includes(filterValue));
  }

  async selectedOption(event){    
    const selectedValue = event.option.value;
    
    this.getSearch(this.translate.instant(selectedValue), '', true);
  }

  async getSearch(tab, event, dropDown){    
    this.translate.instant(tab);
    const response=await Object.keys(this.dataConfigTab).find(key => this.dataConfigTab[key] == tab);
    if(response!=undefined){  
      this.getCurrentRoute(tab, '', true)
    }
    else{
      const customerResponse=await Object.keys(this.locationTab).find(key => this.locationTab[key] == tab)
      if(customerResponse!=undefined){
        this.getLocationTab(tab, '')
      }
      else{
        const customerManagementResponse=await Object.keys(this.ticketTab).find(key => this.ticketTab[key] == tab)
        if(customerManagementResponse!=undefined){
          this.getTicketTab(tab, '')
        }
      }    
    } 
    }

  async getPermissionsList(key){
    // const data = {roleNames:["Super Admin"]}
     this.childPermissions = await this.rolesService.getLoginPermissionsForModuleChildList(key);
     
     if(this.childPermissions.length > 0){
      
      for (const iterator of this.childPermissions) {
        // if(iterator.View==true){
        //   this.options.push(iterator.key)
        // }
        if(iterator.children.length>0){
          iterator.childPermissions = await this.rolesService.getLoginPermissionsForModuleChildList(iterator.key);
          
          for(const iterator1 of iterator.childPermissions){
           
              // if(iterator1.View==true){
              //   this.options.push(iterator1.key)
              //   }
            }
          }      
        

        else{
          iterator.childPermissions = [];
        }
        
      }
     }
     setTimeout(() => {
      this.locationStrategy.onPopState(() => {
        setTimeout(() => {
          this.CurrentRoute();
        });
       
      });
    $('#ass').on('hide.bs.dropdown', function (e:any) {
      if (e.clickEvent) {
      e.preventDefault();
      }
    });   
      $('#ticket').on('hide.bs.dropdown', function (e:any) {
        if (e.clickEvent) {
        e.preventDefault();
      }
    });   
  
      this.CurrentRoute();
     });
   
  }

  checkPermissions(key,child){
    if(child == false){
      for (const iterator of this.childPermissions) {
        if(key == iterator.key && iterator.View == true){
          return true;
        }
      }
      return false;
    }else{
      for (const iterator of this.childPermissions) {
        if(iterator.childPermissions.length > 0){
          for (const child of iterator.childPermissions) {
            if(key == child.key && child.View == true){
              return true;
            }
          }
        }
      }
      return false;
    }
    
  }


  async CurrentRoute(){ 
      const url = this.router.url;
      const path = url;
      const route = `/${path.split('/')[3]}`;
      this.currentRoute = route.split('?')[0].replace('/','');
  

      if(this.currentRoute == this.dataConfigTab.LOCATION){
        if(this.activeDropDown != this.dataConfigTab.LOCATION){
          $('#locationDropDown').dropdown('toggle');
          this.activeDropDown = this.currentRoute;
          this.firstDropDown = this.currentRoute;
        }else{
          // $('#ticketDropDown').dropdown('toggle');
        }
        const active_tab: any = await this.localStorage.getDataFromIndexedDB(LOCAL_STORAGE.DATA_ACTIVE_TAB);
        if(active_tab != null && (this.locationTab.COUNTRY == active_tab || this.locationTab.STATE == active_tab || this.locationTab.DISTRICT == active_tab || this.locationTab.CITY == active_tab || this.locationTab.ZIP == active_tab)){
          const permissions = await this.rolesService.getLoginPermissionsForModule(active_tab);
          if(permissions.length > 0){
            if(permissions[0].View == false){
              const childPermissions = await this.rolesService.getLoginPermissionsForModuleChildList(this.currentRoute);
              for (const iterator of childPermissions) {
                if(iterator.View == true){
                  this.activeTab=iterator.key;
                }
              }
            }else{
              this.activeTab = active_tab;
            }
          }else{
            this.activeTab = active_tab;
          }
         
        }else{
          const childPermissions = await this.rolesService.getLoginPermissionsForModuleChildList(this.currentRoute);
          let active='';
          for (const iterator of childPermissions) {
            if(iterator.View == true){
              active=iterator.key;
            }
          }
          this.activeTab = active;
          await this.localStorage.setDataInIndexedDB(
            LOCAL_STORAGE.DATA_ACTIVE_TAB,
            this.activeTab
          );
          await this.localStorage.setDataInIndexedDB(
            LOCAL_STORAGE.DATA_ACTIVE_FUNCTION,
            TAB_FUNCTION.LIST
          );
        }
        this.userService.sendLocationTab({tab:this.activeTab});
      }else if(this.currentRoute == this.dataConfigTab.TICKET){
        if(this.activeDropDown != this.dataConfigTab.TICKET){
          $('#ticketDropDown').dropdown('toggle');
          this.activeDropDown = this.currentRoute;
          this.firstDropDown = this.currentRoute;
        }else{
          // $('#locationDropDown').dropdown('toggle');
        }
        
        const active_tab: any = await this.localStorage.getDataFromIndexedDB(LOCAL_STORAGE.DATA_ACTIVE_TAB);
        if(active_tab != null && (this.ticketTab.CATEGORY_MANAGEMENT == active_tab || this.ticketTab.SUB_CATEGORY_MANAGEMENT == active_tab || this.ticketTab.TYPE_MANAGEMENT == active_tab)){
          const permissions = await this.rolesService.getLoginPermissionsForModule(TICKET_TABS_KEY[active_tab]);
          if(permissions.length > 0){
            if(permissions[0].View == false){
             
              const childPermissions = await this.rolesService.getLoginPermissionsForModuleChildList(this.currentRoute);
              for (const iterator of childPermissions) {
                if(iterator.View == true){
                  this.activeTab=TICKET_TABS_KEY[iterator.key];
                }
              }
            }else{
              this.activeTab = active_tab;
            }
          }
          else{
            this.activeTab = active_tab;
          }

        }else{
          const childPermissions = await this.rolesService.getLoginPermissionsForModuleChildList(this.currentRoute);
          let active='';
          for (const iterator of childPermissions) {
            if(iterator.View == true){
              active=TICKET_TABS_KEY[iterator.key];
            }
          }
          this.activeTab = active;
          await this.localStorage.setDataInIndexedDB(
            LOCAL_STORAGE.DATA_ACTIVE_TAB,
            this.activeTab
          );
          await this.localStorage.setDataInIndexedDB(
            LOCAL_STORAGE.DATA_ACTIVE_FUNCTION,
            TAB_FUNCTION.LIST
          );
        }
        this.userService.sendTicketTab({tab:this.activeTab});
      }else{
        this.activeDropDown = this.currentRoute;
        await this.localStorage.setDataInIndexedDB(
          LOCAL_STORAGE.DATA_ACTIVE_FUNCTION,
          TAB_FUNCTION.LIST
        );
        if($("#Dropdown").is(".show")){
          $(function () {
            $('#locationDropDown').dropdown('toggle');
          });
        }else if($("#Dropdown2").is(".show")){
          $(function () {
            $('#ticketDropDown').dropdown('toggle');
          });
        }
      }

  
  }


  showLocationMenu(){
    this.locationMenu = !this.locationMenu;
  }

  
  async getCurrentRoute(tab,event,dropDown){
    if(event != ''){
      event.stopPropagation();
    }
  
    for(let i=0;i<this.locationarray.length;i++){
      if(tab==this.locationarray[i]+' location'){
        await  this.getLocationTab(this.locationarray[i],event);
        tab='location-management';
      }
      
    }
  
    await this.localStorage.setDataInIndexedDB(
      LOCAL_STORAGE.DATA_ACTIVE_FUNCTION,
      TAB_FUNCTION.LIST
    );
    this.router.navigate(['/main-home/master-data-configuration/'+tab]);
    
      setTimeout(() => {
        const url = this.router.url;
        const path = url;
        const route = `/${path.split('/')[3]}`;
        this.currentRoute = route.split('?')[0].replace('/','');
      });
     
      if(tab == this.dataConfigTab.LOCATION){
        if(this.activeDropDown != this.dataConfigTab.LOCATION){
          this.activeDropDown = tab;
          const childPermissions = await this.rolesService.getLoginPermissionsForModuleChildList(tab);
          let active='';
          for (const iterator of childPermissions) {
            if(iterator.View == true){
              active=iterator.key;
            }
          }
          this.activeTab = active;
          await this.localStorage.setDataInIndexedDB(
            LOCAL_STORAGE.DATA_ACTIVE_TAB,
            this.activeTab
          );
        this.userService.sendLocationTab({tab:this.activeTab});
        }
        if(this.firstDropDown != this.dataConfigTab.LOCATION && this.firstDropDown != this.dataConfigTab.TICKET){
          this.firstDropDown = tab;
            $(function () {
              $('#locationDropDown').dropdown('toggle');
            });
        }
        if($("#Dropdown").is(".show") == false && (dropDown || this.firstDropDown != this.dataConfigTab.LOCATION)){
          this.firstDropDown = tab;
          $('#locationDropDown').dropdown('toggle');
        }else if($("#Dropdown").is(".show") == true && dropDown){
          $('#locationDropDown').dropdown('toggle');
        }
       
      }else if(tab == this.dataConfigTab.TICKET){
        if(this.activeDropDown != this.dataConfigTab.TICKET){
          this.activeDropDown = tab;
          const childPermissions = await this.rolesService.getLoginPermissionsForModuleChildList(tab);
          let active='';
          for (const iterator of childPermissions) {
            if(iterator.View == true){
              active=TICKET_TABS_KEY[iterator.key];
            }
          }
          this.activeTab = active;
          await this.localStorage.setDataInIndexedDB(
            LOCAL_STORAGE.DATA_ACTIVE_TAB,
            this.activeTab
          );
        this.userService.sendTicketTab({tab:this.activeTab});
        }
        if(this.firstDropDown != this.dataConfigTab.LOCATION && this.firstDropDown != this.dataConfigTab.TICKET){
          this.firstDropDown = tab;
            $(function () {
              $('#ticketDropDown').dropdown('toggle');
            });
        }
        if($("#Dropdown2").is(".show") == false && (dropDown || this.firstDropDown != this.dataConfigTab.TICKET) ){
          this.firstDropDown = tab;
          $('#ticketDropDown').dropdown('toggle');
        }else if($("#Dropdown2").is(".show") == true && dropDown){
          $('#ticketDropDown').dropdown('toggle');
        }
      }else{
        this.activeDropDown = tab;
        if($("#Dropdown").is(".show")){
          $(function () {
            $('#locationDropDown').dropdown('toggle');
          });
        }else if($("#Dropdown2").is(".show")){
          $(function () {
            $('#ticketDropDown').dropdown('toggle');
          });
        }
      }

  
  }

  async getLocationTab(tab,event){
    
    this.activeTab = tab;
    if(event != ''){
      
      event.stopPropagation();
    }
    if(this.currentRoute != this.dataConfigTab.LOCATION){
      this.router.navigate(['/main-home/master-data-configuration/'+this.dataConfigTab.LOCATION]);
      setTimeout(() => {
        const url = this.router.url;
        const path = url;
        const route = `/${path.split('/')[3]}`;
        this.currentRoute = route.split('?')[0].replace('/','');
      });

      this.activeDropDown = this.dataConfigTab.LOCATION;
    }
    await this.localStorage.setDataInIndexedDB(
      LOCAL_STORAGE.DATA_ACTIVE_FUNCTION,
      TAB_FUNCTION.LIST
    );
    await this.localStorage.setDataInIndexedDB(
      LOCAL_STORAGE.DATA_ACTIVE_TAB,
      this.activeTab
    );
    this.userService.sendLocationTab({tab:tab});
  
    if($("#Dropdown").is(".show") == false ){
      this.firstDropDown = tab;
      $('#locationDropDown').dropdown('toggle');
    }
   
  }

  async getTicketTab(tab,event){
    if(event != ''){
      event.stopPropagation();
    }
    if(this.currentRoute != this.dataConfigTab.TICKET){
      this.router.navigate(['/main-home/master-data-configuration/'+this.dataConfigTab.TICKET]);
      setTimeout(() => {
        const url = this.router.url;
        const path = url;
        const route = `/${path.split('/')[3]}`;
        this.currentRoute = route.split('?')[0].replace('/','');
      });

      this.activeDropDown = this.dataConfigTab.TICKET;
    }
    this.activeTab = tab;
    await this.localStorage.setDataInIndexedDB(
      LOCAL_STORAGE.DATA_ACTIVE_FUNCTION,
      TAB_FUNCTION.LIST
    );
    await this.localStorage.setDataInIndexedDB(
      LOCAL_STORAGE.DATA_ACTIVE_TAB,
      this.activeTab
    );
    this.userService.sendTicketTab({tab:tab});
    if($("#Dropdown2").is(".show") == false){
      this.firstDropDown = tab;
      $('#ticketDropDown').dropdown('toggle');
    }
  }

  async getReasonTab(tab,event){
    if(event != ''){
      event.stopPropagation();
    }
    if(this.currentRoute != this.dataConfigTab.REASON){
      this.router.navigate(['/main-home/master-data-configuration/'+this.dataConfigTab.REASON]);
      setTimeout(() => {
        const url = this.router.url;
        const path = url;
        const route = `/${path.split('/')[3]}`;
        this.currentRoute = route.split('?')[0].replace('/','');
      });

      this.activeDropDown = this.dataConfigTab.REASON;
    }
    this.activeTab = tab;
    await this.localStorage.setDataInIndexedDB(
      LOCAL_STORAGE.DATA_ACTIVE_FUNCTION,
      TAB_FUNCTION.LIST
    );
    await this.localStorage.setDataInIndexedDB(
      LOCAL_STORAGE.DATA_ACTIVE_TAB,
      this.activeTab
    );
    this.userService.sendReasonTab({tab:tab});
  }

  async tabDropDown(tab,event){
    
    if(event != ''){
      event.stopPropagation();
    }
    
    if(tab == this.dataConfigTab.TICKET){
      $(function () {
        $('#ticketDropDown').dropdown('toggle');
      });
    }else if(tab == this.dataConfigTab.LOCATION){
      $(function () {
        $('#locationDropDown').dropdown('toggle');
      });
    }
    
  }

}
