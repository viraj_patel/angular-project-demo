import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsentManagementListComponent } from './consent-management-list.component';

describe('ConsentManagementListComponent', () => {
  let component: ConsentManagementListComponent;
  let fixture: ComponentFixture<ConsentManagementListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConsentManagementListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsentManagementListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
