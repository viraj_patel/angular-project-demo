import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { CONSENT_ROUTES,CONSENT_TABLE_SORT, CONSENT_TABLE, LANGUAGE_LABLE, CONSENT_TABLE_HEADING, CONSENT_TABLE_SEARCH, CUSTOMER_TABS_LIST, DATA_CONFIGURATION_TABS_LIST, FILTER_DATE_FORMAT, LOCAL_STORAGE, STATIC_DATA, TABLE_DATA_TYPE, TABLE_NAMES, TAB_FUNCTION, CONSENT_TABLE_ARABIC, CONFIG_KEYS } from '@constant/constants';
import { TranslateService } from '@ngx-translate/core';
import { ConsentService } from '@services/consent.service';
import { JsonToCsvService } from '@services/json-to-csv.service';
import { LocalStorageService } from '@services/localService/localStorage.service';
import { StaffService } from '@services/staff.service';
import { ConfirmDialogComponent } from '@shared/components/confirmation-dialog/confirmation-dialog.component';
import * as moment from 'moment';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ConfirmationModalComponent } from 'src/app/modals/confirmation-modal/confirmation-modal.component';
import { CsvFormatComponent } from 'src/app/modals/csv-format/csv-format.component';
import { RecordViewComponent } from 'src/app/modals/record-view/record-view.component';

@Component({
  selector: 'app-consent-management-list',
  templateUrl: './consent-management-list.component.html',
  styleUrls: ['./consent-management-list.component.scss']
})
export class ConsentManagementListComponent implements OnInit {
  
  createConsent:boolean=false;
  tableName=TABLE_NAMES.CONSENT_TABLE;
  dataConfigTab=DATA_CONFIGURATION_TABS_LIST;
  tableSort=CONSENT_TABLE_SORT;
  paramsObj:any = {
    page:1,
    sortBy:'',
    searchBy:'',
    searchValue:'',
    fromDate:'',
    toDate:'',
    limit:10
  };
  consentList;
  editData:{};
  
  columnCount:number=8;
  totalItems: any;
  editconsent:boolean=false;
  consentTable = [];
  firstPage: number = 1;
  showNoData: boolean = false;
  consentTableFields:any = CONSENT_TABLE;
  consentTableHeading=CONSENT_TABLE_HEADING;
  consentTablSearchFields:any=CONSENT_TABLE_SEARCH;
  shortingFilters:boolean=true;
  dateForm: FormGroup;
  modalRef: BsModalRef;
  listTable:boolean=false;
  typeTable:string=CUSTOMER_TABS_LIST.CONSENT;
  uploadUrl:string=CONSENT_ROUTES.UPLOAD_CSV;
  uploadCsv:boolean=false;
  dateSearch:boolean=false;
  uploadCsvParamsObj = null;
  public tableData: any;
  public prerequisitiesData: any;
  downloadSampleList;
  language:string='en';
  otherLanguage:any;
  edit:boolean=false;
  delete:boolean=false;
  create:boolean=false;
  export:boolean=false;
  import:boolean=false;

  csvUploadTitles = {
    sampleCsv : "CONSENT_SAMPLE_TITLE",
    preRequisites:"CONSENT_PREREQUISITES_FORMAT_TITLE",
    csvFormat:"CONSENT_CSV_FORMAT_TITLE",
  }
  tablePageLimit:number=10;
  view:boolean=false;

  constructor( private consentService: ConsentService,
    private modalService: BsModalService,
    private translate: TranslateService,
    private localStorage: LocalStorageService,
    private jsonToCsvService: JsonToCsvService,
    private rolesService : StaffService,
    private router:Router) { 
      
    const today = new Date();
    const month = today.getMonth();
    const year = today.getFullYear();
    this.dateForm = new FormGroup({
      start: new FormControl(new Date(year, month, 13)),
      end: new FormControl(new Date(year, month, 16)),
    });
  }
  showFilters:boolean=false;
  async ngOnInit() {
    const url = this.router.url;
    const path = url;
    const route = `/${path.split('/')[3]}`;
    const currentRoute = route.split('?')[0].replace('/','');
        const permissions = await this.rolesService.getLoginPermissionsForModule(currentRoute);
        if(permissions.length > 0){
          if(permissions[0].View == false){
            this.router.navigate(['/main-home/my-profile']);
          }else{
            this.view = true;
          }
          this.create = permissions[0].Create;
          this.edit = permissions[0].Edit;
          this.delete = permissions[0].Delete;
          this.export = permissions[0].Export;
          this.import = permissions[0].Import;
        }else{
          this.view = true;
        }
    await this.callConfiguration();
    this.activeFuction();
    this.paramsObj.sortBy = 'id:DESC';
    this.listApiCall(this.paramsObj);
    // this.downloadSampleList = await this.downloadSampleFunction()
    // this.table_data();
    // this.setPrerequisitiesData();
  }

  async downloadSampleFunction(){
    return await this.consentService.getConsentsCsv();
  }
 async callConfiguration(){
    const keyParam = {fields:CONFIG_KEYS.LANGUAGE_KEY}
    let param = new HttpParams();
    Object.keys(keyParam).forEach((key) => {
      if (keyParam[key] != "") {
        param = param.set(
          key,
          keyParam[key]
        );
      }
    });
    const res: any = await this.consentService.getConfig(param);
      if(res?.result){
        this.otherLanguage = res?.result[0];
      }
             
  }

  async activeFuction(){
    const active_tab_function: any = await this.localStorage.getDataFromIndexedDB(LOCAL_STORAGE.DATA_ACTIVE_FUNCTION);
    const url = this.router.url;
    const path = url;
    const route = `/${path.split('/')[3]}`;
    const currentRoute = route.split('?')[0].replace('/','');
    if(active_tab_function == TAB_FUNCTION.CREATE && currentRoute == this.dataConfigTab.CONSENT){
      this.editconsent = false;
      this.createConsent = true;
      await this.localStorage.setDataInIndexedDB(
        LOCAL_STORAGE.DATA_ACTIVE_FUNCTION,
        TAB_FUNCTION.CREATE
      );
    }else if(active_tab_function == TAB_FUNCTION.EDIT && currentRoute == this.dataConfigTab.CONSENT){
      const active_tab: any = await this.localStorage.getDataFromIndexedDB(LOCAL_STORAGE.DATA_ACTIVE_EDIT_DATA);
      this.editData= JSON.parse(active_tab);
      this.editconsent=true;
      this.createConsent=true;
      await this.localStorage.setDataInIndexedDB(
        LOCAL_STORAGE.DATA_ACTIVE_FUNCTION,
        TAB_FUNCTION.EDIT
      );
    }else if(active_tab_function == TAB_FUNCTION.LIST && currentRoute == this.dataConfigTab.CONSENT){
      await this.localStorage.setDataInIndexedDB(
        LOCAL_STORAGE.DATA_ACTIVE_FUNCTION,
        TAB_FUNCTION.LIST
      );
    }else if(active_tab_function == TAB_FUNCTION.UPLOAD && currentRoute == this.dataConfigTab.CONSENT){
     this.showCsvUpload();
    }
  }

  async listApiCall(param) {
    // this.paramsObj = param;
    this.firstPage = this.paramsObj.page;
    this.tablePageLimit = this.paramsObj.limit;
    this.paramsObj.sortBy = param.sortBy;
    this.paramsObj.limit=param.limit;
    this.paramsObj.page=param.page;
    let params = new HttpParams();
    Object.keys(this.paramsObj).forEach((key) => {
      params = params.set(key, this.paramsObj[key]);
    });
    
      const responce:any = await this.consentService.getConsents(params);
      if(this.language == this.otherLanguage?.configurableValue?.abbreviation){
        const index = this.consentTableFields.indexOf(LANGUAGE_LABLE.CONSENT);
         if (index == -1) { 
          this.consentTableFields.splice(2, 0, LANGUAGE_LABLE.CONSENT);
          this.tableSort.splice(2, 0, false);
          this.consentTablSearchFields.splice(2, 0, false);
         }
       }else{
         const index = this.consentTableFields.indexOf(LANGUAGE_LABLE.CONSENT);
         if (index > -1) { 
           this.consentTableFields.splice(index, 1); 
           this.tableSort.splice(index, 1);
           this.consentTablSearchFields.splice(index, 1);
         }
       }
    if(this.language=='en'){
      responce.result.consents.forEach((element,index) => {
        
        var data=[];
        data[0]={value:element?.id,type:TABLE_DATA_TYPE.ID};
        data[1]={value:this.language == 'en' ? element?.['category.en']:element?.['category.'+this.otherLanguage?.configurableValue?.abbreviation],type:TABLE_DATA_TYPE.TEXT};
        data[2]={value:this.language == 'en' ? element?.['text.en']:element?.['text.'+this.otherLanguage?.configurableValue?.abbreviation],type:TABLE_DATA_TYPE.TEXT};
        data[3]={
          value:element?.createdAt==undefined || element?.createdAt==null || element?.createdAt=='' ? '--': element?.createdAt ,
          type:TABLE_DATA_TYPE.DATE,
        };
        data[4]={
          value:element?.updatedAt == undefined ||  element?.updatedAt==null || element?.updatedAt=='' ? '--': element?.updatedAt,
          type:TABLE_DATA_TYPE.DATE,
        }
        data[5]={value:element?.enabled,type:TABLE_DATA_TYPE.BUTTON,color:element?.enabled == "1" ? STATIC_DATA.GREEN : STATIC_DATA.RED};
        element.data=data;
        element.edit=false;
      });
      this.totalItems = responce.result.totalCount;
      this.consentTable = responce.result.consents;
      this.listTable=true;
    }
    else{
      responce.result.consents.forEach((element,index) => {
        
        var data=[];
        data[0]={value:element?.id,type:TABLE_DATA_TYPE.ID};
        data[1]={value:element?.['category.'+this.otherLanguage?.configurableValue?.abbreviation],type:TABLE_DATA_TYPE.TEXT};
        data[2]={value:element?.['category.en'],type:TABLE_DATA_TYPE.TEXT};
        data[3]={value: element?.['text.en'],type:TABLE_DATA_TYPE.TEXT};
        data[4]={
          value:element?.createdAt,
          type:TABLE_DATA_TYPE.DATE,
        };
        data[5]={
          value:element?.updatedAt,
          type:TABLE_DATA_TYPE.DATE,
        }
        data[6]={value:element?.enabled,type:TABLE_DATA_TYPE.BUTTON,color:element?.enabled == "1" ? STATIC_DATA.GREEN : STATIC_DATA.RED};
        element.data=data;
        element.edit=false;
      });
      this.totalItems = responce.result.totalCount;
      this.consentTable = responce.result.consents;
      this.listTable=true;
    }





      
    if (this.consentTable.length === 0 && this.paramsObj.sortBy == '' && this.paramsObj.searchBy == '' && this.paramsObj.searchValue == '' && this.paramsObj.fromDate == '' && this.paramsObj.toDate == '') {
      this.showNoData = true;
    }
  }


  async sortData(type, key) {
    this.paramsObj.sortBy = key+":"+type;
    this.paramsObj.page = 1;
    this.firstPage = 1;
    
    await this.listApiCall(this.paramsObj); 

  }

  dateFilterSearch(data){
    
    this.paramsObj.fromDate = data.start.value != '' &&  data.start.value != null? moment(data.start.value).format(FILTER_DATE_FORMAT.DATE_FORMAT): '';
    this.paramsObj.toDate = data.end.value != '' && data.end.value != null ? moment(data.end.value).format(FILTER_DATE_FORMAT.DATE_FORMAT): '';
    if(this.dateSearch == true || this.paramsObj.fromDate != ''){
      this.paramsObj.page = 1;
      this.firstPage = 1;
      this.listApiCall(this.paramsObj);
      if(this.dateSearch == true){
        this.dateSearch = false; 
      }else{
        this.dateSearch = true; 
      }
    }
   
  }

  searchFilter(){
    this.showFilters = !this.showFilters;
    
  }

  
  keySearch(param){
   
    if(param[this.consentTableFields[0]+"_text"]?.value != ''){
      this.paramsObj.searchBy  = this.consentTableFields[0];
      this.paramsObj.searchValue = param[this.consentTableFields[0]+"_text"]?.value;
    }else if(param[this.consentTableFields[1]+"_text"]?.value != ''){
      this.paramsObj.searchBy  = this.consentTableFields[1];
      this.paramsObj.searchValue = param[this.consentTableFields[1]+"_text"]?.value;
    }else if(param[this.consentTableFields[2]+"_text"]?.value != ''){
      this.paramsObj.searchBy  = this.consentTableFields[2];
      this.paramsObj.searchValue = param[this.consentTableFields[2]+"_text"]?.value;
    }else if(param[this.consentTableFields[3]+"_text"]?.value != ''){
      this.paramsObj.searchBy  = "enabled";
      this.paramsObj.searchValue = param[this.consentTableFields[3]+"_text"]?.value;
    }else{
      this.paramsObj.searchBy  = "";
      this.paramsObj.searchValue = "";
    }
    this.listApiCall(this.paramsObj);
  }
 
  async hideShowCreateForm(){
    this.editconsent = false;
    this.createConsent = await !this.createConsent;
    // await this.listApiCall(this.paramsObj);
    // this.editconsent = await !this.editconsent;
    // await this.listApiCall(this.paramsObj);
  }

  async backToTable(){
    this.language = 'en';
    if(this.language == this.otherLanguage?.configurableValue?.abbreviation){
      const index = this.consentTableFields.indexOf(LANGUAGE_LABLE.CONSENT);
       if (index == -1) { 
        this.consentTableFields.splice(2, 0, LANGUAGE_LABLE.CONSENT);
        this.tableSort.splice(2, 0, false);
        this.consentTablSearchFields.splice(2, 0, false);
       }
     }else{
       const index = this.consentTableFields.indexOf(LANGUAGE_LABLE.CONSENT);
       if (index > -1) { 
         this.consentTableFields.splice(index, 1); 
         this.tableSort.splice(index, 1);
         this.consentTablSearchFields.splice(index, 1);
       }
     }
    this.tableSort = CONSENT_TABLE_SORT;
    if(this.createConsent == true){
      this.paramsObj.sortBy = 'id:DESC';
    }
    this.createConsent = await !this.createConsent;
    if(this.editconsent){
      this.editconsent = false;
      this.listTable=false;
      this.tablePageLimit = this.paramsObj.limit;
      await this.listApiCall(this.paramsObj);
    }
    else{
    this.editconsent = false;
    this.listTable=false;
    this.paramsObj.limit=10;
    this.paramsObj.firstPage=1;
    this.paramsObj.page=1;

    await this.listApiCall(this.paramsObj);
    }
    // this.editconsent = await !this.editconsent;
    // await this.listApiCall(this.paramsObj);
  }

  
  async editLocation(data) {
    if(this.language=='en'){
      data.language='en';
    }
    else{
      data.language=this.otherLanguage?.configurableValue?.abbreviation;
    }
    this.editData=data;
    this.editconsent=true;
    this.createConsent=true;
  }
  async deleteLocation(data) {
      const editConsent=true;
      const data1={}
      let title= this.typeTable.charAt(0).toUpperCase() + this.typeTable.slice(1);
      const initialState = {
        title:STATIC_DATA.DELETE+title,
        confirmText:STATIC_DATA.DELETE_MSG + title.toLocaleLowerCase() + "?",
      };
      this.modalRef = this.modalService.show(ConfirmationModalComponent, {
        class: 'modal-sm modal-dialog-centered custom-modal',
        backdrop: 'static',
        initialState,
      });
      this.modalRef.content.closeButtonText = this.translate.instant('MODAL.NO_BTN');
      this.modalRef.content.acceptButtonText = this.translate.instant('MODAL.YES_BTN');
      this.modalRef.content.response.subscribe(async (result) => {
        if (result) {
          let id
          id=data.id;
        
          await this.consentService.deleteConsents(id);
          if(this.consentTable.length === 1){
            this.paramsObj.page = this.paramsObj.page - 1;
            this.firstPage =  this.paramsObj.page;
          }
          await this.listApiCall(this.paramsObj); 

        } else {
          
        }
      });

    }

    async hideCsvUpload() {
      setTimeout(() => {
        this.uploadCsv = false;
        this.listApiCall(this.paramsObj);
      });
      await this.localStorage.setDataInIndexedDB(
        LOCAL_STORAGE.DATA_ACTIVE_FUNCTION,
        TAB_FUNCTION.LIST
      );
  }
    async downloadCSVSample(){
      const response:any = await this.consentService.getConsentsCsv();
  
      let header = Object.keys(response.result.format[0]);
      let data = response.result.format;
      this.jsonToCsvService.downloadFile(header,data,this.typeTable+"_Sample_CSV")
  
  
    }

    async table_data() { 
      this.tableData = { header: [], data: [] };
      const response:any = this.downloadSampleList;
      this.tableData.header = Object.keys(response.result.format[0])
      let recordsList = [...response.result.format]
      let data = [];
      recordsList.map((item)=>{
        
        let row = [];
        for(let key of Object.keys(item)){
          let obj= {
            value:item[key],
            type: TABLE_DATA_TYPE.TEXT,
            class1:""
          };
          row.push(obj);
        }
        data.push({row})
      })
  
      this.tableData.data = data;
    }

    setPrerequisitiesData(){
      this.prerequisitiesData = { header: [], data: [] };
      const response:any = this.downloadSampleList;
      this.prerequisitiesData.header = Object.keys(response.result.prerequisites[0]);
      let preRecordsList = [...response.result.prerequisites];
      let perData = [];
      preRecordsList.map((item)=>{
        let row = [];
        for(let key of Object.keys(item)){
          let obj= {
            value:item[key],
            type: key =="required" ?  TABLE_DATA_TYPE.ICON : TABLE_DATA_TYPE.TEXT,
            class1:key=="required" ? (item.required ? "fa solid fa-check" : "fa solid fa-xmark") : ""
          };
          row.push(obj);
        }
        perData.push({row})
      })
      
      this.prerequisitiesData.data = perData;
    }

    

    async showCsvUpload() {
        setTimeout(() => {
          this.uploadCsv = true;
        });
        await this.localStorage.setDataInIndexedDB(
          LOCAL_STORAGE.DATA_ACTIVE_FUNCTION,
          TAB_FUNCTION.UPLOAD
        );
    }
  changeLanguage(type){
    this.language = type;
    if(this.language == this.otherLanguage?.configurableValue?.abbreviation){
      const index = this.consentTableFields.indexOf(LANGUAGE_LABLE.CONSENT);
       if (index == -1) { 
        this.consentTableFields.splice(2, 0, LANGUAGE_LABLE.CONSENT);
        this.tableSort.splice(2, 0, false);
        this.consentTablSearchFields.splice(2, 0, false);
       }
     }else{
       const index = this.consentTableFields.indexOf(LANGUAGE_LABLE.CONSENT);
       if (index > -1) { 
         this.consentTableFields.splice(index, 1); 
         this.tableSort.splice(index, 1);
         this.consentTablSearchFields.splice(index, 1);
       }
     }
    if(this.language=='en'){
      this.tableSort=CONSENT_TABLE_SORT;
      this.listTable=false;
      setTimeout(() => {
        const responce =  this.consentTable ;
        responce.forEach((element,index) => {
          var data=[];
          data[0]={value:element?.id,type:TABLE_DATA_TYPE.ID};
          data[1]={value:this.language == 'en' ? element?.['category.en']:element?.['category.'+this.otherLanguage?.configurableValue?.abbreviation],type:TABLE_DATA_TYPE.TEXT};
          data[2]={value:this.language == 'en' ? element?.['text.en']:element?.['text.'+this.otherLanguage?.configurableValue?.abbreviation],type:TABLE_DATA_TYPE.TEXT};
          data[3]={
            value:element?.createdAt,
            type:TABLE_DATA_TYPE.DATE,
          };
          data[4]={
            value:element?.updatedAt,
            type:TABLE_DATA_TYPE.DATE,
          }
          data[5]={value:element?.enabled,type:TABLE_DATA_TYPE.BUTTON,color:element?.enabled == "1" ? STATIC_DATA.GREEN : STATIC_DATA.RED};
          element.data=data;
          element.edit=false;
        });
        this.consentTable = responce;
        this.listTable=true;
      });
    }
    else{
    this.listTable=false;
    setTimeout(() => {
      const responce =  this.consentTable ;
      responce.forEach((element,index) => {
        var data=[];
        data[0]={value:element?.id,type:TABLE_DATA_TYPE.ID};
        data[1]={value: element?.['category.'+this.otherLanguage?.configurableValue?.abbreviation],type:TABLE_DATA_TYPE.TEXT};
        data[2]={value: element?.['category.en'],type:TABLE_DATA_TYPE.TEXT};
        data[3]={value:element?.['text.'+this.otherLanguage?.configurableValue?.abbreviation],type:TABLE_DATA_TYPE.TEXT};
        data[4]={
          value:element?.createdAt,
          type:TABLE_DATA_TYPE.DATE,
        };
        data[5]={
          value:element?.updatedAt,
          type:TABLE_DATA_TYPE.DATE,
        }
        data[6]={value:element?.enabled,type:TABLE_DATA_TYPE.BUTTON,color:element?.enabled == "1" ? STATIC_DATA.GREEN : STATIC_DATA.RED};
        element.data=data;
        element.edit=false;
      });
      this.consentTable = responce;
      this.listTable=true;
    });
  }
  }
    
}
