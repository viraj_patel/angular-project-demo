import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormControl,FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ConsentService } from '@services/consent.service';
import { FormService } from '@services/form.service';
import { data } from 'jquery';
@Component({
  selector: 'app-create-consent',
  templateUrl: './create-consent.component.html',
  styleUrls: ['./create-consent.component.scss']
})
export class CreateConsentComponent implements OnInit {

  @Input() editConsent;
  @Input() editData:any;
  @Input() otherLanguage:any;
  @Input() language : any;
  @Output() back = new EventEmitter<object>();
  errorMsg: any = {};
  isChecked = true;
 
  consent:FormGroup;
  constructor(private fb:FormBuilder,
    private formService: FormService,
    public translation: TranslateService,
    private consentService: ConsentService
    ) {
    this.consent = this.fb.group({
      consentName: ['',[Validators.required,Validators.minLength(3),Validators.maxLength(256),Validators.pattern('[a-zA-Z\u0600-\u06FF ]*')]],
      // consentNameArabic: ['',[Validators.required,Validators.minLength(3),Validators.maxLength(256),Validators.pattern('[a-zA-Z\u0600-\u06FF ]*')]],
      id:['',],
      consentStatus:[true],
      description:[''],
      // descriptionArabic:[''],
      isChecked:[true],
      language:['en']
    })
   }
  ngOnInit(): void {
    this.intializingMessage();
    this.setConsent(this.language);
    if(this.editConsent){
      this.setConsent(this.editData.language)  
    }   
  }

  intializingMessage() {
    this.errorMsg.consentName = {
      pattern: this.translation.instant('ERR_MSG_CHARACTER_ONLY'),
    };
    // this.errorMsg.consentNameArabic = {
    //   pattern: this.translation.instant('ERR_MSG_CHARACTER_ONLY'),
    // };
  }

  changeStatus(){
    
    // if(this.consentStatus==true){
    //   this.consentStatus=false
    // }
    // else{
    //   this.consentStatus=true
    // }
    //   console.log(this.consentStatus);
  }

 

  setConsent(type){
    console.log(type,'......');
    
    this.consent.patchValue({["language"]: type });
    if(this.editConsent){
      this.isChecked = this.editData.enabled == "1"?true:false;
    
    this.consent.patchValue({
      id:this.editData?.['id'],
      consentName:this.consent.value.language == 'en'?this.editData?.['category.en']:this.editData?.['category.ar'],
      // consentNameArabic:this.editData?.['category.ar'],
      // descriptionArabic:this.editData?.['text.ar'],
      description:this.consent.value.language == 'en'?this.editData?.['text.en']:this.editData?.['text.ar'],
      consentStatus:this.editData.enabled == "1"?true:false,
      isChecked:this.editData.enabled == "1"?true:false
    });
    }  
  }


  async onSubmit(){
    this.consent.patchValue({
      consentName:this.consent.value.consentName.trim(),
      // consentNameArabic:this.consent.value.consentNameArabic.trim()
    });
    // if(this.consent.get('consentName').value == ''){
    //   this.consent.patchValue({
    //     ['language']:'en',
    //   });
    // }else if(this.consent.get("consentNameArabic").value == ''){
    //   this.consent.patchValue({
    //     ['language']:'ar',
    //   });
    // }
    this.formService.markFormGroupTouched(this.consent);
    if (this.consent.valid) {
      if(this.editConsent==true){
        let data={id:'',enabled:''};
        let enabled;
        if(this.consent.value.isChecked==true){
          enabled='1';
        }
        else{
          enabled='0';
        }
        console.log(this.consent.controls['language'].value,'rprprprrp');
        
        data['category.'+this.consent.controls['language'].value]=this.consent.value.consentName.trim();
        data['text.'+this.consent.controls['language'].value]=this.consent.value.description.trim();
       
      data['enabled']=enabled;
     
      data['id']=this.consent.value.id;

      await this.consentService.putConsents(data);
         this.back.emit();
        }
      else{
        let data={enabled:''};
        let enabled;
        if(this.consent.value.isChecked==true){
          enabled='1';
        }
        else{
          enabled='0';
        }
        data['category.en']=this.consent.value.consentName.trim();
        data['text.en']=this.consent.value.description.trim();
        data['text.ar']=this.consent.value.description.trim();
      data['category.ar']=this.consent.value.consentName.trim();
        data.enabled=enabled;
        await this.consentService.postConsents(data);
        this.back.emit();
        
      } 

    }
  }

  checkInput(event){
    if(event.code === "Space" && this.consent.value.consentName.trim().length === 0){
      setTimeout(() => {
        this.consent.patchValue({["consentName"]: "" });
      });
    }else if(event.type === "paste"){
      setTimeout(() => {
        if(this.consent.value.consentName.trim().length === 0){
          this.consent.patchValue({["consentName"]: "" });
        }
      });
    } 

    // if(event.code === "Space" && this.consent.value.consentNameArabic.trim().length === 0){
    //   setTimeout(() => {
    //     this.consent.patchValue({["consentNameArabic"]: "" });
    //   });
    // }else if(event.type === "paste"){
    //   setTimeout(() => {
    //     if(this.consent.value.consentNameArabic.trim().length === 0){
    //       this.consent.patchValue({["consentNameArabic"]: "" });
    //     }
    //   });
    // } 

    if(event.code === "Space" && this.consent.value.consentStatus.trim().length === 0){
      setTimeout(() => {
        this.consent.patchValue({["consentStatus"]: "" });
      });
    }else if(event.type === "paste"){
      setTimeout(() => {
        if(this.consent.value.consentStatus.trim().length === 0){
          this.consent.patchValue({["consentStatus"]: "" });
        }
      });
    } 
  }
}
