import { ErrorHandler, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DataConfigurationRoutingModule } from './data-configuration-routing.module';
import { ApiService } from '@services/api.service';
import { FormService } from '@services/form.service';
import { TicketService } from '@services/ticket.service';
import { GlobalErrorHandler } from '@services/globalErrorHandler';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatTabsModule } from '@angular/material/tabs';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatNativeDateModule } from '@angular/material/core';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { SharedModule } from '@shared/shared.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrModule } from 'ngx-toastr';
import { NgxSpinnerModule } from 'ngx-spinner';
import { MatExpansionModule } from '@angular/material/expansion';
import { ModalsModule } from 'src/app/modals/modals.module';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { createTranslateLoader } from 'src/app/app.module';
import { DataConfigurationComponent } from './data-configuration/data-configuration.component';
import { ConsentManagementListComponent } from './consent-management-list/consent-management-list.component';
import { LocationManagementComponent } from './location-management/location-management.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ZipCodeManagementComponent } from './location-management/zip-code-management/zip-code-management.component';
import { CreateConsentComponent } from './consent-management-list/create-consent/create-consent.component';
import { LocationService } from '@services/location.service';
import { LocationTableComponent } from './location-management/location-table/location-table.component';
import { CapitalfirstLetterPipe } from '@pipes/capitalfirst-letter.pipe';
import { JsonToCsvService } from '@services/json-to-csv.service';
import { TicketManagementComponent } from './ticket-management/ticket-management.component';
import { CreateSearchTicketComponent } from './ticket-management/create-search-ticket/create-search-ticket.component';
import { ReasonManagementComponent } from './reason-management/reason-management.component';
import { SearchReasonComponent } from './reason-management/search-reason/search-reason.component';
import { SystemCinfigurationsService } from '@services/system-cinfigurations.service';


@NgModule({
  declarations: [
    DataConfigurationComponent,
    ConsentManagementListComponent,
    LocationManagementComponent,
    CreateConsentComponent,
    ZipCodeManagementComponent,
    LocationTableComponent,
    CapitalfirstLetterPipe,
    TicketManagementComponent,
    CreateSearchTicketComponent,
    ReasonManagementComponent,
    SearchReasonComponent
  ],
  imports: [
    CommonModule,
    DataConfigurationRoutingModule,
    MatDatepickerModule,
    MatTabsModule,
    FormsModule,
    ReactiveFormsModule,
    MatNativeDateModule,
    NgxMaterialTimepickerModule,
    NgxMaterialTimepickerModule.setLocale('en-GB'),
    HttpClientModule,
    SharedModule,
    NgxPaginationModule,
    ToastrModule.forRoot(),
    NgxSpinnerModule,
    MatFormFieldModule,
    MatExpansionModule,
    ModalsModule,
    
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient],
      },
    }),
  ],
  providers: [
    ApiService,
    FormService,
    TicketService,
    LocationService,
    JsonToCsvService,SystemCinfigurationsService,
    { provide: ErrorHandler, useClass: GlobalErrorHandler }
  ]
})
export class DataConfigurationModule { }
