import { HttpParams } from '@angular/common/http';
import { Component, OnInit,Input,Output,EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { async } from '@angular/core/testing';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { CONFIG_KEYS, LOCATION_CSV_HEADER, LOCATION_DROPDOWN_FIELDS, LOCATION_TYPE, STATIC_DATA } from '@constant/constants';
import { TranslateService } from '@ngx-translate/core';
import { FormService } from '@services/form.service';
import { JsonToCsvService } from '@services/json-to-csv.service';
import { LocationService } from '@services/location.service';
import { UsersService } from '@services/users.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { elementAt } from 'rxjs/operators';

@Component({
  selector: 'app-zip-code-management',
  templateUrl: './zip-code-management.component.html',
  styleUrls: ['./zip-code-management.component.scss']
})
export class ZipCodeManagementComponent implements OnInit {
  @Input() createLocation;
  @Input() locationType;
  @Input() tableFilds;
  @Input() formFields;
  @Input() forminputs:any;
  @Input() edit:boolean=true;
  @Input() delete:boolean=true;
  @Input() export:boolean=true;
  showNoData: boolean = false;
  @Output() back = new EventEmitter<object>();
  firstPage:number=1;
  pageSet:number=1;
  paramsObj:any = {
    location: '',
    page: this.firstPage,
    limit:10,
    countryId:'',
    stateId:'',
    districtId:'',
    cityId:'',
    name:'',
    isActive:'',
    sortBy:'id:DESC' 
  };
  public locationParam: FormGroup;
  errorMsg: any = {};
  isSearchDone = false;
  locationList:any=[];
  locationDropDownList:any=[];
  totalItems;
  language:string='en';
  otherLanguage:any;
  // dropdown values
  country;
  state;
  district;
  city;

  isChecked = true;
//-----------------------------------------------------------------------
  formData; 
  Name=''  
  data:any = {
    name: '',
    countryId:'',
    stateId:'',
    cityId:'',
    districtId:'',
    code:'',
    isActive:null,
  }
  response:any=[];
//-----------------------------------------------------------------------

  constructor(
    private fb:FormBuilder,
    public translation: TranslateService,
    private formService: FormService,
    private spinner: NgxSpinnerService,
    private locationService: LocationService,
    private downloadCSVService: JsonToCsvService,
  )
   {
      translation.setDefaultLang('en');
      this.locationParam = this.fb.group({
      countryName: [''],
      // countryNameArabic: [''],
      countryNameDropdown: [''],
      stateName: [''],
      // stateNameArabic: [''],
      stateNameDropdown: [''],
      districtName: [''],
      // districtNameArabic: [''],
      districtNameDropdown: [''],
      cityName: [''],
      // cityNameArabic: [''],
      cityNameDropdown: [''], 
      zipcodeName: [''],
      status: ['100'],
      isChecked: ['true'],
      language:['en']
    });
  }

  async ngOnInit() {
    
    await this.validating();
    this.intializingMessage();
    await this.callConfiguration();
    this.paramsObj.location = this.locationType;

    this.formData = this.locationParam.value
    this.Name = this.locationType + 'Name' //to access form values
    
    this.data = {
      name: this.formData[this.Name],
      countryId:this.formData.countryNameDropdown,
      stateId:this.formData.stateNameDropdown,
      cityId:this.formData.cityNameDropdown,
      districtId:this.formData.districtNameDropdown,
      isActive:(this.createLocation ? (this.formData.isChecked? 1:0) :parseInt(this.formData.status)),
    }
    this.data["name_"+this.otherLanguage?.configurableValue?.abbreviation]=this.otherLanguage?.configurableValue?.title;
    
    // get country list for dropdown
    if(this.locationType != LOCATION_TYPE[0]){
      this.country =  await this.getLocationByType(LOCATION_TYPE[0])
    }
   
    
    this.paramsObj.limit =10;
    this.paramsObj.page = 1;
    this.firstPage = 1;
    this.pageSet = 1;
    this.paramsObj.location = this.locationType;
    if(this.createLocation == false){
      this.locationList = this.getLocation({})
    }
  }

  async callConfiguration(){
    const keyParam = {fields:CONFIG_KEYS.LANGUAGE_KEY}
    let param = new HttpParams();
    Object.keys(keyParam).forEach((key) => {
      if (keyParam[key] != "") {
        param = param.set(
          key,
          keyParam[key]
        );
      }
    });
    const res: any = await this.locationService.getConfig(param);
      if(res?.result){
        this.otherLanguage = res?.result[0];
      }
             
  }
  async pageChange(params){
    
    let bodyData = {
      searchValue:this.data.name,
      status:this.data.isActive,
      countryId:this.data.countryId,
      stateId:this.data.stateId,
      districtId:this.data.districtId,
      cityId:this.data.cityId
    }
    if(bodyData.status == 100){
      delete bodyData.status
    }
    bodyData = this.cleanObject(bodyData)
    this.paramsObj.page = params.page;
    this.firstPage = params.page;
    this.pageSet = params.page;
    this.paramsObj.limit = params.limit;
    params = this.generateQueryParams()
    const response:any = await this.locationService.getLocation(bodyData,params,this.locationType);
    this.totalItems = response.result.totalCount;
    
    if(this.locationType!=LOCATION_TYPE[4]){
      this.locationList = response.result[this.locationType].map(v => ({
        ...v, 
        edit: false, 
        status:v.isActive==1?true:false,
        oldEn:v.name,
        oldAr:v["name_" + this.otherLanguage.configurableValue.abbreviation],
        oldStatus:v.isActive==1?true:false
      }));
    }
    // location = zipcode
    else{
      this.locationList = response.result[this.locationType].map(v => ({
        ...v,
        edit: false, 
        status:v.isActive==1?true:false,
        name:v.code,
        ["name_"+this.otherLanguage.configurableValue.abbreviation]:v.code,
        oldEn:v.code,
        oldAr:v.code,
        oldStatus:v.isActive==1?true:false
      }));
      
    }
    //---------------------------
    
  }

  async listApiCall(obj){
    
    this.paramsObj.page = obj.page;
    this.firstPage = this.paramsObj.page;
    this.paramsObj.limit = obj.limit;

    let bodyData = {
      searchValue:this.data.name,
      status:this.data.isActive,
      countryId:this.data.countryId,
      stateId:this.data.stateId,
      districtId:this.data.districtId,
      cityId:this.data.cityId
    }

    bodyData = this.cleanObject(bodyData)
    if(bodyData.status==100){
      delete bodyData.status;
    }
    await this.getLocation(bodyData);
  }
  
  async getLocation(body,sort=false){
    let params = this.generateQueryParams()
    
    const response:any = await this.locationService.getLocation(body,params,this.locationType);  
    this.totalItems = response.result.totalCount;
    
    if(this.locationType!=LOCATION_TYPE[4]){
      this.locationList = response.result[this.locationType].map(v => ({
        ...v,
        edit: false, 
        status:v.isActive==1?true:false,
        oldEn:v.name,
        oldAr:v["name_" + this.otherLanguage.configurableValue.abbreviation],
        oldStatus:v.isActive==1?true:false
      }));
    }
    // location = zipcode
    else{
      this.locationList = response.result[this.locationType].map(v => ({
        ...v,
        edit: false, 
        status:v.isActive==1?true:false,
        oldEn:v.code,
        name:v.code,
        ["name_"+this.otherLanguage.configurableValue.abbreviation]:v.code,
        oldAr:v.code,
        oldStatus:v.isActive==1?true:false
      }));
    }
  }

  async getDropDownLocation(body,type){

    let url_params = {
      ...this.paramsObj
    };

    url_params.location = type;

    let params = new HttpParams();
    Object.keys(url_params).forEach((key) => {
      if(url_params[key]!=''){
        params = params.set(key, url_params[key]);
      }
    });

    const response:any = await this.locationService.getLocation(body,params,type);
    this.spinner.hide();

    this.locationDropDownList = response.result[type].map(v => ({
      ...v, 
      edit: false, 
      status:v.isActive==1?true:false,
      oldEn:v.name,
      oldAr:v["name_" + this.otherLanguage?.configurableValue?.abbreviation],
      oldStatus:v.isActive==1?true:false
    }));
  }

  async getLocationByType(type){
    this.paramsObj.location = type;
    this.paramsObj.page=''
    this.paramsObj.limit = ''

    let bodyData= {
      searchValue:'',
      status:1
    }

    bodyData = this.cleanObject(bodyData)

    let params = this.generateQueryParams()
    
    const response:any = await this.locationService.getLocation(bodyData,params,LOCATION_TYPE[0]);
    return response.result[type];  
  }

  async addNewLocation(data){

    let params = this.generateQueryParams()
    await this.locationService.addLocation(data,params);
    this.back.emit(); 
  }

  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }

  
  validating(){
        if(this.createLocation){
          if(this.forminputs.countryName){
            this.locationParam.controls['countryName'].setValidators([Validators.required,Validators.minLength(3),Validators.maxLength(256),Validators.pattern('[a-zA-Z\u0600-\u06FF ]*')]); 
            this.locationParam.controls['countryName'].updateValueAndValidity();
            // this.locationParam.controls['countryNameArabic'].setValidators([Validators.required,Validators.minLength(3),Validators.maxLength(256),Validators.pattern('[a-zA-Z\u0600-\u06FF ]*')]); 
            // this.locationParam.controls['countryNameArabic'].updateValueAndValidity();
            } 
          if(this.forminputs.countryNameDropdown){
            this.locationParam.controls['countryNameDropdown'].setValidators([Validators.required]); 
            this.locationParam.controls['countryNameDropdown'].updateValueAndValidity();
            } 
  
          if(this.forminputs.stateName){
              this.locationParam.controls['stateName'].setValidators([Validators.required,Validators.minLength(3),Validators.maxLength(256),Validators.pattern('[a-zA-Z\u0600-\u06FF ]*')]); 
              this.locationParam.controls['stateName'].updateValueAndValidity();
              // this.locationParam.controls['stateNameArabic'].setValidators([Validators.required,Validators.minLength(3),Validators.maxLength(256),Validators.pattern('[a-zA-Z\u0600-\u06FF ]*')]); 
              // this.locationParam.controls['stateNameArabic'].updateValueAndValidity();
              } 
          // if(this.forminputs.stateNameDropdown){
          //     this.locationParam.controls['stateNameDropdown'].setValidators([Validators.required]); 
          //     this.locationParam.controls['stateNameDropdown'].updateValueAndValidity();
          //     } 
          if(this.forminputs.districtName){
               this.locationParam.controls['districtName'].setValidators([Validators.required,Validators.minLength(3),Validators.maxLength(256),Validators.pattern('[a-zA-Z\u0600-\u06FF ]*')]); 
               this.locationParam.controls['districtName'].updateValueAndValidity();
              //  this.locationParam.controls['districtNameArabic'].setValidators([Validators.required,Validators.minLength(3),Validators.maxLength(256),Validators.pattern('[a-zA-Z\u0600-\u06FF ]*')]); 
              //  this.locationParam.controls['districtNameArabic'].updateValueAndValidity();
               } 
          // if(this.forminputs.districtNameDropdown){
          //      this.locationParam.controls['districtNameDropdown'].setValidators([Validators.required]); 
          //      this.locationParam.controls['districtNameDropdown'].updateValueAndValidity();
          //      } 
          if(this.forminputs.cityName){
               this.locationParam.controls['cityName'].setValidators([Validators.required,Validators.minLength(3),Validators.maxLength(256),Validators.pattern('[a-zA-Z\u0600-\u06FF ]*')]); 
               this.locationParam.controls['cityName'].updateValueAndValidity();
              //  this.locationParam.controls['cityNameArabic'].setValidators([Validators.required,Validators.minLength(3),Validators.maxLength(256),Validators.pattern('[a-zA-Z\u0600-\u06FF ]*')]); 
              //  this.locationParam.controls['cityNameArabic'].updateValueAndValidity();
               } 
          if(this.forminputs.cityNameDropdown){
               this.locationParam.controls['cityNameDropdown'].setValidators([Validators.required,]); 
               this.locationParam.controls['cityNameDropdown'].updateValueAndValidity();
               } 
          if(this.forminputs.zipCode){
               this.locationParam.controls['zipcodeName'].setValidators([Validators.required,Validators.minLength(5),Validators.maxLength(5),Validators.pattern("^[0-9]*$")]); 
               this.locationParam.controls['zipcodeName'].updateValueAndValidity();
               }
        } 
  }

  intializingMessage() {
    this.errorMsg.countryName = {
      pattern: this.translation.instant('ERR_MSG_CHARACTER_ONLY'),
    };
    // this.errorMsg.countryNameArabic = {
    //   pattern: this.translation.instant('ERR_MSG_CHARACTER_ONLY'),
    // };
    this.errorMsg.countryNameDropdown = {
      required: this.translation.instant('ERR_MSG_REQUIERD_NAME'),
    };

    this.errorMsg.stateName = {
      pattern: this.translation.instant('ERR_MSG_CHARACTER_ONLY'),
    };
    // this.errorMsg.stateNameArabic = {
    //   pattern: this.translation.instant('ERR_MSG_CHARACTER_ONLY'),
    // };
    this.errorMsg.stateNameDropdown = {
      required: this.translation.instant('ERR_MSG_REQUIERD_NAME'),
    };

    this.errorMsg.districtName = {
      pattern: this.translation.instant('ERR_MSG_CHARACTER_ONLY'),
    };
    // this.errorMsg.districtNameArabic = {
    //   pattern: this.translation.instant('ERR_MSG_CHARACTER_ONLY'),
    // };
    this.errorMsg.districtNameDropdown = {
      required: this.translation.instant('ERR_MSG_REQUIERD_NAME'),
    };

    this.errorMsg.cityName = {
      pattern: this.translation.instant('ERR_MSG_CHARACTER_ONLY'),
    };
    // this.errorMsg.cityNameArabic = {
    //   pattern: this.translation.instant('ERR_MSG_CHARACTER_ONLY'),
    // };
    this.errorMsg.cityNameDropdown = {
      required: this.translation.instant('ERR_MSG_REQUIERD_NAME'),
    };

    this.errorMsg.zipcodeName = {
      pattern: this.translation.instant('ERR_MSG_NUMBER_ONLY'),
      minlength: STATIC_DATA.ZIPCODE_MIN_VALIDATION,
      maxlength: STATIC_DATA.ZIPCODE_MIN_VALIDATION,
      required: "Zip Code"+STATIC_DATA.IS_REQUIRED,
    };
  }


  async onSubmit(showTable){
    this.formData = this.locationParam.value
   
      this.data = {
        name: this.formData[this.Name],
        countryId:this.formData.countryNameDropdown,
        stateId:this.formData.stateNameDropdown,
        cityId:this.formData.cityNameDropdown,
        districtId:this.formData.districtNameDropdown,
        isActive:(this.createLocation ? (this.formData.isChecked? 1:0) :parseInt(this.formData.status)),
      }
      this.data["name_"+this.otherLanguage?.configurableValue?.abbreviation]=this.formData[this.Name];
      
      // when click on "Search Button"
      if(showTable){
        this.showNoData = true;
        let bodyData = {
          searchValue:this.data.name,
          status:this.data.isActive,
          countryId:this.formData.countryNameDropdown,
          stateId:this.formData.stateNameDropdown,
          cityId:this.formData.cityNameDropdown,
          districtId:this.formData.districtNameDropdown,
        }
        bodyData = this.cleanObject(bodyData)
        if(bodyData.status == 100){
          delete bodyData.status
        }
        this.firstPage = 1;
        this.paramsObj.page = 1;
        this.pageSet = 1;
        await this.getLocation(bodyData);
        this.isSearchDone = true;
        
        this.createLocation = false;
      }
      // when click on "SAVE Button"
      else{
        this.locationParam.patchValue({
          [this.Name]:this.formData[this.Name].trim(),
          // [this.Name+"Arabic"]:this.formData[this.Name+"Arabic"].trim()
        });
        // if(this.locationParam.get(this.Name).value == ''){
        //   this.locationParam.patchValue({
        //     ['language']:'en',
        //   });
        // }else if(this.locationParam.get(this.Name+"Arabic").value == ''){
        //   this.locationParam.patchValue({
        //     ['language']:'ar',
        //   });
        // }
        
        this.formService.markFormGroupTouched(this.locationParam);
        if (this.locationParam.valid) {
          this.data.name =this.formData[this.Name].trim();
          this.data["name_" + this.otherLanguage?.configurableValue?.abbreviation] =this.formData[this.Name].trim()
          this.paramsObj.id= ''
          this.paramsObj.page=''
          this.paramsObj.limit=''
          if(this.locationType==LOCATION_TYPE[4]){
            this.data.code = this.data.name
          }
          this.data = this.cleanObject(this.data);
          this.addNewLocation(this.data)
          await this.getLocation({},true)
        }
      }
   
  }

  // on select any coutry,state,district,city dropdown
  async onSelectDropdown(field, type){
    if((field==LOCATION_DROPDOWN_FIELDS[0] )|| (this.locationParam.get(LOCATION_DROPDOWN_FIELDS[0]).value=='')){
      this.state = null;
      this.district = null;
      this.city = null;
      this.locationParam.patchValue({
        stateNameDropdown:'',
        districtNameDropdown:'',
        cityNameDropdown:''
      })
      if(this.locationParam.get(LOCATION_DROPDOWN_FIELDS[0]).value==''){
        return;
      }
      
    }
    else if((field==LOCATION_DROPDOWN_FIELDS[1] || (this.locationParam.get(LOCATION_DROPDOWN_FIELDS[1]).value=='') && field!=LOCATION_DROPDOWN_FIELDS[3])){
      this.district = null;
      this.city = null;
      this.locationParam.patchValue({
        districtNameDropdown:'',
        cityNameDropdown:''
      })
      if(this.locationParam.get(LOCATION_DROPDOWN_FIELDS[1]).value==''){
        return;
      }
    }
    

    
    this.paramsObj.id = this.locationParam.controls[field].value
    this.paramsObj.page = '',
    this.paramsObj.limit = ''

    this.formData = this.locationParam.value 
    this.data = {
      name: this.formData[this.Name],
      countryId:this.formData.countryNameDropdown,
      stateId:this.formData.stateNameDropdown,
      cityId:this.formData.cityNameDropdown,
      districtId:this.formData.districtNameDropdown,
      isActive:(this.createLocation ? (this.formData.isChecked? 1:0) :parseInt(this.formData.status)),
    }
    this.data["name_"+this.otherLanguage?.configurableValue?.abbreviation]=this.otherLanguage?.configurableValue?.title;

    let bodyData = {
      searchValue:'',
      status:1,
      countryId:this.formData.countryNameDropdown,
      stateId:this.formData.stateNameDropdown,
      cityId:this.formData.cityNameDropdown,
      districtId:this.formData.districtNameDropdown,
    }

    bodyData = this.cleanObject(bodyData)
    

    if(type==LOCATION_TYPE[1]){
      delete bodyData.stateId;
      delete bodyData.districtId;
      delete bodyData.cityId;
    }
    if(type==LOCATION_TYPE[2]){
      delete bodyData.districtId;
      delete bodyData.cityId;
    }
    if(type==LOCATION_TYPE[3]){
      delete bodyData.cityId;
    }

    
    await this.getDropDownLocation(bodyData,type)

    // will assign api data to respected dropdowns
    if(type=='country'){
      this.country = this.locationDropDownList;
    }
    else if(type=='state'){
      this.state = this.locationDropDownList;
      if(this.locationType != LOCATION_TYPE[1] && this.locationType != LOCATION_TYPE[2]){
        await this.getDropDownLocation(bodyData,LOCATION_TYPE[3]);
      this.city = this.locationDropDownList;
      await this.getDropDownLocation(bodyData,LOCATION_TYPE[2]);
      this.district = this.locationDropDownList;
      }
    }
    else if(type=='district'){
      this.district = this.locationDropDownList;
      if(this.locationType != LOCATION_TYPE[2]){
        await this.getDropDownLocation(bodyData,LOCATION_TYPE[3]);
        this.city = this.locationDropDownList;
      }
    }
    else if(type=='city'){
        this.city = this.locationDropDownList;
    }
  }

  selectCity(){
    setTimeout(async () => {
    const cityID =this.locationParam.get(LOCATION_DROPDOWN_FIELDS[3]).value;
    var stateId;
    await this.city.find(function (element) {
      if(element.id == cityID){
        stateId = element.stateId;
      }
    });
   
    // if(this.locationParam.value.stateNameDropdown != stateId){
    //   let bodyData = {
    //     searchValue:'',
    //     status:1,
    //     countryId:this.formData.countryNameDropdown,
    //     stateId:stateId,
    //   }
    //   await this.getDropDownLocation(bodyData,'district');
    //   this.district = this.locationDropDownList;
    // }
   let stateSelect=false;
    await this.state.find(function (element) {
      if(element.id == stateId){
        stateSelect = true;
      }
    });
    setTimeout(async () => {
      if(stateSelect == true){
        this.locationParam.patchValue({
          stateNameDropdown:stateId,
        })
      }
    });
    });
  }
  selectDistrict(){
    setTimeout(async () => {
    const districtID =this.locationParam.get(LOCATION_DROPDOWN_FIELDS[2]).value;
    var stateId;
    await this.district.find(function (element) {
      if(element.id == districtID){
        stateId = element.stateId;
      }
    });
    await this.state.find(function (element) {
      if(element.id == stateId){
        this.locationParam.patchValue({
          stateNameDropdown:stateId,
        })
      }
    });
   
    });
  }

  // remove unneccery data from object
  cleanObject(obj) {
    for (var propName in obj) {
      if (obj[propName] === '' || obj[propName] === undefined) {
        delete obj[propName];
      }
    }
    return obj
  }

  // generates query params
  generateQueryParams(){
    this.paramsObj = this.cleanObject(this.paramsObj)
    let params = new HttpParams();
    Object.keys(this.paramsObj).forEach((key) => {
      if(this.paramsObj[key]!=''){
        params = params.set(key, this.paramsObj[key]);
      }
    });
    return params;
  }



  resetForm(){
    this.locationParam.reset()
    this.locationParam.patchValue({
      status:'100', 
      countryNameDropdown:'',
      stateNameDropdown:'',
      districtNameDropdown:'',
      cityNameDropdown:'',
      language:'en'
    })
    this.data = {
      name: '',
      countryId:'',
      stateId:'',
      cityId:'',
      districtId:'',
      code:'',
      isActive:null,
    }
    this.data["name_"+this.otherLanguage?.configurableValue?.abbreviation]='';
    this.showNoData = false

    this.state = null
    this.city = null
    this.district = null
    this.showNoData = false
    this.changeLanguage('en');
    if(this.isSearchDone){
      
      this.locationList = this.getLocation({})
      this.isSearchDone = false;
    }
  }



  async downloadCSV(){
    let header = LOCATION_CSV_HEADER[this.locationType]

    let bodyData = {
      searchValue:this.data.name,
      status:this.data.isActive,
      countryId:this.formData.countryNameDropdown,
      stateId:this.formData.stateNameDropdown,
      cityId:this.formData.cityNameDropdown,
      districtId:this.formData.districtNameDropdown,
    }
    bodyData = this.cleanObject(bodyData)

    if(bodyData.status == 100){
      delete bodyData.status
    }

    const url_params = {
      location:this.locationType
    }

    let params = new HttpParams();
    Object.keys(url_params).forEach((key) => {
      if(url_params[key]!=''){
        params = params.set(key, url_params[key]);
      }
    });

     this.response = await this.locationService.getLocation(bodyData,params,this.locationType);
     const csvData = this.response.result[this.locationType];

    let data;
    const otherNameCountryKey = "OtherLanguageCountry("+this.otherLanguage.configurableValue.title.charAt(0).toUpperCase() + this.otherLanguage.configurableValue.title.slice(1)+")";
    const otherNameStateKey = "OtherLanguageState("+this.otherLanguage.configurableValue.title.charAt(0).toUpperCase() + this.otherLanguage.configurableValue.title.slice(1)+")";
    const otherNameDistrictKey = "OtherLanguageDistrict("+this.otherLanguage.configurableValue.title.charAt(0).toUpperCase() + this.otherLanguage.configurableValue.title.slice(1)+")";
    const otherNameCityKey = "OtherLanguageCity("+this.otherLanguage.configurableValue.title.charAt(0).toUpperCase() + this.otherLanguage.configurableValue.title.slice(1)+")";
    
    if(this.locationType== LOCATION_TYPE[0]){
      
      data = csvData.map(v => ({
        Country:v.name,
        [otherNameCountryKey]:v["name_"+ this.otherLanguage?.configurableValue?.abbreviation],
        Status:v.isActive==1?'Active':'InActive'
      }));

      
    }

    if(this.locationType==LOCATION_TYPE[1]){
      data = csvData.map(v => ({
        State:v.name,
        [otherNameStateKey]:v["name_"+this.otherLanguage?.configurableValue?.abbreviation],
        Country:v.country.name,
        [otherNameCountryKey]:v.country["name_"+this.otherLanguage?.configurableValue?.abbreviation],
        Status:v.isActive==1?'Active':'InActive'
      }));
    }

    if(this.locationType==LOCATION_TYPE[2]){
      data = csvData.map(v => ({
        District:v.name,
        [otherNameDistrictKey]:v["name_"+this.otherLanguage?.configurableValue?.abbreviation],
        State:v.state.name,
        [otherNameStateKey]:v.state["name_"+this.otherLanguage?.configurableValue?.abbreviation],
        Country:v.country.name,
        [otherNameCountryKey]:v.country["name_"+this.otherLanguage?.configurableValue?.abbreviation],
        Status:v.isActive==1?'Active':'InActive'
      }));
    }

    if(this.locationType==LOCATION_TYPE[3]){
      data = csvData.map(v => ({
        City:v.name,
        [otherNameCityKey]:v["name_"+this.otherLanguage?.configurableValue?.abbreviation],
        District:v.district.name,
        [otherNameDistrictKey]:v.district["name_"+this.otherLanguage?.configurableValue?.abbreviation],
        State:v.state.name,
        [otherNameStateKey]:v.state["name_"+this.otherLanguage?.configurableValue?.abbreviation],
        Country:v.country.name,
        [otherNameCountryKey]:v.country["name_"+this.otherLanguage?.configurableValue?.abbreviation],
        Status:v.isActive==1?'Active':'InActive'
      }));
    }

    if(this.locationType==LOCATION_TYPE[4]){
      data = csvData.map(v => ({
        Zipcode:v.code,
        CityId:v.id,
        City:v.city.name,
        [otherNameCityKey]:v.city["name_"+this.otherLanguage?.configurableValue?.abbreviation],
        District:v.district.name,
        [otherNameDistrictKey]:v.district["name_"+this.otherLanguage?.configurableValue?.abbreviation],
        State:v.state.name,
        [otherNameStateKey]:v.state["name_"+this.otherLanguage?.configurableValue?.abbreviation],
        Country:v.country.name,
        [otherNameCountryKey]:v.country["name_"+this.otherLanguage?.configurableValue?.abbreviation],
        Status:v.isActive==1?'Active':'InActive'
      }));
    }
    
    this.downloadCSVService.downloadFile(header,data, this.locationType+'Data',true);
  }

  checkInput(event){
    if(event.code === "Space" && this.locationParam.value.countryName.trim().length === 0){
      setTimeout(() => {
        this.locationParam.patchValue({["countryName"]: "" });
      });
    }else if(event.type === "paste"){
      setTimeout(() => {
        if(this.locationParam.value.countryName.trim().length === 0){
          this.locationParam.patchValue({["countryName"]: "" });
        }
      });
    } 

    // if(event.code === "Space" && this.locationParam.value.countryNameArabic.trim().length === 0){
    //   setTimeout(() => {
    //     this.locationParam.patchValue({["countryNameArabic"]: "" });
    //   });
    // }else if(event.type === "paste"){
    //   setTimeout(() => {
    //     if(this.locationParam.value.countryNameArabic.trim().length === 0){
    //       this.locationParam.patchValue({["countryNameArabic"]: "" });
    //     }
    //   });
    // } 

    if(event.code === "Space" && this.locationParam.value.stateName.trim().length === 0){
      setTimeout(() => {
        this.locationParam.patchValue({["stateName"]: "" });
      });
    }else if(event.type === "paste"){
      setTimeout(() => {
        if(this.locationParam.value.stateName.trim().length === 0){
          this.locationParam.patchValue({["stateName"]: "" });
        }
      });
    } 

    // if(event.code === "Space" && this.locationParam.value.stateNameArabic.trim().length === 0){
    //   setTimeout(() => {
    //     this.locationParam.patchValue({["stateNameArabic"]: "" });
    //   });
    // }else if(event.type === "paste"){
    //   setTimeout(() => {
    //     if(this.locationParam.value.stateNameArabic.trim().length === 0){
    //       this.locationParam.patchValue({["stateNameArabic"]: "" });
    //     }
    //   });
    // } 

    if(event.code === "Space" && this.locationParam.value.districtName.trim().length === 0){
      setTimeout(() => {
        this.locationParam.patchValue({["districtName"]: "" });
      });
    }else if(event.type === "paste"){
      setTimeout(() => {
        if(this.locationParam.value.districtName.trim().length === 0){
          this.locationParam.patchValue({["districtName"]: "" });
        }
      });
    } 

    // if(event.code === "Space" && this.locationParam.value.districtNameArabic.trim().length === 0){
    //   setTimeout(() => {
    //     this.locationParam.patchValue({["districtNameArabic"]: "" });
    //   });
    // }else if(event.type === "paste"){
    //   setTimeout(() => {
    //     if(this.locationParam.value.districtNameArabic.trim().length === 0){
    //       this.locationParam.patchValue({["districtNameArabic"]: "" });
    //     }
    //   });
    // } 

    if(event.code === "Space" && this.locationParam.value.cityName.trim().length === 0){
      setTimeout(() => {
        this.locationParam.patchValue({["cityName"]: "" });
      });
    }else if(event.type === "paste"){
      setTimeout(() => {
        if(this.locationParam.value.cityName.trim().length === 0){
          this.locationParam.patchValue({["cityName"]: "" });
        }
      });
    } 

    // if(event.code === "Space" && this.locationParam.value.cityNameArabic.trim().length === 0){
    //   setTimeout(() => {
    //     this.locationParam.patchValue({["cityNameArabic"]: "" });
    //   });
    // }else if(event.type === "paste"){
    //   setTimeout(() => {
    //     if(this.locationParam.value.cityNameArabic.trim().length === 0){
    //       this.locationParam.patchValue({["cityNameArabic"]: "" });
    //     }
    //   });
    // } 

    if(event.code === "Space" && this.locationParam.value.zipcodeName.trim().length === 0){
      setTimeout(() => {
        this.locationParam.patchValue({["zipcodeName"]: "" });
      });
    }else if(event.type === "paste"){
      setTimeout(() => {
        if(this.locationParam.value.zipcodeName.trim().length == 0){
          this.locationParam.patchValue({["zipcodeName"]: "" });
        }
      });
    } 
    
  }

  changeLanguage(type){
    if(type == this.otherLanguage?.configurableValue?.abbreviation){
    
     const index = this.tableFilds.indexOf(this.locationType.charAt(0).toUpperCase() + this.locationType.slice(1) + '(EN)');
      if (index == -1) { 
        this.tableFilds.splice(2, 0, this.locationType.charAt(0).toUpperCase() + this.locationType.slice(1) + '(EN)');
      }
    }else{
      const index = this.tableFilds.indexOf(this.locationType.charAt(0).toUpperCase() + this.locationType.slice(1) + '(EN)');
      if (index > -1) { 
        this.tableFilds.splice(index, 1); 
      }
    }
   this.locationParam.patchValue({["language"]: type });
    this.language = type;
  }

}
