import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { LOCATION_TYPE, MODAL_NOTES, STATIC_DATA } from 'src/app/constants/constants';
import { ConfirmationModalComponent } from 'src/app/modals/confirmation-modal/confirmation-modal.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { TranslateService } from '@ngx-translate/core';
import { LocationService } from '@services/location.service';
import { HttpParams } from '@angular/common/http';


@Component({
  selector: 'app-location-table',
  templateUrl: './location-table.component.html',
  styleUrls: ['./location-table.component.scss']
})
export class LocationTableComponent implements OnInit {
  @Input() tableData:any;
  @Input() tableFilds;
  @Input() locationType;
  @Input() showNoData;
  @Input() totalItems: any;
  @Input() title;
  @Input() language:string;
  @Input() otherLanguage:any;
  @Input() firstPage: number;
  @Input() edit:boolean=true;
  @Input() delete:boolean=true;
  @Input() export:boolean=true;
  @Output() pageChange = new EventEmitter<object>();
  @Output() listApiCall = new EventEmitter<object>();
  @Output() generateExcelFile = new EventEmitter<object>();
  paramsObj:any = {};
  tablePageLimit = 10;
  editModeOn = false;
  isChecked = false;
  emptyValueError = false;
  locationTab = LOCATION_TYPE;
  validationErrorMessage='';
  currentEditObj = null
  modalRef: BsModalRef;
  constructor( private modalService: BsModalService,
    private translate: TranslateService,
    private locationService:LocationService) { }

  tableFields=["Sr. No","City Name","State / Province Name","Country / Region Name","Status","Action"]

  
  ngOnInit(): void {
    
    this.paramsObj = {
      location: '',
      id:'',
      page:this.firstPage,
      limit:10
    };
      this.paramsObj.location = this.locationType;  
  }

  @Input() 
    public set pageSet(page: any) {
      this.firstPage = page;
    }

  changePage(event){
    this.editModeOn = false;
    this.currentEditObj = null;
    this.paramsObj.page = event;
    this.firstPage = event;
    this.pageChange.emit(this.paramsObj);
  }
  
  editLocation(i){
    if(this.currentEditObj){
      this.dontSaveLocation(this.currentEditObj);
    }
    this.currentEditObj = i;
    this.tableData[i].edit=true;
    this.tableData = this.tableData.map(function(item,ind){
      if(i!==ind){
        item.edit=false;
      }
      return item;
    });


    
    // if(!this.editModeOn){
      
      // this.editModeOn = !this.editModeOn
      this.isChecked = this.tableData[i].status=='enabled';
    // }
  }
  
  deleteLocation(i){

    if(this.currentEditObj){
      this.dontSaveLocation(this.currentEditObj);
    }
    let title= this.title.charAt(0).toUpperCase() + this.title.slice(1);
    const initialState = {
      title:STATIC_DATA.DELETE+title,
      confirmText:STATIC_DATA.DELETE_MSG + title.toLowerCase() + "?",
    };
    this.modalRef = this.modalService.show(ConfirmationModalComponent, {
      class: "modal-sm modal-dialog-centered custom-modal",
      backdrop: 'static',
      initialState,
    });
    this.modalRef.content.closeButtonText = this.translate.instant('MODAL.NO_BTN');
    this.modalRef.content.acceptButtonText = this.translate.instant('MODAL.YES_BTN');
    this.modalRef.content.response.subscribe(async (result) => {
      if (result) {
        const param = {
          location:this.locationType,
          id : this.tableData[i].id
        }
        
        let params = new HttpParams();
        Object.keys(param).forEach((key) => {
          params = params.set(key, param[key]);
        });

        const obj = { 
          id:this.tableData[i].id
        };
        const response:any = await this.locationService.deleteLocation(obj,params);
        
        //if page has only  one row
        if(this.tableData.length==1){
          
            this.paramsObj.page = this.paramsObj.page-1;
            this.firstPage = this.paramsObj.page;
            this.listApiCall.emit(this.paramsObj)
            
        }
        else{
          this.listApiCall.emit(this.paramsObj)
        }
      }
      else {
        
      }
    });
  }

  async saveLocation(i,event){
    if(event != ''){
      event.stopPropagation();
    }
    this.tableData[i].isActive = this.tableData[i].status ? 1:0;

    let params = new HttpParams();
    Object.keys(this.paramsObj).forEach((key) => {
      params = params.set(key, this.paramsObj[key]);
    });

    if(this.locationType!=LOCATION_TYPE[4]){
      if((this.language == 'en' && /\d/.test(this.tableData[i].name.trim()))|| (this.language == this.otherLanguage?.configurableValue?.abbreviation && /\d/.test(this.tableData[i]["name_" +this.otherLanguage?.configurableValue?.abbreviation].trim()))){
        this.validationErrorMessage = STATIC_DATA.CHAR_AND_SPACE_ALLOWED; 
        this.emptyValueError =true;
      }
      else if((this.language == 'en' && this.tableData[i].name.trim().length < 3) || (this.language == this.otherLanguage?.configurableValue?.abbreviation && this.tableData[i]["name_" + this.otherLanguage?.configurableValue?.abbreviation].trim().length < 3)){
        this.validationErrorMessage = STATIC_DATA.LOCATION_INPUTE_MIN_VALIDATION; 
        this.emptyValueError =true;
      }
      else if((this.language == 'en' &&this.tableData[i].name.trim().length > 256) || (this.language == this.otherLanguage?.configurableValue?.abbreviation &&this.tableData[i].name.trim().length > 256)){
        this.validationErrorMessage = STATIC_DATA.LOCATION_INPUTE_MAX_VALIDATION; 
        this.emptyValueError =true;
      }
      else{
        this.emptyValueError =false;
      }
      if(this.emptyValueError){
        this.tableData[i].name = this.tableData[i].name.trim();
        return;
      }
    }
    else if(this.locationType==LOCATION_TYPE[4]){
      if(!this.isNumeric(this.tableData[i].name.trim())){
        this.validationErrorMessage = STATIC_DATA.ONLY_NUMBERS_ALLOWED; 
        this.emptyValueError =true;
      }
      else if(this.tableData[i].name.trim().length < 5){
        this.validationErrorMessage = STATIC_DATA.ZIPCODE_MIN_VALIDATION; 
        this.emptyValueError =true;
      }
      else if(this.tableData[i].name.trim().length > 5){
        this.validationErrorMessage = STATIC_DATA.ZIPCODE_MIN_VALIDATION; 
        this.emptyValueError =true;
      }
      else{
        this.emptyValueError =false;
      }
      if(this.emptyValueError){
        this.tableData[i].name=this.tableData[i].name.trim();
        return;
      }
    }

    if(this.locationType==LOCATION_TYPE[4]){
      this.tableData[i].code = Number(this.tableData[i].name)
    }

    let obj;
    if(this.locationType!==LOCATION_TYPE[4]){
      obj = {
        id:this.tableData[i].id,
        ["name_"+this.otherLanguage?.configurableValue?.abbreviation]: this.tableData[i]["name_" +this.otherLanguage?.configurableValue?.abbreviation].trim(),
        isActive : this.tableData[i].status ? 1:0,
        name:this.tableData[i].name.trim()
      } 
    }
    else{
      obj = {
        id:this.tableData[i].id,
        ["name_"+this.otherLanguage?.configurableValue?.abbreviation]: this.tableData[i]["name_" +this.otherLanguage?.configurableValue?.abbreviation].trim(),
        isActive : this.tableData[i].status ? 1:0,
        code:this.tableData[i].name.trim()
      } 
    }
    if(this.tableData[i].name == this.tableData[i].oldEn && this.tableData[i].status == this.tableData[i].oldStatus && this.tableData[i]["name_" +this.otherLanguage?.configurableValue?.abbreviation] == this.tableData[i].oldAr){    
      this.dontSaveLocation(i);
      return;
    } else{
      if(this.tableData[i].name == this.tableData[i].oldEn){
        if(this.locationType!==LOCATION_TYPE[4]){
          delete obj.name;
        }
        else{
          delete obj.code;
        }
      }else if( obj.name == ""){
        delete obj.name;
      }
      if(this.tableData[i].status == this.tableData[i].oldStatus){
        delete obj.isActive;
      }
      if(this.tableData[i]["name_" + this.otherLanguage?.configurableValue?.abbreviation] == this.tableData[i].oldAr){
        delete obj["name_"+this.otherLanguage?.configurableValue?.abbreviation];
      }
      if(obj.name || obj["name_"+this.otherLanguage?.configurableValue?.abbreviation] || obj.code || obj.isActive == 0 || obj.isActive == 1){
        const response:any = await this.locationService.editLocation(obj,params);
        this.listApiCall.emit(this.paramsObj)
      }
    
      this.tableData[i].edit = false
      this.editModeOn = false
    }
    
  }

  dontSaveLocation(i){
    this.emptyValueError=false;
    this.tableData[i].name = this.tableData[i].oldEn;
    this.tableData[i]["name_" + this.otherLanguage?.configurableValue?.abbreviation] = this.tableData[i].oldAr;
    this.tableData[i].status = this.tableData[i].oldStatus;
    
    this.tableData[i].edit = false
    // this.editModeOn = false

  }

  
  func(data){
    data.status = !data.status
  }

  isEnabled(status){
    if(status=='enabled'){
      this.isChecked = true;
    }
    else{
      this.isChecked=false
    }
    return this.isChecked
  }

  generateExcel() {
    this.generateExcelFile.emit()
    // this.locationService.generateExcel();
   }

   cleanObject(obj) {
    for (var propName in obj) {
      if (obj[propName] === '' || obj[propName] === undefined) {
        delete obj[propName];
      }
    }
    return obj
  }

  dropdownPages = [5,10,15,25,50,100]

  tablelimit(event){  
    this.firstPage=1;
    this.paramsObj.page=1;
    this.tablePageLimit=event.target.value;
    this.paramsObj.limit=event.target.value;
    this.listApiCall.emit(this.paramsObj);
  }

  isNumeric(num){
    return !isNaN(num)
  }

  onKeydown(event,i) {
    if (event.key === "Enter") {
      this.saveLocation(i,'')
    }
  }

}


