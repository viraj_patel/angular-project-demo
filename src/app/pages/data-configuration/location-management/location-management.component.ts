import { HttpParams } from '@angular/common/http';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { Router } from '@angular/router';
import { JsonToCsvService } from '@services/json-to-csv.service';
import { JsonToXlsxService } from '@services/json-to-xlsx.service';
import { LocalStorageService } from '@services/localService/localStorage.service';
import { LocationService } from '@services/location.service';
import { StaffService } from '@services/staff.service';
import { UsersService } from '@services/users.service';
import { Subscription } from 'rxjs';
import { elementAt } from 'rxjs/operators';
import { DATA_CONFIGURATION_TABS_LIST, LOCAL_STORAGE, LOCATION_PREREQUISITES, LOCATION_ROUTE, LOCATION_TABLE, LOCATION_TABS, LOCATION_TABS_LIST, LOCATION_TYPE, TABLE_DATA_TYPE, TAB_FUNCTION } from 'src/app/constants/constants';
@Component({
  selector: 'app-location-management',
  templateUrl: './location-management.component.html',
  styleUrls: ['./location-management.component.scss']
})
export class LocationManagementComponent implements OnInit, OnDestroy {
  createLocation:boolean=false;
  location_table:boolean=false;
  dataConfigTab=DATA_CONFIGURATION_TABS_LIST;
  uploadCsv:boolean=false;
  tabs = LOCATION_TYPE
  activeTab = 0;
  uploadUrl = LOCATION_ROUTE.FILE_UPLOAD;
  tabHeads:Array<any> = LOCATION_TABS;
  public tableData: any;
  public prerequisitiesData: any;
  paramsObj = null;
  params = null;
  downloadSampleList;
  edit:boolean=true;
  delete:boolean=true;
  export:boolean=true;
  import:boolean=true;
  create:boolean=true;
  csvUploadTitles = {
    sampleCsv : "LOCATION_SAMPLE_TITLE",
    preRequisites:"LOCATION_PREREQUISITES_FORMAT_TITLE",
    csvFormat:"LOCATION_CSV_FORMAT_TITLE",
  }
  obs: Subscription;

  formFields = {
    0:["countryName"],
    1:["countryNameDropdown","stateName"],
    2:["countryNameDropdown","stateNameDropdown","districtName"],
    3:["countryNameDropdown","stateNameDropdown","districtNameDropdown","cityName"],
    4:["countryNameDropdown","stateNameDropdown","districtNameDropdown","cityNameDropdown","zipCode"],
  }

  tableFields = {
    0:["Sr. No","Country","Created Date","Updated Date","Status","Action"],
    1:["Sr. No","State","Country","Created Date","Updated Date","Status","Action"],
    2:["Sr. No","District","State","Country","Created Date","Updated Date","Status","Action"],
    3:["Sr. No","City","District","State", "Country","Created Date","Updated Date","Status","Action"],
    4:["Sr. No","Zip Code","City","District","State","Country","Created Date","Updated Date","Status","Action"],
  }

  forminputs = {
    0:{countryName:true,countryNameDropdown:false,stateName:false,stateNameDropdown:false,districtName:false,districtNameDropdown:false,cityName:false,cityNameDropdown:false,zipCode:false},
    1:{countryName:false,countryNameDropdown:true,stateName:true,stateNameDropdown:false,districtName:false,districtNameDropdown:false,cityName:false,cityNameDropdown:false,zipCode:false},
    2:{countryName:false,countryNameDropdown:true,stateName:false,stateNameDropdown:true,districtName:true,districtNameDropdown:false,cityName:false,cityNameDropdown:false,zipCode:false},
    3:{countryName:false,countryNameDropdown:true,stateName:false,stateNameDropdown:true,districtName:false,districtNameDropdown:true,cityName:true,cityNameDropdown:false,zipCode:false},
    4:{countryName:false,countryNameDropdown:true,stateName:false,stateNameDropdown:true,districtName:false,districtNameDropdown:true,cityName:false,cityNameDropdown:true,zipCode:true},
  }
  view:boolean=false;

  constructor(
    private router:Router,
    private userService:UsersService,
    private locationService:LocationService,
    private jsonToCsvService:JsonToCsvService,
    private localStorage: LocalStorageService,
    private rolesService : StaffService,
    private jsonToXlsx : JsonToXlsxService) { }

  async ngOnInit(){
    const url = this.router.url;
    const path = url;
    const route = `/${path.split('/')[3]}`;
    const currentRoute = route.split('?')[0].replace('/','');
        const permissions = await this.rolesService.getLoginPermissionsForModule(currentRoute);
        if(permissions.length > 0){
          if(permissions[0].View == false){
            this.router.navigate(['/main-home/my-profile']);
          }else{
            this.view = true;
          }
        }else{
          this.view = true;
        }
    this.obs = this.userService
    .getLocationTab()
    .subscribe((flag) => {
      this.location_table = false;
      this.uploadCsv = false;
      setTimeout(async () => {
       if(flag.tab == ""){
        this.router.navigate(['/main-home/my-profile']);
       }
        if (flag.tab == LOCATION_TABS_LIST.COUNTRY) {
          this.activeTab = 0;
          const permissions = await this.rolesService.getLoginPermissionsForModule(flag.tab);
          if(permissions.length > 0){
            this.create = permissions[0].Create;
            this.edit = permissions[0].Edit;
            this.delete = permissions[0].Delete;
            this.export = permissions[0].Export;
            this.import = permissions[0].Import;
          }
        }
        if (flag.tab == LOCATION_TABS_LIST.STATE) {
          this.activeTab = 1;
          const permissions = await this.rolesService.getLoginPermissionsForModule(flag.tab);
          if(permissions.length > 0){
            this.create = permissions[0].Create;
            this.edit = permissions[0].Edit;
            this.delete = permissions[0].Delete;
            this.export = permissions[0].Export;
            this.import = permissions[0].Import;
          }
        }
        if (flag.tab == LOCATION_TABS_LIST.DISTRICT) {
          this.activeTab = 2;
          const permissions = await this.rolesService.getLoginPermissionsForModule(flag.tab);
          if(permissions.length > 0){
            this.create = permissions[0].Create;
            this.edit = permissions[0].Edit;
            this.delete = permissions[0].Delete;
            this.export = permissions[0].Export;
            this.import = permissions[0].Import;
          }
        }
        if (flag.tab == LOCATION_TABS_LIST.CITY) {
          this.activeTab = 3;
          const permissions = await this.rolesService.getLoginPermissionsForModule(flag.tab);
          if(permissions.length > 0){
            this.create = permissions[0].Create;
            this.edit = permissions[0].Edit;
            this.delete = permissions[0].Delete;
            this.export = permissions[0].Export;
            this.import = permissions[0].Import;
          }
        }
        if (flag.tab == LOCATION_TABS_LIST.ZIP) {
          this.activeTab = 4; 
          const permissions = await this.rolesService.getLoginPermissionsForModule(flag.tab);
          if(permissions.length > 0){
            this.create = permissions[0].Create;
            this.edit = permissions[0].Edit;
            this.delete = permissions[0].Delete;
            this.export = permissions[0].Export;
            this.import = permissions[0].Import;
          }
        }
        
        this.activeFuction();

        this.paramsObj = {
          location:this.tabs[this.activeTab]
        }
        
        this.params = new HttpParams();
        Object.keys(this.paramsObj).forEach((key) => {
          if(this.paramsObj[key]!=''){
            this.params = this.params.set(key, this.paramsObj[key]);
          }
        });
        this.downloadSampleList = await this.downloadSampleFunction()

        this.table_data();
        this.prerequisities_Data();
        
      });
     
     
    });
    // this.paramsObj = {
    //   location:this.tabs[this.activeTab]
    // }
    
    // this.params = new HttpParams();
    // Object.keys(this.paramsObj).forEach((key) => {
    //   if(this.paramsObj[key]!=''){
    //     this.params = this.params.set(key, this.paramsObj[key]);
    //   }
    // });

    // this.downloadSampleList = this.downloadSampleFunction()
    // this.table_data();
    // this.prerequisities_Data();
  }
  ngOnDestroy() {
    this.obs.unsubscribe();
  }
 
  async downloadSampleFunction(){
    return await this.locationService.downloadSampleCsv(this.params);
  }

  async activeFuction(){
    const active_tab_function: any = await this.localStorage.getDataFromIndexedDB(LOCAL_STORAGE.DATA_ACTIVE_FUNCTION);
    const url = this.router.url;
    const path = url;
    const route = `/${path.split('/')[3]}`;
    const currentRoute = route.split('?')[0].replace('/','');
    if(active_tab_function == TAB_FUNCTION.CREATE && currentRoute == this.dataConfigTab.LOCATION){
      this.showCreateForm();
    }else if(active_tab_function == TAB_FUNCTION.UPLOAD && currentRoute == this.dataConfigTab.LOCATION){
      this.showCsvUpload();
    }else if(active_tab_function == TAB_FUNCTION.LIST && currentRoute == this.dataConfigTab.LOCATION){
      this.createLocation= false;
      this.location_table = true;
      await this.localStorage.setDataInIndexedDB(
        LOCAL_STORAGE.DATA_ACTIVE_FUNCTION,
        TAB_FUNCTION.LIST
      );
    }
  }


  async showCreateForm(){
    this.uploadCsv = false
    this.location_table = false;
    setTimeout(() => {
      this.createLocation= true;
      this.location_table = true;
    });
    await this.localStorage.setDataInIndexedDB(
      LOCAL_STORAGE.DATA_ACTIVE_FUNCTION,
      TAB_FUNCTION.CREATE
    );
  }

  async hideCreateForm(){
    this.location_table = false;
    setTimeout(() => {
      this.createLocation= false;
      this.location_table = true;
    });
    await this.localStorage.setDataInIndexedDB(
      LOCAL_STORAGE.DATA_ACTIVE_FUNCTION,
      TAB_FUNCTION.LIST
    );
  }

  tabChanged(tabChangeEvent: MatTabChangeEvent) {
    this.activeTab = tabChangeEvent.index
  }

   async showCsvUpload() {
    this.location_table = false;
      setTimeout(() => {
        this.uploadCsv = true;
        this.location_table = false;
      });
      await this.localStorage.setDataInIndexedDB(
        LOCAL_STORAGE.DATA_ACTIVE_FUNCTION,
        TAB_FUNCTION.UPLOAD
      );
  }

  async hideCsvUpload() {
      setTimeout(() => {
        this.uploadCsv = false;
        this.location_table = true;
      });
      await this.localStorage.setDataInIndexedDB(
        LOCAL_STORAGE.DATA_ACTIVE_FUNCTION,
        TAB_FUNCTION.LIST
      );
  }


  async table_data() { 
    this.tableData = { header: [], data: [] };
    const response:any = this.downloadSampleList;
    this.tableData.header = Object.keys(response.result.format[0])
    let recordsList = [...response.result.format]
    let data = [];
    recordsList.map((item)=>{
      let row = [];
      for(let key of Object.keys(item)){
        let obj= {
          value:item[key],
          type: TABLE_DATA_TYPE.TEXT,
          class1:""
        };
        row.push(obj);
      }
      data.push({row})
    })
    this.tableData.data = data;
  }
  
  async prerequisities_Data() {
    this.prerequisitiesData = { header: [], data: [] };
    const response:any = this.downloadSampleList;
    this.prerequisitiesData.header = Object.keys(response.result.prerequisites[0])
    let recordsList = [...response.result.prerequisites]
    let data = [];
    recordsList.map((item)=>{ 
      let row = [];
      for(let key of Object.keys(item)){
        let obj= {
          value:item[key],
          type: key =="required" ?  TABLE_DATA_TYPE.ICON : TABLE_DATA_TYPE.TEXT,
          class1:key=="required" ? (item.required ? "fa solid fa-check" : "fa solid fa-xmark") : ""
        };
        row.push(obj);
      }
      data.push({row})
    })
    this.prerequisitiesData.data = data;
  }


  async downloadCSVSample(){
    const response:any = this.downloadSampleList;
    let header = Object.keys(response.result.format[0]);
    header = this.addToLast(header, 'isActive')
    let data = response.result.format;
    this.jsonToCsvService.downloadFile(header,data,this.tabs[this.activeTab]+"_Sample_CSV")

    // this.exportAsXLSX(data,this.tabs[this.activeTab]+"_Sample_Excel");
  }

  addToLast(header, n) {
    const newArray = [];

    for ( let i = 0; i < header.length; i++) {
        if(header[i] !== n) {
            newArray.push(header[i]);
        }
    }
    newArray.push(n)
    return newArray;
  }

  exportAsXLSX(data, name):void {
    this.jsonToXlsx.exportAsExcelFile(data, name);
  }

}
