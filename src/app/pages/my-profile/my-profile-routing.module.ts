import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Web_Language } from '@constant/constants';
import { TranslateService } from '@ngx-translate/core';
import { MyProfileComponent } from './my-profile/my-profile.component';
import { PasswordChangeComponent } from './password-change/password-change.component';
import { ProfileComponent } from './profile/profile.component';

const routes: Routes = [
  
  {
    path: '', component: MyProfileComponent,
    children: [
      {
        path: '',
        redirectTo: 'profile',
        pathMatch: 'full'
      },
      {
        path: 'profile', component: ProfileComponent,
      },
      {
        path : 'change-password',component:PasswordChangeComponent
      }
      
    ]
},

];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyProfileRoutingModule { 
  constructor(
    public translate: TranslateService,
  ) {
    translate.setDefaultLang(Web_Language.EN);
  }
}
