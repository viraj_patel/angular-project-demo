import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MyProfileService } from '@services/my-profile.service';
import { StaffService } from '@services/staff.service';
import {
  STATIC_DATA
} from "@constant/constants";
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  public profileForm: FormGroup;
  paramsObj:any={};
  errorMsg: any = {};
  formService: any;
  firstPage: number;
  searchResult: boolean;
  response:any={};
  roles:any=[];
  initialFirstName;
  initialLastName;
  constructor(
    private fb: FormBuilder,
    private profileService:MyProfileService,
    private staffService:StaffService,
    
    ) { 
    this.profileForm = this.fb.group({
      firstName: ["",[Validators.required,Validators.pattern('^[a-zA-Z \-\']+'),Validators.minLength(3),Validators.maxLength(50)]],
      lastName: ["",[Validators.required,Validators.pattern('^[a-zA-Z \-\']+'),Validators.minLength(3),Validators.maxLength(50)]],
      email: [""],
      roleName: [""],
    });
  }

  ngOnInit(): void {
    this.intializingMessage();
    this.getUserDetails();
    this.getRolesList();
   
  }

  intializingMessage() {
    this.errorMsg.firstName = {
      pattern: STATIC_DATA.CHAR_AND_SPACE_ALLOWED,
    };
   
    this.errorMsg.lastName = {
      pattern: STATIC_DATA.CHAR_AND_SPACE_ALLOWED,
    };
  }


  async getRolesList(){
    this.roles = await this.staffService.getLoginUserRoles();
    const loginRoles = await this.roles.map(v=>{return v.name});
   if(loginRoles.length>0){
   this.profileForm.patchValue({["roleName"]: loginRoles.join(', ') });
   }
  }
  async getUserDetails(){
    this.response= await this.profileService.getUserDeatails();
    this.profileForm.patchValue({
     ['firstName']:this.response.firstName,
     ['lastName']:this.response.lastName,
     ['email']:this.response.email,
    });
     this.initialFirstName=this.response.firstName;
     this.initialLastName=this.response.lastName;
  }

  checkInput(event,input){
    if(input == 'first'){
      if(event.code === "Space" && this.profileForm.value.firstName.trim().length === 0){
        setTimeout(() => {
          this.profileForm.patchValue({["firstName"]: "" });
        });
      }else if(event.type === "paste"){
        setTimeout(() => {
          if(this.profileForm.value.firstName.trim().length === 0){
            this.profileForm.patchValue({["firstName"]: "" });
          }
        });
      } else  if (event.key === "Enter") {
        this.profileForm.patchValue({["firstName"]: this.profileForm.value.firstName.trim() });
        this.profileForm.patchValue({["lastName"]: this.profileForm.value.lastName.trim() });
        setTimeout(() => {
          this.Save()
        });
      }
    }else{
      if(event.code === "Space" && this.profileForm.value.lastName.trim().length === 0){
        setTimeout(() => {
          this.profileForm.patchValue({["lastName"]: "" });
        });
      }else if(event.type === "paste"){
        setTimeout(() => {
          if(this.profileForm.value.lastName.trim().length === 0){
            this.profileForm.patchValue({["lastName"]: "" });
          }
        });
      } else  if (event.key === "Enter") {
        this.profileForm.patchValue({["firstName"]: this.profileForm.value.firstName.trim() });
        this.profileForm.patchValue({["lastName"]: this.profileForm.value.lastName.trim() });
        setTimeout(() => {
          this.Save()
        });
      }
    }
  }
   async Save(){
    this.profileForm.controls['firstName'].markAsTouched();
    this.profileForm.controls['firstName'].markAsDirty();
    this.profileForm.controls['lastName'].markAsTouched();
    this.profileForm.controls['lastName'].markAsDirty();

    if (this.profileForm.valid && (this.profileForm.controls.firstName.value != this.response.firstName || this.profileForm.controls.lastName.value != this.response.lastName)) { 
    this.paramsObj.firstName=this.profileForm.controls.firstName.value;
    this.paramsObj.lastName=this.profileForm.controls.lastName.value;
     
        
    await this.profileService.updateUser(this.paramsObj);  
    this.response = await this.profileService.getUserDeatails();
    this.profileService.sendProfile(true);
  }
  }
  reset(){
    this.profileForm.patchValue({
      ['firstName']:this.response.firstName,
      ['lastName']:this.response.lastName,
     });
  }
}
