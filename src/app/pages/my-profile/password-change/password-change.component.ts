import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { UsersService } from '@services/users.service';
import { LOCAL_STORAGE, STATIC_DATA, Web_Language,PASSWORD_STRENGTH,ConstantsFreeze,PASSWORD_VALIDATION_MSG } from '@constant/constants';
import { ConfirmedValidator } from './confirmed.validator';
import {checkSequnceInPassword,checkContainUnicode,checkPasswordChar,checkLowerCaseUpperCaseCount} from './password-validator';
import { FormService } from '@services/form.service';
import { LocalStorageService } from '@services/localService/localStorage.service';
@Component({
  selector: 'app-password-change',
  templateUrl: './password-change.component.html',
  styleUrls: ['./password-change.component.scss']
})
export class PasswordChangeComponent implements OnInit {
  public setPasswordParam: FormGroup;
  errorMsg: any = {};
  public isRefresh: boolean;
  type:string;
  oldPasswordShow:boolean=false;
  newPasswordShow:boolean=false;
  confirmPasswordShow:boolean=false;
  passwordStrength;
  userPasswordLength;
  userPasswordUsernameLength;
  userPasswordsequenceLength;
  userPasswordUnicode;

  userPasswordLowercase;
  userPasswordUppercase;
  userPasswordDigits;
  userPasswordallowSpecialChar;
  passwordType={
    className: "",
    value: "",
    show:false
};
  constructor(private fb: FormBuilder,
    private usersService: UsersService,
    private formService: FormService,
    private localStorage: LocalStorageService) { 
    this.setPasswordParam = this.fb.group({
      currentPassword: ['', [Validators.required]],
      newPassword: ['', [Validators.required]],
      confirmPassword: ['', [Validators.required]],
      username:['']
    },{ 
      validator: ConfirmedValidator('newPassword', 'confirmPassword')
    });
  }

  async ngOnInit(){
    this.callConfiguration();
    this.intializingMessage();
    const user_info:any = await this.localStorage.getDataFromIndexedDB(LOCAL_STORAGE.USER_INFO);
        if(user_info != null){
          const userInfo = JSON.parse(user_info);
          this.setPasswordParam.patchValue({["username"]: userInfo.username });
        }
  }

  intializingMessage() {
    this.errorMsg.newPassword = {
      required: STATIC_DATA.ERR_MSG_REQUIERD_NEW_PASSWORD,
      PasswordValidator: STATIC_DATA.ERR_MSG_INVALID_PASSWORD,
      confirmedValidator: STATIC_DATA.ERR_MSG_INVALID_PASSWORD_CON_PASSWORD,
    };
    this.errorMsg.confirmPassword = {
      required: STATIC_DATA.ERR_MSG_REQUIERD_CON_PASSWORD,
      pattern: STATIC_DATA.ERR_MSG_INVALID_PASSWORD,
      confirmedValidator: STATIC_DATA.ERR_MSG_INVALID_PASSWORD_CON_PASSWORD,
    };
   
  }

  
  async Save(data, isValid){
    this.formService.markFormGroupTouched(this.setPasswordParam);
    if (isValid) {
      const values={
        password : data.currentPassword,
        newPassword : data.newPassword,
        username : data.username,
      }
   
      const response: any = await this.usersService.changePass(values);
      this.resetFilter('');
    }
    
  }
  async resetFilter(event){
    if(event != ''){
      event.stopPropagation();
    }
    this.setPasswordParam.get('newPassword').clearValidators(); 
    this.setPasswordParam.controls["newPassword"].updateValueAndValidity();     
    this.setPasswordParam.reset();  
    this.passwordType={
      className: "",
      value: "",
      show:false};
    const user_info:any = await this.localStorage.getDataFromIndexedDB(LOCAL_STORAGE.USER_INFO);
    if(user_info != null){
      const userInfo = JSON.parse(user_info);
      this.setPasswordParam.patchValue({["username"]: userInfo.username });
      this.setPasswordParam.controls["newPassword"].setValidators([
        this.PasswordValidator(/bob/i)                      
      ]);
      this.setPasswordParam.controls["newPassword"].updateValueAndValidity();
    }  
    this.oldPasswordShow = false;
    this.newPasswordShow = false;
    this.confirmPasswordShow = false;
  }


  async callConfiguration(){
    const keyParam = {fields:"user.password"}
    let param = new HttpParams();
    Object.keys(keyParam).forEach((key) => {
      if (keyParam[key] != "") {
        param = param.set(
          key,
          keyParam[key]
        );
      }
    });
    const res: any = await this.usersService.getConfig(param);
                  if (res?.result?.length) {
                      const response = res?.result;
                      const passwordStrengthConfig:any = this.findConfigurationValue(response,  ConstantsFreeze.USER_PASSWORD_STRENGTH);
                      if (passwordStrengthConfig) {
                          this.passwordStrength = passwordStrengthConfig.configurableValue;
                      }
  
                      const userPasswordUsernameLengthResponse:any = this.findConfigurationValue(
                          response,
                          ConstantsFreeze.USER_PASSWORD_USERNAME_LENGTH
                      );
                      if (userPasswordUsernameLengthResponse) {
                          this.userPasswordUsernameLength = userPasswordUsernameLengthResponse.configurableValue;
                      }
  
                      const userPasswordsequenceLengthResponse:any = this.findConfigurationValue(
                          response,
                          ConstantsFreeze.USER_PASSWORD_SEQUENCE_LENGTH
                      );
                      if (userPasswordsequenceLengthResponse) {
                          this.userPasswordsequenceLength = userPasswordsequenceLengthResponse.configurableValue;
                      }
  
                      const userPasswordUppercaseResponse:any = this.findConfigurationValue(
                          response,
                          ConstantsFreeze.USER_PASSWORD_UPPERCASE
                      );
                      if (userPasswordUppercaseResponse) {
                          this.userPasswordUppercase = userPasswordUppercaseResponse.configurableValue;
                      }
  
                      const userPasswordLowercaseResponse:any = this.findConfigurationValue(
                          response,
                          ConstantsFreeze.USER_PASSWORD_LOWERCASE
                      );
                      if (userPasswordLowercaseResponse) {
                          this.userPasswordLowercase = userPasswordLowercaseResponse.configurableValue;
                      }
  
                      const userPasswordLengthResponse:any = this.findConfigurationValue(
                          response,
                          ConstantsFreeze.USER_PASSWORD_LENGTH
                      );
                      if (userPasswordLengthResponse) {
                          this.userPasswordLength = userPasswordLengthResponse.configurableValue;
                      }
  
                      const userPasswordDigitsResponse:any = this.findConfigurationValue(
                          response,
                          ConstantsFreeze.USER_PASSWORD_DIGITS
                      );
                      if (userPasswordDigitsResponse) {
                          this.userPasswordDigits = userPasswordDigitsResponse.configurableValue;
                      }
  
                      const userPasswordUnicodeResponse:any = this.findConfigurationValue(
                          response,
                          ConstantsFreeze.USER_PASSWORD_UNICODE
                      );
                      if (userPasswordUnicodeResponse) {
                          this.userPasswordUnicode = userPasswordUnicodeResponse.configurableValue;
                      }
                      const userPasswordAllowSpecialChar:any = this.findConfigurationValue(
                        response,
                        ConstantsFreeze.USER_PASSWORD_SPECIALCHAR
                    );
                    if (userPasswordAllowSpecialChar) {
                        this.userPasswordallowSpecialChar = userPasswordAllowSpecialChar.configurableValue;
                    }
                      this.setPasswordParam.controls["newPassword"].setValidators([
                        this.PasswordValidator(/bob/i)                      
                      ]);
                      this.setPasswordParam.controls["newPassword"].updateValueAndValidity();
                      
                  }
             
  }

  findConfigurationValue(result,key){
    return  result.find(item => item.configurableKey === key);
  }

  PasswordValidator(nameRe): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
    if(control.value != null){
      if(control.value.length == 0){
        this.errorMsg.newPassword.PasswordValidator = STATIC_DATA.ERR_MSG_REQUIERD_NEW_PASSWORD;
        return {PasswordValidator:true};
       }else if(control.value.length < +this.userPasswordLength){
        this.errorMsg.newPassword.PasswordValidator = PASSWORD_VALIDATION_MSG.userPasswordLength.replace('{{length}}',this.userPasswordLength);
        return {PasswordValidator:true};
       }else if(checkSequnceInPassword(control.value, this.setPasswordParam.controls['username'].value, this.userPasswordUsernameLength)){
        this.errorMsg.newPassword.PasswordValidator = PASSWORD_VALIDATION_MSG.userPasswordUsernameLength;
        return {PasswordValidator:true};
       }else if(checkSequnceInPassword(control.value, "0123456789", this.userPasswordsequenceLength)){
        this.errorMsg.newPassword.PasswordValidator = PASSWORD_VALIDATION_MSG.userPasswordsequenceLength.replace('{{length}}',this.userPasswordsequenceLength);
        return {PasswordValidator:true};
       }else if(checkContainUnicode(control.value, this.userPasswordUnicode)){
        this.errorMsg.newPassword.PasswordValidator =  PASSWORD_VALIDATION_MSG.userPasswordUnicode;
        return {PasswordValidator:true};
       }
       else if(checkLowerCaseUpperCaseCount(control.value, +this.userPasswordUppercase,'upper')){
        this.errorMsg.newPassword.PasswordValidator =  PASSWORD_VALIDATION_MSG.userPasswordUppercase.replace('{{length}}',this.userPasswordUppercase);
        return {PasswordValidator:true};
       }  
       else if(checkLowerCaseUpperCaseCount(control.value, +this.userPasswordLowercase,'lower')){
        this.errorMsg.newPassword.PasswordValidator =  PASSWORD_VALIDATION_MSG.userPasswordLowercase.replace('{{length}}',this.userPasswordLowercase);
        return {PasswordValidator:true};
       }  
       else if(checkLowerCaseUpperCaseCount(control.value, +this.userPasswordDigits,'number')){
        this.errorMsg.newPassword.PasswordValidator =  PASSWORD_VALIDATION_MSG.userPasswordDigits.replace('{{length}}',this.userPasswordDigits);
        return {PasswordValidator:true};
       }  
       else if(checkLowerCaseUpperCaseCount(control.value, +this.userPasswordallowSpecialChar,'special')){
        this.errorMsg.newPassword.PasswordValidator =  PASSWORD_VALIDATION_MSG.userPasswordallowSpecialChar.replace('{{length}}',this.userPasswordallowSpecialChar);
        return {PasswordValidator:true};
       }  
       else if(checkPasswordChar(control.value)){
        this.errorMsg.newPassword.PasswordValidator ="New password must contain at least three of the four categories: "+this.userPasswordUppercase+" uppercase character, "+this.userPasswordLowercase+" lowercase character, "+this.userPasswordDigits+" digit, "+this.userPasswordallowSpecialChar+" special symbol";
        return {PasswordValidator:true};
       }else{
         this.checkPasswordType(control.value,1);
       }
    }
    };
  }

  checkPasswordType(value,check){
    const password = value;

    const checkedPolicies: string[] = [];
    //     // ref : https://codesandbox.io/s/creating-a-secure-password-registration-form-with-react-and-formik-poezx?from-embed=&file=/src/Requirement.jsx:246-254
    const checkLowerCase = /[a-z]/.test(password);
    const checkUpperCase = /[A-Z]/.test(password);
    const checkDigits = /\d/.test(password);
    const checkSpecialChars = /^(?:.*[ `!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]){2}/.test(password);

    if (checkLowerCase) {
        checkedPolicies.push(PASSWORD_STRENGTH.lowerCase);
    }

    if (checkUpperCase) {
        checkedPolicies.push(PASSWORD_STRENGTH.upperCase);
    }

    if (checkDigits) {
        checkedPolicies.push(PASSWORD_STRENGTH.digits);
    }

    if (checkSpecialChars) {
        checkedPolicies.push(PASSWORD_STRENGTH.twoSpecialChar);
    }

    const sortedCheckedPolicies = checkedPolicies.sort((a, b) => a.localeCompare(b));

    let score = "weak";

    if (this.passwordStrength) {
        const keys = Object.keys(this.passwordStrength);
        keys.forEach((key: string, index: number) => {
            const splittedKey = key.split("+").sort((a, b) => a.localeCompare(b));

            const result = splittedKey.every(val => checkedPolicies.includes(val));
            var is_same = (sortedCheckedPolicies.length == splittedKey.length) && sortedCheckedPolicies.every(function(element, index) {
                            return element === splittedKey[index]; 
                          });
            if (is_same || result) {
                score = this.passwordStrength?.[key];
            }
        });
    }
    if (score.toLocaleLowerCase() === "weak") {
      this.passwordType = {
        className: score.toLocaleLowerCase(),
        value: "New password is "+score.toLocaleLowerCase(),
        show:false
      };
      if(check == 1){
        this.errorMsg.newPassword.PasswordValidator ="New password is "+score;
        return {PasswordValidator:true};
      }
       
    } else if (score.toLocaleLowerCase() === "medium") {
        this.passwordType = {
          className: score.toLocaleLowerCase(),
          value: "New password is "+score.toLocaleLowerCase(),
          show:true
      };
      if(check == 1){
        return null;
      }
        
    } else {
        this.passwordType = {
          className: score.toLocaleLowerCase(),
          value: "New password is "+score.toLocaleLowerCase(),
          show:true
      };
      if(check == 1){
        return null;
      }
    }
  }

  showHideNewPassword(){
    this.newPasswordShow = !this.newPasswordShow;
  }

  showHideConfirmPassword(){
    this.confirmPasswordShow = !this.confirmPasswordShow;
  }

  showHideOldPassword(){
    this.oldPasswordShow = !this.oldPasswordShow;
  }



}
