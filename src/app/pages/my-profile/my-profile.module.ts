import { ErrorHandler, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MyProfileComponent } from './my-profile/my-profile.component';
import { MyProfileRoutingModule } from './my-profile-routing.module';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatTabsModule } from '@angular/material/tabs';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { SharedModule } from '@shared/shared.module';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ToastrModule } from 'ngx-toastr';
import { createTranslateLoader } from 'src/app/app.module';
import { ModalsModule } from 'src/app/modals/modals.module';
import { PasswordChangeComponent } from './password-change/password-change.component';
import { ProfileComponent } from './profile/profile.component';
import { CapitalfirstLetterPipe } from '@pipes/capitalfirst-letter.pipe';
import { FormService } from '@services/form.service';
import { ApiService } from '@services/api.service';
import { GlobalErrorHandler } from '@services/globalErrorHandler';
import { UsersService } from '@services/users.service';
import { MatTooltipModule } from '@angular/material/tooltip';
import { TooltipModule } from 'ngx-bootstrap/tooltip';


@NgModule({
  declarations: [
    MyProfileComponent,
    ProfileComponent,
    PasswordChangeComponent
  ],
  imports: [
    CommonModule,
    MyProfileRoutingModule,
    MatDatepickerModule,
    MatTabsModule,
    FormsModule,
    ReactiveFormsModule,
    MatNativeDateModule,
    NgxMaterialTimepickerModule,
    NgxMaterialTimepickerModule.setLocale('en-GB'),
    HttpClientModule,
    SharedModule,
    NgxPaginationModule,
    ToastrModule.forRoot(),
    NgxSpinnerModule,
    MatFormFieldModule,
    MatExpansionModule,
    ModalsModule,
    TooltipModule.forRoot(),
    
    
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient],
      },
    }),
  ],
  providers: [
    ApiService,
    FormService,
    UsersService,
    TranslateService,
    CapitalfirstLetterPipe,
    { provide: ErrorHandler, useClass: GlobalErrorHandler }
  ]
})
export class MyProfileModule { }
