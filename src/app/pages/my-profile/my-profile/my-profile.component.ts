import { LocationStrategy } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { DATA_CONFIGURATION_TABS_LIST, LOCAL_STORAGE, PROFILE_TABS, PROFILE_TABS_LIST, TAB_FUNCTION } from '@constant/constants';
import { LocalStorageService } from '@services/localService/localStorage.service';
import { StaffService } from '@services/staff.service';
import { UsersService } from '@services/users.service';

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.scss']
})
export class MyProfileComponent implements OnInit {
  activeTab = PROFILE_TABS_LIST.PROFILE;
  locationMenu:boolean=false;
  dataConfigTab=PROFILE_TABS;
  currentRoute
  activeDropDown;
  @ViewChild('tabBox') inputBox: ElementRef;
  constructor(  private router:Router,
    private userService:UsersService,
    private localStorage: LocalStorageService,
    private locationStrategy: LocationStrategy,
    private rolesService : StaffService) { }
  ngOnInit(): void {
    const url = this.router.url;
    const path = url;
    const route = `/${path.split('/')[3]}`;
    this.currentRoute = route.split('?')[0].replace('/','');
    if(this.currentRoute == this.dataConfigTab.CHANGE_PASSWORD){
      this.activeTab = PROFILE_TABS_LIST.CHANGE_PASSWORD
    }else{
      this.activeTab = PROFILE_TABS_LIST.PROFILE
    }
    this.router.events.subscribe(val => {
      const url = this.router.url;
      const path = url;
      const route = `/${path.split('/')[3]}`;
      this.currentRoute = route.split('?')[0].replace('/','');
      if(this.currentRoute == this.dataConfigTab.CHANGE_PASSWORD){
        this.activeTab = PROFILE_TABS_LIST.CHANGE_PASSWORD
      }else{
        this.activeTab = PROFILE_TABS_LIST.PROFILE
      }
    });
  }


async getCurrentRoute(tab,event,dropDown){
    if(event != ''){
      event.stopPropagation();
    }
    this.router.navigate(['/main-home/my-profile/'+tab]);
      setTimeout(() => {
        const url = this.router.url;
        const path = url;
        const route = `/${path.split('/')[3]}`;
        this.currentRoute = route.split('?')[0].replace('/','');
        if(this.currentRoute == this.dataConfigTab.CHANGE_PASSWORD){
          this.activeTab = PROFILE_TABS_LIST.CHANGE_PASSWORD
        }else{
          this.activeTab = PROFILE_TABS_LIST.PROFILE
        }
      });
  
  }

}
