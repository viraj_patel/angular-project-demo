import { Component, HostListener, OnInit } from '@angular/core';
import { HttpParams } from "@angular/common/http";
import { UsersService } from '@services/users.service';
import { ADMIN_SORT_FIELD, USER_TABLE, BsConfig, FILTER_DATE_FORMAT, USER_INFO, TABLE_DATA_TYPE, USER_TABLE_SEARCH, TABLE_NAMES} from 'src/app/constants/constants';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { FormService } from '@services/form.service';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { RecordViewComponent } from 'src/app/modals/record-view/record-view.component';
@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.scss']
})
export class UserManagementComponent implements OnInit {
  userTable = [];
  userList;
  columnCount:number=9;
  shortingFilters:boolean=true;
  tableName=TABLE_NAMES.USER_TABLE;
  showNoData: boolean = false;
  showDataFilter: boolean = false;
  totalItems: any;
  bsConfig = BsConfig;
  firstPage: number = 1;
  usetTableFields:any = USER_TABLE;
  usetTablSearchFields:any = USER_TABLE_SEARCH;
  showFilters:boolean=false;
  paramsObj:any = {
    sortBy: '',
    page: this.firstPage,
    limit: 10
  };
  campaignOne: FormGroup;
  modalRef: BsModalRef;
  activeUSer:number;
  terminated:number;
  statuslist:any;
  listTable:boolean=false;

  constructor(
    private usersService: UsersService,
    private formbuilder: FormBuilder,
    private formService: FormService,
    private translate: TranslateService,
    private modalService: BsModalService,
  ) {
    const today = new Date();
    const month = today.getMonth();
    const year = today.getFullYear();
    this.campaignOne = new FormGroup({
      start: new FormControl(new Date(year, month, 13)),
      end: new FormControl(new Date(year, month, 16)),
    });
  }
  ngOnInit(){
    const initialValue = {};
    
    this.listApiCall( this.paramsObj);
    this.paramsObj.status='Active';
    this.activeUserCount(this.paramsObj);
    this.paramsObj.status='terminated';
    this.terminatedUserCount(this.paramsObj);

  }

  async activeUserCount(param){
    this.paramsObj = param;
    this.firstPage = this.paramsObj.page;
    let params = new HttpParams();
    Object.keys(this.paramsObj).forEach((key) => {
      params = params.set(key, this.paramsObj[key]);
    });
    this.statuslist = await this.usersService.getstatus(params);
    this.activeUSer=this.statuslist.result.count;
  }
  async terminatedUserCount(param){
    this.paramsObj = param;
    this.firstPage = this.paramsObj.page;
    let params = new HttpParams();
    Object.keys(this.paramsObj).forEach((key) => {
      params = params.set(key, this.paramsObj[key]);
    });
    this.statuslist = await this.usersService.getstatus(params);
    this.terminated=this.statuslist.result.count;
  }


  async listApiCall(param) {
    this.paramsObj = param;
    this.firstPage = this.paramsObj.page;
    let params = new HttpParams();
    Object.keys(this.paramsObj).forEach((key) => {
      params = params.set(key, this.paramsObj[key]);
    });
      this.userList = await this.usersService.getUsers(params,this.paramsObj.searchValue == '' ? false:true);
      this.totalItems = this.userList.result.totalCount;
      this.userTable = [];
      this.userTable = this.userList.result.users;
    
      this.userTable.forEach((element,index) => {
        var data=[];
        data[0]={value:element?.firstName,type:TABLE_DATA_TYPE.TEXT};
        data[1]={value:element?.lastName,type:TABLE_DATA_TYPE.TEXT};
        data[2]={value:element?.username,type:TABLE_DATA_TYPE.TEXT};
        data[3]={value:element?.email,type:TABLE_DATA_TYPE.TEXT};
        data[4]={value:element?.attributes?.gender ? element?.attributes?.gender : '',type:TABLE_DATA_TYPE.TEXT};
        data[5]={value:element?.createdTimestamp,type:TABLE_DATA_TYPE.DATE};
        data[6]={value:element?.createdTimestamp,type:TABLE_DATA_TYPE.DATE};
        data[7]={value:element?.attributes?.status? element?.attributes?.status : '',type:TABLE_DATA_TYPE.BUTTON,color:element?.attributes?.status[0] == "Active" ? "Green" : "red"};
        element.data=data;
      });
      this.listTable=true;
    if (this.userTable.length === 0 && this.paramsObj.searchBy == '' && this.paramsObj.searchValue == '' && this.paramsObj.fromDate == '' && this.paramsObj.toDate == '') {
       this.showNoData = true;
    }
  }

  sortData(type, key) {
    this.paramsObj.sortBy = key+":"+type;
    this.paramsObj.page = 1;
    this.firstPage = 1;
  }

  searchFilter(){
    this.showFilters = !this.showFilters;
  }

  
  keySearch(param){
   
    if(param[this.usetTableFields[0]+"_text"]?.value != ''){
      this.paramsObj.searchBy  = this.usetTableFields[0];
      this.paramsObj.searchValue = param[this.usetTableFields[0]+"_text"]?.value;
    }else
    if(param[this.usetTableFields[1]+"_text"]?.value != ''){
      this.paramsObj.searchBy  = this.usetTableFields[1];
      this.paramsObj.searchValue = param[this.usetTableFields[1]+"_text"]?.value;
    }else
    if(param[this.usetTableFields[2]+"_text"]?.value != ''){
      this.paramsObj.searchBy  = this.usetTableFields[2];
      this.paramsObj.searchValue = param[this.usetTableFields[2]+"_text"]?.value;
    }else
    if(param[this.usetTableFields[3]+"_text"]?.value != ''){
      this.paramsObj.searchBy  = this.usetTableFields[3];
      this.paramsObj.searchValue = param[this.usetTableFields[3]+"_text"]?.value;
    }else{
      this.paramsObj.searchBy  = '';
      this.paramsObj.searchValue = '';
    }
    this.listApiCall(this.paramsObj);
  }

  openDateFilter(campaignOnePicker){
    if(this.showDataFilter == true){
      this.showDataFilter = false;
      delete this.paramsObj.fromDate;
      delete this.paramsObj.toDate;
      this.listApiCall(this.paramsObj);
    }else{
      this.showDataFilter = true
      campaignOnePicker.open();
    }
    
  }

  selectDate(event){
    if(this.campaignOne.controls.end.value != null){
      this.paramsObj.fromDate = moment(this.campaignOne.controls.start.value).format(FILTER_DATE_FORMAT.DATE_FORMAT);
      this.paramsObj.toDate = moment(this.campaignOne.controls.end.value).format(FILTER_DATE_FORMAT.DATE_FORMAT);
      this.listApiCall(this.paramsObj);
    }
  }

  dateFilterSearch(data){
    this.paramsObj.fromDate = data.start.value != '' ? moment(data.start.value).format(FILTER_DATE_FORMAT.DATE_FORMAT) : '';
    this.paramsObj.toDate = data.end.value != '' ? moment(data.end.value).format(FILTER_DATE_FORMAT.DATE_FORMAT) : '';
    this.listApiCall(this.paramsObj);
  }


  viewInfo(data){
    const recordData=
    [
      {key:USER_INFO.FIRST_NAME,value:data.firstName},
      {key:USER_INFO.MIDDLE_NAME,value:data.attributes.status},
      {key:USER_INFO.LAST_NAME,value:data.lastName},
      {key:USER_INFO.USER_NAME,value:data.email},
      {key:USER_INFO.USER_STATUS,value:data.attributes.status[0]},
      {key:USER_INFO.GENDER,value:data.attributes.gender[0] != 'null'?data.attributes.gender[0]:""},
      {key:USER_INFO.DATE_OF_BIRTH,value:data.attributes.dob[0] != 'null'? data.attributes.dob[0]:""},
      // {key:USER_INFO.ALTERNATE_CONTACT_NO,value:data.attributes.caNumber[0]? data.attributes.caNumber[0]:""},
      {key:USER_INFO.ALTERNATE_EMAIL_ADDRESS,value:data.email},
      {key:USER_INFO.COMMUNICATION_LANGUAGE,value:data.attributes.preferredLanguage[0] != 'null'? data.attributes.preferredLanguage[0]:""},
      {key:USER_INFO.CREATION_DATE,value:moment(data.createdTimestamp).format(FILTER_DATE_FORMAT.DATE_FORMAT)},
      {key:USER_INFO.LAST_MODIFIED_DATE,value:moment(data.createdTimestamp).format(FILTER_DATE_FORMAT.DATE_FORMAT)},
    ]
   
    const initialState = {
      title: 'View Deatile',
      redordList: recordData,
    };
    this.modalRef = this.modalService.show(RecordViewComponent, {
      class: 'modal-dialog-centered',
      backdrop: 'static',
      initialState,
    });
    this.modalRef.content.response.subscribe(async (result) => {
      if (result) {
       
      } else {
        
      }
    });
  }
}
