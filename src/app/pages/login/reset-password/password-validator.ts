import { AbstractControl, FormGroup, ValidationErrors, ValidatorFn } from '@angular/forms';
export const checkSequnceInPassword = (password: any, metaString: any, maxLimit: any) => {
  if (password && metaString) {
      const { length: metaStringLength } = metaString;
      let start = 0;
      let end = Number(maxLimit);
      while (end <= metaStringLength) {
          const subStr = metaString.substring(start, end);
          const isContain = password.includes(subStr);
          if (isContain) {
              return true;
          }
          start++;
          end++;
      }
      return false;
  }

  return false;
};

export const checkPasswordChar = (password: string) => {
  let data = [];
  data.push(
      /[A-Z]/.test(password),
      /[a-z]/.test(password),
      /\d/.test(password),
      /[ `!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/.test(password)
  );
  data = data.filter(item => item);
  return data.length < 3;
};

export const checkContainUnicode = (password: string, isCheck: any) => {
  // eslint-disable-next-line no-control-regex
  const specialCharCheck = /[^\u0000-\u00ff]/;
  const result = isCheck === "1" ? specialCharCheck.test(password) : true;
  return result;
};

export const checkLowerCaseUpperCaseCount = (password: string, isCheck: any,type:any) => {
  // eslint-disable-next-line no-control-regex
  const specialCharCheck = /[^\u0000-\u00ff]/;
  const result = isCheck === "1" ? specialCharCheck.test(password) : true;

  var upper = 0,
          lower = 0,
          number = 0,
          special = 0;
        for (var i = 0; i < password.length; i++)
        {
          if (password[i] >= "A" && password[i] <= "Z") upper++;
          else if (password[i] >= "a" && password[i] <= "z") lower++;
          else if (password[i] >= "0" && password[i] <= "9") number++;
          else special++;
        }

      if(type=='upper'){
        return upper >= isCheck ? false:true;
      }else if(type=='lower'){
        return lower >= isCheck ? false:true;
      }else if(type=='number'){
        return number >= isCheck ? false:true;
      }else{
        return special >= isCheck ? false:true;
      }
};