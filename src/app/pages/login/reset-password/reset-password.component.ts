import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, ValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilityService } from '@services/utility.service';
import { Message } from 'src/app/shared/models/messages';
import { HttpRequestsService } from '@services/http-requests.service';
import { SharedService } from 'src/app/shared/shared.service';
import { TranslateService } from '@ngx-translate/core';
import { LoginService } from '@services/login.service';
import { LocalStorageService } from '@services/localService/localStorage.service';
import { LOCAL_STORAGE, STATIC_DATA, Web_Language,PASSWORD_STRENGTH,ConstantsFreeze,PASSWORD_VALIDATION_MSG } from '@constant/constants';
import { environment } from 'src/environments/environment';
import { FormService } from '@services/form.service';
import { ConfirmedValidator } from './confirmed.validator';
import {checkSequnceInPassword,checkContainUnicode,checkPasswordChar,checkLowerCaseUpperCaseCount} from './password-validator';
import { HttpParams } from '@angular/common/http';
@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
  env=process.env.NG_APP_ENV;
  errorMsg: any = {};
  public isRefresh: boolean;
  type:string;
  newPasswordShow:boolean=false;
  confirmPasswordShow:boolean=false;
  passwordStrength;
  userPasswordLength;
  userPasswordUsernameLength;
  userPasswordsequenceLength;
  userPasswordUnicode;

  userPasswordLowercase;
  userPasswordUppercase;
  userPasswordDigits;
  userPasswordallowSpecialChar;
  passwordType={
    className: "",
    value: "",
    show:false
};
  public setPasswordParam: FormGroup;
   constructor(
    private fb: FormBuilder,
    private formService: FormService,
    public httpService: HttpRequestsService,
    private localStorage: LocalStorageService,
    public helper: UtilityService,
    public translate: TranslateService,
    public shared: SharedService,
    public loginService: LoginService,
    private route: ActivatedRoute,
    private router: Router) {
      translate.setDefaultLang(Web_Language.EN);
    this.isRefresh = false;
    this.setPasswordParam = this.fb.group({
      newPassword: ['', [Validators.required]],
      confirmPassword: ['', [Validators.required]],
      link: [''],
      username:['']
    },{ 
      validator: ConfirmedValidator('newPassword', 'confirmPassword')
    });
  }

  /**
   * login(data, isValid) => check login credentials with api call @login
   * @param data in form control field value
   * @param isValid in form validation done or not
   */
   public async ngOnInit() {
     this.callConfiguration();
    this.route.queryParams.subscribe(async (params) => {
     if(params.username && params.token){
      this.setPasswordParam.patchValue({["link"]: params.token });
      this.setPasswordParam.patchValue({["username"]: params.username });
      this.type = "link";
     }else{
      const type:any = await this.localStorage.getDataFromIndexedDB(LOCAL_STORAGE.FORGOT_TYPE);
      this.type = type;
      const emeil: any = await this.localStorage.getDataFromIndexedDB(LOCAL_STORAGE.FORGOT_EMAIL);
      if(emeil != null){
        this.setPasswordParam.patchValue({["username"]: emeil });
      }
     }
     
    })
  
     const token = await this.localStorage.getDataFromIndexedDB(LOCAL_STORAGE.TOKEN);
    this.loginService
    .getLanguage()
    .subscribe(async (flag) => {
    });
    if (token == null) {
    }else{
        this.router.navigate(['/main-home/my-profile']);
    }
    this.intializingMessage();
  }
  get f(){
    return this.setPasswordParam.controls;
  }
  async login(data, isValid) {
    this.formService.markFormGroupTouched(this.setPasswordParam);
    if (isValid) {
      if(this.type == "otp"){
        delete data.link;
        delete data.confirmPassword;
        const response: any = await this.loginService.resetPasswordWithOTP(data);
        if(response.responseCode == 200){
          localStorage.clear();
          this.router.navigate(['/']);
        }
      }else{
        delete data.confirmPassword;
        const response: any = await this.loginService.resetPasswordWithLink(data);
        if(response.responseCode == 200){
          localStorage.clear();
          this.router.navigate(['/']);
        }
      }
      
    }
  }

  async callConfiguration(){
    const keyParam = {fields:"user.password"}
    let param = new HttpParams();
    Object.keys(keyParam).forEach((key) => {
      if (keyParam[key] != "") {
        param = param.set(
          key,
          keyParam[key]
        );
      }
    });
    const res: any = await this.loginService.getConfig(param);
                  if (res?.result?.length) {
                      const response = res?.result;
                      const passwordStrengthConfig:any = this.findConfigurationValue(response,  ConstantsFreeze.USER_PASSWORD_STRENGTH);
                      if (passwordStrengthConfig) {
                          this.passwordStrength = passwordStrengthConfig.configurableValue;
                      }
  
                      const userPasswordUsernameLengthResponse:any = this.findConfigurationValue(
                          response,
                          ConstantsFreeze.USER_PASSWORD_USERNAME_LENGTH
                      );
                      if (userPasswordUsernameLengthResponse) {
                          this.userPasswordUsernameLength = userPasswordUsernameLengthResponse.configurableValue;
                      }
  
                      const userPasswordsequenceLengthResponse:any = this.findConfigurationValue(
                          response,
                          ConstantsFreeze.USER_PASSWORD_SEQUENCE_LENGTH
                      );
                      if (userPasswordsequenceLengthResponse) {
                          this.userPasswordsequenceLength = userPasswordsequenceLengthResponse.configurableValue;
                      }
  
                      const userPasswordUppercaseResponse:any = this.findConfigurationValue(
                          response,
                          ConstantsFreeze.USER_PASSWORD_UPPERCASE
                      );
                      if (userPasswordUppercaseResponse) {
                          this.userPasswordUppercase = userPasswordUppercaseResponse.configurableValue;
                      }
  
                      const userPasswordLowercaseResponse:any = this.findConfigurationValue(
                          response,
                          ConstantsFreeze.USER_PASSWORD_LOWERCASE
                      );
                      if (userPasswordLowercaseResponse) {
                          this.userPasswordLowercase = userPasswordLowercaseResponse.configurableValue;
                      }
  
                      const userPasswordLengthResponse:any = this.findConfigurationValue(
                          response,
                          ConstantsFreeze.USER_PASSWORD_LENGTH
                      );
                      if (userPasswordLengthResponse) {
                          this.userPasswordLength = userPasswordLengthResponse.configurableValue;
                      }
  
                      const userPasswordDigitsResponse:any = this.findConfigurationValue(
                          response,
                          ConstantsFreeze.USER_PASSWORD_DIGITS
                      );
                      if (userPasswordDigitsResponse) {
                          this.userPasswordDigits = userPasswordDigitsResponse.configurableValue;
                      }
  
                      const userPasswordUnicodeResponse:any = this.findConfigurationValue(
                          response,
                          ConstantsFreeze.USER_PASSWORD_UNICODE
                      );
                      if (userPasswordUnicodeResponse) {
                          this.userPasswordUnicode = userPasswordUnicodeResponse.configurableValue;
                      }
                      const userPasswordAllowSpecialChar:any = this.findConfigurationValue(
                        response,
                        ConstantsFreeze.USER_PASSWORD_SPECIALCHAR
                    );
                    if (userPasswordAllowSpecialChar) {
                        this.userPasswordallowSpecialChar = userPasswordAllowSpecialChar.configurableValue;
                    }
                      this.setPasswordParam.controls["newPassword"].setValidators([
                        this.PasswordValidator(/bob/i)                      
                      ]);
                      this.setPasswordParam.controls["newPassword"].updateValueAndValidity();
                      
                  }
             
  }

  findConfigurationValue(result,key){
    return  result.find(item => item.configurableKey === key);
  }


  passwordConfig(){
     
  }

  intializingMessage() {
    this.errorMsg.newPassword = {
      required: STATIC_DATA.ERR_MSG_REQUIERD_NEW_PASSWORD,
      PasswordValidator: STATIC_DATA.ERR_MSG_INVALID_PASSWORD,
      confirmedValidator: STATIC_DATA.ERR_MSG_INVALID_PASSWORD_CON_PASSWORD,
    };
    this.errorMsg.confirmPassword = {
      required: STATIC_DATA.ERR_MSG_REQUIERD_CON_PASSWORD,
      pattern: STATIC_DATA.ERR_MSG_INVALID_PASSWORD,
      confirmedValidator: STATIC_DATA.ERR_MSG_INVALID_PASSWORD_CON_PASSWORD,
    };
   
  }

  checkPasswordType(value,check){
    const password = value;

    const checkedPolicies: string[] = [];
    //     // ref : https://codesandbox.io/s/creating-a-secure-password-registration-form-with-react-and-formik-poezx?from-embed=&file=/src/Requirement.jsx:246-254
    const checkLowerCase = /[a-z]/.test(password);
    const checkUpperCase = /[A-Z]/.test(password);
    const checkDigits = /\d/.test(password);
    const checkSpecialChars = /^(?:.*[ `!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]){2}/.test(password);

    if (checkLowerCase) {
        checkedPolicies.push(PASSWORD_STRENGTH.lowerCase);
    }

    if (checkUpperCase) {
        checkedPolicies.push(PASSWORD_STRENGTH.upperCase);
    }

    if (checkDigits) {
        checkedPolicies.push(PASSWORD_STRENGTH.digits);
    }

    if (checkSpecialChars) {
        checkedPolicies.push(PASSWORD_STRENGTH.twoSpecialChar);
    }

    const sortedCheckedPolicies = checkedPolicies.sort((a, b) => a.localeCompare(b));

    let score = "weak";

    if (this.passwordStrength) {
        const keys = Object.keys(this.passwordStrength);
        keys.forEach((key: string, index: number) => {
            const splittedKey = key.split("+").sort((a, b) => a.localeCompare(b));

            const result = splittedKey.every(val => checkedPolicies.includes(val));
            var is_same = (sortedCheckedPolicies.length == splittedKey.length) && sortedCheckedPolicies.every(function(element, index) {
                            return element === splittedKey[index]; 
                          });
            if (is_same || result) {
                score = this.passwordStrength?.[key];
            }
        });
    }
    if (score.toLocaleLowerCase() === "weak") {
      this.passwordType = {
        className: score.toLocaleLowerCase(),
        value: "New password is "+score.toLocaleLowerCase(),
        show:false
      };
      if(check == 1){
        this.errorMsg.newPassword.PasswordValidator ="New password is "+score;
        return {PasswordValidator:true};
      }
       
    } else if (score.toLocaleLowerCase() === "medium") {
        this.passwordType = {
          className: score.toLocaleLowerCase(),
          value: "New password is "+score.toLocaleLowerCase(),
          show:true
      };
      if(check == 1){
        return null;
      }
        
    } else {
        this.passwordType = {
          className: score.toLocaleLowerCase(),
          value: "New password is "+score.toLocaleLowerCase(),
          show:true
      };
      if(check == 1){
        return null;
      }
    }
  }


  PasswordValidator(nameRe): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
    
     if(control.value.length == 0){
      this.errorMsg.newPassword.PasswordValidator = STATIC_DATA.ERR_MSG_REQUIERD_NEW_PASSWORD;
      return {PasswordValidator:true};
     }else if(control.value.length < +this.userPasswordLength){
      this.errorMsg.newPassword.PasswordValidator = PASSWORD_VALIDATION_MSG.userPasswordLength.replace('{{length}}',this.userPasswordLength);
      return {PasswordValidator:true};
     }else if(checkSequnceInPassword(control.value, this.setPasswordParam.controls['username'].value, this.userPasswordUsernameLength)){
      this.errorMsg.newPassword.PasswordValidator = PASSWORD_VALIDATION_MSG.userPasswordUsernameLength;
      return {PasswordValidator:true};
     }else if(checkSequnceInPassword(control.value, "0123456789", this.userPasswordsequenceLength)){
      this.errorMsg.newPassword.PasswordValidator = PASSWORD_VALIDATION_MSG.userPasswordsequenceLength.replace('{{length}}',this.userPasswordsequenceLength);
      return {PasswordValidator:true};
     }else if(checkContainUnicode(control.value, this.userPasswordUnicode)){
      this.errorMsg.newPassword.PasswordValidator =  PASSWORD_VALIDATION_MSG.userPasswordUnicode;
      return {PasswordValidator:true};
     }
     else if(checkLowerCaseUpperCaseCount(control.value, +this.userPasswordUppercase,'upper')){
      this.errorMsg.newPassword.PasswordValidator =  PASSWORD_VALIDATION_MSG.userPasswordUppercase.replace('{{length}}',this.userPasswordUppercase);
      return {PasswordValidator:true};
     }  
     else if(checkLowerCaseUpperCaseCount(control.value, +this.userPasswordLowercase,'lower')){
      this.errorMsg.newPassword.PasswordValidator =  PASSWORD_VALIDATION_MSG.userPasswordLowercase.replace('{{length}}',this.userPasswordLowercase);
      return {PasswordValidator:true};
     }  
     else if(checkLowerCaseUpperCaseCount(control.value, +this.userPasswordDigits,'number')){
      this.errorMsg.newPassword.PasswordValidator =  PASSWORD_VALIDATION_MSG.userPasswordDigits.replace('{{length}}',this.userPasswordDigits);
      return {PasswordValidator:true};
     }  
     else if(checkLowerCaseUpperCaseCount(control.value, +this.userPasswordallowSpecialChar,'special')){
      this.errorMsg.newPassword.PasswordValidator =  PASSWORD_VALIDATION_MSG.userPasswordallowSpecialChar.replace('{{length}}',this.userPasswordallowSpecialChar);
      return {PasswordValidator:true};
     }  
     else if(checkPasswordChar(control.value)){
      this.errorMsg.newPassword.PasswordValidator ="New password must contain at least three of the four categories: "+this.userPasswordUppercase+" uppercase character, "+this.userPasswordLowercase+" lowercase character, "+this.userPasswordDigits+" digit, "+this.userPasswordallowSpecialChar+" special symbol";
      return {PasswordValidator:true};
     }else{
       this.checkPasswordType(control.value,1);
     }
    };
  }

  showHideNewPassword(){
    this.newPasswordShow = !this.newPasswordShow;
  }

  showHideConfirmPassword(){
    this.confirmPasswordShow = !this.confirmPasswordShow;
  }

  back(){
    localStorage.clear();
    this.router.navigate(['/']);
  }
}
