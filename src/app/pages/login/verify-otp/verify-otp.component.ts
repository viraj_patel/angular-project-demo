import { Component, ViewChild} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UtilityService } from '@services/utility.service';
import { Message } from 'src/app/shared/models/messages';
import { HttpRequestsService } from '@services/http-requests.service';
import { SharedService } from 'src/app/shared/shared.service';
import { TranslateService } from '@ngx-translate/core';
import { NgOtpInputConfig } from 'ng-otp-input';
import { LOCAL_STORAGE, STATIC_DATA, OTP_CONFIG } from '@constant/constants';
import { FormService } from '@services/form.service';
import { LocalStorageService } from '@services/localService/localStorage.service';
import { LoginService } from '@services/login.service';
import { HttpParams } from '@angular/common/http';
@Component({
  selector: 'app-verify-otp',
  templateUrl: './verify-otp.component.html',
  styleUrls: ['./verify-otp.component.scss']
})
export class VerifyOtpComponent {

  public isRefresh: boolean;
  public otpParam: FormGroup;
 
  errorMsg: any = {};
  otpLength;
  otpResendTime;
  otpMaxAttempt;
  otpMaxAttemptResend;
  otpBlockAfterMaxAttempt;
  otpValidFor;
  otpValidTime:string="";
  otpResendTry:number=0;
  verifyOtpTry:number=0;
  config :NgOtpInputConfig = {
    allowNumbersOnly: true,
    length: 0,
    isPasswordInput: false,
    disableAutoFocus: false,
    placeholder: '',
    containerClass:'otp-all-input mb-3',
    inputClass:'otp-input mr-3'
  };
  @ViewChild('ngOtpInput') ngOtpInputRef:any;
  constructor(
    private fb: FormBuilder,
    private formService: FormService,
    public httpService: HttpRequestsService,
    public translate: TranslateService,
    public helper: UtilityService,
    public shared: SharedService,
    public loginService: LoginService,
    private router: Router,
    private localStorageService: LocalStorageService) {
      translate.setDefaultLang('en');
    this.isRefresh = false;
    this.otpParam = this.fb.group({
      otp: ['', [Validators.required,Validators.maxLength(4),Validators.minLength(4)]],
      uuid:[''],
      username:[''],
      admin: true
    });
  }

  async ngOnInit(){
    const emeil: any = await this.localStorageService.getDataFromIndexedDB(LOCAL_STORAGE.FORGOT_EMAIL);
    if(emeil != null){
      this.otpParam.patchValue({["username"]: emeil });
    }
    const uuid: any = await this.localStorageService.getDataFromIndexedDB(LOCAL_STORAGE.FORGOT_UUID);
    if(emeil != null){
      this.otpParam.patchValue({["uuid"]: uuid });
    }
    this.intializingMessage();
    this.callConfiguration();
  }
  
  async callConfiguration(){
    const keyParam = {fields:"user.otp"}
    let param = new HttpParams();
    Object.keys(keyParam).forEach((key) => {
      if (keyParam[key] != "") {
        param = param.set(
          key,
          keyParam[key]
        );
      }
    });
    const res: any = await this.loginService.getConfig(param);
    if (res?.result?.length) {
      const response = res?.result;

      const otpLength:any = this.findConfigurationValue(response,  OTP_CONFIG.USER_OTP_LENGTH);
      if (otpLength) {
          this.otpLength = otpLength.configurableValue;
          this.config= {
            allowNumbersOnly: true,
            length: +this.otpLength,
            isPasswordInput: false,
            disableAutoFocus: false,
            placeholder: '',
            containerClass:'otp-all-input mb-3',
            inputClass:'otp-input mr-3'
          };
          this.otpParam.controls["otp"].setValidators([
            Validators.required,Validators.maxLength(+this.otpLength),Validators.minLength(+this.otpLength)
          ]);
          this.otpParam.controls["otp"].updateValueAndValidity();
      }

      const otpResendTime:any = this.findConfigurationValue(
          response,
          OTP_CONFIG.USER_OTP_RESEND_TIME
      );
      if (otpResendTime) {
          this.otpResendTime = otpResendTime.configurableValue;
      }

      const otpMaxAttempt:any = this.findConfigurationValue(
          response,
          OTP_CONFIG.USER_OTP_MAXATTEMPT
      );
      if (otpMaxAttempt) {
          this.otpMaxAttempt = otpMaxAttempt.configurableValue;
      }

      const otpMaxAttemptResend:any = this.findConfigurationValue(
          response,
          OTP_CONFIG.USER_OTP_RESEND_MAXATTEMPT
      );
      if (otpMaxAttemptResend) {
          this.otpMaxAttemptResend = otpMaxAttemptResend.configurableValue;
      }

      const otpBlockAfterMaxAttempt:any = this.findConfigurationValue(
          response,
          OTP_CONFIG.USER_OTP_BLOCK_AFTER_MAXATTEMPT
      );
      if (otpBlockAfterMaxAttempt) {
          this.otpBlockAfterMaxAttempt = otpBlockAfterMaxAttempt.configurableValue;
      }

      const otpValidFor:any = this.findConfigurationValue(
          response,
          OTP_CONFIG.USER_OTP_VALIDFOR
      );
      if (otpValidFor) {
          this.otpValidFor = otpValidFor.configurableValue;
          if(this.otpValidFor.search("min") > -1 || this.otpValidFor.search("Min") > -1){
            if( this.otpValidFor.match(/\d+/) > 1){
              this.otpValidTime = this.otpValidFor.match(/\d+/)+" minutes"
            }else{
              this.otpValidTime = this.otpValidFor.match(/\d+/)+" minute"
            }
          }else if(this.otpValidFor.search("sec") > -1 || this.otpValidFor.search("Sec") > -1){
            if( this.otpValidFor.match(/\d+/) > 1){
              this.otpValidTime = this.otpValidFor.match(/\d+/)+" seconds"
            }else{
              this.otpValidTime = this.otpValidFor.match(/\d+/)+" second"
            }
          }else if(this.otpValidFor.search("hour") > -1 || this.otpValidFor.search("Hour") > -1){
            if( this.otpValidFor.match(/\d+/) > 1){
              this.otpValidTime = this.otpValidFor.match(/\d+/)+" Hours"
            }else{
              this.otpValidTime = this.otpValidFor.match(/\d+/)+" Hour"
            }
          }
      }
  }
  }
  findConfigurationValue(result,key){
    return  result.find(item => item.configurableKey === key);
  }
  async reSendOTP(){
    if(this.otpResendTry < +this.otpMaxAttemptResend){
      const data = {username:this.otpParam.controls['username'].value,requestType:"otp" };
      this.otpResendTry = this.otpResendTry + 1;
      this.otpParam.controls['otp'].reset();
      this.ngOtpInputRef.setValue('');
      const response: any = await this.loginService.forgot(data);
      
      this.router.navigate(['/verify-otp']);
      await this.localStorageService.setDataInIndexedDB(
        LOCAL_STORAGE.FORGOT_UUID,
        response.result.uuid
      );
      this.otpParam.patchValue({["uuid"]: response.result.uuid });
    }
  }

  onOtpChange(otp) {
    this.otpParam.controls.otp.patchValue(otp);
  }
  intializingMessage() {
    this.errorMsg.otp = {
      required: STATIC_DATA.ERR_MSG_REQUIERD_OTP,
      // maxlength: STATIC_DATA.ERR_MSG_MAX_OTP,
      // minlength: STATIC_DATA.ERR_MSG_MIN_OTP
    };
   
  }
  
  async otpValidate(data, isValid) {
    this.formService.markFormGroupTouched(this.otpParam);
    if (isValid) {
      this.verifyOtpTry = this.verifyOtpTry + 1;
      const response: any = await this.loginService.otpVerify(data);
      
      this.router.navigate(['/reset-password']);
      // this.helper.showLoading();
      // localStorage.clear();
      // this.helper.hideLoading();
      // this.router.navigate(['/home/data-configuration']);
      // this.helper.hideLoading();
    }
  }

  back(){
    localStorage.clear();
    this.router.navigate(['/']);
  }
}
