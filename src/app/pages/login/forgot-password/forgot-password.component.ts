import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UtilityService } from '@services/utility.service';
import { Message } from 'src/app/shared/models/messages';
import { HttpRequestsService } from '@services/http-requests.service';
import { SharedService } from 'src/app/shared/shared.service';
import { TranslateService } from '@ngx-translate/core';
import { LOCAL_STORAGE, STATIC_DATA } from '@constant/constants';
import { LocalStorageService } from '@services/localService/localStorage.service';
import { FormService } from '@services/form.service';
import { LoginService } from '@services/login.service';
import { HttpParams } from '@angular/common/http';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent {
  public isRefresh: boolean;
  public forgotParam: FormGroup;
  errorMsg: any = {};
  otp:boolean=false;
  link:boolean=false;
  constructor(
    private fb: FormBuilder,
    public httpService: HttpRequestsService,
    public helper: UtilityService,
    public shared: SharedService,
    public translate: TranslateService,
    private localStorageService: LocalStorageService,
    private formService: FormService,
    public loginService: LoginService,
    private router: Router) {
      translate.setDefaultLang('en');
    this.isRefresh = false;
    this.forgotParam = this.fb.group({
      username: ['', Validators.compose([Validators.required])],
      requestType: ['link'],
      preferredLanguage: ['english']
    });
  }

  async ngOnInit() {
    const emeil: any = await this.localStorageService.getDataFromIndexedDB(LOCAL_STORAGE.FORGOT_EMAIL);
    if(emeil != null){
      this.forgotParam.patchValue({["username"]: emeil });
    }
    const token = await this.localStorageService.getDataFromIndexedDB(LOCAL_STORAGE.TOKEN);
    this.loginService
    .getLanguage()
    .subscribe(async (flag) => {
    });
    if (token == null) {
    }else{
        this.router.navigate(['/main-home/my-profile']);
    }
    this.intializingMessage();
    this.callConfiguration();

  }

  async callConfiguration(){
    const keyParam = {fields:"user.forgotPassword.options"}
    let param = new HttpParams();
    Object.keys(keyParam).forEach((key) => {
      if (keyParam[key] != "") {
        param = param.set(
          key,
          keyParam[key]
        );
      }
    });
    const res: any = await this.loginService.getConfig(param);
    for (let index = 0; index < res?.result[0].configurableValue.length; index++) {
      const element = res?.result[0].configurableValue[index];
     
      if( element.toLowerCase() == 'otp'){
        this.otp = true;
      }
      if( element.toLowerCase() == 'link'){
        this.link = true;
      }
    }
   
  }
  
  async forgot(data, isValid,type) {
   this.formService.markFormGroupTouched(this.forgotParam);
    if (isValid) {
      await this.forgotParam.patchValue({["requestType"]: type });
      data.requestType = type;
      const response: any = await this.loginService.forgot(data);
     if( type == 'otp'){
      this.router.navigate(['/verify-otp']);
      await this.localStorageService.setDataInIndexedDB(
        LOCAL_STORAGE.FORGOT_UUID,
        response.result.uuid
      );
      await this.localStorageService.setDataInIndexedDB(
        LOCAL_STORAGE.FORGOT_TYPE,
        this.forgotParam.controls['requestType'].value
      );
     }else{
      await this.localStorageService.setDataInIndexedDB(
        LOCAL_STORAGE.FORGOT_TYPE,
        this.forgotParam.controls['requestType'].value
      );
     }
      
    }
  }

  intializingMessage() {
    this.errorMsg.username = {
      required: STATIC_DATA.ERR_MSG_REQUIERD_EMAIL,
      email: STATIC_DATA.ERR_MSG_VALID_EMAIL,
    };
   
  }

  back(){
    localStorage.clear();
    this.router.navigate(['/']);
  }
}
