import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UtilityService } from '@services/utility.service';
import { Message } from 'src/app/shared/models/messages';
import { HttpRequestsService } from '@services/http-requests.service';
import { SharedService } from 'src/app/shared/shared.service';
import { TranslateService } from '@ngx-translate/core';
import { LoginService } from '@services/login.service';
import { LocalStorageService } from '@services/localService/localStorage.service';
import { CONFIG_KEYS, LOCAL_STORAGE, STATIC_DATA, Web_Language } from '@constant/constants';
import { environment } from 'src/environments/environment';
import { FormService } from '@services/form.service';
import { HttpParams } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  env=process.env.NG_APP_ENV;
  errorMsg: any = {};
  passwordShow:boolean=false;
  public isRefresh: boolean;
  public loginParam: FormGroup;
   constructor(
    private fb: FormBuilder,
    private formService: FormService,
    public httpService: HttpRequestsService,
    private localStorage: LocalStorageService,
    public helper: UtilityService,
    public translate: TranslateService,
    public shared: SharedService,
    public loginService: LoginService,
    private router: Router) {
      translate.setDefaultLang(Web_Language.EN);
    this.isRefresh = false;
    this.loginParam = this.fb.group({
      username: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.maxLength(30), Validators.required]]
    });
  }

  /**
   * login(data, isValid) => check login credentials with api call @login
   * @param data in form control field value
   * @param isValid in form validation done or not
   */
   public async ngOnInit() {
    // this.get_drupal_image();

     const token = await this.localStorage.getDataFromIndexedDB(LOCAL_STORAGE.TOKEN);
    this.loginService
    .getLanguage()
    .subscribe(async (flag) => {
    });
    if (token == null) {
    }else{
        this.router.navigate(['/main-home/my-profile']);
    }
    this.intializingMessage();
    this.callConfiguration();
  }

  async callConfiguration(){
    const keyParam = {fields:CONFIG_KEYS.USER_EMAIL}
    let param = new HttpParams();
    Object.keys(keyParam).forEach((key) => {
      if (keyParam[key] != "") {
        param = param.set(
          key,
          keyParam[key]
        );
      }
    });
    const res: any = await this.loginService.getConfig(param);
    this.loginParam.controls["username"].setValidators([
      Validators.required, Validators.email,
      Validators.maxLength(+res?.result[0].configurableValue),
    ]);
    this.loginParam.controls["username"].updateValueAndValidity();
   
  }

  
  async login(data, isValid) {
    this.formService.markFormGroupTouched(this.loginParam);
    if (isValid) {
      const response: any = await this.loginService.login(data);
       this.helper.showLoading();
      localStorage.clear();
      this.helper.hideLoading();
      
      await this.localStorage.setDataInIndexedDB(
        LOCAL_STORAGE.TOKEN,
        response.result.token
      );
      await this.localStorage.setDataInIndexedDB(
        LOCAL_STORAGE.REFRESH_TOKEN,
        response.result.refreshToken
      );

      
      this.router.navigate(['/main-home/my-profile']);
      this.helper.hideLoading();
    }
  }

  intializingMessage() {
    this.errorMsg.username = {
      required: STATIC_DATA.ERR_MSG_REQUIERD_EMAIL,
      email: STATIC_DATA.ERR_MSG_VALID_EMAIL,
    };
   
  }
  async get_drupal_image(){
    
    const response = this.loginService.get_drupal_image();
      
  }


  async forgotPass(){
    this.loginParam.controls['username'].markAsTouched();
    this.loginParam.controls['username'].markAsDirty();
    if (this.loginParam.controls['username'].valid) {
      await this.localStorage.setDataInIndexedDB(
        LOCAL_STORAGE.FORGOT_EMAIL,
        this.loginParam.controls['username'].value
      );
    this.router.navigate(['/forgot-password']);
    }
  }

  showHidePassword(){
    this.passwordShow = !this.passwordShow;
  }

}
