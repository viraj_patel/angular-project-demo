import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CartManagementListComponent } from './cart-management-list/cart-management-list.component';

const routes: Routes = [
  {
    path: '', component: CartManagementListComponent,
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CartManagementRoutingModule { }
