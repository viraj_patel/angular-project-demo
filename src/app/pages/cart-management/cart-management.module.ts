import { ErrorHandler, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CartManagementRoutingModule } from './cart-management-routing.module';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatTabsModule } from '@angular/material/tabs';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatNativeDateModule } from '@angular/material/core';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { SharedModule } from '@shared/shared.module';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrModule } from 'ngx-toastr';
import { NgxSpinnerModule } from 'ngx-spinner';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatExpansionModule } from '@angular/material/expansion';
import { ModalsModule } from 'src/app/modals/modals.module';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { createTranslateLoader } from 'src/app/app.module';
import { CartManagementListComponent } from './cart-management-list/cart-management-list.component';
import { ApiService } from '@services/api.service';
import { FormService } from '@services/form.service';
import { GlobalErrorHandler } from '@services/globalErrorHandler';


@NgModule({
  declarations: [CartManagementListComponent],
  imports: [
    CommonModule,
    CartManagementRoutingModule,
    MatDatepickerModule,
    MatTabsModule,
    FormsModule,
    ReactiveFormsModule,
    MatNativeDateModule,
    NgxMaterialTimepickerModule,
    NgxMaterialTimepickerModule.setLocale('en-GB'),
    HttpClientModule,
    SharedModule,
    NgxPaginationModule,
    ToastrModule.forRoot(),
    NgxSpinnerModule,
    MatExpansionModule,
    ModalsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient],
      },
    }),
  ],
  providers: [
    ApiService,
    FormService,
   
    { provide: ErrorHandler, useClass: GlobalErrorHandler }
  ]
})
export class CartManagementModule { }
