import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CartManagementListComponent } from './cart-management-list.component';

describe('CartManagementListComponent', () => {
  let component: CartManagementListComponent;
  let fixture: ComponentFixture<CartManagementListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CartManagementListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CartManagementListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
