import { HttpParams } from "@angular/common/http";
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnDestroy, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import {
  BsConfig,
  CART_TABLE,
  FILTER_DATE_FORMAT,
  TABLE_DATA_TYPE,
  TICKET_TABLE,
  CART_INFO,
  CART_TABLE_SEARCH,
  CART_TABLE_HEADING,
  CART_TABLE_SORT,
  STATIC_DATA
} from "@constant/constants";
import { TranslateService } from "@ngx-translate/core";
import { FormService } from "@services/form.service";
import { CartService } from "@services/cart.service";
import * as moment from "moment";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { RecordViewComponent } from "src/app/modals/record-view/record-view.component";
import { CsvFormatComponent } from "src/app/modals/csv-format/csv-format.component";
import { TicketService } from "@services/ticket.service";
import { elementAt, takeUntil } from "rxjs/operators";
import { DateAdapter, MAT_DATE_FORMATS, MatDateFormats } from "@angular/material/core";
import { MatCalendar } from "@angular/material/datepicker";
import { Subject } from "rxjs";
import { ViewDetailNewComponent } from "src/app/modals/view-detail-new/view-detail-new.component";
import { Router } from "@angular/router";
import { StaffService } from "@services/staff.service";

@Component({
  selector: "app-cart-management-list",
  templateUrl: "./cart-management-list.component.html",
  styleUrls: ["./cart-management-list.component.scss"],
})
export class CartManagementListComponent implements OnInit {
  // ------------------------------------------cart management copy of CART
  title
  cartTable:any = [];
  ticketList;
  tableName='Cart';
  columnCount: number =9;
  shortingFilters: boolean = true;
  showNoData: boolean = false;
  showDataFilter: boolean = false;
  totalItems: any;
  bsConfig = BsConfig;
  firstPage: number = 1;
  tablePageLimit:number=10;
  listTable:boolean=false;
  cartTableSort:any=CART_TABLE_SORT;
  cartTableFields: any = CART_TABLE;
  cartTableHeading:any = CART_TABLE_HEADING;
  cartTableSearchFields: any = CART_TABLE_SEARCH;
  showFilters: boolean = false;
 
  paramsObj: any = {
    page: this.firstPage,
    limit: 10
  };
  exampleHeader = ExampleHeader;
  startdate;
  endDate;
  openCount;
  closedCount;
  campaignOne: FormGroup;
  modalRef: BsModalRef;
  public cartForm: FormGroup;
  errorMsg: any={};
  translation: any;
  searchResult:boolean=false;
  today = new Date();
  view:boolean=false;
  constructor(
    private fb: FormBuilder,
    public cartService: CartService,
    private formbuilder: FormBuilder,
    private formService: FormService,
    private translate: TranslateService,
    private modalService: BsModalService,
    private rolesService : StaffService,
    private router:Router
  ) {
    translate.setDefaultLang('en');
    const today = new Date();
    const month = today.getMonth();
    const year = today.getFullYear();
    this.cartForm = this.fb.group({
      id: [""],
      startDate: [""],
      endDate: [""],
      status: ["100"],
    });
  }
  ngOnDestroy(): void {}


  async ngOnInit() {
    const url = this.router.url;
    const path = url;
    const route = `/${path.split('/')[2]}`;
    const currentRoute = route.split('?')[0].replace('/','');
    const permissions = await this.rolesService.getLoginPermissionsForModule(currentRoute);
    if(permissions.length > 0){
      if(permissions[0].View == false){
        this.router.navigate(['/main-home/my-profile']);
      }else{
        this.view = true;
      }
    }else{
      this.view = true;
    }
    this.endDate = this.today;
    const initialValue = {};
    this.paramsObj.sortBy='updatedAt:DESC';
    this.listApiCall(this.paramsObj);
    
    this.getCartCount();
  }

  async getCartCount(){
    
    let paramsObj1={status:'active'};
    let paramsObj2={status:'closed'};

    let params1 = new HttpParams();
    Object.keys(paramsObj1).forEach((key) => {
      params1 = params1.set(key, paramsObj1[key]);
    });
    let params2 = new HttpParams();
    Object.keys(paramsObj2).forEach((key) => {
      params2 = params2.set(key, paramsObj2[key]);
    });
    const response1 :any = await this.cartService.getCount(params1); 
    const response2 :any = await this.cartService.getCount(params2); 
    this.openCount=response1.result;
    this.closedCount=response2.result;
    this.intializingMessage();
  }


  async listApiCall(data) {
    if(data.sortBy=='cartID:ASC'){
      this.paramsObj.sortBy='id:ASC'
    }
    else if(data.sortBy=='cartID:DESC'){
      this.paramsObj.sortBy='id:DESC'
    }else if(data.sortBy != ''){
      this.paramsObj.sortBy = data.sortBy;
    }
    else if(data.sortBy != ''){
      this.paramsObj.sortBy = data.sortBy;
    }
  
    this.paramsObj.page = data.page;
    this.paramsObj.limit = data.limit;
    this.tablePageLimit = data.limit;
    this.firstPage = this.paramsObj.page;

      let params = new HttpParams();
      Object.keys(this.paramsObj).forEach((key) => {
        params = params.set(key, this.paramsObj[key]);
      });
      const response :any = await this.cartService.getCart(params); 
   
    
      
    response.result.rows.forEach((element, index) => {
      var data = [];
      data[0] = { value: element?.id, type: TABLE_DATA_TYPE.TEXT };
      data[1] = { value: element?.relatedParty.length == 0 ? '--' : element?.relatedParty[0].name , type: TABLE_DATA_TYPE.TEXT };
      data[2] = { value: element?.planName == null || element.planName.length == 0 ? '--' : element.planName, type: TABLE_DATA_TYPE.TEXT };
      data[3] = { value: element?.relatedParty.length == 0 || element?.relatedParty[0].caNumber == null ? "--":element?.relatedParty[0].caNumber , type: TABLE_DATA_TYPE.TEXT };
    
     
      data[4] = { value: element?.createdAt, type: TABLE_DATA_TYPE.DATE };
      data[5] = { value :element?.updatedAt,type: TABLE_DATA_TYPE.DATE};
      data[6] = { value : element?.totalAmount == null || element.cartItems.length === 0 ? '--' : element?.totalAmount ,type:TABLE_DATA_TYPE.TEXT }
      data[7] = {value: element?.status,type: TABLE_DATA_TYPE.TEXT};
      element.data = data;
    });
    
    this.totalItems = response.result.totalCount;
    
    this.cartTable = [];
    this.cartTable = response.result.rows;
    this.listTable=true;

    if (this.cartTable.length === 0) {
      // this.showNoData = true;
    }
  }


  sortData(type, key) {
    this.paramsObj.sortBy = key+":"+type;
    this.paramsObj.page = 1;
    this.firstPage = 1;
  }


  searchFilter(){
    this.showFilters = !this.showFilters;
  }



  keySearch(param) {
    
    
    if (param[this.cartTableFields[0] + "_text"]?.value != "") {
      this.paramsObj.searchBy  = 'id';
      this.paramsObj.searchValue = param[this.cartTableFields[0]+"_text"]?.value;
    }
    if (param[this.cartTableFields[1] + "_text"]?.value != "") {
      this.paramsObj.searchBy  = this.cartTableFields[1];
      this.paramsObj.searchValue = param[this.cartTableFields[1]+"_text"]?.value;
    }
    if (param[this.cartTableFields[2] + "_text"]?.value != "") {
      this.paramsObj.searchBy  = this.cartTableFields[2];
      this.paramsObj.searchValue = param[this.cartTableFields[2]+"_text"]?.value;
    }
    if (param[this.cartTableFields[3] + "_text"]?.value != "") {
      this.paramsObj.searchBy  = this.cartTableFields[3];
      this.paramsObj.searchValue = param[this.cartTableFields[3]+"_text"]?.value;
    }
    if (param[this.cartTableFields[4] + "_text"]?.value != "") {
      this.paramsObj.searchBy  = this.cartTableFields[4];
      this.paramsObj.searchValue = param[this.cartTableFields[4]+"_text"]?.value;
    }
    if (param[this.cartTableFields[5] + "_text"]?.value != "") {
      this.paramsObj.searchBy  = this.cartTableFields[5];
      this.paramsObj.searchValue = param[this.cartTableFields[5]+"_text"]?.value;
    }
    
    this.listApiCall(this.paramsObj);
  }
  checkInput(event){
    if(event.code === "Space" && this.cartForm.value.id.trim().length === 0){
      setTimeout(() => {
        this.cartForm.patchValue({["id"]: "" });
      });
    }else if(event.type === "paste"){
      setTimeout(() => {
        if(this.cartForm.value.id.trim().length === 0){
          this.cartForm.patchValue({["id"]: "" });
        }
      });
    } 
    
  }

  

  Search(){
    // this.paramsObj.fromDate = moment(data.start.value).format(FILTER_DATE_FORMAT.DATE_FORMAT);
    // this.paramsObj.toDate = moment(data.end.value).format(FILTER_DATE_FORMAT.DATE_FORMAT);

    // this.paramsObj.fromDate = data.start.value != '' &&  data.start.value != null? moment(data.start.value).format(FILTER_DATE_FORMAT.DATE_FORMAT): '';
    // this.paramsObj.toDate = data.end.value != '' && data.end.value != null ? moment(data.end.value).format(FILTER_DATE_FORMAT.DATE_FORMAT): '';
    this.formService.markFormGroupTouched(this.cartForm);
    if (this.cartForm.valid) {
      this.firstPage=1;
      this.paramsObj.page=1;
    this.cartForm.get('startDate').clearValidators();   
    this.cartForm.get('endDate').clearValidators();   
    this.paramsObj.page=1;
    this.paramsObj.firstPage=1;
    this.paramsObj.id=this.cartForm.controls.id.value;
    this.paramsObj.fromDate= this.cartForm.controls.startDate.value != '' &&  this.cartForm.controls.startDate.value != null? moment(this.cartForm.controls.startDate.value).format(FILTER_DATE_FORMAT.DATE_FORMAT): '';
    this.paramsObj.toDate=this.cartForm.controls.endDate.value != '' &&  this.cartForm.controls.endDate.value != null? moment(this.cartForm.controls.endDate.value).format(FILTER_DATE_FORMAT.DATE_FORMAT): '';
    this.paramsObj.status=this.cartForm.controls.status.value == "100"?'':this.cartForm.controls.status.value;
    this.searchResult = true;
    this.listApiCall(this.paramsObj)
    
    }
  }
  async resetFilter(){
    this.cartForm.get('startDate').clearValidators();   
    this.cartForm.get('endDate').clearValidators();   
    this.cartForm.patchValue({
      ["id"]: "",
      ["startDate"]: "",
      ["endDate"]: "",
      ["status"]: "100",
    });
    this.paramsObj.id='';
    this.paramsObj.fromDate='';
    this.paramsObj.toDate='';
    this.paramsObj.status='';
    this.paramsObj.page=1;
    this.firstPage = 1;
    this.startdate='';
    this.endDate='';
    if( this.searchResult == true){
      this.listApiCall(this.paramsObj);
      this.searchResult = false;
    }
    // this.startdate='';
    // this.endDate='';
    // this.dateFilterSearch.emit(this.dateForm.controls);

  }
  

  setdate(event){
      this.cartForm.controls["endDate"].setValidators([
        Validators.required,
      ]);
      this.cartForm.controls["endDate"].updateValueAndValidity();
    this.startdate= new Date(event.target.value);
  }
  setEndDate(event){
    this.cartForm.controls["startDate"].setValidators([
      Validators.required,
    ]);
    this.cartForm.controls["startDate"].updateValueAndValidity();
    this.endDate= new Date(event.target.value);
  }
 

  async viewInfo(data) {
    
    var totalprice;
    
    const response=data;
    
    response.cartItems.forEach((element, index) => {
      var data = [];
      data[0] = { value: element?.product.attachmentUrl == null || element?.product.attachmentUrl == "" ? " " : element?.product.attachmentUrl, type: TABLE_DATA_TYPE.TEXT };

      data[1] = { value: element?.product.name, type: TABLE_DATA_TYPE.TEXT };
      data[2] = { value: element?.priceData.recurringCharge ==null || element?.priceData.recurringCharge == undefined || element?.priceData.recurringCharge == 0 ? "--":element?.priceData.recurringCharge , type: TABLE_DATA_TYPE.TEXT };
      data[3] = { value: element?.priceData.oneTimeCharge ==null || element?.priceData.oneTimeCharge == undefined || element?.priceData.oneTimeCharge == 0 ? "--":element?.priceData.oneTimeCharge , type: TABLE_DATA_TYPE.TEXT }; 
      data[4] = { value: element?.priceData.deposit ==null ||  element?.priceData.deposit == undefined || element?.priceData.deposit == 0 ? "--":element?.priceData.deposit , type: TABLE_DATA_TYPE.TEXT };
      data[5] = { value :element?.priceData.total ==null || element?.priceData.total == undefined || element?.priceData.total == 0 ? "--":element?.priceData.total , type: TABLE_DATA_TYPE.TEXT};
      element.data = data;
    });
   
     
    
    const initialState = {
      title: "Cart Details",
      viewData: response,
    };
    this.modalRef = this.modalService.show(ViewDetailNewComponent, {
      class: "modal-dialog-centered modal-lg view-detail-modal",
      backdrop: "static",
      initialState,
    });
    this.modalRef.content.response.subscribe(async (result) => {
      if (result) {
      } else {
      }
    });
  }

  intializingMessage() {
   
    this.errorMsg.startDate = {
      required: STATIC_DATA.ERR_MSG_REQUIERD_START_DATE,
    };
    this.errorMsg.endDate = {
      pattern: STATIC_DATA.ERR_MSG_REQUIERD_END_DATE,
    };
 
  }
}
function mapfunction() {
  throw new Error("Function not implemented.");
}

// please do not change-----------------------------------------------------------------------------------------------------------------------------------------------------------------
/** Custom header component for datepicker. */
@Component({
  selector: 'example-header',
  styles: [
    `
    .example-header {
      display: flex;
      align-items: center;
      padding: 0.5em;
    }

    .example-header-label {
      flex: 1;
      height: 1em;
      font-weight: 500;
      text-align: center;
    }

    .example-double-arrow .mat-icon {
      margin: -22%;
    }
  `,
  ],
  template: `
    <div class="example-header">
      <button mat-icon-button (click)="previousClicked('month')">
      <i class="fa-solid fa-angle-left"></i>
      </button>
      <span class="example-header-label">{{periodLabel}}</span>
      <button mat-icon-button (click)="nextClicked('month')">
      <i class="fa-solid fa-angle-right"></i>
      </button>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExampleHeader<D> implements OnDestroy {
  private _destroyed = new Subject<void>();

  constructor(
    private _calendar: MatCalendar<D>,
    private _dateAdapter: DateAdapter<D>,
    @Inject(MAT_DATE_FORMATS) private _dateFormats: MatDateFormats,
    cdr: ChangeDetectorRef,
  ) {
    _calendar.stateChanges.pipe(takeUntil(this._destroyed)).subscribe(() => cdr.markForCheck());
  }

  ngOnDestroy() {
    this._dateFormats.parse.dateInput = 'DD-MM-YYYY';
    this._destroyed.next();
    this._destroyed.complete();
  }

  get periodLabel() {
    return this._dateAdapter
      .format(this._calendar.activeDate, this._dateFormats.display.monthYearLabel)
      .toLocaleUpperCase();
  }

  previousClicked(mode: 'month' | 'year') {
    this._calendar.activeDate =
      mode === 'month'
        ? this._dateAdapter.addCalendarMonths(this._calendar.activeDate, -1)
        : this._dateAdapter.addCalendarYears(this._calendar.activeDate, -1);
  }

  nextClicked(mode: 'month' | 'year') {
    this._calendar.activeDate =
      mode === 'month'
        ? this._dateAdapter.addCalendarMonths(this._calendar.activeDate, 1)
        : this._dateAdapter.addCalendarYears(this._calendar.activeDate, 1);
  }
}
