import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { LOGIN_ROUTE, USER_ROUTES } from 'src/app/constants/constants';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  private subjectForlovationTabChange = new Subject<any>();
  private subjectForLanguageChange = new Subject<any>();
  private subjectForTicketTabChange = new Subject<any>();
  private subjectForReasonTabChange = new Subject<any>();
  private subjectForUserInfo = new Subject<any>();
  private subjectForConfigChange = new Subject<any>();
 
  url = process.env.NG_APP_BACKOFFICE_URL;
  loginUrl = process.env.NG_APP_USER_MAIN_URL;
  urlConfigurator = process.env.NG_APP_configurator_URL;
  constructor(
    private apiService: ApiService
  ) { }
  sendLocationTab(data) {
    this.subjectForlovationTabChange.next(data);
  }
  getLocationTab() {
    return this.subjectForlovationTabChange.asObservable();
  }

  sendLanguage(data) {
    this.subjectForLanguageChange.next(data);
  }
  getLanguage() {
    return this.subjectForLanguageChange.asObservable();
  }
  sendReasonTab(data) {
    this.subjectForReasonTabChange.next(data);
  }
  getReasonTab() {
    return this.subjectForReasonTabChange.asObservable();
  }
  sendTicketTab(data) {
    this.subjectForTicketTabChange.next(data);
  }
  getTicketTab() {
    return this.subjectForTicketTabChange.asObservable();
  }

  sendUserInfo(data) {
    this.subjectForUserInfo.next(data);
  }
  getUserInfo() {
    return this.subjectForUserInfo.asObservable();
  }

  getUsers(params,loader) {
    return this.apiService.get( this.url+USER_ROUTES.USER_LIST + '?' + params, true,false,loader);
  }

  getUserProfile() {
    return this.apiService.get( this.url+USER_ROUTES.USER_PROFILE, true,false,false);
  }

  getstatus(params){
    return this.apiService.get( this.url+USER_ROUTES.USER_STATUS + '?' +params, true,false,false);
  }

  sendConfigTab(data) {
    this.subjectForConfigChange.next(data);
  }
  getConfigTab() {
    return this.subjectForConfigChange.asObservable();
  }
  getConfig(param) {
    return this.apiService.get(this.urlConfigurator + LOGIN_ROUTE.GET_CONFIG+"?"+param, false,false,false);
  }
  changePass(body) {
    return this.apiService.post( this.url+USER_ROUTES.USER_CHANGE_PASS , body , false,false,true);
  }

}
