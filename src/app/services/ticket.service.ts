import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LOGIN_ROUTE, TICKET_ROUTES } from '@constant/constants';
import { param } from 'jquery';
import { environment } from 'src/environments/environment';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class TicketService {
    url = process.env.NG_APP_BACKOFFICE_URL;
    urlConfigurator = process.env.NG_APP_configurator_URL;
  constructor(
    private apiService: ApiService
  ) { }
    
  getTickets(params,activeTabName) {
    if(activeTabName=='ticketType'){
    return this.apiService.get( this.url+TICKET_ROUTES.GET_TICKET_TYPES_LIST + '?' + params, true,false);
    }
    else if( activeTabName == 'category'){
      return this.apiService.get( this.url+TICKET_ROUTES.GET_TICKET_CATEGORY_LIST + '?' + params, true,false);
    }
    else if( activeTabName == 'subCategory'){
      return this.apiService.get( this.url+TICKET_ROUTES.GET_TICKET_SUB_CATEGORY_LIST + '?' + params, true,false);
    }
   }
   postTicket(body,activeTabName) {
    
    if(activeTabName=='ticketType'){
    return this.apiService.post( this.url+TICKET_ROUTES.POST_TICKET_TYPE , body , true,false,true);
    }
    else if( activeTabName == 'category'){
      return this.apiService.post( this.url+TICKET_ROUTES.POST_TICKET_CATEGORY , body, true,false,true);
    }
    else if( activeTabName == 'subCategory'){
      return this.apiService.post( this.url+TICKET_ROUTES.POST_TICKET_SUB_CATEGORY , body, true,false,true);
    }
  }

   editTicket(body,activeTabName) {
      if(activeTabName=='ticketType'){
      return this.apiService.put( this.url+TICKET_ROUTES.PUT_TICKET_TYPE , body , true,false);
      }
      else if( activeTabName == 'category'){
        return this.apiService.put( this.url+TICKET_ROUTES.PUT_TICKET_CATEGORY , body, true,false);
      }
      else if( activeTabName == 'subCategory'){
        return this.apiService.put( this.url+TICKET_ROUTES.PUT_TICKET_SUB_CATEGORY , body, true,false);
      }
   }

   deleteTicket(paramsObj,activeTabName) {
    let params = new HttpParams();
    Object.keys(paramsObj).forEach((key) => {
      if(paramsObj[key]!=''){
        params = params.set(key, paramsObj[key]);
      }
    });
    
    if(activeTabName=='ticketType'){
    return this.apiService.delete( this.url+TICKET_ROUTES.DELET_TICKET_TYPE + '?' + params, '',true);
    }
    else if( activeTabName == 'category'){
      return this.apiService.delete( this.url+TICKET_ROUTES.DELETE_TICKET_CATEGORY + '?' + params, '',true);
    }
    else if( activeTabName == 'subCategory'){
      return this.apiService.delete( this.url+TICKET_ROUTES.DELETE_TICKET_SUB_CATEGORY + '?' + params, '',true);
    }
   }

   getCSVTickets(activeTabName,params) {
      return this.apiService.get( this.url+TICKET_ROUTES.GET_CSV_TICKET_CATEGORY_LIST + '?' + params, true,false);
   }

   getConfig(param) {
    return this.apiService.get(this.urlConfigurator + LOGIN_ROUTE.GET_CONFIG+"?"+param, false,false,false);
  }
 
}
