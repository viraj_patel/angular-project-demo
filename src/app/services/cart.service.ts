import { Injectable } from "@angular/core";
import { CART_ROUTES, TICKET_ROUTES } from "@constant/constants";
import { environment } from "src/environments/environment";
import { ApiService } from "./api.service";

@Injectable({
  providedIn: "root",
})
export class CartService {
  url = process.env.NG_APP_BACKOFFICE_URL;
  constructor(private apiService: ApiService) {}
  getCart(params) {
    
   return this.apiService.get(this.url + CART_ROUTES.GET_CART_LIST+ '?'+params  , true,false,false,true);
  }
  getCount(params) {
    
   return this.apiService.get(this.url + CART_ROUTES.GET_CART_COUNT+ '?'+params  , true,false,false,true);
  }
 
}
