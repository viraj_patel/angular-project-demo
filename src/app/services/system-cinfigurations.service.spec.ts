import { TestBed } from '@angular/core/testing';

import { SystemCinfigurationsService } from './system-cinfigurations.service';

describe('SystemCinfigurationsService', () => {
  let service: SystemCinfigurationsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SystemCinfigurationsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
