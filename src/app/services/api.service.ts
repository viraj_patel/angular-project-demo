import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { HttpClient, HttpEventType, HttpHeaders } from '@angular/common/http';
import { Constant, ERROR_CODE, LOCAL_STORAGE, LOGIN_ROUTE, LOGIN_USER, REDIRECT_AFTER_ERROR_CODE, REFRESH_TOKEN_EMPTY_CODE } from 'src/app/constants/constants';
import { LocalStorageService } from 'src/app/services/localService/localStorage.service';
import { environment } from '../../environments/environment';
import { HttpErrorHandler } from 'src/app/services/error-handler/http-error-handler.service';
import { constants, USER_ROUTES, USER_LOGIN_TOKEN } from 'src/app/constants/constants';
import { NgxSpinnerService } from 'ngx-spinner';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';


import * as _ from 'lodash';
import { BehaviorSubject, Subject } from 'rxjs';
import { UtilityService } from './utility.service';

@Injectable({
    providedIn: 'root',
})
export class ApiService {
  private subjectForLanguageChange = new Subject<any>();
    URL = environment.apiEndPoint;
    modalRef: BsModalRef;
    fingerJsToken:any;
    userRoles = LOGIN_USER;
    constructor(
        private http: HttpClient,
        private toastr: ToastrService,
        private localStorageService: LocalStorageService,
        private httpErrorHandler: HttpErrorHandler,
        private spinner: NgxSpinnerService,
        private modalService: BsModalService,
        private translate: TranslateService,
        private router: Router,
        public helper: UtilityService,
        public localStorage: LocalStorageService

    ) { }
    urlUser = process.env.NG_APP_BACKOFFICE_URL;

    private data = new BehaviorSubject('default data');
  data$ = this.data.asObservable();
  sendLanguage(data) {
    this.subjectForLanguageChange.next(data);
  }
  getLanguage() {
    return this.subjectForLanguageChange.asObservable();
  } 
  changeData(data: any) {
    this.data.next(data)
  }

    async getHeaders(tokenRequired, formData?) {
        const token: any = await this.localStorageService.getDataFromIndexedDB(LOCAL_STORAGE.TOKEN);

        if (tokenRequired) {
                    const headers = new HttpHeaders()
                    .set('Authorization', token)
                    .set('Content-Type', 'application/json')
                    ;
                    return headers;
        } else if (formData) {
                const headers = new HttpHeaders()
                .set('Authorization', token)
                ;
                return headers;
        } else {
            const headers = new HttpHeaders()
                .set('Authorization', '')
                .set('Content-Type', 'application/json');
            return headers;
        }
    }

    async get(path: any, authorize, loaderContinue?, noLoder?,noToastr?:boolean) {
        // ----------------------------------------------------------------------------------- refresh token
        const obj={}
        return new Promise(async (resolve, _) => {
            if (!noLoder) {
                this.spinner.show();
            }
            if(noLoder){
            }
            const success = async (res) => {
                if (res && (res.message || res.msg)) {
                    if(REDIRECT_AFTER_ERROR_CODE.includes(res.code) ||  REDIRECT_AFTER_ERROR_CODE.includes(res.responseCode)){
                        res = await this.unauthorize_401_Handler('get',path,obj,authorize);
                      
                            this.spinner.hide();
                    }else if(ERROR_CODE.includes(res.code) || ERROR_CODE.includes(res.responseCode)){
                        this.toastr.error(res.responseMessage);
                    
                    }else if(noToastr==false){
                        this.toastr.success(res.message ? res.message : res.msg);
                    }
                    
                }else if(ERROR_CODE.includes(res.code) || ERROR_CODE.includes(res.responseCode)){
                    this.toastr.error(res.responseMessage);
                
                }
                if (!loaderContinue && !noLoder) {
                    
                    this.spinner.hide();
                }
                resolve(res);
            };
            const error = async (err) => {
                this.spinner.hide();
                if(REDIRECT_AFTER_ERROR_CODE.includes(err.error.responseCode)){
                    const res = await this.unauthorize_401_Handler('get',path,obj,authorize);
              
                        this.spinner.hide();
                        resolve(res);
                }
                else{
                    this.toastr.error(err.error.responseMessage);

                }
            };
            const netowrkIsConnected = await this.getNetworkConnection();
            if (netowrkIsConnected) {
                const headers = await this.getHeaders(authorize, false);
                return this.http.get(`${this.URL}${path}`, { headers })
                    .subscribe(success, error);
            } else {
                this.spinner.hide();
                this.toastr.error(constants.NO_INTERNET_CONNECTION_MSG);
            }
        });
    }
    async unauthorize_401_Handler(serviceType,path, obj: any, authorize, fromData?, isToaster: boolean = false,noLoder?,loading?,successMessage?){
        const refreshToken: any = await this.localStorageService.getDataFromIndexedDB(LOCAL_STORAGE.REFRESH_TOKEN);
        const objr={'refreshToken':refreshToken}
        const response:any =  await this.resetToken(this.urlUser + LOGIN_ROUTE.REFRESH_TOKEN, objr, false);

        await localStorage.removeItem(LOCAL_STORAGE.TOKEN);
        await localStorage.removeItem(LOCAL_STORAGE.REFRESH_TOKEN);
        await this.localStorage.setDataInIndexedDB(
            LOCAL_STORAGE.TOKEN,
            response.result.token
          );
          await this.localStorage.setDataInIndexedDB(
            LOCAL_STORAGE.REFRESH_TOKEN,
            response.result.refreshToken
          );
          if(serviceType=='post'){
            return await this.post(path, obj, authorize,fromData ? fromData : false, false, false,true);
          }
          else if( serviceType=='get'){
            return await this.get(path, authorize,false,false);
          }
          else if( serviceType=='put'){
            return await this.put(path,obj, authorize,false,false,successMessage);
          }
          else if( serviceType=='delete'){
            return await this.delete(path,obj,authorize);
          }
    }

    async resetToken(path: any, obj: any, authorize, fromData?, isToaster: boolean = false,noLoder?,loading?) {
 

        return new Promise(async (resolve, _) => {
            const rolePath = path.indexOf('user-type/') !== -1;
            if(!noLoder){
                this.spinner.show();
            }
            const success = async (res) => {
                if (res && (res.message || res.msg || res.responseMessage) && !rolePath) {
                    if(REDIRECT_AFTER_ERROR_CODE.includes(res.code) ||  REDIRECT_AFTER_ERROR_CODE.includes(res.responseCode) || REFRESH_TOKEN_EMPTY_CODE.includes(res.code) || REFRESH_TOKEN_EMPTY_CODE.includes(res.responseCode)){
                        
                        // this.toastr.error(res.message ? res.message : res.msg);
                        localStorage.removeItem('token');
                        await this.localStorage.clearStorage();
                        await this.localStorage.clearDataFromIndexedDB();
                        this.router.navigate(['/']);    
                        
                        this.spinner.hide();
                    }else if(ERROR_CODE.includes(res.code) || ERROR_CODE.includes(res.responseCode || REFRESH_TOKEN_EMPTY_CODE.includes(res.code) || REFRESH_TOKEN_EMPTY_CODE.includes(res.responseCode)) ){
                        // this.toastr.error(res.responseMessage);
                        localStorage.removeItem('token');
                        await this.localStorage.clearStorage();
                        await this.localStorage.clearDataFromIndexedDB();
                        this.router.navigate(['/']); 
                    }
                    else{
                        if(isToaster){
                            this.toastr.success(res.message ? res.message : res.msg?res.msg:res.responseMessage);
                        }
                        
                    }
                }
                if(!noLoder && !loading){
                    this.spinner.hide();
                }
                resolve(res);
            };
            const error = async (err) => {
                this.spinner.hide();
                // this.toastr.error(err.error.responseMessage);
                
                if(REDIRECT_AFTER_ERROR_CODE.includes(err.error.responseCode) || ERROR_CODE.includes(err.error.responseCode) || REFRESH_TOKEN_EMPTY_CODE.includes(err.error.code) || REFRESH_TOKEN_EMPTY_CODE.includes(err.error.responseCode)){
                    localStorage.removeItem('token');
                    await this.localStorage.clearStorage();
                    await this.localStorage.clearDataFromIndexedDB();
                    this.router.navigate(['/']);    
                    this.spinner.hide();

                }
               
            };
            const netowrkIsConnected = await this.getNetworkConnection();
            if (netowrkIsConnected) {
                let headers: any;
                if (path.indexOf(USER_ROUTES.SIGN_UP) === -1) {
                    headers = await this.getHeaders(authorize, fromData ? fromData : false);
                }
                return this.http.post<any>(`${this.URL}${path}`, obj, { headers })
                    .subscribe(success, error);
            } else {
                
                if(!noLoder){
                    this.spinner.hide();
                }
                this.toastr.error(constants.NO_INTERNET_CONNECTION_MSG);
            }

        });
    }
    async post(path: any, obj: any, authorize, fromData?, isToaster: boolean = false,noLoder?,loading?) {
 

        return new Promise(async (resolve, _) => {
            const rolePath = path.indexOf('user-type/') !== -1;
            if(!noLoder){
                this.spinner.show();
            }
            const success = async (res) => {
                if (res && (res.message || res.msg || res.responseMessage) && !rolePath) {
                    if(REDIRECT_AFTER_ERROR_CODE.includes(res.code) ||  REDIRECT_AFTER_ERROR_CODE.includes(res.responseCode)){
                    res = await this.unauthorize_401_Handler('post',path,obj, authorize,fromData, isToaster, noLoder,loading);
                    this.spinner.hide();
                    }else if(ERROR_CODE.includes(res.code) || ERROR_CODE.includes(res.responseCode) ){
                        this.toastr.error(res.responseMessage);
                    }
                    else{
                        if(isToaster){
                            this.toastr.success(res.message ? res.message : res.msg?res.msg:res.responseMessage);
                        }
                        
                    }
                }
                if(!noLoder && !loading){
                    this.spinner.hide();
                }
                resolve(res);
            };
            const error = async (err) => {
                this.spinner.hide();
                if(REDIRECT_AFTER_ERROR_CODE.includes(err.error.responseCode)){
                    const res = await this.unauthorize_401_Handler('post',path,obj, authorize,fromData, isToaster, noLoder,loading);
                    
            
                    resolve(res);

                }
                else{
                    this.toastr.error(err.error.responseMessage);

                }
            };
            const netowrkIsConnected = await this.getNetworkConnection();
            if (netowrkIsConnected) {
                let headers: any;
                if (path.indexOf(USER_ROUTES.SIGN_UP) === -1) {
                    headers = await this.getHeaders(authorize, fromData ? fromData : false);
                }
                return this.http.post<any>(`${this.URL}${path}`, obj, { headers })
                    .subscribe(success, error);
            } else {
                
                if(!noLoder){
                    this.spinner.hide();
                }
                this.toastr.error(constants.NO_INTERNET_CONNECTION_MSG);
            }

        });
    }


    async put(path: any, obj: any, authorize, fromData?, isToaster: boolean = true,noLoder?,loading?,successMessage?) {
        return new Promise(async (resolve, _) => {
            const rolePath = path.indexOf('user-type/') !== -1;
            if(!noLoder){
                this.spinner.show();
            }
            
            const success = async (res) => {
                if (res && (res.message || res.msg || res.responseMessage) && !rolePath) {
                    if(REDIRECT_AFTER_ERROR_CODE.includes(res.code) ||  REDIRECT_AFTER_ERROR_CODE.includes(res.responseCode)){
                        res = await this.unauthorize_401_Handler('put',path,obj, authorize,fromData, isToaster, noLoder,loading,successMessage);
                        
                  
                            this.spinner.hide();
                    }else if(ERROR_CODE.includes(res.code) || ERROR_CODE.includes(res.responseCode)){
                        this.toastr.error(res.responseMessage);
                    }else{
                        if(successMessage!=undefined){
                        this.toastr.success(successMessage);
                        }
                        else{
                            this.toastr.success(res.message ? res.message : res.msg?res.msg:res.responseMessage);
                        }
                    }
                }
                if(!noLoder && !loading){
                    this.spinner.hide();
                }
                resolve(res);
            };
            const error = async (err) => {
                this.spinner.hide();
                if(REDIRECT_AFTER_ERROR_CODE.includes(err.error.responseCode)){
                    const res = await this.unauthorize_401_Handler('put',path,obj,  authorize,fromData, isToaster, noLoder,loading,successMessage);
        
                    resolve(res);

                }
                else{
                    this.toastr.error(err.error.responseMessage);

                }
            };
            const netowrkIsConnected = await this.getNetworkConnection();
            if (netowrkIsConnected) {
                let headers: any;
                if (path.indexOf(USER_ROUTES.SIGN_UP) === -1) {
                    headers = await this.getHeaders(authorize, fromData ? fromData : false);
                }
                return this.http.put<any>(`${this.URL}${path}`, obj, { headers })
                    .subscribe(success, error);
            } else {
                
                if(!noLoder){
                    this.spinner.hide();
                }
                this.toastr.error(constants.NO_INTERNET_CONNECTION_MSG);
            }

        });
    }

    async postFormDataReqWithToken(path: any, obj: any) {
        return new Promise(async (resolve, _) => {
            const success = (res) => {
                if (res && res.message) {
                    this.toastr.success(res.message);
                }
                this.spinner.hide();
                resolve(res);
            };
            const error = (err) => {
                return this.httpErrorHandler.handleError(path, err);
            };
            const netowrkIsConnected = await this.getNetworkConnection();
            if (netowrkIsConnected) {
                const headers = await this.getHeaders(false, true);
                return this.http.post<any>(`${this.URL}${path}`, obj, {
                    headers
                })
                    .subscribe(success, error);
            } else {
                this.spinner.hide();
                this.toastr.error(constants.NO_INTERNET_CONNECTION_MSG);
            }
        });
    }
    async delete(path: any, obj: any, authorize?) {
        return new Promise(async (resolve, _) => {
            this.spinner.show();
            const success = async (res) => {
             
                if (res && (res.message || res.msg || res.responseMessage)) {
                    if(REDIRECT_AFTER_ERROR_CODE.includes(res.code) || REDIRECT_AFTER_ERROR_CODE.includes(res.responseCode)){
                        res = await this.unauthorize_401_Handler('delete',path,obj, authorize);
                            this.spinner.hide();
                    }else if(ERROR_CODE.includes(res.code) || ERROR_CODE.includes(res.responseCode)){
                        this.toastr.error(res.responseMessage);
                    }else{
                        this.toastr.success(res.message ? res.message : res.msg?res.msg:res.responseMessage);
                    }
                }
                this.spinner.hide();
                resolve(res);
            };
            const error = async (err) => {
                this.spinner.hide();
                if(REDIRECT_AFTER_ERROR_CODE.includes(err.error.responseCode )){
                    const res = await this.unauthorize_401_Handler('delete',path,obj, authorize);
                    resolve(res);

                }
                else{
                    this.toastr.error(err.error.responseMessage);

                }
            };
            const netowrkIsConnected = await this.getNetworkConnection();
            if (netowrkIsConnected) {
                const headers = await this.getHeaders(authorize, false);
                return this.http.delete(`${this.URL}${path}`, { headers, body:obj})
                    .subscribe(success, error);
            } else {
                this.spinner.hide();
                this.toastr.error(constants.NO_INTERNET_CONNECTION_MSG);
            }
        });
    }


    getNetworkConnection() {
        const isOnline: boolean = navigator.onLine;
        if (isOnline) {
            return true;
        }
        return false;
    }

 


}
