import { ConfirmDialogModel, ConfirmDialogComponent } from 'src/app/shared/components/confirmation-dialog/confirmation-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class ConfirmationDialogService {

    constructor(public dialog: MatDialog) { }

    async show(title, message) {
        return new Promise(async (resolve, reject) => {
            const dialogData = new ConfirmDialogModel(title, message);

            const dialogRef = this.dialog.open(ConfirmDialogComponent, {
                maxWidth: '400px',
                data: dialogData
            });

            dialogRef.afterClosed().subscribe(dialogResult => {
                resolve(dialogResult);
            });
        });
    }
}
