import { Injectable } from '@angular/core';
import { PROFILE_ROUTE, USER_ROUTES } from '@constant/constants';
import { Subject } from 'rxjs';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class MyProfileService {
  private subjectForProfileChange = new Subject<any>();
  urlConfigurator = process.env.NG_APP_configurator_URL;
  url = process.env.NG_APP_BACKOFFICE_URL;
  constructor(
    private apiService: ApiService
  ) { }
  sendProfile(data) {
    this.subjectForProfileChange.next(data);
  }
  getProfile() {
    return this.subjectForProfileChange.asObservable();
  }
  getUserDeatails() {
     return this.apiService.get( this.url+PROFILE_ROUTE.GET_PROFILE  ,true,false);
   }
  updateUser(param) {
    return this.apiService.put( this.url+PROFILE_ROUTE.UPDATE_PROFILE  ,param ,true,false);
  }

}
