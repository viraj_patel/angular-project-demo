import { Injectable } from '@angular/core';
import { CONSENT_ROUTES, LOGIN_ROUTE, ROLES_AND_PERMISSIONS_ROUTE, STAFF_ROUTE } from '@constant/constants';
import { ApiService } from './api.service';
@Injectable({
  providedIn: 'root'
})
export class StaffService {

  url = process.env.NG_APP_BACKOFFICE_URL;
  urlConfigurator = process.env.NG_APP_configurator_URL;
  permissionsList:any=[];
  permissions:any=[];
  loginRoles:any=[];
  constructor(
    private apiService: ApiService
  ) { }

  async getLoginUserRoles() {
    const res:any = await this.apiService.get( this.url + ROLES_AND_PERMISSIONS_ROUTE.USER_LOGIN_ROLES,true,false);
    // this.loginRoles = res.result.map(v=>{return v.name});
    return  res.result;
  }

  getConfig(param) {
    return this.apiService.get(this.urlConfigurator + LOGIN_ROUTE.GET_CONFIG+"?"+param, false,false,false);
  }

  getStaffUser() {
    return this.apiService.get( this.url + STAFF_ROUTE.USER_BY_GROUP,true,false);
  }
  getRoles(){
    return this.apiService.get( this.url+ROLES_AND_PERMISSIONS_ROUTE.USER_ROLES,true,false);
  }

  updatePermission(data){
    return this.apiService.post(this.url+ ROLES_AND_PERMISSIONS_ROUTE.UPDATE_PERMISSIONS , data, true, false, true,false,true)
  }

  async getLoginPermissions(params){
    const resAction:any = await this.apiService.get( this.url + ROLES_AND_PERMISSIONS_ROUTE.ACTION_LIST,true,false);
    if(resAction?.result){
        this.permissions= resAction?.result;
    }
    const res:any = await this.apiService.post( this.url+ROLES_AND_PERMISSIONS_ROUTE.GET_PERMISSIONS,params, true, false, false);
    if(res?.result){
        this.permissionsList= res?.result?.permissions;
    }
    let module=[];
    for (const iterator of this.permissionsList) {
        let permissionsValue={key:iterator.alias,name:iterator.name,children:iterator.children};
        let keyOrPermissions =this.permissions.reduce((obj, item) => {
            return {
              ...obj,
              [item.name]: false,
            };
          }, permissionsValue);
           
           
        if(iterator.actions){
            const values = iterator.actions.map(v=>{return v.name});
            for (const iterator of Object.keys(keyOrPermissions)) {
                const index = values.indexOf(iterator);
                if(index > -1){
                    keyOrPermissions[iterator] = true;
                }
            }
            module.push(keyOrPermissions);
        }else{
            module.push(keyOrPermissions);
        }
    }
    return module;
  }

  async getLoginPermissionsForModuleChildList(key){
    if(this.permissions.length == 0){
        const resAction:any = await this.apiService.get( this.url + ROLES_AND_PERMISSIONS_ROUTE.ACTION_LIST,true,false);
        if(resAction?.result){
            this.permissions= resAction?.result;
        }
    }
    if(this.permissionsList.length == 0){
      if(this.loginRoles.length == 0){
        const res:any = await this.apiService.get( this.url + ROLES_AND_PERMISSIONS_ROUTE.USER_LOGIN_ROLES,true,false);
        this.loginRoles = res.result;
      }
      const data = {roles:this.loginRoles}
      const res:any = await this.apiService.post( this.url+ROLES_AND_PERMISSIONS_ROUTE.GET_PERMISSIONS,data, true, false, false);
        if(res?.result){
            this.permissionsList= res?.result?.permissions;
        }
    }
  let child=[];
  for (const iterator of this.permissionsList) {
      if(key == iterator.alias && iterator.children && iterator.children.length > 0){
        child = iterator.children;
      }else if(iterator.children && iterator.children.length > 0){

          const service = this.checkChildren(iterator.children,key);
          if(service != false){
              child = service.children;
          }
      }
      
  }

  let module=[];
  for (const iterator of child) {
      let permissionsValue={key:iterator.alias, children:iterator.children};
      let keyOrPermissions =this.permissions.reduce((obj, item) => {
          return {
            ...obj,
            [item.name]: false,
          };
        }, permissionsValue);
         
         
      if(iterator.actions){
          const values = iterator.actions.map(v=>{return v.name});
          for (const iterator of Object.keys(keyOrPermissions)) {
              const index = values.indexOf(iterator);
              if(index > -1){
                  keyOrPermissions[iterator] = true;
              }
          }
          module.push(keyOrPermissions);
      }else{
          module.push(keyOrPermissions);
      }
  }
      return module;
  
}
  
  async getLoginPermissionsForModule(key){
    if(this.permissions.length == 0){
        const resAction:any = await this.apiService.get( this.url + ROLES_AND_PERMISSIONS_ROUTE.ACTION_LIST,true,false);
        if(resAction?.result){
            this.permissions= resAction?.result;
        }
    }
    if(this.permissionsList.length == 0){
      if(this.loginRoles.length == 0){
        const res:any = await this.apiService.get( this.url + ROLES_AND_PERMISSIONS_ROUTE.USER_LOGIN_ROLES,true,false);
        this.loginRoles = res.result;
      }
      const data = {roles:this.loginRoles}
      const res:any = await this.apiService.post( this.url+ROLES_AND_PERMISSIONS_ROUTE.GET_PERMISSIONS,data, true, false, false);
        if(res?.result){
            this.permissionsList= res?.result?.permissions;
        }
    }
    let module=[];
    for (const iterator of this.permissionsList) {
        if(key == iterator.alias){
            let permissionsValue={key:iterator.alias};
            let keyOrPermissions =this.permissions.reduce((obj, item) => {
                return {
                  ...obj,
                  [item.name]: false,
                };
              }, permissionsValue);
               
               
            if(iterator.actions){
                const values = iterator.actions.map(v=>{return v.name});
                for (const key of Object.keys(keyOrPermissions)) {
                    const index = values.indexOf(key);
                    if(index > -1){
                        keyOrPermissions[key] = true;
                    }
                }
                module.push(keyOrPermissions);
            }else{
                module.push(keyOrPermissions);
            }
        }else if(iterator.children && iterator.children.length > 0){
            const service = this.checkChildren(iterator.children,key);
            if(service != false){
                let permissionsValue={key:service.alias};
                let keyOrPermissions =this.permissions.reduce((obj, item) => {
                    return {
                      ...obj,
                      [item.name]: false,
                    };
                  }, permissionsValue);
                   
                if(service.actions){
                    const values = service.actions.map(v=>{return v.name});
                    for (const key of Object.keys(keyOrPermissions)) {
                        const index = values.indexOf(key);
                        if(index > -1){
                            keyOrPermissions[key] = true;
                        }
                    }
                    module.push(keyOrPermissions);
                }else{
                    module.push(keyOrPermissions);
                }
            }
        }
        
    }
   
        return module;
    
  }

  checkChildren(array,key){
    for (const iterator of array) {
        if(key == iterator.alias){
            return iterator;
        }else if(iterator.children && iterator.children.length > 0){
          const res = this.checkChildren(iterator.children,key);
            if( res != false){
              return res;
            }
        }
    }
    return false;
  }
  getPermissions(params){
    return this.apiService.post( this.url+ROLES_AND_PERMISSIONS_ROUTE.GET_PERMISSIONS,params, true, false, false);
  }

  getActionsList(){
    return this.apiService.get( this.url + ROLES_AND_PERMISSIONS_ROUTE.ACTION_LIST,true,false);
  }
}
