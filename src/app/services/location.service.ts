import { Injectable } from '@angular/core';
import { LOCATION_ROUTE, LOGIN_ROUTE } from '@constant/constants';
import { ApiService } from './api.service';
import { environment } from 'src/environments/environment';
import { Subject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class LocationService {
  private subjectForLanguageChange = new Subject<any>();

  url = process.env.NG_APP_BACKOFFICE_URL;
  urlConfigurator = process.env.NG_APP_configurator_URL;
  spinnerType = "sp1"


  constructor(
    private apiService: ApiService
  ) { }

  sendLanguage(data) {
    this.subjectForLanguageChange.next(data);
  }
  getLanguage() {
    return this.subjectForLanguageChange.asObservable();
  }

  editLocation(data,params) {
      return this.apiService.put( this.url+LOCATION_ROUTE.EDIT_LOCATION+'?'+params, data,true,false);
  }

  addLocation(data,params) {
    return this.apiService.post( this.url+LOCATION_ROUTE.ADD_LOCATION+'?'+params, data,true,false,true);
  }


  deleteLocation(data,params) {
    return this.apiService.delete( this.url+LOCATION_ROUTE.DELETE_LOCATION+'?'+params, data,true);
  }

  getLocation(data,params,type) {
      return this.apiService.post( this.url+LOCATION_ROUTE.GET_LOCATION+'?'+params, data,true,false,false,false,false);    
  }
  uploadFile(params,FormData){
    // return this.apiService.csvPost( this.url + LOCATION_ROUTE.FILE_UPLOAD + '?'+params,FormData,false,true,true );
  }

  // downloadCSVSample(params) {
  //   return this.apiService.get( this.url+LOCATION_ROUTE.GET_LOCATION+'?'+params,true,false);   
  // }

  downloadSampleCsv(param) {
    return this.apiService.get( this.url + LOCATION_ROUTE.DOWNLOAD_SAMPLE_CSV + '?' +param  ,true,false,false);
  }

  getConfig(param) {
    return this.apiService.get(this.urlConfigurator + LOGIN_ROUTE.GET_CONFIG+"?"+param, false,false,false);
  }

}
