import { Injectable } from '@angular/core';
import { CONSENT_ROUTES, LOGIN_ROUTE } from '@constant/constants';
import { environment } from 'src/environments/environment';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class ConsentService {

  urlConfigurator = process.env.NG_APP_configurator_URL;
  url = process.env.NG_APP_BACKOFFICE_URL;
  constructor(
    private apiService: ApiService
  ) { }

  getConsents(param) {
     return this.apiService.get( this.url+CONSENT_ROUTES.GET_CONSENT_LIST  + '?' +param ,true,false);
   }
  postConsents(data) {
    return this.apiService.post( this.url+CONSENT_ROUTES.POST_CONSENT_CREATE ,data,true,false,true);  
  }
  putConsents(data)  {
    return this.apiService.put( this.url+CONSENT_ROUTES.PUT_CONSENT_UPDATE ,data,true,false);
  }
  deleteConsents(param) {
    return this.apiService.delete( this.url+CONSENT_ROUTES.DELETE_CONSENT_ID + '/' + param ,'',true);
  }

  getConsentsCsv() {
    return this.apiService.get( this.url+CONSENT_ROUTES.SAMPLE_CSV,true,false);
  }

  getConfig(param) {
    return this.apiService.get(this.urlConfigurator + LOGIN_ROUTE.GET_CONFIG+"?"+param, false,false,false);
  }
}
