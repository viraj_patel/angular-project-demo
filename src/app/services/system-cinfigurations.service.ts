import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SYS_ROUTES } from '@constant/constants';
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class SystemCinfigurationsService {
  url = process.env.NG_APP_BACKOFFICE_URL;
  public subjectForLanguageChange = new Subject<any>();

  constructor(
    private apiService: ApiService,
    private http:HttpClient
  ) { }
  getTabs(){
    return this.apiService.get(this.url+SYS_ROUTES.TAB_ROUTE ,true,false)
  }
  sendLanguage(data) {
    this.subjectForLanguageChange.next(data);
  }
  getLanguage() {
    return this.subjectForLanguageChange.asObservable();
  }
  getSysConfig(param) {    

     return this.apiService.get( this.url+SYS_ROUTES.GET_SYS_LIST  + '?' +param ,true,false,false);

  }
 
  postSysConfig(data) {
    return this.apiService.post( this.url+SYS_ROUTES.POST_SYS_CREATE ,data,true,false);  
  }
  postSysConfigStatus(data) {
    return this.apiService.post( this.url+SYS_ROUTES.POST_SYS_STATUS ,data,true,false);  
  }
  putSysConfig(data,msg?)  {
    return this.apiService.put( this.url+SYS_ROUTES.PUT_SYS_UPDATE ,data,true,false,false,false,false,msg);
  }
  deleteSysConfig(param) {
    return this.apiService.delete( this.url+SYS_ROUTES.DELETE_SYS_ID + '/' + param ,true);
  }

  postSysCheckBoxStatus(data) {
    return this.apiService.post( this.url+SYS_ROUTES.UPDATE_CUSTOMER_ACCESS_CHECKBOX ,data,true,false,true); 
    // return this.http.post(this.url+SYS_ROUTES.UPDATE_CUSTOMER_ACCESS_CHECKBOX, data) 
  }

  editCustomerAccess(data) {
    return this.apiService.post( this.url+SYS_ROUTES.UPDATE_CUSTOMER_ACCESS ,data,true,false, true);  
  }

  getCustomerAccessDataData() {
    return this.apiService.get( this.url+SYS_ROUTES.GET_CUSTOMER_ACCESS_PERMISSION,true,false);
  };

  getCustomerAccessExtraColumns() {
    return this.apiService.get( this.url+SYS_ROUTES.GET_CUSTOMER_ACCESS_ROLES,true,false);
  }
  downloadSampleCsv() {
    return this.apiService.get( this.url+SYS_ROUTES.DOWNLOAD_SAMPLE_CSV ,true,false,false);
  }

  exportData(data) {
    return this.apiService.post( this.url+SYS_ROUTES.EXPORT_DATA ,data,true,false, true);  
  }
  
}
