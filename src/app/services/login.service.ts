import { Injectable } from '@angular/core';
import { constants, LOGIN_ROUTE, USER_ROUTES, LOCAL_STORAGE, USER_LOGIN_TOKEN } from 'src/app/constants/constants';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { HttpErrorHandler } from './error-handler/http-error-handler.service';
import { LocalStorageService } from './localService/localStorage.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { BehaviorSubject, Subject } from 'rxjs';
import { UtilityService } from './utility.service';
@Injectable({
  providedIn: 'root'
})
export class LoginService {
private subjectForLanguageChange = new Subject<any>();

  constructor(
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private httpErrorHandler: HttpErrorHandler,
    private http: HttpClient,
    private localStorageService: LocalStorageService,
    public helper: UtilityService
  ) { }
  URL = environment.apiEndPoint;
  drupal_URL = process.env.NG_APP_drupal_image_URL;
  urlUser = process.env.NG_APP_BACKOFFICE_URL;
  urlLOGIN = process.env.NG_APP_USER_MAIN_URL;
  urlConfigurator = process.env.NG_APP_configurator_URL;
  private data = new BehaviorSubject('default data');
  data$ = this.data.asObservable();

  sendLanguage(data) {
    this.subjectForLanguageChange.next(data);
  }
  getLanguage() {
    return this.subjectForLanguageChange.asObservable();
  }
  changeData(data: any) {
    this.data.next(data)
  }
  async getHeaders(tokenRequired, formData?) {
    const token: any = await this.localStorageService.getDataFromIndexedDB(LOCAL_STORAGE.TOKEN);
    if (tokenRequired) {
                const headers = new HttpHeaders()
                .set('Authorization', token)
                .set('Content-Type', 'application/json')
                ;
                return headers;
    } else if (formData) {
            const headers = new HttpHeaders()
            .set('Authorization', token)
            ;
            return headers;
    } else {
        const headers = new HttpHeaders()
            .set('Authorization', '')
            .set('Content-Type', 'application/json');
        return headers;
    }
}
  async post(path: any, obj: any, authorize, fromData?, isToaster: boolean = true,noLoder?,loading?) {
    return new Promise(async (resolve, _) => {
        const rolePath = path.indexOf('user-type/') !== -1;
        if(!noLoder){
            this.spinner.show();
        }
        
        const success = (res) => {
            if (res && (res.message || res.msg || res.responseMessage) && !rolePath) {
                if (path.indexOf(USER_ROUTES.DEVICE_INFO) === -1) {
                    if (isToaster) {
                        this.toastr.success(res.message ? res.message : res.msg?res.msg:res.responseMessage,'');
                    }
                }
            }
            if(!noLoder && !loading){
                this.spinner.hide();
            }
            
            
            resolve(res);
        };
        const error = (err) => {
          // return this.httpErrorHandler.handleError(path, err);
        
          this.helper.hideLoading();
          return this.helper.showErrorToast(err.error.responseMessage);
        };
        const netowrkIsConnected = await this.getNetworkConnection();
        if (netowrkIsConnected) {
            let headers: any;
            if (path.indexOf(USER_ROUTES.SIGN_UP) === -1) {
                headers = await this.getHeaders(authorize, fromData ? fromData : false);
            }
            return this.http.post<any>(`${this.URL}${path}`, obj, { headers })
                .subscribe(success, error);
        } else {
            
            if(!noLoder){
                this.spinner.hide();
            }
            this.toastr.error(constants.NO_INTERNET_CONNECTION_MSG,'');
        }

    });
}

async get(path: any, authorize, loaderContinue?, noLoder?) {
  return new Promise(async (resolve, _) => {
      if (loaderContinue || !noLoder) {
          this.spinner.show();
      }
      if(noLoder){
      }
      const success = (res) => {
          // toaster success message
          if (res && (res.message || res.msg)) {
              this.toastr.success(res.message ? res.message : res.msg,'');
          }
          if (!loaderContinue && !noLoder) {
              this.spinner.hide();
          }
          resolve(res);
      };
      const error = (err) => {
          // return this.httpErrorHandler.handleError(path, err);
          return this.helper.showErrorToast(err.responseMessage);
      };
      const netowrkIsConnected = await this.getNetworkConnection();
      if (netowrkIsConnected) {
          const headers = await this.getHeaders(authorize, false);
          return this.http.get(`${this.URL}${path}`, { headers })
              .subscribe(success, error);
      } else {
          this.spinner.hide();
          this.toastr.error(constants.NO_INTERNET_CONNECTION_MSG,'');
      }
  });
}

getUserProfile() {
  return this.get(this.urlUser+USER_ROUTES.USER_PROFILE, true,false,false);
}

getNetworkConnection() {
  const isOnline: boolean = navigator.onLine;
  if (isOnline) {
      return true;
  }
  return false;
}
  login(data) {
    return this.post(this.urlUser + LOGIN_ROUTE.LOGIN, data, false);
  }

  forgot(data) {
    return this.post(this.urlUser + LOGIN_ROUTE.FORGOT, data, false);
  }

  otpVerify(data) {
    return this.post(this.urlUser + LOGIN_ROUTE.OTP, data, false);
  }

  resetPasswordWithOTP(data) {
    return this.post(this.urlUser + LOGIN_ROUTE.RESET_OTP, data, false);
  }

  getConfig(param) {
    return this.get(this.urlConfigurator + LOGIN_ROUTE.GET_CONFIG+"?"+param, false,false,false);
  }

  resetPasswordWithLink(data) {
    return this.post(this.urlUser + LOGIN_ROUTE.RESET_LINK, data, false);
  }

  get_drupal_image(){
    return this.get(this.drupal_URL ,false,false,false);
  }
}
