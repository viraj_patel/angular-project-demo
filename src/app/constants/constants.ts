export class Constants {
  public static pageLimit = 5;
  public static otherLocationId = '9999';
  public static memberRoutes = {
    member: '/user/get-member',
    addMember: '/user/member/add-member',
    allMembers: '/user/all-member',
    setorWiseMember: '/user/sectors-wise-members',
    UpdateMember: '/user/member/update-member',
    sendMessage: '/user/send-message',
    importMember: '/user/import-member',
    exportMember: '/user/export-member',
    updateStatus: '/user/update-status',
    memberAttendance: '/attendance/member-session',
    loginDetails: '/user/sendloginDetails',
    deleteMember: '/user/delete-members',
    roleId: 2,
    pageNo: 1,
  };
  public static sectorRoutes = {
    getSectors: ''
  }
  public static adminRoutes = {
    roleId: 1,
    adminProfile: '/user/admin-profile',
    adminUpdateProfile: '/user/update-admin-profile'
  };

}

export const Web_Language = {
  EN:'en',
  AR:'ar'
}

export const constants = {
  NO_INTERNET_CONNECTION_MSG: 'No Internet Connection',
  INITIAL_PAGE: 1
};
export const LOCAL_STORAGE = {
  ID: 'id',
  EMAIL: 'email',
  NAME: 'name',
  USER_INFO:'user_info',
  MOBILE_NUMBER: 'mobileNumber',
  USER_TYPE: 'userType',
  TOKEN: 'token',
  REFRESH_TOKEN:'refresh_token',
  LANGUAGE: 'language',
  SET_TOKEN: 'setToken',
  DEVICE_ID: 'deviceId',
  DEVICE_TOKEN: 'deviceToken',
  USER_TYPES: 'userTypes',
  PROFILE_IMAGE: 'thumbImagePath',
  ACTIVE_TAB: 'activeTab',
  ADD_EDIT_USER_INFO: 'addEditUserInfo',
  ACTIVE_TAB_BY_ROLE: 'activeTabByRole',
  SESSION_TIME:'sessionTime',
  APP_TOKEN:'apptoken',
  DEVICE_TYPE:'devicetype',
  DATA_MAIN_ACTIVE_TAB:"data_main_active_tab",
  DATA_ACTIVE_TAB:"data_active_tab",
  DATA_ACTIVE_FUNCTION:"data_active_function",
  DATA_ACTIVE_EDIT_DATA:"data_active_edit_data",
  FORGOT_EMAIL:"forgot_email",
  FORGOT_TYPE:"forgot_type",
  FORGOT_OTP:"forgot_otp",
  FORGOT_UUID:"uuid"
};
export const Constant = {
  MAX_FILE_SIZE_UPLOAD: 2000000, // byte
  PASSWORD_MAXLENGTH: 30,
  PASSWORD_MINLENGTH: 6,
  MOBILE_NUMBER_ZONE: 'US',
  DATE_FORMAT: 'dd-MM-yyyy',
  TOKEN: 'token',
  NO_INTERNET_CONNECTION_MSG: 'No internet connection!',
  MOBILE_MAXLENGTH: 15,
  ERROR_PRECONDITION_CODE: 412,
  SOCKET_CONNECTION_TIMEOUT: 20000,
};
export const LOGIN_USER = {
  ADMIN: 'Super Admin',
  PRIMARY_SUB_ADMIN: 'Primary SubAdmin',
  CLIENT_ADMIN: 'Client Admin',
  THERAPIST_MANAGER: 'Therapist Manager',
  THERAPIST: 'Therapist',
  PATIENT: 'Patient',
  SECONDARY_SUB_ADMIN: 'Secondary SubAdmin',
  CLIENT: 'Client',
  C_ADMIN: 'Admin'
};
export const USER_ROUTES = {
  SIGN_UP: 'auth/login',
  OTP_SIGN_UP: 'auth/verify-otp',
  RESEND_OTP: 'auth/resend-otp',
  OTP_AUTH_OPTION: 'auth/select-2fa-method',
  FORGOT_PASSWORD: 'auth/forgot-password',
  RESET_PASSWORD: 'auth/reset-password',
  CHANGE_PASSWORD: '/user/change-password',
  DEVICE_INFO: 'auth/device',
  USER_LIST:'user/list',
  USER_PROFILE:'user/profile',
  USER_STATUS:'user/list/status',
  USER_CHANGE_PASS:'user/changePassword'
};

export const TICKET_ROUTES = {
  GET_TICKET_TYPES_LIST: 'ticket/get-ticketTypes',
  GET_TICKET_CATEGORY_LIST:'ticket/get-categories',
  GET_TICKET_SUB_CATEGORY_LIST:'ticket/get-subCategories',
  POST_TICKET_TYPE:'ticket/create-ticketType',
  POST_TICKET_CATEGORY:'ticket/create-category',
  POST_TICKET_SUB_CATEGORY:'ticket/create-subCategory',
  PUT_TICKET_TYPE:'ticket/update-ticketType',
  PUT_TICKET_CATEGORY:'ticket/update-category',
  PUT_TICKET_SUB_CATEGORY:'ticket/update-subCategory',
  DELET_TICKET_TYPE:'ticket/delete-ticketType',
  DELETE_TICKET_CATEGORY:'ticket/delete-category',
  DELETE_TICKET_SUB_CATEGORY:'ticket/delete-subCategory',
  UPLOAD_TICKET:'ticket/bulk-upload',
  GET_CSV_TICKET_CATEGORY_LIST:'ticket/download-sample',
}
export const CART_ROUTES = {
  GET_CART_LIST: 'cart/cartList',
  GET_CART_COUNT:'cart/status',
}


export const CONSENT_ROUTES = {
  GET_CONSENT_LIST: 'consent/list',
  POST_CONSENT_CREATE: 'consent/create',
  PUT_CONSENT_UPDATE: 'consent/update',
  DELETE_CONSENT_ID:'consent',
  UPLOAD_CSV:'consent/bulk-upload',
  SAMPLE_CSV:'consent/download-sample'
}
export const SYS_ROUTES = {
  TAB_ROUTE:'configuration/groupsAndSubGroups',
  GET_SYS_LIST: 'configuration/list',
  POST_SYS_CREATE: 'configuration/create',
  POST_SYS_STATUS: 'configuration/changeStatus',
  PUT_SYS_UPDATE: 'configuration/update',
  DELETE_SYS_ID:'configuration',
  BULK_UPLOAD:'configuration/bulk-upload',
  DOWNLOAD_SAMPLE_CSV:"configuration/download-sample",
  EXPORT_DATA:"configuration/export",

  GET_CUSTOMER_ACCESS_PERMISSION:"user/getRolePermissionMapping",
  GET_CUSTOMER_ACCESS_ROLES:"user/userAccessRoleList",
  UPDATE_CUSTOMER_ACCESS_CHECKBOX:"user/updateRolePermissionMapping",
  UPDATE_CUSTOMER_ACCESS:"user/userAccessUpdatePermission",
}
 
export const STAFF_ROUTE = {
  USER_BY_GROUP:'user/usersByGroup'
}

export const ROLES_AND_PERMISSIONS_ROUTE = {
  USER_ROLES:'user/roles',
  USER_LOGIN_ROLES:'user/userRoles',
  UPDATE_PERMISSIONS:'user/staff/updateRolePermissionActionMapping',
  ACTION_LIST:'user/staff/getActions',
  GET_PERMISSIONS:'user/staff/getRolePermissionActionMapping',
}

export const ADMIN_SORT_FIELD = {
  FIRST_NAME: 'firstName',
  LAST_NAME:'lastName',
  USER_NAME:'username'
};

export const USER_LOGIN_TOKEN = {
  TOKEN: 'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICI2aE5QQkFjUzZiSUZ1Q1pTeENFcWlxYzN1S1BLRUtDM3VZQ2lCcW1kd19jIn0.eyJqdGkiOiI0NjQ0MDVjNi1iNzNhLTQyYzAtYTAwYi00NjgwN2U3OWFlOGMiLCJleHAiOjE2NTQ1MTgxNjgsIm5iZiI6MCwiaWF0IjoxNjU0NTEwOTY4LCJpc3MiOiJodHRwOi8vMTAuMTUxLjM1LjE0ODozMDAwNy9hdXRoL3JlYWxtcy9kZXA3IiwiYXVkIjpbInJlYWxtLW1hbmFnZW1lbnQiLCJhY2NvdW50Il0sInN1YiI6IjA0Y2E1NjFlLWZjOGUtNDYwZi1hYTc3LWIyMDcwODg4YWVkNiIsInR5cCI6IkJlYXJlciIsImF6cCI6ImRlcDctbm9kZWpzLW1zLXVzZXJtYW5hZ2VtZW50IiwiYXV0aF90aW1lIjowLCJzZXNzaW9uX3N0YXRlIjoiNjZkYWUyNDctZjdkNi00YzUwLWIwNGYtZDE1ZmYwMTQ0NDdhIiwiYWNyIjoiMSIsImFsbG93ZWQtb3JpZ2lucyI6WyJodHRwOi8vMTAuMTUxLjMyLjIwMDozMDU1NiJdLCJyZWFsbV9hY2Nlc3MiOnsicm9sZXMiOlsib2ZmbGluZV9hY2Nlc3MiLCJ1bWFfYXV0aG9yaXphdGlvbiIsImRlcDctYXBwLWFkbWluIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsicmVhbG0tbWFuYWdlbWVudCI6eyJyb2xlcyI6WyJ2aWV3LXJlYWxtIiwidmlldy1pZGVudGl0eS1wcm92aWRlcnMiLCJtYW5hZ2UtaWRlbnRpdHktcHJvdmlkZXJzIiwiaW1wZXJzb25hdGlvbiIsInJlYWxtLWFkbWluIiwiY3JlYXRlLWNsaWVudCIsIm1hbmFnZS11c2VycyIsInF1ZXJ5LXJlYWxtcyIsInZpZXctYXV0aG9yaXphdGlvbiIsInF1ZXJ5LWNsaWVudHMiLCJxdWVyeS11c2VycyIsIm1hbmFnZS1ldmVudHMiLCJtYW5hZ2UtcmVhbG0iLCJ2aWV3LWV2ZW50cyIsInZpZXctdXNlcnMiLCJ2aWV3LWNsaWVudHMiLCJtYW5hZ2UtYXV0aG9yaXphdGlvbiIsIm1hbmFnZS1jbGllbnRzIiwicXVlcnktZ3JvdXBzIl19LCJkZXA3LW5vZGVqcy1tcy11c2VybWFuYWdlbWVudCI6eyJyb2xlcyI6WyJkZXA3LWFkbWluIl19LCJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6Im9wZW5pZCBlbWFpbCBwcm9maWxlIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJnZW5kZXIiOiJNQUxFIiwibmFtZSI6IkZGRk9VUiBERU1PIiwicHJlZmVycmVkX3VzZXJuYW1lIjoiZmY0QG10bnN0bC5jb20iLCJnaXZlbl9uYW1lIjoiRkZGT1VSIiwiZmFtaWx5X25hbWUiOiJERU1PIn0.dz4nw8N-RxCRwfaxaJBmePVmyHQNAvXe3K_kZU62I3iRU0DLHjG1WeN-IFTXblmQ6hh-5sjyq-qs87lPZSkyotgxuh9XdBh91bhyBT5QCBtm0zKpJVrEup4jqcyNWLV1MBfrjaxw8xdJOye4Gy3dVolC8eDb8XOSLoxk_PHMXfUt5jee4hPxOZK3r8RguhQ7TI5qGkRiCgFivFAZxQcYgjEvKP3hOYwgb-mXdjXr4XihNLeILr76HJUTtVnX4hz9KbQSAH9LONPf5x2fjhuIIyA4UH-CuawN5gqC96PKzBENLw-zR0hj0JQc2ZbyAulLGN2op0Q66IzzIphYai73Tg'
};
export const ENCRDECR = {
  ENCRDECR_CODE: '123456$#@$^@1ERF'
}
export const FILTER_DATE_FORMAT = {
DATE_FORMAT: 'YYYY-MM-DD'
}
export const BsConfig = {
  isAnimated: true,
  containerClass: 'theme-dark-blue',
  dateInputFormat: 'DD-MM-YYYY',
};

export const USER_TABLE = ['firstName','lastName','username','email','gender','createdTimestamp','updateTimestamp','status','action']
export const USER_TABLE_SEARCH = [true,true,true,true,false,false,false,false,false]

export const CART_TABLE = ['cartID','name','orderReferenceNumber','caNumber','createdAt','updatedAt','quantity','status','action']
export const CART_TABLE_HEADING =['Cart ID','User Type','Plan name','CA Number','Created Date','Updated Date','Total Amount','Status','Action']
export const CART_TABLE_SEARCH = [false,true,true,true,true, true,true,false,false]
export const CART_TABLE_SORT = [true,false,false,false,true,true,false,true]
export const CART_INFO = {
  CART_ID:"Id",
  SELECTED_PRODUCT:"Product",
  QTY:"Quantity",
  BASE_PRICE:"Price",
  // DISCOUNT:"User_Status", 
  TAX:"Tax Rate",
  PRORATED_CHARGE:"Proration Amount Money", 
  DEPOSIT:"Price Type", 
  // DELIVERY_CHARGE:"Alternate_email_address", 
  TOTAL_PRICE:"Cart Total Price",
  // SUBSCRIPTION_TERM:"Creation_Date", 
  CREATED_TIME:"CreatedAt",
  MODIFIED_Time:"UpdatedAt",
  ORDER_REFERENCE_NUMBER:"Order Reference Number",
  TYPE:"Type",
  USER_STATUS:"User Status",
  ACTION:"Action"
};


export const USER_INFO = {
  FIRST_NAME:"First_Name",
  MIDDLE_NAME:"Middle_Name",
  LAST_NAME:"Last_Name",
  USER_NAME:"User_Name",
  USER_STATUS:"User_Status", 
  GENDER:"gender",
  DATE_OF_BIRTH:"Date_of_Birth", 
  ALTERNATE_CONTACT_NO:"Alternate_contact_No", 
  ALTERNATE_EMAIL_ADDRESS:"Alternate_email_address", 
  COMMUNICATION_LANGUAGE:"Communication_Language",
  CREATION_DATE:"Creation_Date", 
  LAST_MODIFIED_DATE:"Last_Modified_Date",
};
export const TABLE_DATA_TYPE = {
  ID:'id',
  TEXT:"text",
  DATE:"date",
  BUTTON:"button",
  SWITCH:"switch",
  ICON:"icon"
}

export const TICKET_TABLE = ['ticketNo','category','Sub-Category','status','action']
export const TICKET_TABLE_SEARCH = [true,true,true,true,false]

export const TICKET_TABLE_FIELDS = {
  ticketType:['id','title','createdAt','updatedAt','isActive','action'],
  category:['id','TicketType','title','createdAt','updatedAt','isActive','action'],
  subCategoy:['id','Category','title','createdAt','updatedAt','isActive','action']
}
export const TICKET_TABLE_FIELDS_ARABIC = {
  ticketType:['id','title','createdAt','updatedAt','isActive','action'],
  category:['id','TicketType','title','english','createdAt','updatedAt','isActive','action'],
  subCategoy:['id','Category','title','english','createdAt','updatedAt','isActive','action']
}
export const TICKET_TABLE_HEADING = {
  ticketType:['Sr.No','Type','createdAt','updatedAt','Status','Action'],
  category:['Sr.No','Type','Category','createdAt','updatedAt','Status','Action'],
  subCategoy:['Sr.No','Category','Sub-Category','createdAt','updatedAt','isActive','action']
}

export const REASON__TABLE_HEADING = {
  ticketType:['Sr.No','Parameter Name','Description','Value','Status','Action'],
  category:['Sr.No','Parameter Name','Description','Value','Status','Action'],
}

export const REASON_TABLE = ['id','configurableKey','configurableValueDisplayText','configurableValue','createdAt','updatedAt','action']
export const REASON_TABLE_SEARCH = [true,true,true,true,false,false,true]

export const REASON_TEXT = {
LABLE:"Reason"
}

export const TICKET_INFO = {
  TICKETNO:"ticketNo",
  SUBJECT:"subject",
  CATEGORY:"category",
  TICKETSCLEVEL1:"ticketSCLevel1",
  STATUS:"status",
  CREATION_DATE:"Creation_Date", 
  LAST_MODIFIED_DATE:"Last_Modified_Date",
};
export const CONSENT_TABLE = ['id','category.en','text.en','createdAt','updatedAt','enabled','action']
export const CONSENT_TABLE_SEARCH = [false,true,true,false,false,false,false]
export const CONSENT_TABLE_HEADING = ['Sr.No' , 'Name','Description','Status','Action'] 
export const CONSENT_TABLE_ARABIC = ['id','category.ar','category.en','text.ar','text.en','createdAt','updatedAt','enabled','action']
export const CONSENT_TABLE_SORT = [true,true,true,true,true,true]
export const CONSENT_TABLE_SORT_ARABIC = [true,true,true,true,true,true,false,true,false]



export const SYS_TABLE = ['id','configurableKey','configurableValueDisplayText','configurableValue','createdAt','updatedAt','action']
export const SYS_TABLE_SEARCH = [false,true,true,true,false]
export const SYS_TABLE_SORT = [false,true,true,true,false]

export const SYS_TAB_NAME = {
  CHECKOUT:'Checkout',
  CONSENT:'Consent',
  CUSTOMER_DETAIL:'Customer Detail',
  EKYC:'eKYC & Upload Document',
  EXPRESSION:'Expression',
  INVENTORY:'Inventory',
  PAYMENT:'Payment Management',
  QUOTATION:'Quotation',
  SUBSCRIPTION_LIMIT:'Subscription Limit'
}


export const LOGIN_ROUTE = {
  REFRESH_TOKEN:"user/refreshToken",
  LOGIN:"user/login",
  FORGOT:"user/forgotPassword",
  OTP:"user/verifyOtp",
  RESET_OTP:"user/resetPassword",
  RESET_LINK:"user/resetPassword/link",
  GET_CONFIG:"configuration"
}

export const PROFILE_ROUTE = {
  GET_PROFILE:'user/getUser',
  UPDATE_PROFILE:'user/updateUser'
}

export const LOCATION_ROUTE = {
  GET_LOCATION:"location/list",
  EDIT_LOCATION:"location/update",
  DELETE_LOCATION:"location/delete",
  ADD_LOCATION:"location/add",
  FILE_UPLOAD:"location/bulk-upload",
  DOWNLOAD_SAMPLE_CSV:"location/download-sample"
}

export const LOCATION_TABLE = 
  { header:['sr_no', 'country','state','district','city','zip'],
  data:[
    {row:[{value:"1",class1:"fa solid fa-xmark",type:TABLE_DATA_TYPE.TEXT},{value:"India", class1:"fa solid fa-xmark",type:TABLE_DATA_TYPE.TEXT},{value:"Gujarat", class1:"fa solid fa-xmark",type:TABLE_DATA_TYPE.TEXT},{value:"Mahesana", class1:"fa solid fa-xmark",type:TABLE_DATA_TYPE.TEXT},{value:"Ahmedabad", class1:"fa solid fa-xmark",type:TABLE_DATA_TYPE.TEXT},{value:"388877", class1:"fa solid fa-xmark",type:TABLE_DATA_TYPE.TEXT}]},
    {row:[{value:"2",class1:"fa solid fa-xmark",type:TABLE_DATA_TYPE.TEXT},{value:"India", class1:"fa solid fa-xmark",type:TABLE_DATA_TYPE.TEXT},{value:"Gujarat", class1:"fa solid fa-xmark",type:TABLE_DATA_TYPE.TEXT},{value:"Mahesana", class1:"fa solid fa-xmark",type:TABLE_DATA_TYPE.TEXT},{value:"Ahmedabad", class1:"fa solid fa-xmark",type:TABLE_DATA_TYPE.TEXT},{value:"388877", class1:"fa solid fa-xmark",type:TABLE_DATA_TYPE.TEXT}]},
    {row:[{value:"3",class1:"fa solid fa-xmark",type:TABLE_DATA_TYPE.TEXT},{value:"India", class1:"fa solid fa-xmark",type:TABLE_DATA_TYPE.TEXT},{value:"Gujarat", class1:"fa solid fa-xmark",type:TABLE_DATA_TYPE.TEXT},{value:"Mahesana", class1:"fa solid fa-xmark",type:TABLE_DATA_TYPE.TEXT},{value:"Ahmedabad", class1:"fa solid fa-xmark",type:TABLE_DATA_TYPE.TEXT},{value:"388877", class1:"fa solid fa-xmark",type:TABLE_DATA_TYPE.TEXT}]},
  ]
}

export const LOCATION_PREREQUISITES = {
  header:['fieldName', 'mandatory','specificValidations'],
  data:[
    {row:[{value:"sr_no",class1:"",type:TABLE_DATA_TYPE.TEXT},{class1:"fa solid fa-xmark",type:TABLE_DATA_TYPE.ICON},{value:"In case specified, won't be considered", class1:"",type:TABLE_DATA_TYPE.TEXT}]},
    {row:[{value:"country",class1:"",type:TABLE_DATA_TYPE.TEXT},{class1:"fa solid fa-check",type:TABLE_DATA_TYPE.ICON},{value:"Name of Country", class1:"",type:TABLE_DATA_TYPE.TEXT}]},
    {row:[{value:"state",class1:"",type:TABLE_DATA_TYPE.TEXT},{class1:"fa solid fa-check",type:TABLE_DATA_TYPE.ICON},{value:"Name Of State", class1:"",type:TABLE_DATA_TYPE.TEXT}]},
    {row:[{value:"district",class1:"",type:TABLE_DATA_TYPE.TEXT},{class1:"fa solid fa-check",type:TABLE_DATA_TYPE.ICON},{value:"Name of District", class1:"",type:TABLE_DATA_TYPE.TEXT}]},
    {row:[{value:"city",class1:"",type:TABLE_DATA_TYPE.TEXT},{class1:"fa solid fa-check",type:TABLE_DATA_TYPE.ICON},{value:"Name of City", class1:"",type:TABLE_DATA_TYPE.TEXT}]},
    {row:[{value:"zip",class1:"",type:TABLE_DATA_TYPE.TEXT},{class1:"fa solid fa-check",type:TABLE_DATA_TYPE.ICON},{value:"Zip Code", class1:"",type:TABLE_DATA_TYPE.TEXT}]},
  ]
}


export const LOCATION_TABS = [
  {lable:"Country Management",formView:true},
  {lable:"State Management",formView:false},
  {lable:"District Management",formView:false},
  {lable:"City Management",formView:false},
  {lable:"Zip Code Management",formView:false}
]

export const LOCATION_Table = [
  {header:['sr_no', 'name']},
  {data:[
    {row:[{value:"1"},{value:"abc"}]},
    {row:[{value:"1"},{value:"abc"}]},
    {row:[{value:"1"},{value:"abc"}]},
    {row:[{value:"1"},{value:"abc"}]},
  ]},
]

export const LOCATION_TABS_LIST = {
 COUNTRY:"country-management",
 STATE:"state-management",
 DISTRICT:"district-management",
 CITY:"city-management",
 ZIP:"zip-code-management"
}
export const TICKET_UPLOAD_CSV_TYPE = {
  TYPE_MANAGEMENT:"ticketType",
  CATEGORY_MANAGEMENT:"category",
  SUB_CATEGORY_MANAGEMENT:"subcategory",
 }
export const TICKET_TABS_LIST = {
  TYPE_MANAGEMENT:"ticketType",
  CATEGORY_MANAGEMENT:"category",
  SUB_CATEGORY_MANAGEMENT:"subCategory",
 }

 export const TICKET_TABS_KEY = {
  "ticket-type":"ticketType",
  category:"category",
  "sub-category":"subCategory",
  ticketType:"ticket-type",
  subCategory:"sub-category",
 }

 export const TICKET_TABS = {
  TYPE_MANAGEMENT:"Ticket Type Management",
  CATEGORY_MANAGEMENT:"Category Management",
  SUB_CATEGORY_MANAGEMENT:"SubCategory Management",
 }

 export const TICKET_TABS_NAME = {
  TYPE_MANAGEMENT:"Ticket Type",
  CATEGORY_MANAGEMENT:"Category",
  SUB_CATEGORY_MANAGEMENT:"SubCategory",
 }

 export const REASON__TABS_NAME = {
  ADD_ON_UNSUBSCRIBE:"Add-On Unsubscribe",
  CHANGE_SIM:"Change SIM",
 }
 export const REASON_TABLE_SHORT = [true,true,true,true,true,true,true]
 export const REASON_TABS_LIST = {
  ADD_ON_UNSUBSCRIBE:"AddOnUnsubscribe",
  CHANGE_SIM:"ChangeSIM",
 }

 export const REASON_KEY_LIST = {
  ADD_ON_UNSUBSCRIBE:"addon.unsubscribe.reasonValue",
  CHANGE_SIM:"changeSIM.reasons",
  REASON:"reason"
 }
  
export const LOCATION_TYPE =['country','state','district','city','zipcode']

export const LOCATION_DROPDOWN_FIELDS =['countryNameDropdown','stateNameDropdown','districtNameDropdown','cityNameDropdown']

export const LOCATION_CSV_HEADER = {
  country:['Country',['OtherLanguageCountry(Arabic)'],'Status'],
  state:['State',['OtherLanguageState(Arabic)'],'Country',['OtherLanguageCountry(Arabic)'],'Status'],
  district:['District',['OtherLanguageDistrict(Arabic)'],'State',['OtherLanguageState(Arabic)'],'Country',['OtherLanguageCountry(Arabic)'],'Status'],
  city:['City',['OtherLanguageCity(Arabic)'],'District',['OtherLanguageDistrict(Arabic)'],'State',['OtherLanguageState(Arabic)'],'Country',['OtherLanguageCountry(Arabic)'],'Status'],
  zipcode:['Zipcode','City',['OtherLanguageCity(Arabic)'],'District',['OtherLanguageDistrict(Arabic)'],'State',['OtherLanguageState(Arabic)'],'Country',['OtherLanguageCountry(Arabic)'],'Status']
}

export const TICKET_CSV_HEADER = {
  ticketType:['TicketType','Status'],
  category:['Category',['OtherLanguageCategory(Arabic)'],'TicketType','Status'],
  subCategory:['SubCategory',['OtherLanguageSubCategory(Arabic)'],'Category',['OtherLanguageCategory(Arabic)'],'Status'],
}

export const DATA_CONFIGURATION_TABS_LIST = {
  LOCATION:"location-management",
  STORE:"store",
  AUTH:"auth",
  CONSENT:"consent-management",
  TICKET:"ticket-management",
  ORDER:"order",
  PRODUCT:"product",
  ADD_ON:"add-on",
  REASON:"reason-management"
 }

 export const SYSTEM_CONFIGURATION_TABS_LIST = {
  CUSTOMER_MANAGEMENT:'Customer Management',
  AUTHENTICATION:"Authentication",
  CUSTOMER:"Customer",
  E_CARE:"E-Care",
  PRODUCT:"Product",
  SHOPPING_CART:"Shopping Cart",
  SYSTEM:"System",
  USER:"User",
  OTHER_CONFIGURATION:'Other Configuration'
 }


 export const TRUE_FALSE_RADIO_GROUP = ['Yes','No']
 export const PROFILE_TABS_LIST ={
  PROFILE:'My Profile',
  CHANGE_PASSWORD:'Change Password'
 }
 export const PROFILE_TABS={
  PROFILE:'profile',
  CHANGE_PASSWORD:'change-password'
 }
 export const CUSTOMER_TABS_LIST = {
  AUTHENTICATION:"Authentication",
  CHECKOUT:"Checkout",
  CONSENT:"Consent",
  CUSTOMER_DETAIL:"Customer Detail",
  EKYC:"eKYC & Upload Document",
  EXPRESSION:"Expression",
  INVENTORY:"Inventory",
  PAYMENT_MANAGEMENT:"Payment Management",
  QUOTATION:"Quotation",
  SUBSCRIPTION:"Subscription Limit"
 }

 export const CUSTOMER_MANAGEMENT_TAB_LIST = {
  B2B:"B2B",
  CUSTOMER_ACCESS:"Customer Access",
  BILLING_DETAILS:"Billing Details",
  CUSTOMER_DETAILS:"Customer Details",
  UPDATE_PROFILE:"Update Profile",
  VANITY_CATEGORIES:"Vanity Categories"
 }
 

 export const MODAL_NOTES = {
  CONFIRM:"Confirmation",
  DELET:"Are you sure want to Delete ?"
}

export const TABLE_NAMES = {
  USER_TABLE:"User_Management_History",
  TICKET_TABLE:"Ticket_Management",
  CONSENT_TABLE:"Consent_Management",
  CART_TABLE:"Cart_Management",
}

export const TICKET_TABLE_NAMES = {
  TICKET_TYPE:"List_of_TicketType",  
  TICKET_CATEGORY:"List_of_Category",
  TICKET_SUB_CATEGORY:"List_of_Sub_Category",
}

export const SYSTEM_CONFIGURATION_TABLE = {
  AUTHENTICATION_TABLE:"System_Configuration.List_of_Authentication",
  CHECKOUT_TABLE:"System_Configuration.List_of_Checkout",
  CONSENT_TABLE:"System_Configuration.List_of_Consent",
  CUSTOMER_TABLE:"System_Configuration.List_of_Customer_Detail",
  KYC_TABLE:"System_Configuration.List_of_eKYC",
  EXPRESSION_TABLE:"System_Configuration.List_of_Expression",
  INVENTORY_TABLE:"System_Configuration.List_of_Inventory",
  PAYMENT_TABLE:"System_Configuration.List_of_Payment_Management",
  QUOTATION_TABLE:"System_Configuration.List_of_Quotation",
  SUBSCRIPTION_TABLE:"System_Configuration.List_of_Subscription",
  Config_TABLE:"System_Configuration.List_of_Configurations"
}

export const SYSTEM_CSV_DEMO = 
  { header:['sr_no', 'country','state','district','city','zip'],
  data:[
    {row:[{value:"1",class1:"fa solid fa-xmark",type:TABLE_DATA_TYPE.TEXT},{value:"India", class1:"fa solid fa-xmark",type:TABLE_DATA_TYPE.TEXT},{value:"Gujarat", class1:"fa solid fa-xmark",type:TABLE_DATA_TYPE.TEXT},{value:"Mahesana", class1:"fa solid fa-xmark",type:TABLE_DATA_TYPE.TEXT},{value:"Ahmedabad", class1:"fa solid fa-xmark",type:TABLE_DATA_TYPE.TEXT},{value:"388877", class1:"fa solid fa-xmark",type:TABLE_DATA_TYPE.TEXT}]},
    {row:[{value:"2",class1:"fa solid fa-xmark",type:TABLE_DATA_TYPE.TEXT},{value:"India", class1:"fa solid fa-xmark",type:TABLE_DATA_TYPE.TEXT},{value:"Gujarat", class1:"fa solid fa-xmark",type:TABLE_DATA_TYPE.TEXT},{value:"Mahesana", class1:"fa solid fa-xmark",type:TABLE_DATA_TYPE.TEXT},{value:"Ahmedabad", class1:"fa solid fa-xmark",type:TABLE_DATA_TYPE.TEXT},{value:"388877", class1:"fa solid fa-xmark",type:TABLE_DATA_TYPE.TEXT}]},
    {row:[{value:"3",class1:"fa solid fa-xmark",type:TABLE_DATA_TYPE.TEXT},{value:"India", class1:"fa solid fa-xmark",type:TABLE_DATA_TYPE.TEXT},{value:"Gujarat", class1:"fa solid fa-xmark",type:TABLE_DATA_TYPE.TEXT},{value:"Mahesana", class1:"fa solid fa-xmark",type:TABLE_DATA_TYPE.TEXT},{value:"Ahmedabad", class1:"fa solid fa-xmark",type:TABLE_DATA_TYPE.TEXT},{value:"388877", class1:"fa solid fa-xmark",type:TABLE_DATA_TYPE.TEXT}]},
  ]
}
export const ROLE_TABLE_FIELDS = ['Sr.No','Role Name','Description','action']
export const ROLE_TABLE_NAME= "List_of_Roles"

export const ROLE_Titles = {
  Cart_Management:"cartManagement",
  Admin_access_management:'adminAccessManagement',
  Master_Data_Config:"masterDataConfiguration",
  Authentication:'authentication',
  Location_Management:"locationManagement",
  Country_Management:"countryManagement",
  state_Management:"stateManagement",
  District_Management:"districtManagement",
  City_Management:"cityManagement",
  Zip_Management:"zipcodeManagement",
}


export const PASSWORD_STRENGTH={
  lowerCase:"lowerCase",
  upperCase:"upperCase",
  digits:"digits",
  twoSpecialChar:"twoSpecialChar"
}

export const ConstantsFreeze={
  USER_PASSWORD_USERNAME_LENGTH:"user.password.username.match",
  USER_PASSWORD_SEQUENCE_LENGTH:"user.password.sequential.length",
  USER_PASSWORD_UPPERCASE:"user.password.uppercase",
  USER_PASSWORD_LOWERCASE:"user.password.lowercase",
  USER_PASSWORD_LENGTH:"user.password.length",
  USER_PASSWORD_DIGITS:"user.password.nonalphabetic",
  USER_PASSWORD_UNICODE:"user.password.notcontain.unicode",
  USER_PASSWORD_STRENGTH:"user.password.strength",
  USER_PASSWORD_SPECIALCHAR:"user.password.allowSpecialChar",
}

export const SYSTEM_PREREQUISITES_DEMO = {
  header:['fieldName', 'mandatory','specificValidations'],
  data:[
    {row:[{value:"sr_no",class1:"",type:TABLE_DATA_TYPE.TEXT},{class1:"fa solid fa-xmark",type:TABLE_DATA_TYPE.ICON},{value:"In case specified, won't be considered", class1:"",type:TABLE_DATA_TYPE.TEXT}]},
    {row:[{value:"country",class1:"",type:TABLE_DATA_TYPE.TEXT},{class1:"fa solid fa-check",type:TABLE_DATA_TYPE.ICON},{value:"Name of Country", class1:"",type:TABLE_DATA_TYPE.TEXT}]},
    {row:[{value:"state",class1:"",type:TABLE_DATA_TYPE.TEXT},{class1:"fa solid fa-check",type:TABLE_DATA_TYPE.ICON},{value:"Name Of State", class1:"",type:TABLE_DATA_TYPE.TEXT}]},
    {row:[{value:"district",class1:"",type:TABLE_DATA_TYPE.TEXT},{class1:"fa solid fa-check",type:TABLE_DATA_TYPE.ICON},{value:"Name of District", class1:"",type:TABLE_DATA_TYPE.TEXT}]},
    {row:[{value:"city",class1:"",type:TABLE_DATA_TYPE.TEXT},{class1:"fa solid fa-check",type:TABLE_DATA_TYPE.ICON},{value:"Name of City", class1:"",type:TABLE_DATA_TYPE.TEXT}]},
    {row:[{value:"zip",class1:"",type:TABLE_DATA_TYPE.TEXT},{class1:"fa solid fa-check",type:TABLE_DATA_TYPE.ICON},{value:"Zip Code", class1:"",type:TABLE_DATA_TYPE.TEXT}]},
  ]
}

export const STATIC_DATA = {
  UPLOAD_MSG:" uploaded successfully",
  INPUTE_MIN_VALIDATION:"The min allowed number of characters is ",
  INPUTE_MAX_VALIDATION:"The max allowed number of characters is ",
  DELETE_MSG:"Are you sure you want to delete the selected ",
  DELETE:"Delete ",
  ZIPCODE_MIN_VALIDATION:"Length of zipcode shold be 5",
  CHAR_AND_SPACE_ALLOWED:"Only characters are allowed",
  ONLY_NUMBERS_ALLOWED:"Only numbers are allowed",
  LOCATION_INPUTE_MIN_VALIDATION:"The min allowed number of characters is 3",
  LOCATION_INPUTE_MAX_VALIDATION:"The max allowed number of characters is 256",
  ERR_MSG_REQUIERD_EMAIL:"Email ID is required",
  ERR_MSG_REQUIERD_NEW_PASSWORD:"New Password is required",
  ERR_MSG_REQUIERD_CON_PASSWORD:"Confirm Password is required",
  ERR_MSG_INVALID_PASSWORD:"Invalid password. The password requirement doesn't match.",
  ERR_MSG_INVALID_PASSWORD_CON_PASSWORD:"New Password and Confirm Password must be match.",
  ERR_MSG_REQUIERD_OTP:"OTP is required",
  ERR_MSG_MAX_OTP:"The max allowed number of characters is 6",
  ERR_MSG_MIN_OTP:"The min allowed number of characters is 6",
  ERR_MSG_VALID_EMAIL:"Email ID must be a valid email address",
  IS_REQUIRED:" is required",
  IS_PRE_CSV:"PreRequisites For CSV",
  IS_CSV:"CSV Format",
  ERR_MSG_REQUIERD_END_DATE:"End date is Required",
  ERR_MSG_REQUIERD_START_DATE:"Start date is Required",
  GREEN:"Green",
  RED:"red",
  SESSION_TIMEOUT:"Session Timeout",
  SESSION_TIMEOUT_NOTE: "Your session is about to expire and you will be logged out of the system. Do you wish to logout? (Select No to continue working)",
  JSON_ERROR_ALL_INPUTS:"All inputs are required",
  JSON_ERROR_REQUIERD_INPUTS:"Required input is invalid",
  SUB_MODULE_ERROR_REQUIERD:"Please select at least one sub module",
  EXPORT_SUCCESSFUL:"The export has been successful.",
  EXPORT_ERROR:"The export has been failed, Please try again.",
  SYSTEM_CONFIG:"System Configuration"
}

export const TAB_FUNCTION = {
  CREATE:"create",
  EDIT:"edit",
  UPLOAD:"upload",
  LIST:"list"
};

export const PASSWORD_VALIDATION_MSG = {
  userPasswordLength:"Your new password must be at least {{length}} characters long",
  userPasswordUsernameLength:"New password can not contain username",
  userPasswordsequenceLength:"New password can not contain sequence of more than {{length}} digits",
  userPasswordUnicode:"New password can not contained unicode characters",
  userPasswordUppercase:"New password must contain at least {{length}} uppercase characters",
  userPasswordLowercase:"New password must contain at least {{length}} lowercase characters",
  userPasswordDigits:"New password must contain at least {{length}} number",
  userPasswordallowSpecialChar:"New password must contain at least {{length}} special characters",
  
};


export const OTP_CONFIG={
  USER_OTP_RESEND_TIME:"user.otp.resendTime",
  USER_OTP_LENGTH:"user.otp.otpLength",
  USER_OTP_MAXATTEMPT:"user.otp.maxAttempt",
  USER_OTP_RESEND_MAXATTEMPT:"user.otp.resendMaxAttempt",
  USER_OTP_BLOCK_AFTER_MAXATTEMPT:"user.otp.blockAfterMaxAttempt",
  USER_OTP_VALIDFOR:"user.otp.validFor",
 
}

export const CONFIG_KEYS={
  USER_EMAIL:"user.email",
  LANGUAGE_KEY:"backoffice.otherLanguage",
  IDLE_KEY:"backoffice.front.idleTimeout" 
}

export const LANGUAGE_LABLE={
  CONSENT:"Name(EN)",
}
export const ERROR_CODE=[400,500]
export const REFRESH_TOKEN_EMPTY_CODE=[460]
export const REDIRECT_AFTER_ERROR_CODE = [401]