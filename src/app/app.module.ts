import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
  import { adminLteConf } from './admin-lte.conf';
import { ApiService } from '@services/api.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { NgxSpinnerModule } from 'ngx-spinner';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatInputModule } from '@angular/material/input';
import { MatNativeDateModule } from '@angular/material/core';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { FormService } from '@services/form.service';
import { MatTabsModule } from '@angular/material/tabs';
import { MatExpansionModule } from '@angular/material/expansion';
import {
  TranslateModule,
  TranslateLoader,
  TranslateService,
} from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { GlobalErrorHandler } from '@services/globalErrorHandler';
import { JwtModule } from '@auth0/angular-jwt';
import { MenuSidebarComponent } from './menu-sidebar/menu-sidebar.component';
import { HeaderComponent } from './header/header.component';
import { LayoutModule } from 'angular-admin-lte';
import { AuthService } from '@services/auth.service';
import { LocalStorageService } from '@services/localService/localStorage.service';
import { UsersService } from '@services/users.service';
import { SharedModule } from '@shared/shared.module';
import { LoginService } from '@services/login.service';
import { ModalsModule } from './modals/modals.module';
import { HomeComponent } from './home/home.component';
import { SystemCinfigurationsService } from '@services/system-cinfigurations.service';
import { LocationService } from '@services/location.service';
import { FormsModule } from '@angular/forms';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { NgIdleService } from '@services/ng-idle.service';
export function getToken() {
  return localStorage.getItem('token');
}

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, '../assets/i18n/', '.json');
}
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MenuSidebarComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    SharedModule,
    NgxSpinnerModule, 
    LayoutModule.forRoot(adminLteConf),
    JwtModule.forRoot({
      config: {
        tokenGetter: getToken
      }
    }),
    ModalsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient],
      },
    }),
    TooltipModule.forRoot(),
  ],
  exports: [],
  providers: [
    ApiService,
    FormService,
    UsersService,
    LoginService,
    SystemCinfigurationsService,
    LocationService,
    NgIdleService,
    { provide: ErrorHandler, useClass: GlobalErrorHandler }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
