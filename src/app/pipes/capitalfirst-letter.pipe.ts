import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'capitalfirstLetter'
})
export class CapitalfirstLetterPipe implements PipeTransform {

  transform(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

} 
