import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UniquePipe } from './unique.pipe';
import { DatePipe } from './date.pipe';
import { CapitalfirstLetterPipe } from './capitalfirst-letter.pipe';
@NgModule({
  declarations: [UniquePipe, DatePipe, CapitalfirstLetterPipe],
  imports: [CommonModule],
  exports: [UniquePipe, DatePipe],
  providers: [DatePipe]
})

export class MainPipe { }
