FROM harborreg.sterlitetech-software.com:30010/dep7-mtn/ngt/angular:stlruntime

RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app

WORKDIR /home/node/app

#COPY package*.json ./

#COPY tsconfig.json ./

#COPY angular.json ./

#COPY src ./src

#COPY tslint.json ./
#COPY demo.html ./
#COPY tsconfig.app.json ./
#COPY backoffice_custom_entry.sh ./
#COPY node_modules ./node_modules

USER node

EXPOSE 4200

#RUN npm install

#RUN npm install -g @angular/cli@7.3.9

COPY --chown=node:node . .


ENTRYPOINT ["/bin/sh", "backoffice_custom_entry.sh" ]



